﻿using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Data;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.BLModels
{
    public class MultiProductBL : Core.BasicNotifyModel
    {
        public ObservableCollection<Product> MultiProducts { get; set; } = new ObservableCollection<Product>(); //Все изделия, подходящие по положению в маршрутной карте
        public IList SelectedMultiProducts { get; set; } //Выборка из MultiProducts для группового выполнения
        public IEnumerable<RouteMapProductOperation> ProductOperationViews { get; set; } //Выборка однотипных операций для разных изделий
        public Product SelectedMultiProduct { get; set; }
        public int? SelectedGroupExecuterId { get; set; } // Выбранный исполнитель для группы ProductOperationViews

        private RouteMapProductOperation _selectedMultiOperation;
        public RouteMapProductOperation SelectedMultiOperation
        {
            get { return _selectedMultiOperation; }
            set
            {
                _selectedMultiOperation = value;
                if (_selectedMultiOperation != null)
                {
                    SelectedGroupExecuterId = _selectedMultiOperation.OperationExecuterId;
                }
            }
        }

        #region SeatchBar_props
        public ICollectionView MultiProductsView { get; set; }
        private string _numberSearchBar;
        public string NumberSearchBar
        {
            get { return _numberSearchBar; }
            set
            {
                _numberSearchBar = value;
                MultiProductsView.Refresh();
            }
        }
        #endregion
        public GroupActions SelectedGroupAction { get; set; }
        public enum GroupActions
        {
            GroupStart, //Group start selected operation
            GroupEnd, //Group end selected operation
            GroupCancel, //Group cancel selected operation
            GroupReturn //Group return to selected operation
        }

        #region Singleton
        private static readonly Lazy<MultiProductBL> _lazyModel = new Lazy<MultiProductBL>(() => new MultiProductBL());
        public static MultiProductBL Instance => _lazyModel.Value;

        private MultiProductBL() { }
        #endregion

        public bool ExecuteSelectedGroupOperation()
        {
            if (SelectedMultiOperation.OperationId == CodeDictionary.Instance.AuxiliaryOperation.Equipment.Id)
            {
                Dialog.ShowError(TextHelper.CantWorkWithOperationMessage(SelectedMultiOperation.Operation.Name));
                return false;
            }

            try
            {
                List<int> productIds = new List<int>(ConvertIListToList(SelectedMultiProducts).Select(product => product.Id));
                ProductOperationViews = ProductOperationViews.Where(r => productIds.Contains(r.ProductId)); //Фильтрация коллекции согласно выбранным изделиям {SelectedMultiProducts}

                if (ProductOperationViews.Count() != SelectedMultiProducts.Count)
                {
                    Dialog.ShowError(TextHelper.EqualityProductsAndOperationsMessage(SelectedMultiProducts.Count, ProductOperationViews.Count()));
                    return false;
                }               
            }
            catch (Exception ex)
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog(ex));
                Dialog.ShowError(ex.Message);
                return false;
            }

            switch (SelectedGroupAction)
            {
                #region GroupStart_case            
                case GroupActions.GroupStart:

                    //Получение данных индивидуального рассчета прогресса изделий
                    List<CalculatedProductInfo> afterStartInfos = SQLServer.Instance.Product.CalculateProgressAfterStartOperation(
                        ProductOperationViews.Select(po => po.ProductId).ToArray(),
                        CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id,
                        CodeDictionary.Instance.AuxiliaryOperationStatus.InProgress.Id,
                        CodeDictionary.Instance.AuxiliaryOperationStatus.EquipmentInProgress.Id);

                    if (SQLServer.Instance.GroupStartOperation( ProductOperationViews,
                                                                ProductStatusManager.GetPrimitiveStatus("StartOrReturnOperation").Id,
                                                                SelectedGroupExecuterId,
                                                                afterStartInfos,
                                                                Shaper.NewGroupLogs(ProductOperationViews,
                                                                                    AuthorizationModel.Instance.AuthorizedUser,
                                                                                    TextHelper.StartOperation())))
                    {
                        RefrehScreeData(ProductOperationViews.First().Operation, afterStartInfos);
                    }

                    break;
                #endregion
                #region  GroueEnd_case
                case GroupActions.GroupEnd:
                    #region Data_prepearing          
                    List<RouteMapProductOperation> allProductOperationViews =
                                            SQLServer.Instance.RouteMapProductOperation
                                                .GetAllProductOperations(ProductOperationViews
                                                    .Select(productOperation => productOperation.ProductId))
                                                        .ToList(); //Получаем все операции по МК для всех изделий, участвующих в групповом завершении операции

                    List<List<RouteMapProductOperation>> groupedProductOperations = ProductOperationViews.Select(selectedProductOperation => allProductOperationViews.Where(productOperation => productOperation.ProductId == selectedProductOperation.ProductId).ToList()).ToList();
                    List<CalculatedProductInfo> calculatedProductInfos = new List<CalculatedProductInfo>();

                    foreach (List<RouteMapProductOperation> productOperations in groupedProductOperations)
                    {
                        if ((productOperations?.Any() ?? false) && (ProductOperationViews.Count() == 0)) //ProductOperationViews.Any() always is null
                        {
                            Dialog.ShowWarning(TextHelper.CollectionIsNullError);
                            break;
                        }

                        Product currentProduct = productOperations.First().Product;
                        RouteMapProductOperation currentProductOperation = ProductOperationViews.Single(productOperation => productOperation.ProductId == currentProduct.Id);

                        bool routeMapIsCompelete = SQLServerHelper.ProductIsComplete(productOperations, currentProductOperation);

                        //Определение нового статуса для каждого выбранного изделия на основе его .ModelId и готовности маршрутной карты {routeMapIsCompelete}
                        ProductStatus newProductStatus = ProductStatusManager.DefinitionStatusAfterGroupEnd(currentProduct, routeMapIsCompelete);

                        float newProgress = ProductOperationBL.Instance.CalculateAndIncrementProgressToEnd(productOperations);
                        calculatedProductInfos.Add(new CalculatedProductInfo(currentProduct.Id, newProductStatus, newProgress, routeMapIsCompelete));
                    }
                    #endregion

                    if (SQLServer.Instance.GroupEndOperation(   ProductOperationViews,
                                                                groupedProductOperations,
                                                                calculatedProductInfos,
                                                                SelectedGroupExecuterId,
                                                                Shaper.NewGroupLogs(ProductOperationViews,
                                                                                    AuthorizationModel.Instance.AuthorizedUser,
                                                                                    TextHelper.EndOperation())))
                    {
                        RefrehScreeData(ProductOperationViews.First().Operation, calculatedProductInfos);                     
                    }

                    break;
                #endregion
                #region GroupCancel_case
                case GroupActions.GroupCancel:
                    List<CalculatedProductInfo> afterCancelInfos = SQLServer.Instance.Product.CalculateProgressAfterReExecuteOrCancelOperation(
                        ProductOperationViews.Select(po => po.ProductId).ToArray(),
                        CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id,
                        CodeDictionary.Instance.AuxiliaryOperationStatus.InProgress.Id,
                        CodeDictionary.Instance.AuxiliaryOperationStatus.EquipmentInProgress.Id);

                    if (SQLServer.Instance.GroupCancelOperation(ProductOperationViews,
                                                                ProductStatusManager.GetPrimitiveStatus("CancelOperation").Id,
                                                                afterCancelInfos,
                                                                Shaper.NewGroupLogs(ProductOperationViews,
                                                                                    AuthorizationModel.Instance.AuthorizedUser,
                                                                                    TextHelper.CancelOperation())))
                    {
                        RefrehScreeData(ProductOperationViews.First().Operation, afterCancelInfos);
                    }

                    break;
                #endregion
                #region GroupReturn_case
                case GroupActions.GroupReturn:
                    List<CalculatedProductInfo> afretReExecuteInfos = SQLServer.Instance.Product.CalculateProgressAfterReExecuteOrCancelOperation(
                        ProductOperationViews.Select(po => po.ProductId).ToArray(),
                        CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id,
                        CodeDictionary.Instance.AuxiliaryOperationStatus.InProgress.Id,
                        CodeDictionary.Instance.AuxiliaryOperationStatus.EquipmentInProgress.Id);

                    if (SQLServer.Instance.GroupStartOperation( ProductOperationViews,
                                                                ProductStatusManager.GetPrimitiveStatus("StartOrReturnOperation").Id,
                                                                SelectedGroupExecuterId,
                                                                afretReExecuteInfos,
                                                                Shaper.NewGroupLogs(ProductOperationViews,
                                                                                    AuthorizationModel.Instance.AuthorizedUser,
                                                                                    TextHelper.ReturnOperation())))
                    {
                        RefrehScreeData(ProductOperationViews.First().Operation, afretReExecuteInfos);
                    }          
                    break;
                #endregion
                default:
                    Dialog.ShowError(TextHelper.ActionNotFound);
                    break;
            }
            ClearAllMultiProps();
            return true;
        }

        #region Clear_methods
        public void ClearSelectedGroupExecuterId()
        {
            SelectedGroupExecuterId = null;
        }
        private void ClearAllMultiProps()
        {
            SelectedGroupAction = default;
            SelectedMultiProduct = null;

            SelectedMultiProducts = null;
            ProductOperationViews = null;
            MultiProducts.Clear();

            SelectedMultiOperation = null;
            SelectedGroupExecuterId = null;
        }
        #endregion

        //Метод обновления данных на экране после выполнения групповых операций
        private void RefrehScreeData(Operation currentOperation, List<CalculatedProductInfo> infos)
        {
            foreach (CalculatedProductInfo info in infos)
            {
                Product product = MultiProducts.Single(multiProduct => multiProduct.Id == info.ProductId);
                product.Progress = info.NewProductProgress;
                product.CurrentOperationId = currentOperation.Id;
                product.Operation = currentOperation;
            }
        }
        public List<Product> ConvertIListToList(IList products)
        {
            List<Product> SelectedMultiProductsList = new List<Product>();
            for (int index = 0; index < products.Count; index++)
            {
                SelectedMultiProductsList.Add((Product)products[index]);
            }
            return SelectedMultiProductsList;
        }
    }
}
