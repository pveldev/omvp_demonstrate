﻿using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace OMVP.BL.BLModels
{
    public class ProductOperationBL : BasicNotifyModel
    {
        #region Props
        public ObservableCollection<RouteMapProductOperation> CurrentProductOperationsList { get; set; } = new ObservableCollection<RouteMapProductOperation>(); //View.ProductOperationsView
        public List<WorkingGroupEmployee> AllCurrentWorkingGroupEmployees { get; set; } // Все сотрудники (состоящие в разных группах), которых можно выбрать в качестве исполнителя для текущего списка CurrentProductOperationsList
        public RouteMapProductOperation SelectedProductOperation { get; set; }
        public Product SelectedProduct { get; set; }
        public Operation SelectedOperation { get; set; } //AddUnscheduledOperation
        public string NewProductOperationDescription { get; set; }
        #endregion

        #region Singleton
        private static readonly Lazy<ProductOperationBL> _lazyModel = new Lazy<ProductOperationBL>(() => new ProductOperationBL());
        public static ProductOperationBL Instance => _lazyModel.Value;
        private ProductOperationBL()
        {

        }
        #endregion

        #region Operation_Actions
        public bool StartOrReternToSelectedOperation(OperationStatus newOperationStatus, string operationAction, bool isReExecute = false)
        {
            return SQLServer.Instance.StartOperation(SelectedProductOperation,
                                                        newOperationStatus,
                                                        ProductStatusManager.DefinitionProductStatusForOperationAction(SelectedProductOperation),
                                                        isReExecute ? CalculateAndDecrementProgress() : CalculateAndIncrementProgressToStart(),
                                                        Shaper.FormNewLog(  SelectedProductOperation,
                                                                            operationAction,
                                                                            AuthorizationModel.Instance.AuthorizedUser));
        }

        public bool StartEquipment(RouteMapProductOperation productOperation, OperationStatus newOperationStatus) =>
            SQLServer.Instance.StartOperation(productOperation,
                                                newOperationStatus,
                                                ProductStatusManager.DefinitionProductStatusForOperationAction(productOperation),
                                                Shaper.FormNewLog(productOperation,
                                                TextHelper.StartOperation(),
                                                AuthorizationModel.Instance.AuthorizedUser));

        public void EndSelectedOperation()
        {

            bool routeMapIsComplete = SQLServerHelper.ProductIsComplete(CurrentProductOperationsList, SelectedProductOperation);

            SQLServer.Instance.EndOperation(SelectedProductOperation,
                                            CodeDictionary.Instance.AuxiliaryOperationStatus.Completed,
                                            ProductStatusManager.DefinitionProductStatusAfterEndOperation(routeMapIsComplete, SelectedProductOperation),
                                            CalculateAndIncrementProgressToEnd(),
                                            CurrentProductOperationsList,
                                            routeMapIsComplete,
                                            Shaper.FormNewLog(TextHelper.EndOperation(),
                                                                SelectedProduct.Id,
                                                                SelectedProductOperation,
                                                                AuthorizationModel.Instance.AuthorizedUser));
        }



        public void CancelSelectedOperation()
        {
            SQLServer.Instance.CancelOperation(SelectedProductOperation,
                                                CodeDictionary.Instance.AuxiliaryOperationStatus.ReadyToStart,
                                                ProductStatusManager.DefinitionProductStatusForOperationAction(SelectedProductOperation),
                                                CalculateAndDecrementProgress(),
                                                Shaper.FormNewLog(TextHelper.CancelOperation(),
                                                                    SelectedProduct.Id,
                                                                    SelectedProductOperation,
                                                                    AuthorizationModel.Instance.AuthorizedUser));
        }
        #endregion

        #region Other_functional
        public void AddUnscheduledOperation(byte priotity)
        {
            RouteMapProductOperation equipment = null;
            //Если изделие со слотами и его МК не содержит "комплектацию"
            if (SelectedProduct.Revision.RevisionBOMs1.Count != 0 && CurrentProductOperationsList.All(productOperation => productOperation.Operation.Id != CodeDictionary.Instance.AuxiliaryOperation.Equipment.Id))
            {
                equipment = Shaper.FormNewUnscheduledProductOperation(SelectedProduct.Id,
                                                                        GetUnscheduledEquipmentOperationStatusId(InstallationProductBL.Instance.InstallationCompletedCheck(SelectedProduct)),
                                                                        CodeDictionary.Instance.AuxiliaryOperation.Equipment.Id,
                                                                        priotity,
                                                                        description: "");
            }

            List<RouteMapProductOperation> unscheduleds = SQLServer.Instance.RouteMapProductOperation.AddUnscheduledOperation
                (
                    CalculateProgressAfterAddingOperation(equipment != null ? 1 : 0),
                    Shaper.FormNewUnscheduledProductOperation(SelectedProduct.Id,
                                                                CodeDictionary.Instance.AuxiliaryOperationStatus.ReadyToStart.Id,
                                                                SelectedOperation.Id,
                                                                priotity,
                                                                NewProductOperationDescription),
                    ProductStatusManager.DefinitionProductStatusForOperationAction(SelectedProduct),
                    SelectedProduct.ProductStatusId == ProductStatusManager.Assembly.Id,
                    equipment

                );

            if (unscheduleds?.Any() ?? false)
            {
                List<RouteMapProductOperation> sorterProductOperations = CurrentProductOperationsList.ToList();
                sorterProductOperations.AddRange(unscheduleds);
                //sorting
                sorterProductOperations = sorterProductOperations.OrderBy(rmpoView => rmpoView.Priority).ToList();
                CurrentProductOperationsList.Clear();
                sorterProductOperations.ForEach(CurrentProductOperationsList.Add);
                sorterProductOperations = null;
            }

            ResetBLProps();
        }
        //ToDo: в статус менеджер
        private int GetUnscheduledEquipmentOperationStatusId(bool installationIsComplete)
        {
            return installationIsComplete ?
                CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id
                :
                CodeDictionary.Instance.AuxiliaryOperationStatus.ReadyToStart.Id;
        }

        public bool SaveProductOperationDescription() => SQLServer.Instance.RouteMapProductOperation.UpdateProductOperationDescription(SelectedProductOperation);

        //Метод семны статуса изделия с проверкой готовности маршрутной карты. В методе не проверяется заполнение всех посадочных мест, потому что операция "комплектация" не имеет права быть завершенной без заполнения всех посадочных мест
        public Product EndReclamationWork()
        {
            if (!SQLServerHelper.ProductIsComplete(CurrentProductOperationsList)) //Проверка существования и пройденности МК          
            {
                Dialog.ShowWarning(TextHelper.RouteMapIstCompleteWarningMessage);
                return null;
            }

            if (CurrentProductOperationsList?.Any() ?? false) //Обработка изделия с МК
            {
                return SQLServer.Instance.Product.EditProductStatus(CurrentProductOperationsList.First().Product,
                                                                    ProductStatusManager.DefinitionCompleteProductStatus(SelectedProduct));
            }
            else //Обработка изделия без МК
            {
                return SQLServer.Instance.Product.EditProductStatus(SelectedProduct.Id, ProductStatusManager.DefinitionCompleteProductStatus(SelectedProduct));
            }
        }
        #endregion

        #region CalculateProgress_methods
        public float CalculateAndIncrementProgressToStart()
            => (CurrentProductOperationsCompletedWeight() + OmvpConstant.OperationStartWeight) / (CurrentProductOperationsList.Count() * OmvpConstant.StepWeight);

        public float CalculateAndIncrementProgressToEnd()
        {
            int productOperationsCount = CurrentProductOperationsList.Count();
            return productOperationsCount == 0 ? 1 :
            (CurrentProductOperationsCompletedWeight() + OmvpConstant.StepWeight) / (productOperationsCount * OmvpConstant.StepWeight);


        } //return 1 для изделий без маршрута, но с возможностью "комплектации"
        public float CalculateAndIncrementProgressToEnd(IEnumerable<RouteMapProductOperation> productOperations)
        {
            int colCount = productOperations.Count();
            int colCompletedCount = productOperations.Count(productOperation => productOperation.OperationStatusId == CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id);
            return colCount == 0 ? 1 :
            (CurrentProductOperationsCompletedWeight(colCompletedCount) + OmvpConstant.StepWeight) / (colCount * OmvpConstant.StepWeight);


        } //return 1 для изделий без маршрута, но с возможностью "комплектации"

        public float CalculateAndDecrementProgress()
        {
            int completed = CurrentProductOperationsCompletedCount();
            int colCount = CurrentProductOperationsList.Count();

            if (completed == 0)
            {
                return 0.0F;
            }

            if (completed == colCount)
            {
                return (CurrentProductOperationsCompletedWeight(completed) - (1 * OmvpConstant.OperationEndWeight)) / (CurrentProductOperationsList.Count() * OmvpConstant.StepWeight);
            }

            return CurrentProductOperationsCompletedWeight(completed) / (colCount * OmvpConstant.StepWeight);
        }


        private float CalculateProgressAfterAddingOperation(int equipment) => CurrentProductOperationsCompletedWeight() / ((CurrentProductOperationsList.Count() + 1 + equipment) * OmvpConstant.StepWeight);
        private int CurrentProductOperationsCompletedCount() => CurrentProductOperationsList.Count(productOperation => productOperation.OperationStatusId == CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id);
        private float CurrentProductOperationsCompletedWeight() => CurrentProductOperationsCompletedWeight(CurrentProductOperationsCompletedCount());
        private float CurrentProductOperationsCompletedWeight(int productOperationsCompleteOperationsCount) => productOperationsCompleteOperationsCount * OmvpConstant.StepWeight;
        #endregion

        #region Clear_methods
        public void ClearSelectedEmployee()
        {
            if (SelectedProductOperation != null)
            {
                SelectedProductOperation.OperationExecuterId = null;
                SelectedProductOperation.OperationExecuter = null;
            }
        }
        public void ResetBLProps()
        {
            ClearSelectedProductOperation();
            ClearSelectedOperation();
        }

        public void ClearSelectedProductOperation() => SelectedProductOperation = null;
        public void ClearSelectedOperation() => SelectedOperation = null;
        #endregion
    }
}