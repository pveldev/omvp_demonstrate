﻿using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.BLModels
{
    public class ErrorLogBL : BasicNotifyModel
    {
        public ObservableCollection<ErrorLog> ErrorLogs { get; set; } = new ObservableCollection<ErrorLog>();
        public ErrorLog SelectedErrorLog { get; set; }

        #region Singletion
        private static readonly Lazy<ErrorLogBL> _lazyModel = new Lazy<ErrorLogBL>(() => new ErrorLogBL());
        public static ErrorLogBL Instance => _lazyModel.Value;
        private ErrorLogBL() { }
        #endregion

        public void DeleteSelectedErrorLog()
        {
            if (SQLServer.Instance.ErrorLog.DeleteErrorLog(SelectedErrorLog))
                ErrorLogs.Remove(SelectedErrorLog);
        }

        public void DeleteAllErrorLogs()
        {
            if (SQLServer.Instance.RemoveRangeAndReturnBool(ErrorLogs))
                ErrorLogs.Clear();
        }
    }
}