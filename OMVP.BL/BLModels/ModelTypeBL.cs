﻿using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.BLModels
{
	public class ModelTypeBL : BasicNotifyModel
	{
		public SQLServer SQLServer { get; set; }
        public ObservableCollection<ModelType> ModelTypeList { get; set; } = new ObservableCollection<ModelType>();

        public string NewModelTypeName { get; set; }
		public string NewModelTypeDescription { get; set; }

        #region Singletion
        private static readonly Lazy<ModelTypeBL> _lazyModel = new Lazy<ModelTypeBL>(() => new ModelTypeBL());
        public static ModelTypeBL Instance => _lazyModel.Value;

        private ModelTypeBL()
		{

		}
		#endregion

		public void AddModelType()
		{
            ModelTypeList.Add(SQLServer.Instance.AddEntityToDB(Shaper.FormNewModelType(NewModelTypeName, NewModelTypeDescription)));
			ClearModelTypeProps();
		}

		private void ClearModelTypeProps()
		{
			NewModelTypeName = default;
			NewModelTypeDescription = default;
		}
	}
}
