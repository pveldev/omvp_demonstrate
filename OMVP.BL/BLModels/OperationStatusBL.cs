﻿using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.BLModels
{
	public class OperationStatusBL : BasicNotifyModel
	{
		public SQLServer SQLServer { get; set; }
        public ObservableCollection<OperationStatus> OperationStatusList { get; set; } = new ObservableCollection<OperationStatus>();

		public string NewOperationStatusName { get; set; }

        #region Singleton
        private static readonly Lazy<OperationStatusBL> _lazyModel = new Lazy<OperationStatusBL>(() => new OperationStatusBL());
        public static OperationStatusBL Instance => _lazyModel.Value;

        private OperationStatusBL() { }
		#endregion

		public void AddOperationStatus()
		{
			OperationStatusList.Add(SQLServer.Instance.AddEntityToDB(Shaper.FormNewOperationStatus(NewOperationStatusName)));
			ClearProps();
		}

		private void ClearProps()
		{
			NewOperationStatusName = "";
		}
	}
}
