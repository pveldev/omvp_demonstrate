﻿using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.DataViews;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.BLModels
{
	public class OrderBL : BasicNotifyModel
    {
        public ObservableCollection<OrderTree> OrderTrees { get; set; } = new ObservableCollection<OrderTree>();
        public ObservableCollection<GroupOrderTrees> GroupOrderTrees { get; set; } = new ObservableCollection<GroupOrderTrees>();       
        public ObservableCollection<OrderElement> OrderElements { get; set; } = new ObservableCollection<OrderElement>();//ProductView, полный список OrderElements для выбранной ревизии
		public ICollectionView OrderElementsView { get; set; } //ProductView, выбор месяца с нужным литером для отгрузки


		public OrderElement[] HighLevelOrderElements { get; set; }
		public OrderElementView[] GroupedOrderElements { get; set; }
        public ProductCounter CurrentRevisionProductCounter { get; set; } // Свойство, хранящее подсчет изделий (по статусам и готовности) для текущей ревизии
        public List<OrderElement> OneOrderOrderElements { get; set; } //OrderElements от всего дерева ревизий
		public int SummQuantity { get; set; }

        #region Years_props
        public int[] DispatchYears { get; set; }

        private int _selectedYear;
        public int SelectedYear
        {
            get { return _selectedYear; }
            set
            {
                _selectedYear = value;

                OrderTrees.Clear();
                SQLServer.Instance.OrderTree.GetOrderTrees(CodeDictionary.Instance.AuxiliaryModelType.TopLevelModel.Id, SelectedYear).ForEach(OrderTrees.Add);
            }
        }

        #endregion

        #region Order_OrderTreeProps
        public OrderTree SelectedOrderTree { get; set; } //Order.view from datagrid

        public int NewOrderQuantity { get; set; }
		public string NewOrderLetter { get; set; }	

		private Revision _selectedRevision;
		public Revision SelectedRevision
		{
			get { return _selectedRevision; }
			set
			{
				_selectedRevision = value;
				NewOrderLetter = default;
			}
		}

		private DateTime _newOrderExecutionDate;
		public DateTime NewOrderExecutionDate
		{
			get => _newOrderExecutionDate;
			set
			{
				if (value < OmvpConstant.IstokBDay)
					value = OmvpConstant.IstokBDay;
				_newOrderExecutionDate = value;
			}
		}

		public string NewOrderContract { get; set; }
		public string NewOrderCustomer { get; set; }
		public string NewOrderSHPZCode { get; set; }
		public string NewOrderCode { get; set; }
		public string NewOrderDescription { get; set; }
		#endregion

		#region OrderElementProps
		public byte NewOrderElementMonthIndex { get; set; }

		public int NewOEQuantity { get; set; }
		public string NewOEDescription { get; set; }

        public Revision SelectedRevisionParameter { get; set; }

        private OrderTree _selectedOrderTreeFromComboBox; //OrderElements.view from combobox
        public OrderTree SelectedOrderTreeFromComboBox
        {
            get => _selectedOrderTreeFromComboBox;
            set
            {
                _selectedOrderTreeFromComboBox = value;
                if (_selectedOrderTreeFromComboBox != null)
                {
                    HighLevelOrderElements = null; //Refresh
                    HighLevelOrderElements = GetOneYearOrderElements(_selectedOrderTreeFromComboBox);
                }
            }
        }
        #endregion

        #region EditOrder_window_props
        public Order EditableOrder { get; set; }
        #endregion

        #region Singletion
        private static readonly Lazy<OrderBL> _lazyModel = new Lazy<OrderBL>(() => new OrderBL());
        public static OrderBL Instance => _lazyModel.Value;
        private OrderBL()
		{
			NewOrderExecutionDate = DateTime.Now;
		}

		#endregion

		public void AddOrder()
		{
            List<Revision> revisionsTree = GetRevisionTree(SelectedRevision);
            if (revisionsTree.Any(revision => !revision.BomsIsClaimed))
            {
                Dialog.ShowWarning(TextHelper.CantAddOrderWhileRevisionIstClaimed(SelectedRevision.Name));
                return;
            }

            Order order = Shaper.FormNewOrder(NewOrderQuantity, NewOrderLetter, CodeDictionary.Instance.AuxiliaryOrderStatus.InProgress, NewOrderExecutionDate, NewOrderContract, NewOrderCustomer, NewOrderSHPZCode, NewOrderCode, NewOrderDescription);
			List<OrderTree> orderTrees = Shaper.FormNewOrderTrees(revisionsTree, order); // Добавляю заказы(OrderTree) под все дерево(RevisionTree) 

			OrderTrees.Add(SQLServer.Instance.OrderTree.AddOrder(order, orderTrees));
			if (string.IsNullOrEmpty(NewOrderLetter))
				ClearOrderProps();
			else NewOrderLetter = default; //Не обнуляем все поля в случае добавлении литерного заказа
		}
		public async Task UpsertOrderElementsAsync()
		{
            string errorMessages = CheckBL.OrderElementsIsCorrect(SelectedOrderTreeFromComboBox.Order.Quantity, HighLevelOrderElements);

            if (string.IsNullOrEmpty(errorMessages))
			{
				List<OrderTree> orderTrees = SQLServer.Instance.OrderTree.GetActualOrderTreesByOrderTree(SelectedOrderTreeFromComboBox); // Получаю дерево (КИ-модуль-компонент) заказов по заказу верхнего уровня (КИ)
				List<OrderElement> highLevelOrderElements = DeleteEmptyItems(HighLevelOrderElements); //Convert Array[12] to List(0..12)
				CreateOrUpdateOrderElements(orderTrees, highLevelOrderElements);
				await SQLServer.Instance.OrderElement.InsertOrUpdateOrderElementsAsync(OneOrderOrderElements);
				ClearOrderElementProps();
				HighLevelOrderElements = GetOneYearOrderElements(SelectedOrderTreeFromComboBox); //Refresh 

                Dialog.ShowOk(TextHelper.OrderElementsChangesWasSaved);
            }
			else
			{
                Dialog.ShowWarning(errorMessages);
			}
		}
        public void DeleteSelectedOrder()
        {
            if (SQLServer.Instance.Order.DeleteOrderWithTreeAndElements(SelectedOrderTree.OrderId, SelectedOrderTree.Id))
            {
                OrderTrees.Remove(SelectedOrderTree);
                //Очистка прочих коллекций не является обязательной, поскольку при открытии соответствующих страниц будет произведен запрос
            }
        }

        #region GroudepOrderElementView.xaml
        public void GetOrderElementGroupData(Revision selectedRevisionParameter, int executionYear)
        {
            if (selectedRevisionParameter.Model.HasLetter)
            {
                GetOrderElementGroupDataWithLetter(selectedRevisionParameter, executionYear);
            }
            else
            {
                GetOrderElementGroupDataWithoutLetter(selectedRevisionParameter, executionYear);
            }
        }
        #region GroudepOrderElementView.xaml_HasLetter=false
        private void GetOrderElementGroupDataWithoutLetter(Revision selectedRevisionParameter, int executionYear)
        {
            List<OrderElement> highLvlOrderElements = SQLServer.Instance.OrderElement.GetOrderElements(selectedRevisionParameter, executionYear);
            OrderElement[] groupedOrderElements = new OrderElement[12];

            groupedOrderElements = Enumerable.Range(1, 12).Select(counter =>
            {
                IEnumerable<OrderElement> grouppedByMonth = highLvlOrderElements.Where(t => t.Month == counter);

                return new OrderElement
                {
                    Month = (byte)counter,
                    Quantity = grouppedByMonth.Sum(soe => soe.Quantity),
                    CompletedQuantity = grouppedByMonth.Sum(soe => soe.CompletedQuantity),                  
                    Description = ""
                };
            }).ToArray();

            List<float> progressCol = SQLServer.Instance.Product.GetProgresses(selectedRevisionParameter.Id, ProductStatusManager.GetPutToWorkStatusIds());
            GroupedOrderElements = ConverOrderElementToView(groupedOrderElements, progressCol);

            CurrentRevisionProductCounter = SQLServer.Instance.Product.GetProductCounter(
                selectedRevisionParameter.Id, 
                ProductStatusManager.InWork.Id,
                ProductStatusManager.ReadyToInstall.Id,
                ProductStatusManager.Installed.Id,
                ProductStatusManager.ReadyToDispatch.Id,
                ProductStatusManager.Reclamation.Id,
                ProductStatusManager.InstalledInReclamation.Id,
                ProductStatusManager.ReadyToReclamationDispatch.Id,
                ProductStatusManager.ReadyToReclamationInstall.Id);
        }

        //Конвертация в представление
        private OrderElementView[] ConverOrderElementToView(OrderElement[] groupedOrderElements, List<float> progressCol)
        {
            return groupedOrderElements.Select(oe => new OrderElementView()
            {
                Month = oe.Month,
                Quantity = oe.Quantity,
                CompletedQuantity = oe.CompletedQuantity,
                CurrentProgress = Math.Round(CaclulateProgress((float)oe.CompletedQuantity, oe.Quantity)),
                Description = oe.Description,
                Progress = CalculateProgress(oe, progressCol)
            }).ToArray();
        }

        private double CalculateProgress(OrderElement orderElement, List<float> progressCol)
        {
            if (orderElement is null || orderElement.Quantity == 0)
                return float.NaN;

            int quantityDifference = orderElement.Quantity - orderElement.CompletedQuantity;
            if (quantityDifference > 0)
            {
                int progressColCountCheck = progressCol.Count() - quantityDifference;
                if (progressColCountCheck < 0) //Если в коллекции нехватает элементов - досыпаю нулей
                {
                    for (int i = 0; i > progressColCountCheck; i--)
                    {
                        progressCol.Add(0);
                    }
                }


                double result = Math.Round(progressCol.Take(quantityDifference).Average(), 2);
                progressCol.RemoveRange(0, quantityDifference);
                return result;
            }

            if (quantityDifference == 0)
                return 1.0;

            if (quantityDifference < 0)
                throw new ArgumentException();

            return 0.0;
        }
        #endregion
        #region GroudepOrderElementView.xaml_HasLetter=true
        private void GetOrderElementGroupDataWithLetter(Revision selectedRevisionParameter, int executionYear)
        {
            List<OrderElement> orderElements = SQLServer.Instance.OrderElement.GetOrderElements(selectedRevisionParameter, executionYear);
            List<ProductProgressAndLetter> productProgressAndLetters = SQLServer.Instance.Product.GetLetterProgresses(selectedRevisionParameter.Id, ProductStatusManager.GetPutToWorkStatusIds());
            OrderElementView[] orderElementViews = ConverOrderElementToView(orderElements);

            foreach (OrderElementView view in orderElementViews)
            {
                ProjectProgressAndCompletedQuantity(view, productProgressAndLetters);
            }

            GroupedOrderElements = Enumerable.Range(1, 12).Select(counter =>
            {
                IEnumerable<OrderElementView> grouppedByMonth = orderElementViews.Where(t => t.Month == counter);
                int completedProjectedQuantitySum = grouppedByMonth.Sum(soe => soe.ProjectedCompletedQuantity);
                int quantitySum= grouppedByMonth.Sum(soe => soe.Quantity);
                int completedQuantitySum = grouppedByMonth.Sum(soe => soe.CompletedQuantity);
                double completedProjectedQuantityProgressSum = grouppedByMonth.Sum(soe => soe.Progress * soe.ProjectedCompletedQuantity);

                return new OrderElementView
                {
                    Month = (byte)counter,
                    Quantity = quantitySum,
                    CompletedQuantity = completedQuantitySum,
                    ProjectedCompletedQuantity = completedProjectedQuantitySum,
                    Progress = CaclulateProgress(completedProjectedQuantityProgressSum, quantitySum),
                    CurrentProgress = CaclulateProgress((double)completedQuantitySum, quantitySum),
                    OrderLetter = "",
                    Description = OrderElementsToGroupDescription(grouppedByMonth)
                };
            }).ToArray();

            CurrentRevisionProductCounter = SQLServer.Instance.Product.GetProductCounter(
                selectedRevisionParameter.Id,
                ProductStatusManager.InWork.Id,
                ProductStatusManager.ReadyToInstall.Id,
                ProductStatusManager.Installed.Id,
                ProductStatusManager.ReadyToDispatch.Id,
                ProductStatusManager.Reclamation.Id,
                ProductStatusManager.InstalledInReclamation.Id,
                ProductStatusManager.ReadyToReclamationDispatch.Id,
                ProductStatusManager.ReadyToReclamationInstall.Id);
        }

        private double CaclulateProgress(double completedQuantitySum, int quantitySum) => completedQuantitySum / ((double)quantitySum);
        

        //Конвертация в представление
        private OrderElementView[] ConverOrderElementToView(List<OrderElement> orderElements)
        {
            return orderElements.Select(oe => new OrderElementView()
            {
                Month = oe.Month,
                Quantity = oe.Quantity,
                CompletedQuantity = oe.CompletedQuantity,
                ProjectedCompletedQuantity = oe.CompletedQuantity, //Дублирую значение готовых для дальнейших подсчетов
                OrderLetter = oe.OrderTree.Order.Letter
            }).ToArray();
        }
        private void ProjectProgressAndCompletedQuantity(OrderElementView orderElementView, List<ProductProgressAndLetter> productProgressAndLetters)
        {
            if (orderElementView is null || orderElementView.Quantity == 0)
                return;

            int quantityDifference = orderElementView.Quantity - orderElementView.CompletedQuantity;
            if (quantityDifference > 0)//Если требуется "досЫпать"
            {
                IEnumerable<ProductProgressAndLetter> suitableProduct = productProgressAndLetters.Where(product => product.Letter == orderElementView.OrderLetter); //"Подходящие" 

                int suitableProductCount = suitableProduct.Count();
                if (suitableProductCount >= quantityDifference) //Если хватает количество, то "пересыпаю" нужные
                {
                    IEnumerable<ProductProgressAndLetter> productsForPouring = suitableProduct.Take(quantityDifference);// Изделия для "пересыпания"
                    orderElementView.ProjectedCompletedQuantity += quantityDifference;
                    orderElementView.Progress = ((productsForPouring.Sum(p => p.Progress) + orderElementView.CompletedQuantity) / orderElementView.Quantity);

                    productsForPouring.ToList().ForEach(p => productProgressAndLetters.Remove(p)); //Удаление "пересыпанных" изделий
                }
                else //Если НЕ хватает, то "пересыпаю" сколько есть
                {
                    int needed = quantityDifference - suitableProductCount;
                    orderElementView.ProjectedCompletedQuantity += suitableProductCount;
                    orderElementView.Progress = ((suitableProduct.Sum(p => p.Progress) + orderElementView.CompletedQuantity) / orderElementView.Quantity);

                    suitableProduct.ToList().ForEach(p => productProgressAndLetters.Remove(p)); //Удаление "пересыпанных" изделий
                }
                
            }

            if (quantityDifference == 0)//Если требуется "досЫпать"
            {
                orderElementView.Progress = 1.0;
            }

            if (quantityDifference < 0)
                throw new ArgumentException();
        }
        
        private string OrderElementsToGroupDescription(IEnumerable<OrderElementView> orderElementsViews) =>
            string.Join(", ", orderElementsViews.Select(oe => string.Concat("[л.", oe.OrderLetter," ", oe.CompletedQuantity, "/", oe.Quantity, "шт.]")).ToList());

        #endregion
        #endregion

        public void ClaimSelectedOrder()
        {
            SelectedOrderTree.Order.IsClaimed = true;
            if (!SQLServer.Instance.ModifyEntity(SelectedOrderTree.Order))
                SelectedOrderTree.Order.IsClaimed = false;
        }

        public void ArchiveSelectedOrder()
        {
            Order order = SQLServer.Instance.Order.ArchiveSelectedOrder(SelectedOrderTree.Order, CodeDictionary.Instance.AuxiliaryOrderStatus.Archive.Id);

            if (order is null) return;

            SelectedOrderTree.Order = order;
        }
        
        private void CreateOrUpdateOrderElements(List<OrderTree> orderTrees, List<OrderElement> highLevelOrderElements)
		{
			//Обновляю количество
			OneOrderOrderElements.ForEach(orderElement => orderElement.Quantity = highLevelOrderElements
									.Single(highOrderElement => highOrderElement.Month == orderElement.Month).Quantity);

			//Получение новых строк
			List<OrderElement> addedOrderElements = highLevelOrderElements.Except(OneOrderOrderElements).ToList();

			//Добавление новых строк для всего дерева
			orderTrees.ForEach(orderTree => OneOrderOrderElements.AddRange(Shaper.FormNewOrderElementsForNewMonth(orderTree, addedOrderElements)));
		}

		private List<OrderElement> DeleteEmptyItems(OrderElement[] oneYearOrderElements)
		{
			return oneYearOrderElements.ToList().Where(orderElement => orderElement.Id != 0 || orderElement.Quantity != 0).ToList();
		}
		
		private List<Revision> GetRevisionTree(Revision theme)
		{
			List<Revision> revisionsTree = new List<Revision> { theme }; //add theme (lvl_1)

			List<Revision> modules = SQLServer.Instance.Revision.GetRevisionsChildren(theme.Id);
			revisionsTree.AddRange(modules); //add modules (lvl_2)

			foreach (Revision module in modules)
				revisionsTree.AddRange(SQLServer.Instance.Revision.GetRevisionsChildren(module.Id)); //add components (lvl_3)

			return revisionsTree;
		}

        private OrderElement[] GetOneYearOrderElements(OrderTree selectedOrderTree)
		{
			OneOrderOrderElements = SQLServer.Instance.OrderElement.GetOrderElementsByOrderId(selectedOrderTree.OrderId);
			List<OrderElement> highLevelOrderElements = OneOrderOrderElements.Where(orderElement => orderElement.OrderTreeId == selectedOrderTree.Id).ToList();

			OrderElement[] yearOrderElements = new OrderElement[12];			

			for (int i = 0; i < highLevelOrderElements.Count(); i++)
				yearOrderElements[i] = highLevelOrderElements[i];
			
			return Enumerable.Range(1, 12).Select(month => yearOrderElements
                .SingleOrDefault(orderElement => orderElement != null && orderElement.Month == month) ?? new OrderElement { Month = (byte)month }).ToArray();
		} //Формирую (для OrderElementView) 12 строчек OrderElements из n сущестующих строчек заказа 

        #region Edit_order_methods
        public void SaveEditOrderChanges()
        {
            SelectedOrderTree.Order.Contract = EditableOrder.Contract;
            SelectedOrderTree.Order.Customer = EditableOrder.Customer;
            SelectedOrderTree.Order.Description = EditableOrder.Description;
            SelectedOrderTree.Order.IsClaimed = EditableOrder.IsClaimed;
            SelectedOrderTree.Order.Letter = EditableOrder.Letter;
            SelectedOrderTree.Order.OrderCode = EditableOrder.OrderCode;
            SelectedOrderTree.Order.Quantity = EditableOrder.Quantity;
            SelectedOrderTree.Order.SHPZCode = EditableOrder.SHPZCode;

            SQLServer.Instance.ModifyEntity(SelectedOrderTree.Order);
            ClearEditOrderProps();
        }
        public void ClearEditOrderProps()
        {
            SelectedOrderTree = null;
            EditableOrder = null;
        }
        #endregion

        #region Clear_methods
        private void ClearOrderProps()
		{
			NewOrderQuantity = 0;
			NewOrderLetter = default;
			NewOrderExecutionDate = DateTime.Today;
			NewOrderDescription = "";
			NewOrderContract = default;
			NewOrderSHPZCode = default;
			NewOrderCustomer = default;
			NewOrderCode = default;

			SelectedRevision = null;
		}
		private void ClearOrderElementProps()
		{
			NewOrderElementMonthIndex = 0;
			NewOEQuantity = 0;
			NewOEDescription = "";
			HighLevelOrderElements = new OrderElement[12];
		}
        #endregion
    }
}
