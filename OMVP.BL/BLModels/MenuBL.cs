﻿using OMVP.BL.CoreBL;
using OMVP.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace OMVP.BL.BLModels
{
	public class MenuBL : BasicNotifyModel
	{
        public PageInfo CurrentPage { get; set; }

        public bool IsShowedDescription { get; set; }
        public DateTime DataRefreshTime { get; set; }
        public CurrentActionInfo CurrentActionInfo { get; set; } = new CurrentActionInfo() { ActionInProcess = false, Progress = 0.01f, CurrentActionDescription = ""};

        #region Singleton
        private static readonly Lazy<MenuBL> _lazyModel = new Lazy<MenuBL>(() => new MenuBL());
        public static MenuBL Instance => _lazyModel.Value;
        private MenuBL() { }
		#endregion

		public void SetCurrentPageTitle(string title)
		{
            CurrentPage.PageTitle = title;
		}

        #region AnimationManager
        public void StartLoadAnimation(float startProgress = OmvpConstant.DefaultProgressValue, string actionDescription = "") => ChangeProcessStatus(true, startProgress, actionDescription);
        public void StopLoadAnimation(string actionName = "") => ChangeProcessStatus(false, OmvpConstant.FullCompleteProgressValue, actionName);
        private void ChangeProcessStatus(bool newValue, float newProgress, string actionDescription)
        {
            CurrentActionInfo.ActionInProcess = newValue;
            CurrentActionInfo.Progress = newProgress;
            CurrentActionInfo.CurrentActionDescription = actionDescription;
            OnPropertyChanged(nameof(CurrentActionInfo)); //converter trigger
        }

        #region Steps
        public void CheckComplete() => UpdateProgress(OmvpConstant.Step1);//Шаг 1
        public void DataPrepearingComplete() => UpdateProgress(OmvpConstant.Step2);//Шаг 2
        public void UpdateDbComplete() => UpdateProgress(OmvpConstant.Step3); //Шаг 3
        public void CalculateComplete() => UpdateProgress(OmvpConstant.Step3); //Шаг 3
        public void UpdateOnScreenComplete() => UpdateProgress(OmvpConstant.Step4); //Шаг 4
        public void PostUpdateChecksComplete() => UpdateProgress(OmvpConstant.Step4); //Шаг 4
        public void ProcessingComplete() => SetNewProgress(OmvpConstant.FullCompleteProgressValue, ""); //Завершающий шаг (уставнока 100% в прогресс баре)
        #endregion

        public void UpdateProgress(float addedValue, string actionDescription = "")
        {
            SetNewProgress(CurrentActionInfo.Progress + addedValue, actionDescription);
        }
        public void SetNewProgress(float newProgress, string actionDescription)
        {
            CurrentActionInfo.Progress = newProgress;
            CurrentActionInfo.CurrentActionDescription = actionDescription;
            OnPropertyChanged(nameof(CurrentActionInfo)); //converter trigger
        }
        #endregion
    }
}
