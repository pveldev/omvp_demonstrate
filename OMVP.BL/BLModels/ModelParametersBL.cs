﻿using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OMVP.BL.BLModels
{
    //BL for db.ModelParameters, db.ParameterTypes tables
    public class ModelParametersBL : BasicNotifyModel
    {
        public ObservableCollection<ParameterType> ParameterTypes { get; set; } = new ObservableCollection<ParameterType>(); //all of parameter types
        public ObservableCollection<ModelParameter> CurrentModelParameters { get; set; } = new ObservableCollection<ModelParameter>(); //ModelParameters for SelectedModel


        #region ModelParameters_props
        public ModelParameter SelectedModelParameter { get; set; }

        //combobox selected item
        private Model _selectedModel;
        public Model SelectedModel
        {
            get { return _selectedModel; }
            set
            {
                _selectedModel = value;
                if (_selectedModel != null)
                {
                    CurrentModelParameters.Clear();
                    SQLServer.Instance.ModelParameter.GetModelParameters(_selectedModel.Id).ForEach(CurrentModelParameters.Add);
                }
            }
        }

        public string NewModelParameterName { get; set; }
        public string NewModelParameterDescription { get; set; }
        #endregion
        #region ParameterTypes_props
        public ParameterType SelectedParameterType { get; set; }

        #region NewParameterType_props
        //ModelTypeView.xaml
        public string NewParameterTypeName { get; set; }
        public string NewParameterTypeUnit { get; set; }
        public string NewParameterTypeDescription { get; set; }
        #endregion

        #endregion
        #region EditModelParameterWindow_props
        public ModelParameter EditableModelParameter { get; set; }
        #endregion
        #region EditModelParameterWindow_props
        public ParameterType EditableParameterType { get; set; }
        #endregion
        #region Singletion
        private static readonly Lazy<ModelParametersBL> _lazyModel = new Lazy<ModelParametersBL>(() => new ModelParametersBL());
        public static ModelParametersBL Instance => _lazyModel.Value;
        private ModelParametersBL() { }
        #endregion

        #region ModelParameters_methods    
        public async Task AddNewModelParameterToModel()
        {
            if (!CheckBeforeAdding())          
                return;
            
            ModelParameter newModelParam = 
                await SQLServer.Instance.ModelParameter.AddNewModelParameterAsync(
                    Shaper.FormNewModelParameter(SelectedModel.Id, SelectedParameterType.Id, NewModelParameterName, NewModelParameterDescription));

            if (newModelParam != null)
                CurrentModelParameters.Add(newModelParam);

            ClearModelParamData();
        }

        private bool CheckBeforeAdding() => CurrentModelParametersHasDuplicate() ? UserWantToAddDuplicate() : true;
        private bool CurrentModelParametersHasDuplicate() => CurrentModelParameters.Any(param => param.ParameterTypeId == SelectedParameterType.Id);
        private bool UserWantToAddDuplicate() => Dialog.ShowYesNo(TextHelper.DoYouWantToAddParamToModel(SelectedModel.Name, SelectedParameterType.Name)) == MessageBoxResult.Yes;

        public void RemoveModelParameterFromModel()
        {
            if (SQLServer.Instance.ModelParameter.RemoveModelParameterFromModel(SelectedModelParameter))
            {
                CurrentModelParameters.Remove(SelectedModelParameter);
                SelectedModelParameter = null;
            }
        }

        #endregion
        #region EditModelParameterWindow_methods
        public void ClearEditableModelParameter()
        {
            EditableModelParameter = null;
        }
        public void SaveModelParameterChanges()
        {
            SelectedModelParameter.Name = EditableModelParameter.Name;
            SelectedModelParameter.Description = EditableModelParameter.Description;
            SelectedModelParameter.ParameterType.Name = EditableModelParameter.ParameterType.Name;
            SelectedModelParameter.ParameterType.Unit = EditableModelParameter.ParameterType.Unit;
            SelectedModelParameter.ParameterType.Description = EditableModelParameter.ParameterType.Description;

            SQLServer.Instance.ModelParameter.SaveModelParameterChanges(SelectedModelParameter);
            ClearEditableModelParameter();
        }
        #endregion

        #region FormEditParameterTypeWindow_methods
        public void ClearEditableParamTypeProps()
        {
            EditableParameterType = null;
        }
        public void SaveParamTypeChanges()
        {
            SelectedParameterType.Name = EditableParameterType.Name;
            SelectedParameterType.Unit = EditableParameterType.Unit;
            SelectedParameterType.Description = EditableParameterType.Description;

            SQLServer.Instance.ParameterType.SaveParameterTypeChanges(SelectedParameterType);
        }
        #endregion

        #region ParameterTypes_methods
        public async Task AddNewParameterTypeAsync()
        {
            ParameterType newParamType = 
                await SQLServer.Instance.ParameterType.AddNewParameterTypeAsync(
                    Shaper.FormNewParameterType(NewParameterTypeName, NewParameterTypeUnit, NewParameterTypeDescription));

            if (newParamType != null)
                ParameterTypes.Add(newParamType);

            ClearNewParamTypeData();
        }
        #endregion
        #region Clear_methods
        private void ClearNewParamTypeData()
        {
            NewParameterTypeName = default;
            NewParameterTypeUnit = default;
            NewParameterTypeDescription = default;
        }
        private void ClearModelParamData()
        {
            SelectedParameterType = default;
            NewModelParameterName = default;
            NewModelParameterDescription = default;            
        }
        #endregion
    }
}
