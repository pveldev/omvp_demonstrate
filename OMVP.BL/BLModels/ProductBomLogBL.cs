﻿using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.BLModels
{
    public class ProductBomLogBL : BasicNotifyModel
    {
        public ObservableCollection<ProductBomLog> ProductBomLogs { get; set; } = new ObservableCollection<ProductBomLog>();
        public ProductBomLog SelectedProductBomLogBL { get; set; }

        #region Singleton
        private static readonly Lazy<ProductBomLogBL> _lazyModel = new Lazy<ProductBomLogBL>(() => new ProductBomLogBL());
        public static ProductBomLogBL Instance => _lazyModel.Value;
        private ProductBomLogBL() { }
        #endregion

        #region Parameters_props
        private Revision _selectedRevisionParameter;
        public Revision SelectedRevisionParameter
        {
            get { return _selectedRevisionParameter; }
            set
            {
                _selectedRevisionParameter = value;
                if (_selectedRevisionParameter != null)
                {
                    ProductBoms = new List<RevisionBOM>(SQLServer.Instance.RevisionBom.GetRevisionBOMs(_selectedRevisionParameter.Id)); //Заполнения списка посадочных мест для выбранной ревизии
                    ParentProducts = new List<Product>(SQLServer.Instance.Product.GetSortedByNumberProducts(_selectedRevisionParameter.Id));
                }
            }
        }

        private RevisionBOM _selectedSlotParameter;
        public RevisionBOM SelectedSlotParameter
        {
            get { return _selectedSlotParameter; }
            set
            {
                _selectedSlotParameter = value;
                if (_selectedSlotParameter != null)
                {
                    ChildProducts = new List<Product>(SQLServer.Instance.Product.GetSortedByNumberProducts(_selectedSlotParameter.BOMRevisionId));
                }
            }
        }

        public Product SelectedParentProductParameter { get; set; }
        public Product SelectedChildProductParameter { get; set; }
        public DateTime SelectedStartDateParameter { get; set; } = OmvpConstant.DefaultStartDateParameter;
        public DateTime SelectedEndDateParameter { get; set; } = OmvpConstant.DefaultEndDateParameter;


        public List<RevisionBOM> ProductBoms { get; set; } //ComboBox ItemsSource
        public List<Product> ParentProducts { get; set; } //ComboBox ItemsSource
        public List<Product> ChildProducts { get; set; } //ComboBox ItemsSource
        #endregion

        public void GetProductBomLogWithParameters()
        {
            ProductBomLogs.Clear();
            SQLServer.Instance.ProductBomLog.GetProductBomLogs
                ( 
                    SelectedRevisionParameter, 
                    SelectedSlotParameter,
                    SelectedParentProductParameter, 
                    SelectedChildProductParameter,
                    SelectedStartDateParameter,
                    SelectedEndDateParameter
                ).ForEach(ProductBomLogs.Add);

            ClearAllProductBomLogsParameters();
            ClearComboBoxCollections();
        }

        #region Clear_methods
        private void ClearAllProductBomLogsParameters()
        {
            ClearSelectedRevisionParameter();
            ClearSelectedSlotParameter();
            ClearSelectedParentProductParameter();
            ClearSelectedChildProductParameter();
            ClearSelectedStartDateParameter();
            ClearSelectedEndDateParameter();
        }
        private void ClearComboBoxCollections()
        {
            ProductBoms = null;
            ParentProducts = null;
            ChildProducts = null;
        }
        public void ClearSelectedRevisionParameter() => SelectedRevisionParameter = null;
        public void ClearSelectedSlotParameter() => SelectedSlotParameter = null;
        public void ClearSelectedParentProductParameter() => SelectedParentProductParameter = null;
        public void ClearSelectedChildProductParameter() => SelectedChildProductParameter = null;
        public void ClearSelectedStartDateParameter() => SelectedStartDateParameter = OmvpConstant.DefaultStartDateParameter;
        public void ClearSelectedEndDateParameter() => SelectedEndDateParameter = OmvpConstant.DefaultEndDateParameter;
        #endregion
    }
}
