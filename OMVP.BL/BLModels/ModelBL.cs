﻿using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.ObjectModel;

namespace OMVP.BL.BLModels
{
    public class ModelBL : BasicNotifyModel
    {
        public SQLServer SQLServer { get; set; } = SQLServer.Instance;
        public ObservableCollection<Model> ModelList { get; set; } = new ObservableCollection<Model>(); //ModelView, EmployeeView, ModelParameterView
        public ObservableCollection<Model> AllModelList { get; set; } = new ObservableCollection<Model>();//RevisionView

        #region Singleton
        private static readonly Lazy<ModelBL> _lazyModel = new Lazy<ModelBL>(() => new ModelBL());
        public static ModelBL Instance => _lazyModel.Value;
        private ModelBL() { }

		#endregion

		#region ModelProps(ModelView)
		public string NewModelName { get; set; }
		public string NewModelAbbreviationName { get; set; }     
        public string NewRevisionName { get; set; } //Имя ревизии, которая автоматически создается под новую модель
		public bool NewModelHasLetter { get; set; } //Литерный
        public string NewModelKRPG { get; set; }
        public ModelType NewModelType { get; set; }
        #endregion

        public Model SelectedModel { get; set; }

        public void AddModelWithRevision()
        {
            Model newModel = Shaper.FormNewModel(NewModelName, NewModelAbbreviationName, NewModelKRPG, NewModelType, NewModelHasLetter);
            AllModelList.Add(SQLServer.Model.AddModelWithRevision(newModel, Shaper.FormNewRevision(newModel, NewRevisionName))); 
            ClearModelProps();
        }
   
        private void ClearModelProps()
        {
            NewModelName = default;
            NewModelAbbreviationName = default;
            NewRevisionName = default;
            NewModelKRPG = default;
            NewModelType = null;
			NewModelHasLetter = false;
		}
    }
}
