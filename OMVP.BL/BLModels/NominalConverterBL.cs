﻿using OMVP.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.BLModels
{
    public class NominalConverterBL : BasicNotifyModel
    {
        #region Singletion
        private static readonly Lazy<NominalConverterBL> _lazyModel = new Lazy<NominalConverterBL>(() => new NominalConverterBL());
        public static NominalConverterBL Instance => _lazyModel.Value;
        private NominalConverterBL() { }
        #endregion

        private double _nominal_pF;
        public double Nominal_pF
        {
            get { return _nominal_pF; }
            set 
            { 
                _nominal_pF = value;
                Nominal_mF = _nominal_pF / 1000000;
            }
        }

        private double _nominal_nF;
        public double Nominal_nF
        {
            get { return _nominal_nF; }
            set 
            { 
                _nominal_nF = value;
                Nominal_pF = _nominal_nF * 1000;
            }
        }

        private double _nominal_mF;
        public double Nominal_mF
        {
            get { return _nominal_mF; }
            set 
            {
                _nominal_mF = value;
                Nominal_nF = _nominal_mF * 1000;
            }
        }
    }
}
