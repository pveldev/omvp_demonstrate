﻿using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.BLModels
{
	public class RevisionBOMBL : BasicNotifyModel
	{
        public ObservableCollection<Revision> ModelRevisionList { get; set; } = new ObservableCollection<Revision>();//Ревизии выбранной модели
        public ObservableCollection<RevisionBOM> RevisionBOMList { get; set; } = new ObservableCollection<RevisionBOM>(); // Список слотов выбранной ревизии

        private Model _selectedModel;
        public Model SelectedModel
        {
            get => _selectedModel;
            set
            {
                _selectedModel = value;
                if (_selectedModel != null)
                {

                    ModelRevisionList.Clear();
                    SQLServer.Instance.Revision.GetRevisions(_selectedModel.Id).ForEach(ModelRevisionList.Add); 

                    if (ModelRevisionList.Count == 1)
                        SelectedModelRevision = ModelRevisionList[0];
                }
            }
        } //Модель из списка ModelBL.AllModelList

        private Revision _selectedModelRevision;
        public Revision SelectedModelRevision
        {
            get { return _selectedModelRevision; }
            set
            {
                _selectedModelRevision = value;
                if (_selectedModelRevision != null)
                {
                    RevisionBOMList.Clear();
                    SQLServer.Instance.RevisionBom.GetRevisionBOMs(_selectedModelRevision.Id).ForEach(RevisionBOMList.Add);
                }
            }
        } //Ревизия из списка RevisionBOMList

        public RevisionBOM SelectedRevisionBom { get; set; }

        #region NewSlot_props
        public ObservableCollection<Revision> NewSlotRevisions { get; set; } = new ObservableCollection<Revision>();//Ревизии для формирования нового посадочного места
        public string NewSlotName { get; set; }

        private Model _newSelectedSlotModel;
        public Model NewSelectedSlotModel
        {
            get { return _newSelectedSlotModel; }
            set
            {
                _newSelectedSlotModel = value;
                if (_newSelectedSlotModel != null)
                {
                    NewSlotRevisions.Clear();
                    SQLServer.Instance.Revision.GetRevisions(_newSelectedSlotModel.Id).ForEach(NewSlotRevisions.Add);

                    if (NewSlotRevisions.Count == 1)
                        NewSelectedSlotBomRevision = NewSlotRevisions[0];
                }
            }
        }
        public Revision NewSelectedSlotBomRevision { get; set; }

        #endregion

        #region Singleton
        private static readonly Lazy<RevisionBOMBL> _lazyModel = new Lazy<RevisionBOMBL>(() => new RevisionBOMBL());
        public static RevisionBOMBL Instance => _lazyModel.Value;

        private RevisionBOMBL() { }
        #endregion

        public void AddRevisionBOM()
        {
            //ToDo: refactoring череды проверок 

            //claim check
            if (SelectedModelRevision.BomsIsClaimed)
            {
                Dialog.ShowWarning(TextHelper.CantAddSlotWhileRevisionIsClaimed(SelectedModelRevision.Name, SelectedModelRevision.Model.Name));
                return;
            }

            //loop check
            if (SelectedModelRevision.ModelId == NewSelectedSlotBomRevision.ModelId)
            {
                Dialog.ShowWarning(TextHelper.CantAddRevisionAsSlot);
                return;
            }

            //name duplicate check
            if (RevisionBOMList.Any(slot => string.Equals(slot.Name, NewSlotName)))
            {

                Dialog.ShowWarning(TextHelper.CantAddSlotToSelectedRevision(NewSlotName));
                return;          
            }

            RevisionBOMList.Add(SQLServer.Instance.AddEntityToDB(Shaper.FormNewRevisionBOM(NewSlotName, NewSelectedSlotModel, SelectedModelRevision, NewSelectedSlotBomRevision)));
            ClearRevisionBOMProps();
        }

        public void DeleteSelectedSlot()
        {
            if (SQLServer.Instance.Remove(SelectedRevisionBom))
            {
                RevisionBOMList.Remove(SelectedRevisionBom);
                SelectedRevisionBom = null;
            }
        }

        public void ClaimSelectedRevisionBoms()
        {
            Revision revision = SQLServer.Instance.Revision.ClaimRevision(SelectedModelRevision);
            if (revision != null)
            {
                SelectedModelRevision = revision;
            }
            else
            {
                Dialog.ShowError(TextHelper.ClaimRevisionDbTransactionExCatched);
            }          
        }

        private void ClearRevisionBOMProps()
        {
            NewSlotName = null;
            NewSelectedSlotModel = null;
            NewSelectedSlotBomRevision = null;
        }
    }
}
