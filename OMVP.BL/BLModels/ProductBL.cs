﻿using OMVP.DAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ComponentModel;
using OMVP.DAL.Models;
using OMVP.BL.CoreBL;
using OMVP.Modal;
using OMVP.Core;
using OMVP.DAL.CoreDAL;
using System.Windows.Data;
using System.Threading.Tasks;

namespace OMVP.BL.BLModels
{
    public class ProductBL : BasicNotifyModel
    {
        #region All_props

        public ObservableCollection<Product> Products { get; set; } = new ObservableCollection<Product>();
		public ICollectionView ProductsView { get; set; }

        public Revision CurrentRevision { get; set; } //Trigger: refresh commands 

		private Product _selectedProduct;
		public Product SelectedProduct
		{
			get { return _selectedProduct; }
			set
			{
				_selectedProduct = value;
				if (_selectedProduct != null && _selectedProduct.Revision != null && _selectedProduct.Revision.Model != null)
                {              
                    OrderBL.Instance.OrderElementsView?.Refresh(); //refresh по фильтру для отображения месячной отгрузки для выбранной литерности										
                    SelectedOrderElement = null;
                }
			}
		}

		public IList SelectedProducts { get; set; }
        #region Properties_AddNewProduct_Expander
        public ObservableCollection<Employee> Employees { get; set; } = new ObservableCollection<Employee>();

        public ObservableCollection<Product> PreviewNewProducts { get; set; } = new ObservableCollection<Product>();//PreviewNewProductsWindow.xaml
		public RouteMap SelectedRouteMap { get; set; }

        public Employee SelectedEngineer { get; set; }
        public Employee SelectedTechnologist { get; set; }

        private WorkingGroup _selectedWorkingGroup;
		public WorkingGroup SelectedWorkingGroup
		{
			get { return _selectedWorkingGroup; }
			set
			{
				_selectedWorkingGroup = value;
				if (_selectedWorkingGroup != null)
                {
                    Employees.Clear();
                    SQLServer.Instance.Employee.GetSortedNotArchivedEmployees(_selectedWorkingGroup.Id).ForEach(Employees.Add);
                }
            }
		}
		public List<WorkingGroup> CurrentWorkingGroupList { get; set; }
		public string NewProductDescription { get; set; }
		public string StartNumber { get; set; }

		private int _newProductQuantity;
		public int NewProductQuantity
		{
			get { return _newProductQuantity; }
			set
			{
				if (value <= 0)
					value = 1;
				_newProductQuantity = value;
			}
		}
		public string NewProductLetter { get; set; } = null;
		#endregion

		#region EditProduct_props
		private WorkingGroup _editProductWorkingGroup;
		public WorkingGroup EditProductWorkingGroup
		{
			get { return _editProductWorkingGroup; }
			set
			{
				_editProductWorkingGroup = value;
				if (_editProductWorkingGroup!= null)
                {
                    Employees.Clear();
                    SQLServer.Instance.Employee.GetSortedNotArchivedEmployees(_editProductWorkingGroup.Id).ForEach(Employees.Add);
                }
			}
		}

        public Employee EditProductEngineer { get; set; }
        public Employee EditProductTechnologist { get; set; }

        public RouteMap EditProductRouteMap { get; set; }
        #endregion
        #region DispatchedProductsParameters_props
        public DateTime SelectedDispatchedDate { get; set; } = DateTime.Now;
        #endregion

        public OrderElement SelectedOrderElement { get; set; }

		#region SearchBar_Props

		private string _numberSearchBar;
		public string NumberSearchBar
		{
			get { return _numberSearchBar; }
			set
			{
				_numberSearchBar = value;
				ProductsView.Refresh();
			}
		} //Общая строка поиска по всем вкладам
        #endregion

        #region SearchProductByNumberWindow
        public string GlobalSearchPrductBySerialNumber { get; set; }
        public bool IsAdvancedSearch { get; set; } = false;
        #endregion
        #endregion

        #region Singleton
        private static readonly Lazy<ProductBL> _lazyModel = new Lazy<ProductBL>(() => new ProductBL());
        public static ProductBL Instance => _lazyModel.Value;
        private ProductBL() { }
        #endregion

        #region AddProducts_methods
        public async Task AddSingleProductAsync()
        {
            #region Checks
            if (!RevisionClaimCheck())           
                return;            

            if (SQLServer.Instance.Product.SerialNumberIsDuplicateCheck(StartNumber, RevisionBL.Instance.SelectedRevision.Id))
            {
                Dialog.ShowWarning(TextHelper.NumberIsADuplicate(StartNumber));
                return;
            }
            MenuBL.Instance.CheckComplete();
            #endregion

            #region Data_prepearing
            RevisionBL.Instance.SelectedRevisionRefreshQuery(); //Предотвращение бага с потерей навигационного свойства при добавлении изделий
            ProductStatus newProductStatus = ProductStatusManager.DefinitionInitialProductStatus(RevisionBL.Instance.SelectedRevision, SelectedRouteMap);
            float newProductProgress = DefinitiolInitialProductProgress(newProductStatus);
            Operation newProductOperation = GetCurrentOperation();
            Product newProduct = Shaper.FormNewProduct(serialNumber: StartNumber,
                                            status: newProductStatus,
                                            progress: newProductProgress,
                                            letter: NewProductLetter,
                                            currentOperation: newProductOperation,
                                            revision: RevisionBL.Instance.SelectedRevision,
                                            routeMap: SelectedRouteMap,
                                            engineer: SelectedEngineer,
                                            technologist: SelectedTechnologist,
                                            workingGroup: SelectedWorkingGroup,
                                            description: NewProductDescription);

            MenuBL.Instance.DataPrepearingComplete();
            #endregion

            #region UpdateDB
            Product product = await SQLServer.Instance.Product.AddNewProductAsync(newProduct, (RevisionBL.Instance.SelectedRevision.RevisionBOMs1?.Any() ?? false));
            MenuBL.Instance.UpdateDbComplete();
            #endregion

            Products.Add(product);
            ClearProductProps();
        }
        public bool ShapeAndPreviewNewProducts()
        {
            #region Checks
            if (!int.TryParse(StartNumber, out int parsedSerialNumber)) //Входные проверки
            {
                Dialog.ShowError(TextHelper.SerialNumberParseException(StartNumber));
                return false;
            }

            if (!RevisionClaimCheck())
            {
                return false;
            }
            MenuBL.Instance.CheckComplete();
            #endregion

            #region Data_prepearing
            RevisionBL.Instance.SelectedRevisionRefreshQuery(); //Предотвращение бага с потерей навигационного свойства при добавлении изделий

            List<Product> newProducts = new List<Product>();
            List<string> newProductSerialNumbers = new List<string>();
            List<string> duplicateNumbers = new List<string>();
            ProductStatus newProductStatus = ProductStatusManager.DefinitionInitialProductStatus(RevisionBL.Instance.SelectedRevision, SelectedRouteMap);
            float newProductProgress = DefinitiolInitialProductProgress(newProductStatus);
            Operation newProductOperation = GetCurrentOperation();
            MenuBL.Instance.DataPrepearingComplete();
            #endregion

            for (int count = 0; count < NewProductQuantity; count++)
            {
                newProductSerialNumbers.Add((parsedSerialNumber + count).ToString()); //9-значное число склыдвается с 2-значным
            }

            duplicateNumbers = SQLServer.Instance.Product.GetDuplicateSerialNumbers(newProductSerialNumbers, RevisionBL.Instance.SelectedRevision.Id);
            duplicateNumbers.ForEach(duplicate => newProductSerialNumbers.Remove(duplicate));

            newProducts = newProductSerialNumbers.Select(serialNumber 
                => Shaper.FormNewProduct(   serialNumber: serialNumber,
                                            status: newProductStatus,
                                            progress: newProductProgress,
                                            letter: NewProductLetter,
                                            currentOperation: newProductOperation,
                                            revision: RevisionBL.Instance.SelectedRevision,
                                            routeMap: SelectedRouteMap,
                                            engineer: SelectedEngineer,
                                            technologist: SelectedTechnologist,
                                            workingGroup: SelectedWorkingGroup,
                                            description: NewProductDescription)).ToList();

            MenuBL.Instance.CalculateComplete();

            string duplicateNumbersOneLine = TextHelper.ListToString(duplicateNumbers);
            if (!string.IsNullOrEmpty(duplicateNumbersOneLine))
                Dialog.ShowWarning(TextHelper.NumbersIsADuplicate(duplicateNumbersOneLine));

            PreviewNewProducts.Clear();
            newProducts.ForEach(PreviewNewProducts.Add);

            MenuBL.Instance.PostUpdateChecksComplete();

            return true;
        }
        public async Task SavePreviewedProducts()
        {
            List<Product> newProducts = await SQLServer.Instance.Product.AddRangeProducts(PreviewNewProducts, (RevisionBL.Instance.SelectedRevision.RevisionBOMs1?.Any() ?? false));
            MenuBL.Instance.UpdateDbComplete();

            newProducts?.ForEach(Products.Add);

            MenuBL.Instance.UpdateOnScreenComplete();

            ClearProductProps();
        }
        private bool RevisionClaimCheck()
        {
            if (!RevisionBL.Instance.SelectedRevision.BomsIsClaimed)
            {
                Dialog.ShowWarning(TextHelper.CantAddProductWhileRevisionIstClaimed(RevisionBL.Instance.SelectedRevision.Name, AccessorLevelManager.GetAccessorLevel("CanClaimSlots")));
                return false;
            }

            return true;
        }
        #endregion

        #region DeleteProduct
        public void DeleteProduct()
		{
            ProductValidator validator = new ProductValidator(SelectedProduct);
            if (validator.IsValid)
            {
                Products.Remove(SQLServer.Instance.Product.RemoveProduct(SelectedProduct));
            }
            else
            {
                Dialog.ShowWarning(validator.ErrorMessages);
            }
		}
        #endregion

        public bool UpdateSelectedProduct()
		{
            bool needABindingRouteMap = SelectedProduct.RouteMapId != EditProductRouteMap?.Id;
            bool needBindingEmployees = SelectedProduct.EngineerId != EditProductEngineer?.Id || SelectedProduct.TechologistId != EditProductTechnologist?.Id;

            SelectedProduct.WorkingGroupId = EditProductWorkingGroup?.Id;
            SelectedProduct.RouteMapId = EditProductRouteMap?.Id;
            SelectedProduct.EngineerId = EditProductEngineer?.Id;
            SelectedProduct.TechologistId = EditProductTechnologist?.Id;

            SelectedProduct.UpdateDate = DateTime.Now;

			SelectedProduct = SQLServer.Instance.Product.UpdateProduct(SelectedProduct, ProductStatusManager.GetPrimitiveStatus("SaveProductChanges").Id, needABindingRouteMap, needBindingEmployees);
			ClearEditProductWindowProps();

			return SelectedProduct != null;
		}

        //Developer methods для закрытия годовых хвостов
        public async Task<bool> PseudoDispatchProductsAsync()
        {
            #region Data_prepearing
            List<Product> selectedProducts = MultiProductBL.Instance.ConvertIListToList(SelectedProducts);
            string oneLineIds = string.Join(",", selectedProducts.Select(p => p.Id));
            List<Product> productTrees = SQLServer.Instance.Product.GetProductTree(oneLineIds);
            string action = TextHelper.PseudoDispatch;

            ProductStatus newStatus = ProductStatusManager.GetPrimitiveStatus("PseudoDispatchProduct");

            MenuBL.Instance.DataPrepearingComplete();
            #endregion

            if (!await SQLServer.Instance.Product.PseudoDispatchProductsAsync(
                   productTrees,
                   newStatus,
                   Shaper.NewGroupLogs(selectedProducts, AuthorizationModel.Instance.AuthorizedUser, action)))
            {
                return false;
            }

            MenuBL.Instance.UpdateDbComplete();

            selectedProducts.ForEach(p => Products.Remove(p));
            string serialNumbers = string.Join(", ", selectedProducts.Select(product => product.SerialNumber));

            MenuBL.Instance.ProcessingComplete();

            Dialog.ShowOk(TextHelper.ProductsDispatchSuccessfulMessage(serialNumbers));

            return true;
        }

        public async Task<bool> DispatchProductsAsync(bool isPseudoDispatch = false)
        {
            #region Data_prepearing
            List<Product> selectedProducts = MultiProductBL.Instance.ConvertIListToList(SelectedProducts);
            string oneLineIds = string.Join(",", selectedProducts.Select(p => p.Id));
            int productCount = selectedProducts.Count();

            List<Product> productTrees = SQLServer.Instance.Product.GetProductTree(oneLineIds);
            
            List<OrderElement> orderElementsTree = new List<OrderElement>();
            ProductStatus newStatus = null;
            string action = "dispatch";

            if (isPseudoDispatch)
            {
                newStatus = ProductStatusManager.GetPrimitiveStatus("PseudoDispatchProduct");
                orderElementsTree = SQLServer.Instance.OrderElement.GetOrderElements(SelectedOrderElement.OrderTree.OrderId, SelectedOrderElement.Month, productTrees.Select(p => p.RevisionId).ToArray());
                action = TextHelper.PseudoDispatch;
            }
            else
            {
                newStatus = ProductStatusManager.GetPrimitiveStatus("DispatchProduct");
                orderElementsTree = SQLServer.Instance.OrderElement.GetOrderElements(SelectedOrderElement.OrderTree.OrderId, SelectedOrderElement.Month);
                action = TextHelper.Dispatch;
            }

            MenuBL.Instance.DataPrepearingComplete();
            #endregion

            #region Checks
            if (productTrees.Count / productCount != orderElementsTree.Count)
            {
                Dialog.ShowError(TextHelper.CantDispatchProductsMessage);
                return false;
            }
            if (productCount > (SelectedOrderElement.Quantity - SelectedOrderElement.CompletedQuantity))
            {
                Dialog.ShowError(TextHelper.DispatchCountErrorMessage((SelectedOrderElement.Quantity - SelectedOrderElement.CompletedQuantity).ToString(), productCount.ToString()));
                return false;
            }
            if (SelectedProduct.Revision.Model.HasLetter && selectedProducts.Any(product => product.Letter != SelectedOrderElement.OrderTree.Order.Letter))
            {
                Dialog.ShowError(TextHelper.DispatchLetterErrorMessage(SelectedOrderElement.OrderTree.Order.Letter));
                return false;
            }
            MenuBL.Instance.CheckComplete();
            #endregion
          
            if (!await SQLServer.Instance.Product.DispatchProductsAsync(
                    productTrees,
                    orderElementsTree,
                    newStatus,
                    Shaper.NewGroupLogs(selectedProducts,
                                        AuthorizationModel.Instance.AuthorizedUser,
                                        action)))
            {
                return false;              
            }

            MenuBL.Instance.UpdateDbComplete();

            selectedProducts.ForEach(p => Products.Remove(p));
            string serialNumbers = string.Join(", ", selectedProducts.Select(product => product.SerialNumber));

            MenuBL.Instance.ProcessingComplete();

            Dialog.ShowOk(TextHelper.ProductsDispatchSuccessfulMessage(serialNumbers));
            OrderBL.Instance.OrderElementsView.Refresh();

            return true;
        }

        public void ReclamationSelectedProduct()
        {
            int parentNewStatusId;
            List<Product> productsTree = new List<Product>();
            List<OrderElement> orderElementsTree = new List<OrderElement>();
            bool result = false;

            #region TruthTable
            /*
             * Result action        | hasOE     | isPseudo  |
             * -----------------------------------------------
             * ReclamationProduct   |   0       |   0       |
             * ReclamationProduct   |   0       |   1       |
             * return; (error)      |   1       |   0       |
             * ReclamationWithoutOE |   1       |   1       |
             */
            #endregion
            #region ReclamationWithoutOE
            if (ProductStatusManager.IsPseudoDispatched(SelectedProduct.ProductStatusId) && SelectedProduct.OrderElement is null)
            {
                productsTree = SQLServer.Instance.Product.GetProductTree(SelectedProduct.Id.ToString());

                result = SQLServer.Instance.Product.ReclamationProductsWithoutOrderElement(
                    SelectedProduct,
                    productsTree,
                    ProductStatusManager.GetPrimitiveStatus("ReclamationChildrens").Id,
                    ProductStatusManager.GetPrimitiveStatus("ReclamationParentProduct").Id,
                    Shaper.FormNewLog(
                        TextHelper.Reclamation,
                        SelectedProduct.Id,
                        productOperation: null,
                        AuthorizationModel.Instance.AuthorizedUser));

                if (result)
                {
                    Dialog.ShowOk(TextHelper.ProductMovedToAnotherTab(SelectedProduct.SerialNumber, TextHelper.InWorkTabName));
                    Products.Remove(SelectedProduct);
                }
                return; //exit method
            }
            #endregion

            #region ParamsCheck
            if (SelectedProduct.ProductStatus is null || SelectedProduct.OrderElement is null || SelectedProduct.OrderElement.OrderTree is null || SelectedProduct.Revision is null)
                return;
            #endregion

            #region Data_prepearing
            productsTree = SQLServer.Instance.Product.GetProductTree(SelectedProduct.Id.ToString());

            if (ProductStatusManager.IsPseudoDispatched(SelectedProduct.ProductStatusId))
            {
                orderElementsTree = SQLServer.Instance.OrderElement.GetOrderElements(SelectedProduct.OrderElement.OrderTree.OrderId, SelectedProduct.OrderElement.Month, productsTree.Select(p => p.RevisionId).ToArray());
                parentNewStatusId = ProductStatusManager.GetPrimitiveStatus("ReclamationParentProduct").Id;
            }
            else
            {
                orderElementsTree = SQLServer.Instance.OrderElement.GetOrderElements(SelectedProduct.OrderElement.OrderTree.OrderId, SelectedProduct.OrderElement.Month);
                parentNewStatusId = ProductStatusManager.GetPrimitiveStatus("ReclamationParentProduct").Id;
            }
            #endregion



            result = SQLServer.Instance.Product.ReclamationProduct(SelectedProduct,
                                                                    parentNewStatusId,
                                                                    ProductStatusManager.GetPrimitiveStatus("ReclamationChildrens").Id,
                                                                    productsTree,
                                                                    orderElementsTree,
                                                                    Shaper.FormNewLog(  TextHelper.Reclamation,
                                                                                        SelectedProduct.Id,
                                                                                        productOperation: null,
                                                                                        AuthorizationModel.Instance.AuthorizedUser));
            if (result)
            {
                Dialog.ShowOk(TextHelper.ProductMovedToAnotherTab(SelectedProduct.SerialNumber, TextHelper.InWorkTabName));
                Products.Remove(SelectedProduct);
            }
        }
        public void FavoriteUnfavoriteSelectedProduct()
        {
            SelectedProduct = SQLServer.Instance.Product.AddProductToFavorite(SelectedProduct, !SelectedProduct.IsFavorite);
        } 

        #region Reject/Unreject_or_Recycle
        public void RecycleSelectedProduct()
        {
            Product product = SQLServer.Instance.Product.EditProductStatus(SelectedProduct, ProductStatusManager.Recycled);

            if (product != null)
            {
                SelectedProduct = product;
            }
            SelectedProduct = null;
        }
        public void RejectSelectedProduct()
        {
            Product product = SQLServer.Instance.Product.EditProductStatus(SelectedProduct, ProductStatusManager.Rejected);

            if (product != null)
            {
                SelectedProduct = product;

                Dialog.ShowOk(TextHelper.ProductMovedToAnotherTab(SelectedProduct.SerialNumber, TextHelper.NotForWorkTabName));
                Products.Remove(SelectedProduct);
            }
        } //Изменить статус изделия на "нерабочий" (забракован/утилизирован)

		public void RejectModule(Product module)
		{
			if (SQLServer.Instance.Product.RejectModule(module, ProductStatusManager.GetPrimitiveStatus("RejectProduct").Id, AuthorizationModel.Instance.AuthorizedUser))
			{
                Dialog.ShowOk(TextHelper.ProductMovedToAnotherTab(module.SerialNumber, TextHelper.NotForWorkTabName));
				Products.Remove(module); //Удаление из текущего отображения
            }
        }
		public void RejectBody(Product complexProduct)
		{
			if (SQLServer.Instance.Product.RejectBody(complexProduct, ProductStatusManager.GetPrimitiveStatus("RejectProduct").Id, AuthorizationModel.Instance.AuthorizedUser))
			{
                Dialog.ShowOk(TextHelper.ProductMovedToAnotherTab(complexProduct.SerialNumber, TextHelper.NotForWorkTabName));
                Products.Remove(complexProduct); //Удаление из текущего отображения
            }
		}
        public void UnrejectProduct(Product product)
        {
            ProductStatus status = ProductStatusManager.DefenitionUnrejectedProductStatus(product, SQLServerHelper.ProductIsComplete(product), InstallationProductBL.Instance.InstallationCompletedCheck(product));
            if (SQLServer.Instance.Product.UnrejectProduct(product, status, AuthorizationModel.Instance.AuthorizedUser))
            {
                product.ProductStatusId = status.Id;

                Dialog.ShowOk(TextHelper.ProductMovedToAnotherTab(SelectedProduct.SerialNumber, TextHelper.InWorkTabName));
                Products.Remove(SelectedProduct);
            }
        }

        #endregion

        #region Formers
        public void FormEditProductWindow()
		{
			ClearEditProductWindowProps();

			EditProductWorkingGroup = SelectedProduct.WorkingGroup;
            EditProductRouteMap = SelectedProduct.RouteMap;

            EditProductEngineer = SelectedProduct.Engineer;
            EditProductTechnologist = SelectedProduct.Techologist;
        }
        #endregion
        #region ClearMethods
        public void ClearProductProps()
        {
            StartNumber = default;
            NewProductQuantity = 1;
            Employees.Clear();
            SelectedEngineer = null;
            SelectedTechnologist = null;
            NewProductDescription = default;
			NewProductLetter = null;
            SelectedOrderElement = null;
        }

		public void ClearEditProductWindowProps()
		{
            ClearEditProductWorkingGroup();
            ClearEditProductEngineer();
            ClearEditProductTechnologist();
            ClearEditProductRouteMap();
            Employees.Clear();
        }

        public void ClearSelectedWorkingGroup() => SelectedWorkingGroup = null;
		public void ClearSelectedEngineer() => SelectedEngineer = null;
		public void ClearSelectedTechnologist() => SelectedTechnologist = null;
        public void ClearSelectedRouteMap() => SelectedRouteMap = null;
		public void ClearEditProductWorkingGroup() => EditProductWorkingGroup = null;
        public void ClearEditProductEngineer() => EditProductEngineer = null;
        public void ClearEditProductTechnologist() => EditProductTechnologist = null;
		public void ClearEditProductRouteMap() => EditProductRouteMap = null;
        #endregion
        #region Definitions
   
        private float DefinitiolInitialProductProgress(ProductStatus initialStatus) => ProductStatusManager.IsReadyToInstallOrReadyToDispatch(initialStatus.Id) ? 1 : 0;    
        
        #endregion
        #region PreviewNewProductsWindow_methods
        public void ClearRouteMapForSelectedPreviewProduct(Product product)
        {
            product.RouteMapId = null;
            product.RouteMap = null;
        }
        public void DeleteSelectedPreviewedProduct(Product product)
        {
            PreviewNewProducts.Remove(product);
        }
        #endregion
        private Operation GetCurrentOperation()
		{
			if (SelectedRouteMap != null && SelectedRouteMap.Id != 0)			
				return SQLServer.Instance.RouteMapOperation.GetFirstOperation(SelectedRouteMap.Id).Operation;
			return CodeDictionary.Instance.AuxiliaryOperation.Equipment; // Default currentOperation for components
		}

		public bool CanOpenInstallationPage()
		{
            if (SelectedProduct.RouteMap != null)
			{
				RouteMapProductOperation equipment = SQLServer.Instance.RouteMapProductOperation.GetEquipmentOrNull(SelectedProduct.Id);
                ProductOperationBL.Instance.SelectedProductOperation = equipment;
                  
                return StartOrContinueEquipment(equipment);
            }
			return true;
		}
        private bool StartOrContinueEquipment(RouteMapProductOperation equipment)
        {
            if (equipment is null)           
                return true; //Для идзелия с .RouteMapId != null эта строчка кода должна быть недостижимой
            
            if (CheckBL.OperationReadyToStart(equipment))
            {
                return StartEquipment(equipment);
            }
            else
            {
                return CheckBL.EquipmentInProgressOrCompleted(equipment); 
                //true - continue equipment
                //false - для других статусов (equipment.OperationStatus) нельзя открывать страницу укомплектования
            }
        }
        private bool StartEquipment(RouteMapProductOperation equipment) => ProductOperationBL.Instance.StartEquipment(equipment, CodeDictionary.Instance.AuxiliaryOperationStatus.EquipmentInProgress); //Метод для лучшей читаемости кода
        #region Push_product
        public bool PushProductUntilReady(Product product)
        {
            bool installationIsCompleted = true;
            if (product.ProductBOMs?.Any() ?? false) // Проверяем заполнение слотов только в случае, когда слоты есть        
                installationIsCompleted = InstallationProductBL.Instance.InstallationCompletedCheck(product);

            if (SQLServer.Instance.Product.PushProductUntilReady(product, 
                                                                 ProductStatusManager.DefinitionPushedProductStatus(product, installationIsCompleted).Id,
                                                                 ProductOperationBL.Instance.CurrentProductOperationsList.ToList(),
                                                                 installationIsCompleted))
            {
                ProductOperationBL.Instance.CurrentProductOperationsList.Clear();
                ProductOperationBL.Instance.SelectedProductOperation = null;
                return true;
            }
            return false;
        } //push product, delete product operations
        public bool PushProductOperationsUntilReady(Product product) =>
            SQLServer.Instance.Product.PushProductOperationsUntilReady(product, ProductStatusManager.DefinitionCompleteProductStatus(product).Id, ProductOperationBL.Instance.CurrentProductOperationsList.ToList()); //push product with product operations
        #endregion


        #region GetOrRefreshProducts_methods
        public void InWorkProductsTabDataRefresh()
        {
            Products.Clear();
            UseCustomSort(Products, new OrderByFavoriteThenByProgressThenBySerialNumber());

            SQLServer.Instance.Product.GetProducts(RevisionBL.Instance.SelectedRevision.Id, ProductStatusManager.GetInWorkTabProductStatusIds()).ForEach(Products.Add);
        }

        public void InstalledProductsTabDataRefresh()
        {
            Products.Clear();
            UseCustomSort(Products, new OrderByFavoriteThenBySerialNumber());

            SQLServer.Instance.Product.GetProducts(RevisionBL.Instance.SelectedRevision.Id, ProductStatusManager.GetInstalledTabProductStatusIds()).ForEach(Products.Add);
        }

        public void LoadReadyToDispatchProducts()
        {
            Products.Clear();
            UseCustomSort(Products, new OrderByFavoriteThenBySerialNumber());

            SQLServer.Instance.Product
                .GetSortedByFavoriteAndNumberProducts(RevisionBL.Instance.SelectedRevision.Id, ProductStatusManager.GetProductStatusIdsForReadyToDispatchTab())
                    .ForEach(Products.Add);
        }
        public void LoadNotForWorkProducts()
        {
            Products.Clear();
            UseCustomSort(Products, new OrderByFavoriteThenBySerialNumber());

            SQLServer.Instance.Product
                .GetSortedByFavoriteAndNumberProducts(RevisionBL.Instance.SelectedRevision.Id, ProductStatusManager.GetProductStatusIdsForNotForWorkTab())
                    .ForEach(Products.Add);
        }
        public void LoadDispatchedProductsWithSelectedParameters()
        {
            Products.Clear();
            UseCustomSort(Products, new OrderByDispathedMonth());

            SQLServer.Instance.Product
               .GetDispatchedProducts(RevisionBL.Instance.SelectedRevision.Id, SelectedDispatchedDate.Year, ProductStatusManager.GetProductStatusIdsForDispatchTab())
                   .ForEach(Products.Add);
        }
        public void ShowPseudoDispatchedProducts()
        {
            Products.Clear();
            UseCustomSort(Products, new OrderByFavoriteThenBySerialNumber());

            SQLServer.Instance.Product
               .GetPseudoDispatchedProducts(RevisionBL.Instance.SelectedRevision.Id, ProductStatusManager.PseudoDispatched.Id)
                   .ForEach(Products.Add);
        }
        #endregion
        private void UseCustomSort(ICollection col, IComparer comparerRealization)
        {
            var dataView = (ListCollectionView)CollectionViewSource.GetDefaultView(col);
            dataView.CustomSort = comparerRealization;
            dataView.Refresh();
        }

        public bool PrepareLogDataForSelectedProduct()
        {
            ProductLogBL logBL = ProductLogBL.Instance;

            logBL.ProductLogs.Clear();
            SQLServer.Instance.ProductLog.GetCurrentProductLogs(SelectedProduct.Id).ForEach(logBL.ProductLogs.Add);

            logBL.ProductLogView = CollectionViewSource.GetDefaultView(logBL.ProductLogs);
            logBL.BindingLogFilters();

            return (logBL.ProductLogs?.Any() ?? false);
        }
        #region SearchProductByNumberWindow_methods
        public async Task GetProductsBySerialNumberAsync()
        {
            Products.Clear();
            List<Product> products = new List<Product>();
            if (IsAdvancedSearch)
            {
                products = await SQLServer.Instance.Product.GetProductsContainsSerialNumberAsync(GlobalSearchPrductBySerialNumber);
            }
            else
            {
                products = await SQLServer.Instance.Product.GetProductsBySerialNumberAsync(GlobalSearchPrductBySerialNumber);
            }      
            
            products.ForEach(Products.Add);          
        }
        #endregion
    }
}