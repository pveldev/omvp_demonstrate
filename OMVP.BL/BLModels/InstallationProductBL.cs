﻿using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Data;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.BLModels
{
    public class InstallationProductBL : BasicNotifyModel
    {
        public SQLServer SQLServer { get; set; }
        public Dictionary<int, Product> ChangedProductBoms { get; set; }
        public List<ProductBomLog> ProductBomLogs { get; set; } //Цикл жизни коллекции - от сохранения до сохранения
        public ObservableCollection<ProductBOM> ProductBOMList { get; set; } = new ObservableCollection<ProductBOM>();
        public Product SelectedProduct { get; set; } //Combobox selected
        public ProductBOM SelectedProductBom { get; set; }
        public string InstallationLogDescription { get; set; }
        public ProductBOM SelectedBomRow { get; set; }

        #region Singletion
        private static readonly Lazy<InstallationProductBL> _lazyModel = new Lazy<InstallationProductBL>(() => new InstallationProductBL());
        public static InstallationProductBL Instance => _lazyModel.Value;

        private InstallationProductBL()
        {

        }
		#endregion

		public bool SaveComposition(string navigationStack)
		{
            try
            {
                List<ProductBOM> changedSlots = new List<ProductBOM>();       
                List<Product> deinstalledProducts = new List<Product>();
                List<Product> installedProducts = new List<Product>();
                List<CalculatedProductInfo> productInfos = new List<CalculatedProductInfo>();

                foreach (KeyValuePair<int, Product> changedProductBom in ChangedProductBoms)
                {
                    ProductBOM productBom = ProductBOMList.Single(bom => bom.Id == changedProductBom.Key);
                    if (changedProductBom.Value == null) // Если это деинсталляция   
                    {
                        deinstalledProducts.Add(productBom.Product1); 
                        productInfos.Add(new CalculatedProductInfo(productBom.Product1.Id, ProductStatusManager.DefinitionDeInstalledProductStatus(productBom.Product1.ProductStatusId))); //Заполнение инфомарции со статусами для изделий
                    }
                    else // Если это установка   
                    {
                        installedProducts.Add(changedProductBom.Value); 
                        productInfos.Add(new CalculatedProductInfo(changedProductBom.Value.Id, ProductStatusManager.DefinitionInstalledProductStatus(changedProductBom.Value.ProductStatusId))); //Заполнение инфомарции со статусами для изделий
                    }

                    productBom.BOMProductId = changedProductBom.Value?.Id;
                    changedSlots.Add(productBom);
                }
                bool installationIsComplete = InstallationCompletedCheck(ProductBOMList);
                bool result = false;

                if (CheckBL.CurrentProductOperationsIsActualAndNotNull(ProductBL.Instance.SelectedProduct))
                {
                    ProductOperationBL productOperationBL = ProductOperationBL.Instance;                 
                    bool routeMapIsComplete = SQLServerHelper.ProductIsComplete(productOperationBL.CurrentProductOperationsList, productOperationBL.SelectedProductOperation);
                    Product parentProduct = changedSlots.First().Product; //Получение изделия-родителя из текущего контекста

                    result = SQLServer.Instance.ProductBom.SaveSlotChanges(
                        product: parentProduct,
                        installedProducts: installedProducts,
                        deinstalledProducts: deinstalledProducts,
                        productInfos: productInfos,
                        productBomLogs: ProductBomLogs,
                        currentProductOperations: productOperationBL.CurrentProductOperationsList,
                        selectedProductOperaion: productOperationBL.SelectedProductOperation,
                        installationIsCompleted: installationIsComplete,
                        newProgress: productOperationBL.CalculateAndIncrementProgressToEnd(),
                        newProductStatus: ProductStatusManager.DefinitionParentProductStatusAfterChangeSlots(
                            productOperationBL.SelectedProductOperation,
                            productOperationBL.SelectedProductOperation.Product.ProductStatusId,
                            installationIsComplete,
                            routeMapIsComplete),
                        log: Shaper.FormNewLog(
                            TextHelper.EndOperation(),
                            ProductBL.Instance.SelectedProduct.Id,
                            productOperationBL.SelectedProductOperation,
                            AuthorizationModel.Instance.AuthorizedUser));
                }
                else
                {
                    Product parentProduct = changedSlots.First().Product; //Получение изделия-родителя из текущего контекста

                    result = SQLServer.Instance.ProductBom.SaveSlotChanges(
                        installedProducts,
                        deinstalledProducts,
                        productInfos,
                        ProductBomLogs,
                        parentProduct,
                        installationIsComplete,
                        ProductStatusManager.DefinitionParentProductStatusAfterChangeSlots(
                            parentProduct, 
                            installationIsComplete),
                        Shaper.FormNewLog(
                            TextHelper.EndOperation(),
                            ProductBL.Instance.SelectedProduct.Id,
                            null,
                            AuthorizationModel.Instance.AuthorizedUser));
                }
                ResetProductBomLogProps();
                return result;

            }
            catch (Exception ex)
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLogWithCustomInner(ex, navigationStack));
                return false;
            }          
        }

        #region InstallationCompletedCheck
        public bool InstallationCompletedCheck(IEnumerable<ProductBOM> slots) => !slots.Any(bom => bom.BOMProductId is null);
        public bool InstallationCompletedCheck(Product product) => SQLServer.Instance.ProductBom.InstallationIsCompleted(product);
        #endregion

        public void ResetProductBomLogProps()
        {
            SelectedProduct = null;
            InstallationLogDescription = null;
            ChangedProductBoms = new Dictionary<int, Product>();
            ProductBomLogs = new List<ProductBomLog>();
            SelectedProductBom = null; //Trigger for MultiProductBomToCurtainVisibility & MultiProductBomToDescriptionVisibility (обновляется после обновления коллекций)
        }    

        public void UpdateChangeDictionary(ProductBOM bom, bool isInstallation, Product changedProduct)
        {
            int key = bom.Id;
            if (ChangedProductBoms.ContainsKey(key))
            {
                UpdateProductBomLogs(bom, isInstallation);
                ChangedProductBoms[key] = SelectedProduct;
            }
            else
            {
                ProductBomLogs.Add(Shaper.FormNewProductBomLog(bom, changedProduct, InstallationLogDescription, isInstallation));
                ChangedProductBoms.Add(key, SelectedProduct);
            }
            OnPropertyChanged("ChangedProductBoms");
        }

        private void UpdateProductBomLogs(ProductBOM bom, bool isInstallation)
        {
            int key = bom.Id;
            ProductBomLog productBomLog = ProductBomLogs.FirstOrDefault(bomLog => bomLog.ProductBomId == key);
            if (productBomLog != null)
            {
                int index = ProductBomLogs.IndexOf(productBomLog);
                ProductBomLogs[index] = Shaper.FormNewProductBomLog(bom, SelectedProduct, InstallationLogDescription, isInstallation);
            }
            else
                ProductBomLogs.Add(Shaper.FormNewProductBomLog(bom, SelectedProduct, InstallationLogDescription, isInstallation));
        }
    }
}
