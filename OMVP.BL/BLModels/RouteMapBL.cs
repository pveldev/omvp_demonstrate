﻿using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.BLModels
{
	public class RouteMapBL : BasicNotifyModel
	{
        public ObservableCollection<RouteMap> RouteMapList { get; set; } = new ObservableCollection<RouteMap>();
        public ObservableCollection<RouteMapOperation> RouteMapOperations { get; set; } = new ObservableCollection<RouteMapOperation>(); //Коллекция для просмотра выбранного маршрута
        public ObservableCollection<RouteMapOperation> NewRouteMapOperations { get; set; } = new ObservableCollection<RouteMapOperation>(); //Для создания нового маршрута
        public ObservableCollection<Operation> Operations { get; set; } = new ObservableCollection<Operation>();

		#region RouteMapProps
		public string NewRouteName { get; set; }

        private Revision _selectedRevision;
        public Revision SelectedRevision
        {
            get { return _selectedRevision; }
            set
            {
                _selectedRevision = value;
                if (_selectedRevision != null)
                {
                    Operations.Clear();
                    SQLServer.Instance.Operation.GetSortedNotArchivedOperations(_selectedRevision.Model).ForEach(Operations.Add);
                }
            }
        }

        #endregion
        #region RouteMapOperationProps
        public List<int> PriorityList { get; set; }
		public int SelectedPriority { get; set; }
		public Operation SelectedOperation { get; set; }
		private RouteMap _selectedRouteMap;
		public RouteMap SelectedRouteMap
		{
			get { return _selectedRouteMap; }
			set
			{
				_selectedRouteMap = value;
                if (_selectedRouteMap != null)
                { 
                    RouteMapOperations.Clear();
                    SQLServer.Instance.RouteMapOperation.GetRouteMapOperations(_selectedRouteMap.Id).ForEach(RouteMapOperations.Add);
				}
			}
		}
        #endregion

        #region NewOperation_props
        public string NewOperationName { get; set; }
		public string NewOperationAbbreviationName { get; set; }
		public Role NewOperationRole { get; set; }
        public WorkingGroup NewOperationWorkingGroup { get; set; }
        public string NewOperationRoutingNumber { get; set; }
		public string NewOperationDescription { get; set; }

        private Model _selectedModel;
        public Model NewOperationModel
        {
            get { return _selectedModel; }
            set
            {
                _selectedModel = value;
                if (_selectedModel != null)
                {
                    Operations.Clear();
                    SQLServer.Instance.Operation.GetSortedOperations(_selectedModel.Id).ForEach(Operations.Add);
                }
            }
        }
        #endregion

        #region Singleton 
        private static readonly Lazy<RouteMapBL> _lazyModel = new Lazy<RouteMapBL>(() => new RouteMapBL());
        public static RouteMapBL Instance => _lazyModel.Value;

        private RouteMapBL() { }
        #endregion

        #region RouteMapView.xaml
        public void AddOperationToRMOList()
		{
            NewRouteMapOperations.Add(Shaper.FormRouteMapOperation(SelectedOperation, (byte)SelectedPriority));
            RefreshPriorityList();

			SelectedPriority = PriorityList.First();
			SelectedOperation = null;
		}

        public void AddNewOperationToRouteMapOperations()
        {
            Operation operation = Shaper.FormNewOperation(NewOperationName, NewOperationAbbreviationName, SelectedRevision.Model, NewOperationRole, NewOperationWorkingGroup, NewOperationRoutingNumber, NewOperationDescription);

            NewRouteMapOperations.Add(Shaper.FormRouteMapOperation(operation, (byte)SelectedPriority));
            RefreshPriorityList();

            SelectedPriority = PriorityList.First();
            SelectedOperation = null;
        }

        public void SaveNewRouteMap()
		{
            RouteMap newRouteMap = SQLServer.Instance.RouteMap.SaveNewRouteMap(Shaper.FormNewRouteMap(NewRouteName, SelectedRevision), NewRouteMapOperations);

            if (newRouteMap != null)
                RouteMapList.Add(newRouteMap);
            else
                Dialog.ShowError(TextHelper.FailAddingRouteMap);
                                 
			ClearRouteMapOperationProps();
            ClearOperationProps();
        }
        public void DeleteSelectedRouteMap()
        {
            if (!CheckBL.RouteMapComplianceCheck(RouteMapOperations, SelectedRouteMap))
                RefreshRouteMapOperations();

            if (SQLServer.Instance.RouteMap.AmountOfUseRouteMap(SelectedRouteMap) != 0)
            {
                Dialog.ShowWarning(TextHelper.CantDeleteSelectedRouteMap);
                return;
            }

            if (SQLServer.Instance.RouteMap.DeleteRouteMap(SelectedRouteMap, RouteMapOperations))
            {
                RouteMapList.Remove(SelectedRouteMap);
                SelectedRouteMap = null;
            }
            else
            {
                Dialog.ShowError(TextHelper.FailDeletingRouteMap);
            }
        }

        private void RefreshRouteMapOperations()
        {
            RouteMapOperations.Clear();
            SQLServer.Instance.RouteMapOperation.GetRouteMapOperations(SelectedRouteMap.Id).ForEach(RouteMapOperations.Add);
        }

        public void ArchiveSelectedRouteMap()
		{
            SelectedRouteMap.IsArchive = !SelectedRouteMap.IsArchive;

            if (!SQLServer.Instance.RouteMap.ArchiveRouteMap(SelectedRouteMap)) 		
                SelectedRouteMap.IsArchive = !SelectedRouteMap.IsArchive; //Обработка ошибки изменения в БД		
        }

        public void ArchiveOperation(Operation operation)
        {
            operation.IsArchive = !operation.IsArchive;
            if (!SQLServer.Instance.ModifyEntity(operation)) //Обработка ошибки изменения в БД			
                operation.IsArchive = !operation.IsArchive;
        }
        #endregion

        public void AddNewOperation()
		{
			Operations.Add(SQLServer.Instance.AddEntityToDB(Shaper.FormNewOperation(NewOperationName, NewOperationAbbreviationName, NewOperationModel, NewOperationRole, NewOperationWorkingGroup, NewOperationRoutingNumber, NewOperationDescription)));
			ClearOperationProps();
		}

        public void ClaimSelectedRouteMap()
        {
            SelectedRouteMap.IsClaimed = true;
            if (!SQLServer.Instance.ModifyEntity(SelectedRouteMap))
                SelectedRouteMap.IsClaimed = false;
        }

        public void LoadGeneralOperations()
        {
            Operations.Clear();
            SQLServer.Instance.Operation.GetSortedGeneralOperations().ForEach(Operations.Add);
        }

        public void RefreshPriorityList()
		{
			int count = NewRouteMapOperations.Count();
            if (count == 0)
                ResetPriorityList();
            if (count == 1)
				PriorityList = new List<int> { 1, 2 };
			if (count >= 2)
			{
				int priority = NewRouteMapOperations.Last().Priority;
				PriorityList = new List<int> { priority, ++priority };
			}
		}
        private void ResetPriorityList()
        {
            PriorityList = new List<int> { 1 };
        }
        #region Clear_methods
        private void ClearOperationProps()
		{
            ClearNewOperationName();
            ClearNewOperationAbbreviationName();
            ClearNewOperationRoutingNumber();
            ClearNewOperationDescription();
            ClearNewOperationRole();
        }
        public void ClearNewOperationName() => NewOperationName = default;
        public void ClearNewOperationAbbreviationName() => NewOperationAbbreviationName = default;
        public void ClearNewOperationModel() => NewOperationModel = default;
        public void ClearNewOperationRole() => NewOperationRole = default;
        public void ClearNewOperationWorkingGroup() => NewOperationWorkingGroup = default;
        public void ClearNewOperationRoutingNumber() => NewOperationRoutingNumber = default;
        public void ClearNewOperationDescription() => NewOperationDescription = default;


		private void ClearRouteMapOperationProps()
		{
            NewRouteName = "";
            SelectedRevision = null;

            PriorityList = null;
			SelectedRouteMap = null;
            NewRouteMapOperations.Clear();
            ResetPriorityList();
        }
        #endregion

	}
}
