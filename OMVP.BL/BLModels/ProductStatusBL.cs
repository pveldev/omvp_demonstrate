﻿using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.BLModels
{
	public class ProductStatusBL : BasicNotifyModel
	{
		public SQLServer SQLServer { get; set; }
        public ObservableCollection<ProductStatus> ProductStatusList { get; set; } = new ObservableCollection<ProductStatus>();

		public string NewProductStatusName { get; set; }

        #region Singleton
        private static readonly Lazy<ProductStatusBL> _lazyModel = new Lazy<ProductStatusBL>(() => new ProductStatusBL());
        public static ProductStatusBL Instance => _lazyModel.Value;
        private ProductStatusBL() { }
		#endregion

		public void AddProductStatus()
		{
            ProductStatusList.Add(SQLServer.Instance.AddEntityToDB(Shaper.FormNewProductStatus(NewProductStatusName)));
			ClearProps();
		}
	
		private void ClearProps()
		{
			NewProductStatusName = default;
		}
	}
}