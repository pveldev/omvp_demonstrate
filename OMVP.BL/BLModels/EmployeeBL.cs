﻿using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.BLModels
{
	public class EmployeeBL : BasicNotifyModel
	{
		public SQLServer SQLServer { get; set; }
		public RevisionBL RevisionBL { get; set; } = RevisionBL.Instance;

        #region EmployeeTable_props
        public ObservableCollection<Employee> EmployeeList { get; set; } = new ObservableCollection<Employee>();
		public Employee SelectedEmployee { get; set; }

		public string NewEmployeeFirstName { get; set; }
		public string NewEmployeeLastName { get; set; }
		public Role SelectedRole { get; set; }
        #endregion

        #region WorkingGroup_props
        public ObservableCollection<WorkingGroup> AllWorkingGroups { get; set; } = new ObservableCollection<WorkingGroup>(); //вкладка "Рабочие группы"
        public ObservableCollection<WorkingGroup> NotArchiveWorkingGroups { get; set; } = new ObservableCollection<WorkingGroup>(); //вкладка "Сотрудники рабочих групп? Связь групп и моделей"
        public ObservableCollection<ModelXWorkingGroup> ModelsXWorkingGroups { get; set; } = new ObservableCollection<ModelXWorkingGroup>(); //вкладка "Связь групп и моделей"
        public ModelXWorkingGroup SelectedModelXWorkingGroupRelation { get; set; }//вкладка "Связь групп и моделей"
        public Model SelectedModel { get; set; }
        public string NewWorkingGroupName { get; set; }
        #endregion

        #region WorkingGroupEmployee_props

        public ObservableCollection<WorkingGroupEmployee> WorkingGroupEmployeeList { get; set; } = new ObservableCollection<WorkingGroupEmployee>();
		private WorkingGroup _selectedWorkingGroup;
		public WorkingGroup SelectedWorkingGroup
		{
			get => _selectedWorkingGroup;
			set
			{
				_selectedWorkingGroup = value;
				if (_selectedWorkingGroup != null)
                {
                    WorkingGroupEmployeeList.Clear();
                    SQLServer.Instance.WorkingGroupEmployee.GetSortedWorkingGroupEmployees(_selectedWorkingGroup.Id).ForEach(WorkingGroupEmployeeList.Add);
                }
			}
		}
        public WorkingGroupEmployee SelectedWorkingGroupEmployee { get; set; }
        #endregion

        #region Singletion
        private static readonly Lazy<EmployeeBL> _lazyModel = new Lazy<EmployeeBL>(() => new EmployeeBL());
        public static EmployeeBL Instance => _lazyModel.Value;

        private EmployeeBL()
		{

		}
		#endregion

		public void AddEmployee()
		{
            if (NewEmployeeIsDuplicate())
            {
                Dialog.ShowWarning(TextHelper.EmployeeIsADuplicate(NewEmployeeFirstName, NewEmployeeLastName));
                return;
            }

            EmployeeList.Add(SQLServer.Instance.AddEntityToDB(Shaper.FormNewEmployee(NewEmployeeFirstName, NewEmployeeLastName, SelectedRole)));
            ClearEmployeeProps();
		}

        private bool NewEmployeeIsDuplicate() 
            => EmployeeList.Any(employee => employee.LastName == NewEmployeeLastName && employee.FirstName == NewEmployeeFirstName);
        

        public void RemoveSelectedEmployeeFromSelectedGroup()
        {
            WorkingGroupEmployee removedWorkingGroupEmployee = SQLServer.Instance.WorkingGroupEmployee.RemoveEmployeeFromWorkingGroup(SelectedWorkingGroupEmployee);

            if (removedWorkingGroupEmployee != null)           
                WorkingGroupEmployeeList.Remove(removedWorkingGroupEmployee);          
            else           
                Dialog.ShowError(TextHelper.FailRemovingEmployeeFromGroup);

            SelectedWorkingGroupEmployee = null;
        }

        public void ArchiveEmployee(Employee selectedEmployee)
		{
			if (!SQLServer.Instance.Employee.ArchiveEmployee(selectedEmployee)) //Обработка ошибки изменения в БД
				selectedEmployee.IsArchive = !selectedEmployee.IsArchive;
		}

		public void ArchiveWorkingGroup(WorkingGroup selectedWorkingGroup)
		{
			if (SQLServer.Instance.WorkingGroup.ArchiveWorkingGroup(selectedWorkingGroup)) //Обработка ошибки изменения в БД
			{
				if (selectedWorkingGroup.IsArchive)
					NotArchiveWorkingGroups.Remove(selectedWorkingGroup);
				else NotArchiveWorkingGroups.Add(selectedWorkingGroup);	
			}
			else selectedWorkingGroup.IsArchive = !selectedWorkingGroup.IsArchive;
		}

		public void AddEmployeeToWorkingGroupEmployee()
		{
            if (!WorkingGroupEmployeeList.Any(wgEmployee => wgEmployee.EmployeeId == SelectedEmployee.Id))
            {
			    WorkingGroupEmployeeList.Add(Shaper.FormWorkingGroupEmployee(SelectedWorkingGroup, SelectedEmployee));
			    SelectedEmployee = null;                
            }
            else
            {
                Dialog.ShowWarning(TextHelper.CantAddEmployeeToSelectedGroup(SelectedEmployee.FirstName, SelectedEmployee.LastName));
            }
		}

		public void SaveWorkingGroupEmployee()
		{
            if (SQLServer.Instance.InsertAndReturnBool(WorkingGroupEmployeeList))
            {
                Dialog.ShowOk("Успешно!");
            }
            else
            {
                Dialog.ShowError("Ошибка при сохарении!");
            }

            ClearWorkingGroupEmployeeProps();
		}

		public void CreateNewWorkingGroup()
		{
            WorkingGroup newWorkingGroup = SQLServer.Instance.AddEntityToDB(Shaper.FormNewWorkingGroup(NewWorkingGroupName));
            if (newWorkingGroup != null)
            {
                AllWorkingGroups.Add(newWorkingGroup);
                NotArchiveWorkingGroups.Add(newWorkingGroup);
                NewWorkingGroupName = null;
            }		
		}
        public void SaveModelPlusWorkingGroupComposition()
        {
            ModelXWorkingGroup newModelXWorkingGroup = Shaper.FormNewModelXWorkingGroup(SelectedWorkingGroup, SelectedModel);
            if (!CheckBL.ModelXWorkingGroupIsDuplicate(ModelsXWorkingGroups, newModelXWorkingGroup))
            {
                ModelsXWorkingGroups.Add(SQLServer.Instance.AddEntityToDB(newModelXWorkingGroup));
                ClearModelXWorkingGroupProps();
            }
            else
            {
                Dialog.ShowError(TextHelper.ModelXWorkingGroupIsDuplicate);
            }
        }

        public void DeleteModelXWorkingGroupRelation()
        {
            if (SQLServer.Instance.Remove(SelectedModelXWorkingGroupRelation))
            {
                ModelsXWorkingGroups.Remove(SelectedModelXWorkingGroupRelation);
                SelectedModelXWorkingGroupRelation = null;
            } else
            {
                Dialog.ShowError(TextHelper.FailRemovingModelXWorkingGroupRelation);
            }
        }

        #region Clear_methods
        private void ClearModelXWorkingGroupProps()
        {
            SelectedModel = null;
            SelectedWorkingGroup = null;
        }
        private void ClearEmployeeProps()
		{
			NewEmployeeFirstName = "";
			NewEmployeeLastName = "";
			SelectedRole = null;
		}
		private void ClearWorkingGroupEmployeeProps()
		{
			SelectedEmployee = null;
			//SelectedModel = null;
		}
        #endregion
    }
}
