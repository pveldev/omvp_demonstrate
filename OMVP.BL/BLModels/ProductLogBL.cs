﻿using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.BLModels
{
	public class ProductLogBL : BasicNotifyModel
	{
        public ObservableCollection<ProductLog> ProductLogs { get; set; } = new ObservableCollection<ProductLog>(); //ProductLogView
		public ICollectionView ProductLogView { get; set; }

        public ProductLog SelectedProductLog { get; set; }
        
        #region ProductLogParameters_props
        public Revision SelectedRevision { get; set; }
        public User SelectedUser { get; set; }
        public DateTime StartDate { get; set; } = OmvpConstant.DefaultStartDateParameter;
        public DateTime StopDate { get; set; } = OmvpConstant.DefaultEndDateParameter;
        #endregion

        #region Filters
        private string _numberSearchBar;
		public string NumberSearchBar
		{
			get { return _numberSearchBar; }
			set
			{
				_numberSearchBar = value;
				ProductLogView.Refresh();
			}
		}


		private string _revisionSearchBar;
		public string RevisionSearchBar
		{
			get { return _revisionSearchBar; }
			set
			{
				_revisionSearchBar = value.ToLower();
				ProductLogView.Refresh();
			}
		}


		private string _operationExecuterSearchBar;
		public string OperationExecuterSearchBar
		{
			get { return _operationExecuterSearchBar; }
			set
			{
				_operationExecuterSearchBar = value.ToLower();
				ProductLogView.Refresh();
			}
		}


		private string _letterSearchBar;
		public string LetterSearchBar
		{
			get { return _letterSearchBar; }
			set
			{
				_letterSearchBar = value;
				ProductLogView.Refresh();
			}
		}


		private string _productOperationSearchBar;
		public string ProductOperationSearchBar
		{
			get { return _productOperationSearchBar; }
			set
			{
                _productOperationSearchBar = value.ToLower();
				ProductLogView.Refresh();
			}
		}


		private string _dateSearchBar;
		public string DateSearchBar
		{
			get { return _dateSearchBar; }
			set
			{
				_dateSearchBar = value;
				ProductLogView.Refresh();
			}
		}


		private string _pcNameSearchBar;
		public string PCNameSearchBar
		{
			get { return _pcNameSearchBar; }
			set
			{
				_pcNameSearchBar = value.ToLower();
				ProductLogView.Refresh();
			}
		}


		private string _descriptionSearchBar;
		public string DescriptionSearchBar
		{
			get { return _descriptionSearchBar; }
			set
			{
				_descriptionSearchBar = value.ToLower();
				ProductLogView.Refresh();
			}
		}

        #endregion

        #region Singleton
        private static readonly Lazy<ProductLogBL> _lazyModel = new Lazy<ProductLogBL>(() => new ProductLogBL());
        public static ProductLogBL Instance => _lazyModel.Value;
        private ProductLogBL() { }
		#endregion

        public void DeleteSelectedProductLog()
        {
            ProductLogs.Remove(SQLServer.Instance.ProductLog.RemoveProductLog(SelectedProductLog));
            ProductLogView.Refresh();
        }
        public void GetProductLogsWithParameters()
        {
            ProductLogs.Clear();
            SQLServer.Instance.ProductLog.GetProductLogs(SelectedRevision, SelectedUser, StartDate, StopDate).ForEach(ProductLogs.Add);

            ClearProductLogParametersProps();
        }


        #region ProductLog_filters
        public void BindingLogFilters()
        {
            ProductLogView.Filter = item =>
            {
                var productLog = item as ProductLog;
                var product = productLog.Product;
                var operationExecuter = productLog.OperationExecuter;

                return IsNotNullAndContains(NumberSearchBar, product.SerialNumber)
                        && IsNotNullAndContains(RevisionSearchBar, product.Revision.Name)
                        && IsNotNullAndContains(OperationExecuterSearchBar, operationExecuter)
                        && IsNotNullAndContains(LetterSearchBar, product.Letter)
                        && IsNotNullAndContains(ProductOperationSearchBar, productLog)
                        && IsNotNullAndContains(DateSearchBar, productLog.Date.ToString())
                        && IsNotNullAndContains(PCNameSearchBar, productLog.User?.Employee);
            };
        }

        private bool IsNotNullAndContains(string searchBarValue, string productLogValue) => string.IsNullOrEmpty(searchBarValue) || productLogValue.ContainsAsLower(searchBarValue);
        private bool IsNotNullAndContains(string searchBarValue, Employee operationExecuter)
            => string.IsNullOrEmpty(searchBarValue) || (operationExecuter?.FirstName.ContainsAsLower(searchBarValue) ?? false) || (operationExecuter?.LastName.ContainsAsLower(searchBarValue) ?? false);
        private bool IsNotNullAndContains(string searchBarValue, ProductLog log)
            => string.IsNullOrEmpty(searchBarValue) || log == null || log.ProductOperation == null || log.ProductOperation.Operation == null || log.ProductOperation.Operation.Name.ContainsAsLower(searchBarValue);

        #endregion
        #region Clear_methods
        private void ClearProductLogParametersProps()
        {
            ClearSelectedRevision();
            ClearSelectedUser();
            ClearStartDate();
            ClearStopDate();
        }
        public void ClearSelectedRevision() => SelectedRevision = default;    
        public void ClearSelectedUser() => SelectedUser = default;      
        public void ClearStartDate() => StartDate = OmvpConstant.DefaultStartDateParameter;        
        public void ClearStopDate() => StopDate = OmvpConstant.DefaultEndDateParameter;       
        #endregion
    }
}
