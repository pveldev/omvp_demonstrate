﻿using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.BLModels
{
    public class ProductParametersBL : BasicNotifyModel
    {
        public ObservableCollection<ProductParameter> CurrentProductParameters { get; set; } = new ObservableCollection<ProductParameter>();

        #region ProductParametersView_Buffer_props
        public Product SelectedProduct { get; set; }
        public ModelParameter SelectedModelParameter { get; set; }
        public string NewProductParameterValue { get; set; }
        public string NewProductParameterDescription { get; set; }
        #endregion

        #region EditProductParam_props
        public ProductParameter SelectedProductParameter { get; set; }
        public ProductParameter EditableProductParameter { get; set; }
        #endregion

        #region Singleton
        private static readonly Lazy<ProductParametersBL> _lazyModel = new Lazy<ProductParametersBL>(() => new ProductParametersBL());
        public static ProductParametersBL Instance => _lazyModel.Value;
        private ProductParametersBL() { }
        #endregion

        #region ProductParamView_methods
        public async Task AddNewParameterToProduct()
        {
            if (NewProductParameterIsDuplicate())
            {
                Dialog.ShowWarning(TextHelper.ParamIsDuplicateWarningMessage);
                return;
            }

            ProductParameter result = await SQLServer.Instance.ProductParameter.AddNewProductParameterAsync(
                Shaper.FormProductParameter(
                    productId: SelectedProduct.Id,
                    modelParamId: SelectedModelParameter.Id,
                    value: NewProductParameterValue,
                    description: NewProductParameterDescription));

            if (result is null)            
                return;            

            CurrentProductParameters.Add(result);
            ClearProductParamBufferProps();
        }
        public async Task RemoveSelectedProductParameter() {
            if (await SQLServer.Instance.ProductParameter.RemoveProductParameter(SelectedProductParameter))
            {
                CurrentProductParameters.Remove(SelectedProductParameter);
                SelectedProductParameter = null;
            }
        }

        
        public void ClearProductParamBufferProps()
        {
            SelectedModelParameter = default;
            NewProductParameterValue = default;
            NewProductParameterDescription = default;
        }
        private bool NewProductParameterIsDuplicate() => CurrentProductParameters.Any(param => param.ModelParameterId == SelectedModelParameter.Id);
        #endregion

        #region EditProductParamWindow_methods
        public void ProductParamSaveChanges()
        {
            SelectedProductParameter.Value = EditableProductParameter.Value;
            SelectedProductParameter.Description = EditableProductParameter.Description;

            SQLServer.Instance.ProductParameter.ProductParameterSaveChanges(SelectedProductParameter);
            ClearEditableProductParamsProps();
        }
        public void ClearEditableProductParamsProps()
        {
            SelectedProductParameter = default;
            EditableProductParameter = default;
        }
        #endregion
    }
}
