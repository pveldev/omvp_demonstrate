﻿using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Collections.ObjectModel;

namespace OMVP.BL.BLModels
{
    public class UserAccessorLevelBL : BasicNotifyModel
	{
        public ObservableCollection<User> Users { get; set; } = new ObservableCollection<User>();//UserAccessorLevelView, ProductLogParameters

        public Employee SelectedEmployee { get; set; }

		private User _selectedUser;
		public User SelectedUser
		{
			get { return _selectedUser; }
			set
			{
				_selectedUser = value;
				if (_selectedUser != null)
				{
					NewAccessorLevel = _selectedUser.AccessorLevel;
					SelectedEmployee = _selectedUser.Employee;
				}
			}
		}

		private int _newAccessorLevel;
		public int NewAccessorLevel
		{
			get { return _newAccessorLevel; }
			set
			{
				if (value <= 0) value = 1;
				else if (value >= 7) value = 6;

				_newAccessorLevel = value;
			}
		}

        #region Singleton
        private static readonly Lazy<UserAccessorLevelBL> _lazyModel = new Lazy<UserAccessorLevelBL>(() => new UserAccessorLevelBL());
        public static UserAccessorLevelBL Instance => _lazyModel.Value;
        private UserAccessorLevelBL() { }
		#endregion

		public void UpdateUser()
		{
			if (SelectedUser.AccessorLevel != NewAccessorLevel || SelectedUser.Employee != SelectedEmployee)			
				SelectedUser = SQLServer.Instance.User.UpdateUser(SelectedUser, SelectedEmployee, NewAccessorLevel);
			
			ClearProps();
		}

		private void ClearProps()
		{
			NewAccessorLevel = default;
			SelectedEmployee = null;
            SelectedUser = null;
		}
	}
}
