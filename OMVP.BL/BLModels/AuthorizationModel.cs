﻿using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.BLModels
{
	public class AuthorizationModel : BasicNotifyModel
    {
		public User AuthorizedUser { get; set; }
		public string LoginName { get; set; } = Environment.UserName;
		public bool IsVisibleAuthView { get; set; } = true;
		public string SelectedConnection { get; set; }
        public string FailConnectionMessage { get; set; } = TextHelper.BaseFailConnectionMessage();
		public bool IsEnabledConnection { get; set; }

        #region Singleton
        private static readonly Lazy<AuthorizationModel> _lazyModel = new Lazy<AuthorizationModel>(() => new AuthorizationModel());
        public static AuthorizationModel Instance => _lazyModel.Value;
	
		private AuthorizationModel() { }
		#endregion

		public User AddNewUser(User user) => SQLServer.Instance.AddEntityToDB(user);
        public void LoginRefresh() => LoginName = Environment.UserName;
    }
}