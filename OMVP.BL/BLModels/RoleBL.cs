﻿using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.BLModels
{
	public class RoleBL : BasicNotifyModel
	{
        public ObservableCollection<Role> Roles { get; set; } = new ObservableCollection<Role>();

		public string NewRoleName { get; set; }

        #region Singleton
        private static readonly Lazy<RoleBL> _lazyModel = new Lazy<RoleBL>(() => new RoleBL());
        public static RoleBL Instance => _lazyModel.Value;

        private RoleBL() { }
		#endregion

		public void AddRole()
		{
			Roles.Add(SQLServer.Instance.AddEntityToDB(Shaper.FormNewRole(NewRoleName)));
			ClearProps();
		}

		private void ClearProps()
		{
			NewRoleName = default;
		}
	}
}
