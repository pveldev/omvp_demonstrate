﻿using OMVP.BL.CoreBL;
using OMVP.BL.Models;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.BLModels
{
    public class RevisionBL : BasicNotifyModel
    {
        public ObservableCollection<Revision> Revisions { get; set; } = new ObservableCollection<Revision>(); //Общая коллекция для RevisionBOMView, RouteMapView, OrderView, RevisionView, RevisionNavigationView
        public ObservableCollection<ModelType> ModelTypeList { get; set; } = new ObservableCollection<ModelType>();

        #region ExcelExport_Window_Props
        public bool FullExport { get; set; }
        public OrderElement SelectedOrderElement { get; set; }
        public int[] DispatchYears { get; set; }
        private int? _selectedYear;
        public int? SelectedYear
        {
            get { return _selectedYear; }
            set 
            { 
                _selectedYear = value;
                OrderBL.Instance.OrderElementsView?.Refresh();
            }
        }

        #endregion

        #region (2)_Resivion_props
        public string NewRevisionName { get; set; }
        public Model NewRevisionModel { get; set; }
        #endregion

        public Revision SelectedRevision { get; set; }
        #region Singleton
        private static readonly Lazy<RevisionBL> _lazyModel = new Lazy<RevisionBL>(() => new RevisionBL());
        public static RevisionBL Instance => _lazyModel.Value;
        private RevisionBL() { }
        #endregion

        public void AddRevision()
        {
			Revisions.Add(SQLServer.Instance.AddEntityToDB(Shaper.FormNewRevision(NewRevisionModel, NewRevisionName)));
            ClearRevisionProps();
        }

		public bool ArchiveSelectedRevision()
		{
            SelectedRevision.IsArchive = !SelectedRevision.IsArchive;
            if (!SQLServer.Instance.ModifyEntity(SelectedRevision))
            {
                SelectedRevision.IsArchive = !SelectedRevision.IsArchive;
                return false;
            }
            return true;
		}

        public bool DeleteSelectedRevision()
        {
            bool resul = SQLServer.Instance.Revision.DeleteRevision(SelectedRevision);
            if (resul)
            {
                Revisions.Remove(SelectedRevision);
                SelectedRevision = null;
            }
            return resul;
        }
            
        public void ClaimSelectedRevision()
        {
            Revision revision = SQLServer.Instance.Revision.ClaimRevision(SelectedRevision);
            if (revision != null)
            {
                SelectedRevision = revision;
            }
            else
            {
                Dialog.ShowError(TextHelper.ClaimRevisionDbTransactionExCatched);
            }
        }

        #region Clear_methods
        private void ClearRevisionProps()
        {
            NewRevisionName = "";
            NewRevisionModel = null;
        }

        public void ClearExcelExportProps()
        {
            SelectedYear = DateTime.Now.Year;
            FullExport = false;
            SelectedOrderElement = null;
        }
        #endregion

        public string GetCurrentPageTitle() => TextHelper.CurrentModelPageTitle(SelectedRevision.Model.ModelType.Name, SelectedRevision.Name);	
        public void SelectedRevisionRefreshQuery() => SelectedRevision = SQLServer.Instance.Revision.GetRevision(SelectedRevision.Id);
    }
}
