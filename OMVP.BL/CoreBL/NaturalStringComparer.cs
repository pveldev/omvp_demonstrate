﻿using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.CoreBL
{
    internal sealed class OrderByFavoriteThenByProgressThenBySerialNumber : NaturalStringCompareHelper, IComparer
    {
        //return -1 -> выше в списке
        //return  1 -> ниже в писке
        public int Compare(object x, object y)
        {
            Product leftProduct = (Product)x;
            Product rightProduct = (Product)y;

            string leftProductNumber = leftProduct.SerialNumber;
            string rightProductNumber = rightProduct.SerialNumber;

            if (leftProduct.IsFavorite == rightProduct.IsFavorite)
            {
                return leftProduct.Progress == rightProduct.Progress ?
                   CompareAsInt(NormalizeSerialNumber(leftProductNumber), NormalizeSerialNumber(rightProductNumber))
                   :
                   leftProduct.Progress > rightProduct.Progress ? -1 : 1;
            }
            else
            {
                return leftProduct.IsFavorite ? -1 : 1;
            }
        }
    }

    internal sealed class OrderByFavoriteThenBySerialNumber : NaturalStringCompareHelper, IComparer
    {
        //return -1 -> выше в списке
        //return  1 -> ниже в писке
        public int Compare(object x, object y)
        {
            Product leftProduct = (Product)x;
            Product rightProduct = (Product)y;

            string leftProductNumber = leftProduct.SerialNumber;
            string rightProductNumber = rightProduct.SerialNumber;

            if (leftProduct.IsFavorite == rightProduct.IsFavorite)
            {
                return CompareAsInt(NormalizeSerialNumber(leftProductNumber), NormalizeSerialNumber(rightProductNumber));                
            }
            else
            {
                return leftProduct.IsFavorite ? -1 : 1;
            }
        }
    }
    internal sealed class OrderByDispathedMonth : IComparer
    {
        //return -1 -> выше в списке
        //return  1 -> ниже в писке
        public int Compare(object x, object y)
        {
            Product leftProduct = (Product)x;
            Product rightProduct = (Product)y;

            return leftProduct.OrderElement.Month > rightProduct.OrderElement.Month ? 1 : -1;
        }
    }
}