﻿using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.CoreBL
{
    public static class UsersToStringExtension
    {
        public static string ToStringColumn(this IEnumerable<User> input) => string.Join("\n", input.Select(user =>
                string.Concat(
                "[",
                user.Employee?.LastName,
                " ",
                user.Employee?.FirstName,
                $" ({user.PCName})]")));
    }
}
