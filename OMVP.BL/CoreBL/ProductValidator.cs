﻿using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.CoreBL
{
    public class ProductValidator
    {
        public bool IsValid { get; set; }
        public string ErrorMessages { get; set; }
        public Product SelectedProduct { get; set; }

        public ProductValidator(Product product)
        {
            SelectedProduct = product;
            IsValid = ProductCanBeDeleted();
        }

        private bool ProductCanBeDeleted()
        {
            string[] messages = { ProductIsNotDispatched(SelectedProduct), ProductSlotsIsEmpty(SelectedProduct), ProductNotBindingToAnySlot(SelectedProduct), LogIsEmpty(SelectedProduct), BomLogIsEmpty(SelectedProduct) };
            ErrorMessages = string.Join("\n  ", messages.Where(elem => elem != "" && elem != null));

            return string.IsNullOrEmpty(ErrorMessages);
        }

        private string ProductSlotsIsEmpty(Product product)
        {
            if (product.ProductBOMs.Any(bom => bom.BOMProductId != null))
                return TextHelper.SlotsIsNotEmpty;

            return "";
        }
        private string ProductIsNotDispatched(Product product)
        {
            if (product.OrderElement != null)
                return TextHelper.ProductBindedToOrderAndCantbeDeleted(product.OrderElement.OrderTree.Order.OrderCode);

            return "";
        }
        private string ProductNotBindingToAnySlot(Product product)
        {
            if (product.ProductBOMs1.Count == 0)
            {
                return "";
            }
            else
            {
                ProductBOM bom = SelectedProduct.ProductBOMs1.First();
                return TextHelper.ProductBindedToSlotAndCantbeDeleted(bom.Product.Revision.Name, bom.Product.SerialNumber);
            }
        }
        private string LogIsEmpty(Product product)
        {
            if (product.ProductLogs.Count == 0)
                return "";
            else
                return TextHelper.ProductHaveStoryAndCantbeDeleted(product.ProductLogs.Count);
        }
        private string BomLogIsEmpty(Product product)
        {
            if(product.ProductBomLogs.Count == 0)
            {
                return "";
            }
            else
            {
                return TextHelper.ProductHaveBomStoryAndCantbeDeleted(  SQLServer.Instance.ProductBomLog.GetBomLogsCountByParentProduct(product.Id), 
                                                                        SQLServer.Instance.ProductBomLog.GetBomLogsCountByBomProduct(product.Id));
            }
        }
    }
}