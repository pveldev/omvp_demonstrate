﻿using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.CoreBL
{
    public static class ProductStatusManager
    {
        #region Cols
        private static readonly Dictionary<string, ProductStatus> _primitiveStatuses = new Dictionary<string, ProductStatus>();
        private static readonly List<ProductStatus> _productStatuses = new List<ProductStatus>();
        private static readonly Func<string, Func<ProductStatus, bool>> _searchByCode =
            (string code) =>
                (ProductStatus entity) =>
                    entity.Code == code;
        #endregion

        #region Consts
        static readonly string _assemblyCode = "5001";
        static readonly string _inWorkCode = "5002";
        static readonly string _readyToInstallCode = "5004";
        static readonly string _installedCode = "5005";
        static readonly string _readyToDispatchCode = "5007";
        static readonly string _dispatchedCode = "5008";
        static readonly string _rejectedCode = "5009";
        static readonly string _recycledCode = "5010";

        static readonly string _reclamationCode = "5013";
        static readonly string _installedInReclamationCode = "5014";
        static readonly string _readyToReclamationDispatchCode = "5015"; //Статус рекламации(верхнего уровня) после окончания ремонта
        static readonly string _readyToReclamationInstallCode = "5016"; //Статус рекламации(нижнего и среднего уровней) после окончания ремонта

        static readonly string _pseudoDispatchedCode = "5017"; 
        #endregion

        #region props
        public static ProductStatus Assembly { get; set; }
        public static ProductStatus InWork { get; set; }
        public static ProductStatus ReadyToInstall { get; set; }
        public static ProductStatus Installed { get; set; }
        public static ProductStatus ReadyToDispatch { get; set; }
        public static ProductStatus Dispatched { get; set; }
        public static ProductStatus PseudoDispatched { get; set; }
        public static ProductStatus Rejected { get; set; }
        public static ProductStatus Recycled { get; set; }
        public static ProductStatus Reclamation { get; set; }
        public static ProductStatus InstalledInReclamation { get; set; }
        public static ProductStatus ReadyToReclamationDispatch { get; set; }
        public static ProductStatus ReadyToReclamationInstall { get; set; }
        #endregion

        static ProductStatusManager()
        {
            _productStatuses = SQLServer.Instance.ProductStatus.GetAllProductStatuses();
            RegisterPrimitiveStatuses();
            RegisterProps();
        }

        #region PrimitiveStatuses
        private static void RegisterPrimitiveStatuses()
        {
            _primitiveStatuses.Add("RejectProduct", _productStatuses.SingleOrDefault(_searchByCode(_rejectedCode))); //Статус изделия после забракования
            _primitiveStatuses.Add("SaveProductChanges", _productStatuses.SingleOrDefault(_searchByCode(_inWorkCode))); //Статус издеиля после ProductVM.SaveProductChangesCommand 
            _primitiveStatuses.Add("StartOrReturnOperation", _productStatuses.SingleOrDefault(_searchByCode(_inWorkCode))); //Статус изделия после начала (или повторного выполнения) операции для одного изделия или для группы изделий 
            _primitiveStatuses.Add("CancelOperation", _productStatuses.SingleOrDefault(_searchByCode(_inWorkCode))); //Статус изделия после отмены операции
            _primitiveStatuses.Add("DispatchProduct", _productStatuses.SingleOrDefault(_searchByCode(_dispatchedCode))); //Статус изделия после отгрузки
            _primitiveStatuses.Add("PseudoDispatchProduct", _productStatuses.SingleOrDefault(_searchByCode(_pseudoDispatchedCode))); //Статус для отгрузки изделий низшего и среднего уровня для случаев, когда изделие-родитель не ведется в программе
            _primitiveStatuses.Add("ReclamationParentProduct", _productStatuses.SingleOrDefault(_searchByCode(_reclamationCode))); //Статус изделия после рекламационного возврата
            _primitiveStatuses.Add("ReclamationChildrens", _productStatuses.SingleOrDefault(_searchByCode(_installedInReclamationCode))); //Статус изделия после рекламационного возврата
        }
        private static void RegisterProps()
        {
            try
            {
                Assembly = _productStatuses.Single(_searchByCode(_assemblyCode));
                InWork = _productStatuses.Single(_searchByCode(_inWorkCode));
                ReadyToInstall = _productStatuses.Single(_searchByCode(_readyToInstallCode));
                Installed = _productStatuses.Single(_searchByCode(_installedCode));
                ReadyToDispatch = _productStatuses.Single(_searchByCode(_readyToDispatchCode));
                Dispatched = _productStatuses.Single(_searchByCode(_dispatchedCode));
                PseudoDispatched = _productStatuses.Single(_searchByCode(_pseudoDispatchedCode));
                Rejected = _productStatuses.Single(_searchByCode(_rejectedCode));
                Recycled = _productStatuses.Single(_searchByCode(_recycledCode));
                Reclamation = _productStatuses.Single(_searchByCode(_reclamationCode));
                InstalledInReclamation = _productStatuses.Single(_searchByCode(_installedInReclamationCode));
                ReadyToReclamationDispatch = _productStatuses.Single(_searchByCode(_readyToReclamationDispatchCode));
                ReadyToReclamationInstall = _productStatuses.Single(_searchByCode(_readyToReclamationInstallCode));
            }
            catch (ArgumentNullException)
            {
                Dialog.ShowError(TextHelper.MissProductStatusInitialization);
                throw;
            }

        }
    public static ProductStatus GetPrimitiveStatus(string code)
        {
            if (!_primitiveStatuses.ContainsKey(code))
                throw new ArgumentException(TextHelper.NullPageCodeThrow(code));

            return _primitiveStatuses[code];
        }
        #endregion

        #region InitialStatuses
        public static ProductStatus DefinitionInitialProductStatus(Revision currentRevision, RouteMap currentRouteMap)
        {
            #region TruthTable
            /*
             * Return           | HasParent | HasChilds | HasRouteMap   |
             * ----------------------------------------------------------
             * ReadyToDispatch  |   0       |   0       |   0           | //Высший
             * InWork           |   0       |   0       |   1           | //Высший
             * Assembly         |   0       |   1       |   0           | //Высший
             * InWork           |   0       |   1       |   1           | //Высший
             * ReadyToInstal    |   1       |   0       |   0           | //Низший
             * InWork           |   1       |   0       |   1           | //Низший
             * Assembly         |   1       |   1       |   0           | //Средний
             * InWork           |   1       |   1       |   1           | //Средний
             
            Assembly у изделия - процесс заполнения посадочных мест этого изделия
            Высший - высший уровень модели (есть только наследник)
            Средний - средний уровень модели (есть и родитель и наследник)
            Низший - низший уровень модели (есть только родитель)
             */
            #endregion

            bool currentRevisionHasRouteMap = currentRouteMap != null;

            if (currentRevisionHasRouteMap)
                return InWork;

            bool currentRevisionHasChilds = currentRevision.RevisionBOMs1.Count != 0;

            if (!currentRevisionHasRouteMap && currentRevisionHasChilds)
                return Assembly;

            bool currentRevisionHasParent = currentRevision.RevisionBOMs.Count != 0;

            if (!currentRevisionHasRouteMap && !currentRevisionHasChilds && currentRevisionHasParent)
                return ReadyToInstall;
            else
                return ReadyToDispatch;
        }
        #endregion

        #region ManagerInterface
        //Интерфейс взаимодействия с данным классом

        public static bool CanExecutePseudoDispatchStatusCheck(int statusId) => statusId == ReadyToInstall.Id || statusId == ReadyToReclamationInstall.Id;
        public static bool CanExecuteDispatchStatusCheck(int statusId) => statusId == ReadyToDispatch.Id || statusId == ReadyToReclamationDispatch.Id;
        public static bool CanChangeRouteMapCheck(int statusId) => IsAssemblyOrInWork(statusId);
        public static bool CanChangeSlotCheck(int statusId) => IsNotRejectedAndRecycled(statusId);
        public static int[] GetStatusIdsForProductForInstall() => new int[]
            {
                ReadyToInstall.Id,
                ReadyToReclamationInstall.Id
                //.Reclamation нельзя включать в этот массив. Для этого статуса есть спец. кнопка, которая проверяет пройденность МК перед сменой статуса на "готов к установке (рекламация)"

            }; //Статусы изделий, которые готовы к усановке в посадочное место


        public static ProductStatus DefinitionStatusAfterGroupEnd(Product product, bool routeMapIsCompelete) 
            => routeMapIsCompelete ? DefinitionCompleteProductStatus(product) : InWork;

        //Изделие считается готовым (и получает статус готов к отгрузке/установке), если все слоты (при их наличии) заполнены и если все операции в МК выполнены.
        //Если изделие не готово, то оно получает статус в работе
        public static ProductStatus DefenitionUnrejectedProductStatus(Product product, bool routeMapIsCompelete, bool installationIsComplete)
            => (routeMapIsCompelete && installationIsComplete) ? DefinitionCompleteProductStatus(product) : InWork;

        public static ProductStatus DefinitionCompleteProductStatus(Product product)
        {
            /*
                ModelType == ComplexedProduct ? 
                != reclamation ? ReadyToDispatch : readyToReclamationDispatchCode
                : 
                != reclamation ? ReadyToInstall : readyToReclamationInstallCode
             */
            return product.Revision.Model.ModelTypeId == CodeDictionary.Instance.AuxiliaryModelType.TopLevelModel.Id ?
                DefinitionCompleteHighLevelProductStatus(product)
                :
                DefinitionCompleteLowLevelProductStatus(product);
        }

        //Если есть слоты, то статус получается в зависимости от заполнености слотов. Если слотов нет, то в зависимости от product.ModelType
        public static ProductStatus DefinitionPushedProductStatus(Product product, bool installactionIsCompleted) 
            => (product.ProductBOMs?.Any() ?? false) ? 
                DefinitionProductStatus(product, installactionIsCompleted) 
                : 
                DefinitionCompleteProductStatus(product);

        //Если все слоты заполнены, то возвращает статус в зависимости от product.ModelType. Если нет - возвращает статус "сборка".
        private static ProductStatus DefinitionProductStatus(Product product, bool installationIsCompleted) 
            => installationIsCompleted ? 
                DefinitionCompleteProductStatus(product)
                : 
                Assembly;

        private static ProductStatus DefinitionCompleteHighLevelProductStatus(Product highLvlProduct)
            => highLvlProduct.ProductStatus.Id != Reclamation.Id ?
                ReadyToDispatch
                :
                ReadyToReclamationDispatch;
        //КИ не рекламационное ? ReadyToDispatch : ReadyToReclamationDispatch

        private static ProductStatus DefinitionCompleteLowLevelProductStatus(Product lowLvlProduct)
            => (lowLvlProduct.ProductStatus.Id != InstalledInReclamation.Id && lowLvlProduct.ProductStatus.Id != Reclamation.Id) ?
                ReadyToInstall
                :
                ReadyToReclamationInstall;
        //Изделие не рекламационное ? ReadyToInstall : ReadyToReclamationInstall


        public static ProductStatus DefinitionProductStatusForOperationAction(RouteMapProductOperation productOperation)
            => DefinitionProductStatusForOperationAction(productOperation.Product);
        
        //Если изделие имеет статус "рекламация", то он не меняется на статус "в работе" после любого действия (начать-завершить-отменить-продолжить-повторно)
        public static ProductStatus DefinitionProductStatusForOperationAction(Product product)
        {
            if (product.ProductStatusId == Reclamation.Id)
                return product.ProductStatus;

            if (product.ProductStatusId == ReadyToReclamationDispatch.Id || product.ProductStatusId == ReadyToReclamationInstall.Id)
                return Reclamation;

            return InWork; //default
        }
        public static int DefinitionProductStatusIdForOperationAction(int productStatusId)
        {
            if (productStatusId == Reclamation.Id)
                return productStatusId;

            if (productStatusId == ReadyToReclamationDispatch.Id || productStatusId == ReadyToReclamationInstall.Id)
                return Reclamation.Id;

            return InWork.Id; //default
        }

        public static ProductStatus DefinitionProductStatusAfterEndOperation(bool routeMapIsComplete, RouteMapProductOperation productOperation)
        {
            if (routeMapIsComplete && productOperation.Product.ProductStatusId != Reclamation.Id && productOperation.Product.ProductStatusId != InstalledInReclamation.Id)
            {
                return DefinitionCompleteProductStatus(productOperation.Product);
            }
            else
            {
                return DefinitionProductStatusForOperationAction(productOperation);
            }
        }

        //Для изделий без МК
        public static ProductStatus DefinitionParentProductStatusAfterChangeSlots(Product parentProduct, bool installationIsComplete)
            => installationIsComplete ?
                DefinitionCompleteProductStatus(parentProduct)
                :
                DefinitionInWorkStatus(parentProduct.ProductStatusId);

        //Для изделий с МК
        public static ProductStatus DefinitionParentProductStatusAfterChangeSlots(RouteMapProductOperation productOperation, int parentProductStatusId, bool installationIsComplete, bool routeMapIsComplete)
          => installationIsComplete ?
                    DefinitionProductStatusAfterEndOperation(routeMapIsComplete, productOperation)
                    :
                    DefinitionInWorkStatus(parentProductStatusId);

        //Возвращает для рекламационных изделий специальный статус
        public static ProductStatus DefinitionInstalledProductStatus(int productStatusId) 
            => productStatusId != Reclamation.Id ? Installed : InstalledInReclamation;

        //Возвращает для рекламационных изделий специальный статус
        public static ProductStatus DefinitionDeInstalledProductStatus(int productStatusId)
            => productStatusId != InstalledInReclamation.Id ?  ReadyToInstall : Reclamation;
        

        public static ProductStatus DefinitionInWorkStatus(int productStatusId)
        {
            //Возвращает для рекламационных изделий специальный статус
            return productStatusId != Reclamation.Id ?
                   InWork
                   :
                   Reclamation;
        }
        //Массив ИД-статусов "запущенных" в работу изделий ("Запущенным" считается изделие не учтенное в количестве отгруженных изделий (в orderElement.CompletedQuantity и в order.CompletedQuantity))
        //Рекоамационные изделие не учитываются
        #endregion 

        #region ProductView_tabs
        public static int[] GetInWorkTabProductStatusIds()
        {
            return new int[]
            {
                Assembly.Id,
                InWork.Id,
                ReadyToInstall.Id,
                Reclamation.Id,
                InstalledInReclamation.Id,
                ReadyToReclamationInstall.Id
            };
        } // Дает массив ИД статусов, которые относятся к изделиям вкладки "в работе" 
        public static int[] GetInstalledTabProductStatusIds() => new int[]
        {           
            Installed.Id             
        };// Дает массив ИД статусов, которые относятся к изделиям вкладки "установлены" 

        public static int[] GetProductStatusIdsForReadyToDispatchTab()
        {
            return new int[]
            {
                ReadyToDispatch.Id,
                ReadyToReclamationDispatch.Id
            };
        } //Массив ИД-статусов для вкладки "готовы к отгрузке"

        public static int[] GetProductStatusIdsForNotForWorkTab()
        {
            return new int[]
            {
                Rejected.Id,
                Recycled.Id
            };
        } //Массив ИД-статусов для вкладки "Не для работы"

        public static int[] GetProductStatusIdsForDispatchTab()
        {
            return new int[]
            {
                Dispatched.Id,
                PseudoDispatched.Id
            };
        } //Массив ИД-статусов для вкладки "Отгруженные"
        #endregion

        #region Other_GetArray_methods
        public static int[] GetInstallationProductViewIsReadOnlyStatusIds() => new int[]
          {
            Dispatched.Id,
            PseudoDispatched.Id,
            Installed.Id
          };//Статусы, для которых InstallationProductView будет работать в режиме "только для чтения"
        public static int[] GetPutToWorkStatusIds() => new int[]
        {
            InWork.Id,
            ReadyToInstall.Id,
            Installed.Id,
            ReadyToDispatch.Id
        };
        #endregion

        #region CurrentStatusCheck_methods
        public static bool IsReadyToInstallOrReadyToDispatch(int statusId)
        {
            return statusId == ReadyToInstall.Id || statusId == ReadyToDispatch.Id;
        }

        public static bool IsReclamationOrInstalledInReclamation(int statusId)
        {
            return statusId == Reclamation.Id || statusId == InstalledInReclamation.Id;
        }

        public static bool IsNotReclamationAndInstalledInReclamation(int statusId)
        {
            return statusId != Reclamation.Id && statusId != InstalledInReclamation.Id;
        }

        public static bool IsNotReclamationAndInstalledInReclamationAndInstalled(int statusId)
        {
            return statusId != Reclamation.Id && statusId != InstalledInReclamation.Id && statusId != Installed.Id;
        }
        private static bool IsAssemblyOrInWork(int statusId) => statusId == Assembly.Id || statusId == InWork.Id;
        private static bool IsNotRejectedAndRecycled(int statusId) => statusId != Rejected.Id && statusId != Recycled.Id;
        public static bool IsPseudoDispatched(int statusId) => statusId == PseudoDispatched.Id;
        #endregion

        #region CommandCanExecute_getStatus_methods
        public static string GetStatusesThatCanBeOpenParentPage() => string.Join(",", new string[]
        {
            _installedCode,
            _dispatchedCode
        }); //Статусы, разрешающие забраковку изделия
        public static string GetStatusesThatCanBeRejected() => string.Join(",", new string[]
        {
            _readyToInstallCode,
            _inWorkCode,
            _readyToDispatchCode,
            _assemblyCode
        }); //Статусы, разрешающие забраковку изделия
        public static string GetStatusesThatCanTransitToOperationsPage() => string.Join(",", new string[]
        {
            _assemblyCode,
            
            _inWorkCode,
            _readyToInstallCode,
            _readyToDispatchCode,
            
            _reclamationCode,
            _readyToReclamationInstallCode,
            _readyToReclamationDispatchCode
        }); //Статусы, разрешающие открыть список операций для изделия

        public static string GetStatusesThatCanTransitToGroupOperationsPage() => string.Join(",", new string[]
        {
            _inWorkCode,
            _readyToInstallCode
        }); //Статусы, разрешающие открыть список операций для выбранной группы изделий

        public static string GetStatusesForReclamationButton() => string.Join(",", new string[]
        {
                _dispatchedCode,
                _pseudoDispatchedCode
        }); //Статусы, изделия с которыми можно вернуть как рекламационные

        public static string GetStatusesThatCanBeDispatched() => string.Join(",", new string[]
        {
            _readyToDispatchCode,
            _readyToReclamationDispatchCode
        });
        #endregion

        #region Converters
        public static List<int> ConvertCodesToIds(string[] statusCodes) => statusCodes.Select(code => _productStatuses.Single(pStatus => pStatus.Code == code).Id).ToList();
        #endregion
    }
}
