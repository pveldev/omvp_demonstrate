﻿using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.CoreBL
{
    public class NaturalStringCompareHelper
    {
        protected int CompareAsInt(string left, string right)
        {
            if (int.TryParse(left, out int parsedLeft) && int.TryParse(right, out int parsedRight))
            {
                return parsedLeft > parsedRight ? -1 : 1;
            }
            else
            {
                #region Developer_block
                //Можно удалить данный блок. В реалиях данной прграммы этот код должен быть недостижимым
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog
                    (
                        exceptionName: "IntParseException",
                        message: "Fail with parse to int for compare",
                        innerExceptionMessage: $"left operand: {left} right operand: {right}",
                        methodName: "OMVP.BL.CoreBL.OrderByFavoriteThenByProgressThenBySerialNumber.CompareAsInt",
                        projectName: "OMVP"
                    ));
                #endregion

                return left.Count() > right.Count() ? -1 : 1;
            }
        }


        //* - используются у дубликатов
        //. - используются у кабелей
        //пробелы встречаются в ранее занесенных серийниках
        protected string NormalizeSerialNumber(string serialNumber) => serialNumber.Replace("*", "").Replace(".", "").Replace(" ", "");        
    }
}