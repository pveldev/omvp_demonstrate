﻿using OMVP.BL.BLModels;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.BL.CoreBL
{
    public class CheckBL
    {
        public static bool OperationReadyToStart(RouteMapProductOperation rmpo) => rmpo != null && rmpo.OperationStatusId == CodeDictionary.Instance.AuxiliaryOperationStatus.ReadyToStart.Id;
        public static bool EquipmentInProgressOrCompleted(RouteMapProductOperation rmpo) 
            => rmpo != null && rmpo.OperationStatusId == CodeDictionary.Instance.AuxiliaryOperationStatus.EquipmentInProgress.Id || rmpo.OperationStatusId == CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id;
                //В методе нет проверки на то, что Operation.Code == .Equipment.Code. Учитывать это при использовании

        public static bool EqipmentIsCompleted(RouteMapProductOperation equipment) => equipment.OperationStatusId == CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id;
            //В методе нет проверки на то, что Operation.Code == .Equipment.Code. Учитывать это при использовании
        public static bool ModelXWorkingGroupIsDuplicate(IEnumerable<ModelXWorkingGroup> ModelsXWorkingGroups, ModelXWorkingGroup newModelXWorkingGroup) => ModelsXWorkingGroups
           .FirstOrDefault(modelXgroup => modelXgroup.ModelId == newModelXWorkingGroup.ModelId && modelXgroup.WorkingGroupId == newModelXWorkingGroup.WorkingGroupId) != null;

        #region CurrentProductOperationsCheck
        public static bool CurrentProductOperationsIsActualAndNotNull(Product product)        
        {
            //Метод обновляет CurrentProductOperationsList в случае, если коллекция не относится к выбранному изделию или пуста
            //Пустой она может быть в случае, когда пользователь не открывая маршрут сразу перешел к старнице "инсталляции узлов"

            if (ProductOperationBL.Instance.CurrentProductOperationsList?.Any(productOperation => productOperation.ProductId == product.Id) ?? false)
            {
                return RefreshSelectedProductOperation(product);
            }
            else
            {
                return ProductHaveRouteMapCheck(product.Id);
            }
        } // true, если CurrentProductOperationsList актуальна и не пуста
        private static bool ProductHaveRouteMapCheck(int productId)
        {
            ProductOperationBL productOperationBL = ProductOperationBL.Instance;
            productOperationBL.CurrentProductOperationsList.Clear();
            SQLServer.Instance.RouteMapProductOperation.GetSortedRouteMapProductOperations(productId).ForEach(productOperationBL.CurrentProductOperationsList.Add);

            if (ProductOperationBL.Instance.CurrentProductOperationsList?.Any() ?? false)
            {
                ProductOperationBL.Instance.SelectedProductOperation = ProductOperationBL.Instance.CurrentProductOperationsList.SingleOrDefault(productOperation => productOperation.OperationId == CodeDictionary.Instance.AuxiliaryOperation.Equipment.Id);
                return ProductOperationBL.Instance.SelectedProductOperation != null ? true : false;
            }
            else
            {
                return false;
            }
        } // true, если изделие(product) имеет маршрут, в противнос случае - false;
        private static bool RefreshSelectedProductOperation(Product product)
        {
            RouteMapProductOperation equipment = ProductOperationBL.Instance.CurrentProductOperationsList.FirstOrDefault(productOperation => productOperation.OperationId == CodeDictionary.Instance.AuxiliaryOperation.Equipment.Id);

            if (equipment is null)          
                return false;         

            ProductOperationBL.Instance.SelectedProductOperation = equipment;
            
            return true;
        }
        #endregion

        public static bool RouteMapComplianceCheck(IEnumerable<RouteMapOperation> routeMapOperations, RouteMap routeMap)
        {
            return routeMapOperations.All(routeMapOperation => routeMapOperation.RouteMapId == routeMap.Id);
        } // Проверяет относится ли список операций routeMapOperations к маршруту routeMap

        public static bool WorkingGroupEmployee_EmployeeNotNull(WorkingGroupEmployee workingGroupEmployee) => workingGroupEmployee != null && workingGroupEmployee.Employee != null;

        public static bool AccessorLevelCheck(int permissiveLevel) => AuthorizationModel.Instance.AuthorizedUser.AccessorLevel >= permissiveLevel;

        #region SaveOrderElementsChangesIsCorrect
        public static string OrderElementsIsCorrect(int quantity, OrderElement[] orderElements)
        {
            List<string> errors = new List<string>() { };
            errors.AddRange(OrderElementsValueCheck(orderElements.ToList()));
            errors.Add(SumQuantityCheck(quantity, orderElements));

            return string.Join("\n", errors.Where(elem => !string.IsNullOrEmpty(elem)));
        }

        private static string SumQuantityCheck(int quantity, OrderElement[] orderElements)
        {
            int sumQuantity = orderElements.Sum(orderElement => orderElement.Quantity);

            if (quantity >= sumQuantity)
            {
                return "";
            }
            else
            {
                return TextHelper.OrderElementQuantityDestributeMessage(quantity, sumQuantity);
            }
        }

        //Валидация некорректного ввода значений
        private static List<string> OrderElementsValueCheck(List<OrderElement> orderElements)
        {
            List<string> errors = new List<string>();

            if (orderElements.Any(orderElement => orderElement.Quantity < 0))            
                errors.Add(TextHelper.IntegersMustBeAboveZero);

            if (!orderElements.All(orderElement => OrderElementIsCorrect(orderElement)))
                errors.Add(TextHelper.OrderElementQuantityValueError);

            return errors;
        }

        //Если уже есть готовые, то нельзя установить количестко "необходимых" МЕНЬШЕ, чем уже есть "готовых"
        private static bool OrderElementIsCorrect(OrderElement orderElement) =>
            orderElement.CompletedQuantity != 0 ? orderElement.Quantity >= orderElement.CompletedQuantity : true;
        #endregion
    }
}
