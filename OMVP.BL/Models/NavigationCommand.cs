﻿using OMVP.BL.CoreBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows;
using OMVP.Core;

namespace OMVP.BL.Models
{
    public class CustomCommandWrapper : BasicNotifyModel
    {
        public string Title { get; set; }
        public string CommandParameter { get; set; }  
        public string ButtonToolTip { get; set; }
		public ICommand ExecuteCommand { get; set; }
        public string AllowedStatusCode { get; set; } //multiProductToBoolConverter. Можно хранить группу статусов, разделенных запятой
    }
}
