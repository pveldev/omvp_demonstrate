﻿using Newtonsoft.Json;
using OMVP.BL.CoreBL;
using OMVP.Controls;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.Modal;
using System;
using System.IO;

namespace OMVP.ApplicationSettings
{
    public class Settings
	{
        public SettingData SettingsData { get; set; }

        readonly static string _applicationData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        readonly static string _folderName = "OMVP";
        readonly static string _settingsFolderPath = GetFolderPath(_applicationData, _folderName);
        readonly string _settingsPath = GetFullPath(_settingsFolderPath, "settings.json");

        #region Singletion
        private static readonly object _syncRoot = new object();
        private static Settings _instance;
        public static Settings Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncRoot)
                    {
                        if (_instance == null)
                            _instance = new Settings();
                    }
                }
                return _instance;
            }
        }

        private Settings()
        {
            SettingsData = LoadSettings();
        }
        #endregion

        public void SaveSettings()
		{
			try
			{
                if (SettingsFolderCheck() && SettingsFileExists())
					WriteToJsonFile();
			}
			catch (Exception ex)
			{
				SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog(ex));
				Dialog.ShowError(ex.Message);
			}
		}

        public SettingData LoadSettings()
		{
            try
            {
                SettingsFolderCheck();
                return GetSettings();
            }
            catch (Exception ex)
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog(ex));
                Dialog.ShowError(ex.Message);
                return InitDefaultSettingData();
            }
        }
		public bool SettingsFileExists() => File.Exists(_settingsPath) && new FileInfo(_settingsPath).Length != 0;
		private bool SettingFolderExists() => Directory.Exists(_settingsFolderPath);
        private bool SettingsFolderCheck()
        {
            if (!SettingFolderExists())
                Directory.CreateDirectory(_settingsFolderPath);

            return true;        
        }
        private SettingData GetSettings()
        {        
            if (SettingsFileExists())
            {
                return JsonConvert.DeserializeObject<SettingData>(File.ReadAllText(_settingsPath));
            }
            else
            {
                File.Create(_settingsPath).Dispose();
                return WriteToJsonFile(InitDefaultSettingData());
            }        
        }
		private SettingData WriteToJsonFile(SettingData data)
		{
			using (StreamWriter sw = new StreamWriter(_settingsPath, false))
			{
				sw.Write(JsonConvert.SerializeObject(data));
			}
            return data;
        }
        private void WriteToJsonFile()
        {
            using (StreamWriter sw = new StreamWriter(_settingsPath, false))
            {
                sw.Write(JsonConvert.SerializeObject(SettingsData));
            }
        }
        private static string GetFullPath(string settingsFolderPath, string fileName) => Path.Combine(settingsFolderPath, fileName); //full path to settings file
        private static string GetFolderPath(string applicationData, string folderName) => Path.Combine(applicationData, folderName); //full path to settings folder
        private SettingData InitDefaultSettingData() => new SettingData()
        {
            BaseDbPath = OmvpDataModel.GetDefaultConnectionString(),
            BaseTheme = ThemeSelectComboBox.GetDefaultSolorTheme()
        };
    }
}
