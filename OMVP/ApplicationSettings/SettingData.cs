﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Threading;

namespace OMVP.ApplicationSettings
{
	public class SettingData
	{
		public string BaseDbPath { get; set; }
		public string BaseTheme { get; set; }
	}
}
