﻿using OMVP.BL.BLModels;
using OMVP.BL.CoreBL;
using OMVP.BL.Models;
using OMVP.Core;
using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using OMVP.View.Popup;
using OMVP.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace OMVP
{
	public class CommandGenerator
	{
		public RevisionBL RevisionBL { get; set; } = RevisionBL.Instance;
		public ProductBL ProductBL { get; set; } = ProductBL.Instance;
        public PageNavigation PageNavigation { get; set; }

        private readonly Dictionary<int, List<CustomCommandWrapper>> _navigationCommands;
		private readonly Dictionary<int, List<CustomCommandWrapper>> _productCommands;

        #region Singleton
        private static readonly Lazy<CommandGenerator> _lazyModel = new Lazy<CommandGenerator>(() => new CommandGenerator());
        public static CommandGenerator Instance => _lazyModel.Value;

        private CommandGenerator()
		{
			_navigationCommands = new Dictionary<int, List<CustomCommandWrapper>>();
			_productCommands = new Dictionary<int, List<CustomCommandWrapper>>();
			RegisterCommands();
		}

		#endregion

		private void RegisterCommands()
		{
			#region RevisionNavigationView
			_navigationCommands.Add(CodeDictionary.Instance.AuxiliaryModelType.TopLevelModel.Id, new List<CustomCommandWrapper>()
			{
				new CustomCommandWrapper()
				{
					Title = TextHelper.ComplexProductModulesButtonTitle,
					ButtonToolTip= TextHelper.ComplexProductModulesButtonTooltip,
					CommandParameter  = TextHelper.RevisionBomsViewKey,
					ExecuteCommand = new SmartCommand((p) =>
					{
						PageNavigation.LoadNextPage(p.ToString(), RevisionBL.GetCurrentPageTitle(), RevisionBL.Instance.SelectedRevision);
					}, p => RevisionBL.SelectedRevision != null && (RevisionBL.SelectedRevision.RevisionBOMs1?.Any() ?? false), RevisionBL, new string[] { "SelectedRevision" })
				},
				FormShowProductCommand(),
                FormOpenExcelExportWindowCommand(),
                FormGroupOrderElementViewCommand()
            }); //Theme commands
			_navigationCommands.Add(CodeDictionary.Instance.AuxiliaryModelType.MiddleLevelModel.Id, new List<CustomCommandWrapper>()
			{
				new CustomCommandWrapper()
				{
					Title = TextHelper.ModuleComponentsButtonTitle,
					ButtonToolTip= TextHelper.ModuleComponentsButtonTooltip,
					CommandParameter = TextHelper.RevisionBomsViewKey,
					ExecuteCommand = new SmartCommand(p =>
					{
						PageNavigation.LoadNextPage(p.ToString(), RevisionBL.GetCurrentPageTitle(), RevisionBL.SelectedRevision);
					}, p => RevisionBL.SelectedRevision != null && (RevisionBL.SelectedRevision.RevisionBOMs1?.Any() ?? false), RevisionBL, new string[] { "SelectedRevision" })
				},
				FormShowProductCommand(),
                FormOpenExcelExportWindowCommand(),
                FormGroupOrderElementViewCommand()
            }); //Module commands
			_navigationCommands.Add(CodeDictionary.Instance.AuxiliaryModelType.LowerLevelModel.Id, new List<CustomCommandWrapper>()
			{
				FormShowProductCommand(),
                FormOpenExcelExportWindowCommand(),
                FormGroupOrderElementViewCommand()
            }); //Component commands
            #endregion

            #region ProductView
            _productCommands.Add(CodeDictionary.Instance.AuxiliaryModelType.TopLevelModel.Id, new List<CustomCommandWrapper>()
            {
                FormAddToFavoriteCommand(),
                FormShowProductRouteCommand(), //Операционка
                FormShowProductParametersCommand(), // Параметры изделия
                FormShowMultiProductOperationCommand(),
                FormShowLogForSelectedProductCommand(),

                new CustomCommandWrapper()
                {
                    Title = TextHelper.ComplexProductBomsButtonTitle,
                    ButtonToolTip = TextHelper.ComplexProductBomsButtonTooltip,
                    CommandParameter  = TextHelper.InstallationProductViewKey,
                    ExecuteCommand = new SmartCommand((p) =>
                    {
                        if (ProductBL.CanOpenInstallationPage())
                        {
                            PageNavigation.LoadNextPage(p.ToString(), 
                                                        TextHelper.ProductBomsViewTitle(ProductBL.SelectedProduct.Revision.Name, ProductBL.SelectedProduct.SerialNumber),
                                                        ProductBL.SelectedProduct.Revision,
                                                        ProductBL.SelectedProduct,
                                                        refreshContext: false);
                        }
                        else
                        {
                            Dialog.ShowWarning(TextHelper.CantOpenBomsPageWarningMessage);
                        }
                    }, p => ProductBL.SelectedProduct != null && (ProductBL.SelectedProduct.ProductBOMs?.Any() ?? false), ProductBL, new string[] { "SelectedProduct" })
                }, // Состав КИ

				new CustomCommandWrapper()
                {
                    Title = TextHelper.RejectBodyButtonTitle,
                    ButtonToolTip = TextHelper.RejectBodyButtonTooltip,
                    AllowedStatusCode = ProductStatusManager.GetStatusesThatCanBeRejected(),
                    ExecuteCommand = new SmartCommand(_ =>
                    {
                        ProductBL.RejectBody(ProductBL.SelectedProduct);
                    }, p => ProductBL.SelectedProduct != null && CheckBL.AccessorLevelCheck(2), ProductBL, new string[] { "SelectedProduct" })
                }, // Забраковка

                FormRecycleProductCommand(),
                FormOpenOpenEditProductWindowCommand(),
                FormDeleteProductCommand(),
                FormShowLogForSelectedProductCommand(),
                FormReclamationProductCommand()

            }); // Theme-product commands
			_productCommands.Add(CodeDictionary.Instance.AuxiliaryModelType.MiddleLevelModel.Id, new List<CustomCommandWrapper>()
			{
                FormAddToFavoriteCommand(),
                FormShowProductRouteCommand(), //Операционка
                FormShowProductParametersCommand(), // Параметры изделия
                FormShowMultiProductOperationCommand(),
                FormShowLogForSelectedProductCommand(),

                new CustomCommandWrapper()
				{
					Title = TextHelper.ShowComponentsButtonTitle,
					ButtonToolTip = TextHelper.ShowComponentsButtonTooltip,
					CommandParameter  = TextHelper.InstallationProductViewKey,
					ExecuteCommand = new SmartCommand((p) =>
					{
						if (ProductBL.CanOpenInstallationPage())
							PageNavigation.LoadNextPage(
                                p.ToString(), 
                                TextHelper.ProductBomsViewTitle(ProductBL.SelectedProduct.Revision.Name, ProductBL.SelectedProduct.SerialNumber),
                                ProductBL.SelectedProduct.Revision,
                                ProductBL.SelectedProduct, 
                                refreshContext: false);
						else
							Dialog.ShowWarning(TextHelper.CantOpenBomsPageWarningMessage);
                    }, p=> ProductBL.SelectedProduct != null && (ProductBL.SelectedProduct.ProductBOMs?.Any() ?? false), ProductBL, new string[] { "SelectedProduct" })
				}, // Состав модуля

                new CustomCommandWrapper()
				{
					Title = TextHelper.RejectButtonTitle,
					ButtonToolTip = TextHelper.RejectButtonTooltip,
					AllowedStatusCode = ProductStatusManager.GetStatusesThatCanBeRejected(),
					ExecuteCommand = new SmartCommand(_ =>
					{
						ProductBL.RejectModule(ProductBL.SelectedProduct);
                    }, p=> ProductBL.SelectedProduct != null && CheckBL.AccessorLevelCheck(2), ProductBL, new string[] { "SelectedProduct" })
                }, // Забраковать модуль

                FormRecycleProductCommand(),

                new CustomCommandWrapper()
				{
					Title = TextHelper.ComplexProductPageButtonTitle,
					CommandParameter  = TextHelper.ProductParentInstallationViewKey,
					ExecuteCommand = new SmartCommand((p) =>
					{
						PageNavigation.LoadNextPage(p.ToString(), "", GetParentRevision(), selectedProduct: ProductBL.SelectedProduct );
					}, p=> ProductBL.SelectedProduct != null && CanOpenInstallationPage(ProductBL.SelectedProduct), ProductBL, new string[] { "SelectedProduct" })
				}, // Окно для просмотра модулей, установленных в КИ
                
                FormOpenOpenEditProductWindowCommand(),
                FormDeleteProductCommand(),
                FormReclamationProductCommand()

            }); // Module-product commands
			_productCommands.Add(CodeDictionary.Instance.AuxiliaryModelType.LowerLevelModel.Id, new List<CustomCommandWrapper>()
			{
                FormAddToFavoriteCommand(),
                FormShowProductRouteCommand(), //Операционка
                FormShowProductParametersCommand(), // Параметры изделия
                FormShowMultiProductOperationCommand(),

                new CustomCommandWrapper()
				{
					Title = TextHelper.RejectButtonTitle,
					ButtonToolTip = TextHelper.RejectButtonTooltip,
                    AllowedStatusCode = ProductStatusManager.GetStatusesThatCanBeRejected(),
					ExecuteCommand = new SmartCommand(_=>
					{
						ProductBL.RejectSelectedProduct();
					}, p=> ProductBL.SelectedProduct != null && CheckBL.AccessorLevelCheck(2), ProductBL, new string[] { "SelectedProduct" })
				}, // Забраковать компонент

                FormRecycleProductCommand(),

                new CustomCommandWrapper()
				{
					Title = TextHelper.ModulePageButtonTitle,
					ButtonToolTip = TextHelper.ModulePageButtonTooltip,
					CommandParameter  = TextHelper.ProductParentInstallationViewKey,
					ExecuteCommand = new SmartCommand(p =>
					{
						PageNavigation.LoadNextPage(p.ToString(),"", GetParentRevision(), ProductBL.SelectedProduct );
					}, p=> ProductBL.SelectedProduct != null && CanOpenInstallationPage(ProductBL.SelectedProduct), ProductBL, new string[] { "SelectedProduct" })
				}, // Окно для просмотра узлов и компонентов, установленных в модуль
                
                FormOpenOpenEditProductWindowCommand(),
                FormDeleteProductCommand(),
                FormReclamationProductCommand()

            }); // Component-product commands
			#endregion			
		}

        public List<CustomCommandWrapper> GetNavigationCommands(int id)
		{
			if (!_navigationCommands.ContainsKey(id))
				throw new ArgumentException(TextHelper.NullPageCodeThrow(id));

			return _navigationCommands[id];
		}
		public List<CustomCommandWrapper> GetProductCommands(int id)
		{
			if (!_productCommands.ContainsKey(id))
				throw new ArgumentException(TextHelper.NullPageCodeThrow(id));

			return _productCommands[id];
		}
		#region ОбщиеКнопки
		private CustomCommandWrapper FormShowProductCommand() => new CustomCommandWrapper()
		{
			Title = TextHelper.ProductsButtonTitle,
			ButtonToolTip = TextHelper.ProductsButtonTooltip,
			CommandParameter = TextHelper.ProductsViewKey,
			ExecuteCommand = FormExecuteCommandForShowProducts()
        };
        public SmartCommand FormExecuteCommandForShowProducts() => new SmartCommand((p) =>
        {
            PageNavigation.LoadNextPage(p.ToString(), TextHelper.ProductsViewTitle(RevisionBL.SelectedRevision.Name), RevisionBL.Instance.SelectedRevision);

        }, p => RevisionBL.Instance.SelectedRevision != null, RevisionBL, new string[] { "SelectedRevision" });
        private CustomCommandWrapper FormShowProductRouteCommand() => new CustomCommandWrapper()
		{
			Title = TextHelper.OperationsButtonTitle,
			ButtonToolTip = TextHelper.OperationsButtonTooltip,
			AllowedStatusCode = ProductStatusManager.GetStatusesThatCanTransitToOperationsPage(),
			CommandParameter = TextHelper.ProductOperationsViewKey,
			ExecuteCommand = new SmartCommand((p) =>
			{
                PageNavigation.LoadNextPage(p.ToString(), TextHelper.CurrentProductOperationsViewTitle(ProductBL.SelectedProduct.Revision.Name, ProductBL.SelectedProduct.SerialNumber), RevisionBL.SelectedRevision, ProductBL.SelectedProduct);
            }, p => ProductBL.SelectedProduct != null, ProductBL, new string[] { "SelectedProduct" })
		};
        private CustomCommandWrapper FormShowProductParametersCommand() => new CustomCommandWrapper()
        {
            Title = TextHelper.ProductParametersButtonTitle,
            ButtonToolTip = TextHelper.ShowCurrentProductParametersButtonTooltip,
            CommandParameter = TextHelper.ProductParametersViewKey,
            ExecuteCommand = new SmartCommand((p) =>
            {
                PageNavigation.LoadNextPage(p.ToString(), TextHelper.ProductParametersViewTitle(ProductBL.SelectedProduct.Revision.Name, ProductBL.SelectedProduct.SerialNumber), RevisionBL.SelectedRevision, ProductBL.SelectedProduct);
            }, p => ProductBL.SelectedProduct != null && CheckBL.AccessorLevelCheck(2), ProductBL, new string[] { "SelectedProduct" })
        }; 
        private CustomCommandWrapper FormShowMultiProductOperationCommand() => new CustomCommandWrapper()
        {
            Title = TextHelper.GroupOperationsButtonTitle,
            ButtonToolTip = TextHelper.GroupOperationsButtonTooltip,
            AllowedStatusCode = ProductStatusManager.GetStatusesThatCanTransitToGroupOperationsPage(),
            CommandParameter = TextHelper.MultiProductOperationViewKey,
            ExecuteCommand = new SmartCommand((p) =>
            {
                PageNavigation.LoadNextPage(p.ToString(), TextHelper.GroupProductOperationsViewTitle, ProductBL.SelectedProduct.Revision);
            }, p => ProductBL.SelectedProduct != null && ProductBL.SelectedProduct.RouteMap != null && CheckBL.AccessorLevelCheck(2), ProductBL, new string[] { "SelectedProduct" })
        };
        private CustomCommandWrapper FormOpenExcelExportWindowCommand() => new CustomCommandWrapper()
        {
            Title = TextHelper.ExportButtonTitle,
            ButtonToolTip = TextHelper.ExportButtonTooltip,
            ExecuteCommand = new SmartCommand(_ =>
            {
                ExportProductWindow exportWindow = new ExportProductWindow(RevisionNavigationVM.ExcelExporterRefresh);
                exportWindow.Refresh();
                exportWindow.ShowDialog();

            }, p => RevisionBL.SelectedRevision != null, RevisionBL, new string[] { "SelectedRevision" })
        };
        private CustomCommandWrapper FormGroupOrderElementViewCommand() => new CustomCommandWrapper()
        {
            Title = TextHelper.ShowGroupOrderElementstButtonTitle,
            ButtonToolTip = TextHelper.ShowGroupOrderElementstButtonTooltip,
            ExecuteCommand = new SmartCommand(_ =>
            {
                OrderBL.Instance.GetOrderElementGroupData(RevisionBL.SelectedRevision, DateTime.Now.Year);
                PageNavigation.LoadNextPage("ShowGroupOrderElementView", refreshContext: true, pageTitle: $"Сгруппированный по ежемесячной отгрузке годовой план [{RevisionBL.SelectedRevision.Name}] для [{DateTime.Now.Year}] года");
            }, p => RevisionBL.SelectedRevision != null, RevisionBL, new string[] { "SelectedRevision" })
        };
        private CustomCommandWrapper FormAddToFavoriteCommand() => new CustomCommandWrapper()
        {
            Title = TextHelper.ToFavoriteButtonTitle,
            ButtonToolTip = TextHelper.ToFavoriteButtonTooltip,
            ExecuteCommand = new SmartCommand(_ =>
            {
                ProductBL.FavoriteUnfavoriteSelectedProduct();
            }, p => ProductBL.SelectedProduct != null && CheckBL.AccessorLevelCheck(2), ProductBL, new string[] { "SelectedProduct" })
        };
        private CustomCommandWrapper FormOpenOpenEditProductWindowCommand() => new CustomCommandWrapper()
        {
            Title = TextHelper.EditProductButtonTitle,
            ButtonToolTip = TextHelper.EditProductButtonTooltip,
            ExecuteCommand = new SmartCommand(_=>
            {
                ProductBL.FormEditProductWindow();
                new EditProductWindow { }.ShowDialog();
            }, p => ProductBL.SelectedProduct != null && CheckBL.AccessorLevelCheck(2), ProductBL, new string[] { "SelectedProduct" })
        };

        private CustomCommandWrapper FormDeleteProductCommand() => new CustomCommandWrapper()
        {
            Title = TextHelper.DeleteProductButtonTitle,
            ButtonToolTip = TextHelper.DeleteProductButtonTooltip,
            ExecuteCommand = new SmartCommand(_ =>
            {
                MessageBoxResult result = Dialog.ShowOKCancel(TextHelper.DoYouWantDeleteProductMessage(ProductBL.SelectedProduct.Revision.Model.Name, ProductBL.SelectedProduct.SerialNumber));

                if (result == MessageBoxResult.OK)
                    ProductBL.DeleteProduct();

            }, p => ProductBL.SelectedProduct != null && CheckBL.AccessorLevelCheck(2), ProductBL, new string[] { "SelectedProduct" })
        };

        private CustomCommandWrapper FormRecycleProductCommand() => new CustomCommandWrapper()
        {
            Title = TextHelper.RecycleProductButtonTitle,
            ButtonToolTip = TextHelper.RecycleProductButtonTooltip,
            AllowedStatusCode = ProductStatusManager.Rejected.Code,
            ExecuteCommand = new SmartCommand(_ =>
            {
                ProductBL.RecycleSelectedProduct();
            }, p => ProductBL.SelectedProduct != null && CheckBL.AccessorLevelCheck(2), ProductBL, new string[] { "SelectedProduct" })
        };// Утилизировать изделие
        private CustomCommandWrapper FormShowLogForSelectedProductCommand() => new CustomCommandWrapper()
        {
            Title = TextHelper.ShowCurrentProductLogButtonTitle,
            ButtonToolTip = TextHelper.ShowCurrentProductLogButtonTooltip,
            ExecuteCommand = new SmartCommand(_ =>
            {
                if (ProductBL.PrepareLogDataForSelectedProduct())
                {
                PageNavigation.LoadNextPage(
                    key: TextHelper.SelectedProductLogViewKey,
                    pageTitle: TextHelper.SelectedProductLogViewTitle(ProductBL.SelectedProduct.Revision.Name, ProductBL.SelectedProduct.SerialNumber),
                    selectedRevision: ProductBL.SelectedProduct.Revision,
                    selectedProduct: ProductBL.SelectedProduct);
                }
                else
                {
                    Dialog.ShowOk(TextHelper.SelectedProductHaventLog);
                }
            }, p => ProductBL.SelectedProduct != null, ProductBL, new string[] { "SelectedProduct" })
        }; //Открыть историю выбранного изделия

        private CustomCommandWrapper FormReclamationProductCommand() => new CustomCommandWrapper()
        {
            Title = TextHelper.ReclamationButtonTitle,
            ButtonToolTip = TextHelper.ReclamationButtonTooltip,
            AllowedStatusCode = ProductStatusManager.GetStatusesForReclamationButton(),
            ExecuteCommand = new SmartCommand(_ =>
            {
                ProductBL.ReclamationSelectedProduct();
            }, p => ProductBL.SelectedProduct != null && CheckBL.AccessorLevelCheck(2), ProductBL, new string[] { "SelectedProduct" })
        };// Вернуть как рекламацию
        #endregion

        private Revision GetParentRevision() => SQLServer.Instance.ProductBom.GetProductBOM(ProductBL.SelectedProduct.Id).Product.Revision; // Возвращает ревизию-родителя, куда установлено выбарнное изделие

        //Переход от изделия к списку посадочных мест его родителя разрешен {ЕСЛИ .статус = установлен или .статус=отгружен и это не КИ}
        private bool CanOpenInstallationPage(Product product) =>
            product.ProductStatusId == ProductStatusManager.Installed.Id || 
            (product.ProductStatusId == ProductStatusManager.Dispatched.Id && product.Revision.Model.ModelTypeId != CodeDictionary.Instance.AuxiliaryModelType.TopLevelModel.Id);   
    }
}