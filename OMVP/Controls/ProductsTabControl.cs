﻿using OMVP.BL.BLModels;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;

namespace OMVP.Controls
{
    public class ProductsTabControl : TabControl
    {
        protected override void OnSelectionChanged(SelectionChangedEventArgs e)
        {
            base.OnSelectionChanged(e);

            string currentTabName = ((TabItem)this.SelectedItem).Name;

            if (string.IsNullOrEmpty(currentTabName))
                return;

            MenuBL.Instance.DataRefreshTime = DateTime.Now;

            switch (currentTabName)
            {
                case "inWorkProductsTab":
                    ProductBL.Instance.InWorkProductsTabDataRefresh();
                    break;

                case "installedProductsTab":
                    ProductBL.Instance.InstalledProductsTabDataRefresh();
                    break;
                    
                case "readyToDispatchTab":
                    ProductBL.Instance.LoadReadyToDispatchProducts();
                    break;

                case "dispatchedProductsTab":
                    ProductBL.Instance.LoadDispatchedProductsWithSelectedParameters();
                    break;

                case "notForWorkProductsTab":
                    ProductBL.Instance.LoadNotForWorkProducts();
                    break;

                default:
                    throw new InvalidOperationException($"Несуществующая вкладка: {currentTabName}");
            }

        }
    }
}
