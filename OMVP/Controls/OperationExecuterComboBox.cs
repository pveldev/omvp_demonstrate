﻿using OMVP.BL.BLModels;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace OMVP.Controls
{
    public class OperationExecuterComboBox : ComboBox
    {
        protected override void OnSelectionChanged(SelectionChangedEventArgs e)
        {
            base.OnSelectionChanged(e);

            if (SelectedIndex == -1) return; //[fix] SelectedItem не будет сбрасываться при потери фокуса у ComboBox'a

            if (ProductOperationBL.Instance.SelectedProductOperation != null)
            {
                if (SelectedItem is Employee operationExecuter)
                {
                    ProductOperationBL.Instance.SelectedProductOperation.OperationExecuterId = operationExecuter.Id;
                }
                else if (SelectedItem == null)
                {
                    ProductOperationBL.Instance.SelectedProductOperation.OperationExecuter = null;
                    ProductOperationBL.Instance.SelectedProductOperation.OperationExecuterId = null;
                }
                else
                {
                    SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomInvalidCast", "Не удалось привести выбранный элемент к типу-сотруднику", "", "OperationExecuterComboBox_OnSelectionChanged", "OMVP"));
                }
            }

        }
    }
}
