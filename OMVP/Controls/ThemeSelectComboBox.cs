﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows;
using OMVP.ApplicationSettings;

namespace OMVP.Controls
{
    public class ThemeSelectComboBox : ComboBox
	{
		public Settings Settings { get; set; } = Settings.Instance;
		readonly static List<string> _styles = new List<string>
        {
            "ClassicBlueColors",
            "VBColors",
            "WAColors",
            "DarkGreenColors",
            "CloudBurst",
            "LightOrangeColors"
        };

		public ThemeSelectComboBox()
		{
			ItemsSource = _styles;

			if (Settings.SettingsData != null && !string.IsNullOrEmpty(Settings.SettingsData.BaseTheme))
				SelectedItem = Settings.SettingsData.BaseTheme;
			else
				SelectedItem = _styles[0];
		}

		protected override void OnSelectionChanged(SelectionChangedEventArgs e)
		{
			base.OnSelectionChanged(e);
			RefreshSettingsFile();
			ThemeChange();
		}

		private void ThemeChange()
		{
			Application.Current.Resources.MergedDictionaries.Add(Application.LoadComponent(new Uri("/Resources/" + SelectedItem as string + ".xaml", UriKind.Relative)) as ResourceDictionary); // добавляем загруженный словарь ресурсов
		}

		private void RefreshSettingsFile()
		{
			string selectedTheme = SelectedValue.ToString();
			if (Settings.SettingsData.BaseTheme != selectedTheme)
			{
				Settings.SettingsData.BaseTheme = selectedTheme; //Замена значения
				Settings.SaveSettings(); // Перезапись
			}
		}

		public static string GetDefaultSolorTheme() => _styles[0];
	}
}
