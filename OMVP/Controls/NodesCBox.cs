﻿using OMVP.BL.BLModels;
using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace OMVP.Controls
{
	public class NodesCBox : ComboBox
	{
        #region DP
        public ProductBOM Slot
        {
            get { return (ProductBOM)GetValue(SlotProperty); }
            set { SetValue(SlotProperty, value); }
        }
        // Using a DependencyProperty as the backing store for Slot.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SlotProperty =
            DependencyProperty.Register("Slot", typeof(ProductBOM), typeof(NodesCBox), new PropertyMetadata(new ProductBOM()));
        #endregion

        protected override void OnDropDownOpened(EventArgs e)
        {
            base.OnDropDownOpened(e);
            FillComboBox(Slot);
        }

        private void FillComboBox(ProductBOM bom)
		{
            if (bom != null && bom.RevisionBOM != null && bom.RevisionBOM.BOMRevisionId != 0)
            {
                this.ItemsSource = SQLServer.Instance.Product.GetSortedProductsByStatusIds(bom.RevisionBOM.BOMRevisionId, ProductStatusManager.GetStatusIdsForProductForInstall());
            }
            else
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog( exceptionName: "CustomNullReverenceException", 
                                                                            message: "Посадочное место пустое",
                                                                            innerExceptionMessage: $"bom: {bom}",
                                                                            methodName: "Controls.NodesCBox.FillComboBox",
                                                                            projectName: "OMVP"));
            }
		}

        protected override void OnSelectionChanged(SelectionChangedEventArgs e)
        {
            if (SelectedItem is Product product)
            {
                InstallationProductBL.Instance.SelectedProduct = product;
                base.OnSelectionChanged(e);
            }
            else
            {
                string info = SelectedItem is null ? "selectedItemIsNull" : $"{SelectedItem.ToString()}";

                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog(
                    exceptionName: "CustomInvalidCast", 
                    message: "Не удалось привести выбранный элемент к типу-изделию",
                    innerExceptionMessage: $"info about selected item:{info}",
                    methodName: "NodesCBox_OnSelectionChanged", 
                    projectName: "OMVP"));
            }
        }

        private ProductBOM GetFirstElem() => ItemsSource.Cast<ProductBOM>().First();		
    }
}
