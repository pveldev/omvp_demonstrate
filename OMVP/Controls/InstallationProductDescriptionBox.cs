﻿using OMVP.BL.BLModels;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace OMVP.Controls
{
    public class InstallationProductDescriptionBox : TextBox
    {
        #region DP
        public ProductBOM Slot
        {
            get { return (ProductBOM)GetValue(SlotProperty); }
            set { SetValue(SlotProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Slot.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SlotProperty =
            DependencyProperty.Register("Slot", typeof(ProductBOM), typeof(InstallationProductDescriptionBox), new PropertyMetadata(new ProductBOM()));
        #endregion

        private void RefreshLogDescription(ProductBOM bom)
        {         
            ProductBomLog bomLog = InstallationProductBL.Instance.ProductBomLogs.FirstOrDefault(log => log.ProductBomId == bom.Id);
            if (bomLog != null)               
                bomLog.Description = Text;     
        }

        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            base.OnTextChanged(e);
            RefreshLogDescription(Slot);
        }
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (e.Property.Name == nameof(Visibility) && e.NewValue.ToString() == "Visible")            
                Text = null;            
        }
    }
}
