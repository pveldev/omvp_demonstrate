﻿using OMVP.BL.BLModels;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace OMVP.Controls
{
    class MultiOperationExecuterComboBox : ComboBox
    {
        protected override void OnSelectionChanged(SelectionChangedEventArgs e)
        {
            base.OnSelectionChanged(e);

            if (SelectedIndex == -1) return; //[fix] SelectedItem не будет сбрасываться при потери фокуса у ComboBox'a

       
            if (SelectedItem is Employee operationExecuter)
            {
                MultiProductBL.Instance.SelectedGroupExecuterId = operationExecuter.Id;
            }
            else if (SelectedItem == null)
            {
                MultiProductBL.Instance.SelectedGroupExecuterId = null;
            }
            else
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomInvalidCast", "Не удалось привести выбранный элемент к типу-сотруднику", "", "MultiOperationExecuterComboBox_OnSelectionChanged", "OMVP"));
            }
            

        }
    }
}
