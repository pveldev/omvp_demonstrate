﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OMVP
{
	public class SmartCommand : ICommand
	{
		public event EventHandler CanExecuteChanged;

		Func<object, bool> _canExecute = (parameter) => true;
        Action<object> _execute;

		public SmartCommand(Action<object> execute)
		{
			_execute = execute;
		}

		public SmartCommand(Action<object> execute, Func<object, bool> canExecute)
			: this(execute)
		{
			_canExecute = canExecute;
		}

		public SmartCommand(Action<object> execute, Func<object, bool> canExecute, INotifyPropertyChanged vm, string[] propNames)
			: this(execute, canExecute)
		{
			vm.PropertyChanged += (sender, e) =>
			{
				if (propNames.Contains(e.PropertyName))
					CanExecuteChanged?.Invoke(this, EventArgs.Empty);
			};
		}

		public bool CanExecute(object parameter) =>  _canExecute(parameter);

		public void Execute(object parameter)
		{
			_execute(parameter);
		}
	}

    public class SmartCommandAsync : ICommand
    {
        public event EventHandler CanExecuteChanged;

        Func<object, Task> _execute;
        Func<object, bool> _canExecute = (parameter) => true;

        public SmartCommandAsync(Func<object, Task> execute)
        {
            _execute = execute;
        }

        public SmartCommandAsync(Func<object, Task> execute, Func<object, bool> canExecute)
            : this(execute)
        {
            _canExecute = canExecute;
        }

        public SmartCommandAsync(Func<object, Task> execute, Func<object, bool> canExecute, INotifyPropertyChanged vm, string[] propNames)
            : this(execute, canExecute)
        {
            vm.PropertyChanged += (sender, e) =>
            {
                if (propNames.Contains(e.PropertyName))
                    CanExecuteChanged?.Invoke(this, EventArgs.Empty);
            };
        }

        public bool CanExecute(object parameter) => _canExecute(parameter);

        public async void Execute(object parameter)
        {
            await ExecuteAsync(parameter);
        }

        public async Task ExecuteAsync(object parameter)
        {
            try
            {
                InvokeCanExecuteChanged();
                await _execute(parameter);
            }
            finally
            {
                InvokeCanExecuteChanged();
            }
        }
        private void InvokeCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
