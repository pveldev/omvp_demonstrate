﻿using OMVP.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OMVP.CoreOMVP
{
    public class BasePopupWindow : Window
	{
		public void SetContext(Action<BaseVM, bool> refresh)
        {
            (DataContext as BaseVM).Refresh = refresh;
        }

        public void Refresh(bool isNavBack = false)
        {
            (DataContext as BaseVM).CallRefresh(isNavBack);
        }
    }
}
