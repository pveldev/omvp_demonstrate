﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using OMVP.BL.BLModels;
using OMVP.ViewModel;
using OMVP.Core;
using OMVP.View.HelpWindow;

namespace OMVP.CoreOMVP
{
	public class PageManager
	{
		private static PageManager _instance;

		public static PageManager Instance
		{
			get
			{
				if (_instance == null)
					_instance = new PageManager();

				return _instance;
			}
		}

		private readonly Dictionary<string, PageInfo> _pages;

		private PageManager()
		{
			_pages = new Dictionary<string, PageInfo>();
			RegisterPages();
		}

		private void RegisterPages()
		{
			#region MenuButtons			
			_pages.Add(TextHelper.RevisoinNavigationViewKey, new PageInfo { View = new View.RevisionNavigationView(RevisionNavigationVM.ThemePageRefresh), PageTitle="Список всех тем" });
			_pages.Add("ShowRevisionBOMs", new PageInfo { View = new View.RevisionNavigationView(RevisionNavigationVM.RevisionChildRefresh) });
			_pages.Add("ShowProductStatusView", new PageInfo { View = new View.ProductStatusView(ProductStatusVM.ProductStatusRefresh), PageTitle = "Статусы изделий" });
			_pages.Add(TextHelper.SearchedProductViewKey, new PageInfo { View = new View.SearchedProductView() });

            _pages.Add("ShowOrderView", new PageInfo { View = new View.OrderView(OrderVM.OrderRefresh), PageTitle = "Годовой план (заказы)", HelperWindow = new OrderHelper(true) });
			_pages.Add("ShowGroupOrderView", new PageInfo { View = new View.OrderGroupView(OrderVM.GroupOrderRefresh), PageTitle = "Сгруппированный годовой план" });
			_pages.Add("ShowOrderElementView", new PageInfo { View = new View.OrderElementView(OrderVM.OrderRefresh), PageTitle = "Месячный план" });
            _pages.Add("ShowGroupOrderElementView", new PageInfo { View = new View.OrderElementGroupView()});          

			_pages.Add("ShowModelTypeView", new PageInfo { View = new View.ModelTypeView(ModelTypeVM.ModelTypesRefresh), PageTitle = "Типы моделей" });
			_pages.Add("ShowModelView", new PageInfo { View = new View.ModelView(RevisionVM.ModelsPageRefresh), PageTitle = "Модели" });
			_pages.Add("ShowRevisionView", new PageInfo { View = new View.RevisionView(RevisionVM.RevisionsPageRefresh), PageTitle = "Создание ревизии для модели" });
			_pages.Add("ShowRevisionBomView", new PageInfo { View = new View.RevisionBOMView(RevisionVM.SlotsPageRefresh), PageTitle = "Слоты ревизии" });

            _pages.Add("ShowParameterTypeView", new PageInfo { View = new View.ParameterTypesView(ModelParametersVM.ParameterTypesRefresh), PageTitle = "Список типов параметров", HelperWindow = new ParameterTypesHelper(true) });
            _pages.Add("ShowModelParameterView", new PageInfo { View = new View.ModelParameterView(ModelParametersVM.ModelParametersRefresh), PageTitle = "Параметры моделей", HelperWindow = new ModelParametersHelper(true) });

            _pages.Add("RouteMapView", new PageInfo { View = new View.RouteMapView(RouteMapVM.RouteMapRefresh), PageTitle = "Маршруты и операции", HelperWindow = new RouteMapHelper(true) });
			_pages.Add("ShowOperationStatusView", new PageInfo { View = new View.OperationStatusView(OperationStatusVM.OperationStatusRefresh), PageTitle = "Статусы операций" });
			_pages.Add("ShowOperationView", new PageInfo { View = new View.OperationView(RouteMapVM.OperationRefresh), PageTitle = "Список доступных операций" });

			_pages.Add("ShowEmployeeView", new PageInfo { View = new View.EmployeeView(EmployeeVM.EmployeeRefresh), PageTitle = "Сотрудники" });
			_pages.Add("ShowWorkingGroupView", new PageInfo { View = new View.WorkingGroupView(EmployeeVM.WorkingGroupRefresh), PageTitle = "Создание рабочих групп" });
			_pages.Add("ShowRoleView", new PageInfo { View = new View.RoleView(RoleVM.RoleRefresh), PageTitle = "Роли/должности" });

			_pages.Add("ShowNominalConverterView", new PageInfo { View = new View.NominalConverterView(), PageTitle = "Конвертер номиналов" });		

			_pages.Add("ShowAccessorLevelPage", new PageInfo { View = new View.UserAccessorLevelView(UserAccessorLevelVM.UserRefresh), PageTitle="Настройка уровня доступа" });
			_pages.Add("ShowDeveloperPage", new PageInfo { View = new View.DeveloperView(), PageTitle="Инициализация БД" });
            _pages.Add("ShowErrorLogPage", new PageInfo { View = new View.ErrorLogView(ErrorLogVM.ErrorLogtRefresh), PageTitle="Список обработанных ошибок"});
            #endregion

			_pages.Add("Authorization", new PageInfo { View = new View.AuthorizationView() });

			_pages.Add("ShowProducts", new PageInfo { View = new View.ProductView(ProductVM.ProductRefresh) });
			_pages.Add(TextHelper.ProductOperationsViewKey, new PageInfo { View = new View.ProductOperationsView(ProductOperationsVM.ProductOperationsRefresh) });
            _pages.Add(TextHelper.MultiProductOperationViewKey, new PageInfo { View = new View.MultiProductOperationView(MultiProductOperationsVM.MultiProductOperationsRefresh) });
            _pages.Add("ShowSelectProductsForMultiOperationView", new PageInfo { View = new View.SelectProductsForMultiOperationView(MultiProductOperationsVM.SelectProductsForMultiOperationRefresh) });
            _pages.Add(TextHelper.ProductParametersViewKey, new PageInfo { View = new View.ProductParametersView(ProductParametersVM.ProductParametersRefresh), HelperWindow = new ProductParametersHelper(true) });

            _pages.Add("ShowInstallationProductView", new PageInfo { View = new View.InstallationProductView(InstallationProductVM.InstallationProductRefresh) });
			_pages.Add("ShowProductParentInstallationView", new PageInfo { View = new View.InstallationProductView(InstallationProductVM.ProductParentRefresh) });

            _pages.Add(TextHelper.ProductLogViewKey, new PageInfo { View = new View.ProductLogView(ProductLogVM.ProductLogRefresh), PageTitle = "История операций изделий" });
            _pages.Add(TextHelper.SelectedProductLogViewKey, new PageInfo { View = new View.ProductLogView() });
            _pages.Add("ShowProductBomLogView", new PageInfo { View = new View.ProductBomLogView(ProductBomLogVM.ProductBomLogRefresh), PageTitle = "История монтажа/демонтажа изделий" });
        }

		public PageInfo GetPage(string code, string PageTitle)
		{
			if (!_pages.ContainsKey(code))
				throw new ArgumentException(TextHelper.NullPageCodeThrow(code));

			if (!string.IsNullOrEmpty(PageTitle))
				_pages[code].PageTitle = PageTitle;
			return _pages[code];
		}

		//Возвращает список тайтлов страниц, у которых есть хелпер
		public string GetHelpersInfo() =>
			string.Join("\n", _pages.Where(page => page.Value.HelperWindow != null).Select(page => page.Value.PageTitle));
        
	}
}
