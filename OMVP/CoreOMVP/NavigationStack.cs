﻿using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.CoreOMVP
{
	public class NavigationStack
	{
		public static Stack<string> Keys { get; set; } = new Stack<string>();
		public static Stack<string> PageTitles { get; set; } = new Stack<string>();
		public static Stack<Revision> SelectedRevisions { get; set; } = new Stack<Revision>();
		public static Stack<Product> SelectedProducts { get; set; } = new Stack<Product>();

        public static void Push(string key, string title, Revision selectedRevision, Product selectedProduct)
		{
			Keys.Push(key);
			PageTitles.Push(title);
			SelectedRevisions.Push(selectedRevision);
            SelectedProducts.Push(selectedProduct);
        }
		public static void Pop()
		{
			Keys?.Pop();
			PageTitles?.Pop();
			SelectedRevisions?.Pop();
            SelectedProducts?.Pop();
        }

		public static void Clear()
		{
			Keys.Clear();
			PageTitles.Clear();
			SelectedRevisions.Clear();
            SelectedProducts.Clear();
        }

		public static Revision GetCurrentRevision() => SelectedRevisions.Peek();
		public static Product GetCurrentProduct() => SelectedProducts.Peek();
        public static bool CurrentPageIsHomePage() => Keys?.Peek() == OmvpConstant.HomePage;
        public static string GetToStringStack()
        {
            string result = "{ \"Keys\":[";
            string back = "]}";
            foreach (string key in Keys)
            {
                result += " \"key\": {" + key.ToString() + "}";
            }
            result += back;

            return result;
        }
    }
}
