﻿using OMVP.BL.BLModels;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace OMVP.CoreOMVP
{
	public class PageNavigation
	{
		public delegate void ChangeView(string key, string PageTitle, Revision selectedRevision, Product selectedProduct, bool refreshContext = true, bool isNavigationBack = false);
		public static event ChangeView Change;

        public NavigationStack NavigationStack { get; set; }

		public static void InitHomePage(string key, string pageTitle, Revision selectedRevision)
		{
            NavigationStack.Push(key, pageTitle, selectedRevision, selectedProduct: null);
            Change?.Invoke(key, pageTitle, selectedRevision, selectedProduct: null);
		}

		public static void LoadNextPage(string key, string pageTitle = "", Revision selectedRevision = null, Product selectedProduct = null, bool refreshContext = true)
		{

            NavigationStack.Push(key, pageTitle, selectedRevision, selectedProduct);
			ProductBL.Instance.CurrentRevision = NavigationStack.GetCurrentRevision(); //Refresh command-bar
			Change?.Invoke(key, pageTitle, selectedRevision, selectedProduct, refreshContext);
        }
        public static void LoadPreviusPage(bool refreshSelectedRevision = false)
		{
            NavigationStack.Pop();
			ProductBL.Instance.CurrentRevision = NavigationStack.GetCurrentRevision(); //Refresh command-bar 
            
            Change?.Invoke(NavigationStack.Keys?.Peek(), NavigationStack.PageTitles.Peek(), NavigationStack.SelectedRevisions.Peek(), NavigationStack.SelectedProducts.Peek(), isNavigationBack: true);

            if (refreshSelectedRevision)
            {
                RevisionBL.Instance.SelectedRevisionRefreshQuery();
            } //Приведение ревизии, взятой из NavigationStack, к текущему контексту
        }

		public static void LoadHomePage(string key, string PageTitle)
		{
            NavigationStack.Clear();
			InitHomePage(key, PageTitle, selectedRevision: null);
		}
	}
}
