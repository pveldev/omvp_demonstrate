﻿using OMVP.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace OMVP.CoreOMVP
{
	public class BaseView : UserControl
	{
		public void SetContext(Action<BaseVM, bool> refresh)
		{
			(DataContext as BaseVM).Refresh = refresh;
		}

		public void Refresh(bool isNavigationBack = false)
		{
			(DataContext as BaseVM).CallRefresh(isNavigationBack);
		}
	}
}
