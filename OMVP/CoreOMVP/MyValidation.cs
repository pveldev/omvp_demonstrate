﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace OMVP.CoreOMVP
{
    public class MyValidation
    {
        public static bool IsNumber(string str) => new Regex("[^0-9]+").IsMatch(str);

        public static bool IsFloatNumber(string str)
        {
            str = str.Replace(",", ".");
            return new Regex(@"^-?[0-9][0-9,\.]+$").IsMatch(str);     
        }

        public static bool IsSerialNumberWithoutSpaces(string str)
        {
            str = str.Replace(" ", string.Empty);
            return new Regex(@"[^0-9\\s]+").IsMatch(str);
        }
		public static bool IsSerialNumber(string str) => new Regex(@"[^0-9\*]+").IsMatch(str);
        public static bool IsKRPG(string str) => new Regex(@"[^0-9\.]+").IsMatch(str);
        public static bool IsLetter(string str)
        {
            str = str.Replace(",", ".")
                     .Replace(" ", string.Empty);

            return new Regex(@"[^0-9\.]+").IsMatch(str);
        }
    }
}
