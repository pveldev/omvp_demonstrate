﻿using OMVP.DAL;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OMVP.CoreOMVP
{
    public class WindowHelper
    {
        public static bool CloseWindow(object parameter)
        {
            if (parameter != null && parameter is Window window)
            {
                window.Close();
                return true;
            }
            else
            {
                //Esc-spam leads here
                return false;
            }
        }
    }
}