﻿using Microsoft.WindowsAPICodePack.Dialogs;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.VBA;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using OMVP.DAL.PartialModels;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OMVP.CoreOMVP
{
    public static class ExcelExporter
    {
        #region public
        public static void FullExportProductWithOperations(IEnumerable<Product> products, IEnumerable<RouteMapProductOperation> productOpeartions)
        {
            if (products?.Any() ?? false)
            {
                Product firstProduct = products.First();
                string folderPath = StartSelectFolderDialog();
                string fileName = GetUniqueFileName(firstProduct);
                string worksheetHeader = FormWorksheetDescription(firstProduct);
                string worksheetName = firstProduct.Revision.Model.Name;

                FileHelper.OutputDir = new DirectoryInfo(folderPath);
                FileInfo fileInfo = FileHelper.GetFileInfo(fileName, false);
                ExportProductWithOperations(products, productOpeartions, fileInfo, folderPath, worksheetHeader, worksheetName);
            }
        } // Подготовка параметров для экспорта (для ExportProducts)
        public static void ExportProductWithOperations(IEnumerable<Product> products, IEnumerable<RouteMapProductOperation> productOpeartions, OrderElement selectedOrderElement, int? selectedYear)
        {
            if (products?.Any() ?? false)
            {
                string month = selectedOrderElement != null ? CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(selectedOrderElement.Month) : "-";
                string year = selectedYear != null ? selectedYear.ToString() : "-";

                Product firstProduct = products.First();
                string folderPath = StartSelectFolderDialog();
                string fileName = GetUniqueFileName(firstProduct, month, year);
                string worksheetHeader = FormWorksheetDescription(firstProduct, selectedOrderElement);
                string worksheetName = firstProduct.Revision.Model.Name;

                FileHelper.OutputDir = new DirectoryInfo(folderPath);
                FileInfo fileInfo = FileHelper.GetFileInfo(fileName, false);
                ExportProductWithOperations(products, productOpeartions, fileInfo, folderPath, worksheetHeader, worksheetName);
            }
        }// Подготовка параметров для экспорта (для ExportProducts)

        public static void FullExportProductsWithCurrentOperation(IEnumerable<Product> products)
        {
            if (products?.Any() ?? false)
            {
                Product firstProduct = products.First();
                string folderPath = StartSelectFolderDialog();
                string fileName = GetUniqueFileName(firstProduct, ".xlsx");
                string fullPath = folderPath + fileName;
                string worksheetHeader = FormWorksheetDescription(firstProduct);
                string worksheetName = firstProduct.Revision.Model.Name;

                ExportProducts(products, firstProduct.Revision.Model.HasLetter, fullPath, folderPath, worksheetHeader, worksheetName);
            }
        } // Подготовка параметров для экспорта (для ExportProducts)
        public static void ExportProductsWithCurrentOperation(IEnumerable<Product> products, OrderElement selectedOrderElement, int? selectedYear)
        {
            if (products?.Any() ?? false)
            {
                string month = selectedOrderElement != null ? CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(selectedOrderElement.Month) : "-";
                string year = selectedYear != null ? selectedYear.ToString() : "-";

                Product firstProduct = products.First();
                string folderPath = StartSelectFolderDialog();
                string fileName = GetUniqueFileName(products.First(), month, year, ".xlsx");
                string fullPath = folderPath + fileName;
                string worksheetHeader = FormWorksheetDescription(firstProduct, selectedOrderElement);
                string worksheetName = firstProduct.Revision.Model.Name;

                ExportProducts(products, firstProduct.Revision.Model.HasLetter, fullPath, folderPath, worksheetHeader, worksheetName);
            }
        }// Подготовка параметров для экспорта (для ExportProducts)
        #endregion

        private static void ExportProducts(IEnumerable<Product> products, bool hasLetter, string fullPath, string folderPath, string worksheetHeader, string worksheetName)
        {
            try
            {
                using (var package = new ExcelPackage(File.Open(fullPath, FileMode.CreateNew)))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(worksheetName);

                    int startCol = 1, startRow = 2; //["A2"]
                    #region Шапка 
                    worksheet.Cells[startCol, startRow - 1].Value = worksheetHeader;

                    int headerCouner = 0;
                    CreateHeadLine("Номер изделя", worksheet, startCol + headerCouner++, startRow);
                    if (hasLetter)
                        CreateHeadLine("Литерность", worksheet, startCol + headerCouner++, startRow);
                    CreateHeadLine("Текущая операция", worksheet, startCol + headerCouner++, startRow);
                    CreateHeadLine("Готовность", worksheet, startCol + headerCouner++, startRow);
                    CreateHeadLine("Год отгрузки", worksheet, startCol + headerCouner++, startRow);
                    CreateHeadLine("Месяц отгрузки", worksheet, startCol + headerCouner++, startRow);
                    CreateHeadLine("Настройщик", worksheet, startCol + headerCouner++, startRow);
                    CreateHeadLine("Статус изделия", worksheet, startCol + headerCouner++, startRow);
                    CreateHeadLine("Описание", worksheet, startCol + headerCouner++, startRow);
                    #endregion
                    #region Данные
                    int rowIndex = 1, colIndex = 1;
                    ExcelRange cell;
                    products.ToList().ForEach(product =>
                    {
                        colIndex = 0;

                        //Номер изделия
                        cell = worksheet.Cells[startRow + rowIndex, startCol + colIndex++];
                        int numberLength = product.SerialNumber.Count();
                        if (numberLength >= 4)
                        {
                            cell.Value = product.SerialNumber.Substring(0, numberLength - 4);
                            cell.Style.Font.Size = 10;

                            ExcelRichText uniquePart = cell.RichText.Add(product.SerialNumber.Substring(numberLength - 4, 4));
                            uniquePart.Size = 12;
                            uniquePart.Bold = true;
                        }
                        else
                        {
                            cell.Value = product.SerialNumber;
                            cell.Style.Font.Bold = true;
                            cell.Style.Font.Size = 12;
                        }

                        //Литерность
                        if (hasLetter)
                        {
                            cell = worksheet.Cells[startRow + rowIndex, startCol + colIndex++];
                            cell.Value = product.Letter;
                        }

                        //Текущая операция
                        cell = worksheet.Cells[startRow + rowIndex, startCol + colIndex++];
                        cell.Value = product.Operation.Name;

                        //Готовность
                        cell = worksheet.Cells[startRow + rowIndex, startCol + colIndex++];
                        cell.Value = product.Progress;
                        cell.Style.Numberformat.Format = "#0%";

                        //Год отгрузки
                        if (product.OrderElement != null && product.OrderElement.OrderTree != null && product.OrderElement.OrderTree.Order != null && product.OrderElement.OrderTree.Order.ExecutionDate != null)
                        {
                            cell = worksheet.Cells[startRow + rowIndex, startCol + colIndex];
                            cell.Value = product.OrderElement.OrderTree.Order.ExecutionDate.Year;
                        }
                        colIndex = colIndex + 1;

                        //Месяц отгрузки
                        if (product.OrderElement != null)
                        {
                            cell = worksheet.Cells[startRow + rowIndex, startCol + colIndex];
                            cell.Value = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(product.OrderElement.Month);
                        }
                        colIndex++;

                        //Настройщик
                        cell = worksheet.Cells[startRow + rowIndex, startCol + colIndex++];
                        cell.Value = product.Engineer?.LastName + " " + product.Engineer?.FirstName;

                        //Статус изделия
                        cell = worksheet.Cells[startRow + rowIndex, startCol + colIndex++];
                        cell.Value = product.ProductStatus.Name;

                        //Описание
                        cell = worksheet.Cells[startRow + rowIndex, startCol + colIndex];
                        cell.Value = product.Description;

                        rowIndex++;
                    });
                    #endregion
                    #region Блок_стилистический
                    worksheet.Cells[startRow - 1, startCol, startRow - 1, startCol + colIndex].Merge = true;

                    ExcelRange workRange = worksheet.Cells[startRow, startCol, startRow + rowIndex, startCol + colIndex]; //Выбор всей рабочей сетки
                    workRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    worksheet.Cells[startRow, startCol, startRow, startCol + colIndex].AutoFilter = true;

                    // set some document properties
                    package.Workbook.Properties.Title = "Отчет";
                    package.Workbook.Properties.Author = "Робот";
                    package.Workbook.Properties.Comments = "Этот файл был сформирован автоматически из приложения OMVP с помощью библиотеки EPPlus";
                    package.Workbook.Properties.Company = "Исток им. Шокина";

                    #endregion
                    #region Блок_завершения_экспорта
                    worksheet.Cells.AutoFitColumns();
                    package.Save();

                    package.Stream.Close();
                    Process.Start(folderPath);
                    #endregion
                }
            }
            catch (Exception ex)
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog(ex));
                Dialog.ShowError(ex.Message);
            }
        } //Метод экспорта
        private static void ExportProductWithOperations(IEnumerable<Product> products, IEnumerable<RouteMapProductOperation> allProductOperations, FileInfo fileInfo, string folderPath, string worksheetHeader, string worksheetName)
        {
            try
            {
                IEnumerable<RouteMap> routeMaps = products.Select(product => product.RouteMap).Distinct(); //Определяем коллекцию маршрутных карт среди коллекции изделий

                using (ExcelPackage package = new ExcelPackage(fileInfo))
                {
                    package.Workbook.CreateVBAProject();
                    package.Workbook.VbaProject.Protection.SetPassword(< скрыто >);

                    foreach (RouteMap routeMap in routeMaps)
                    {
                        List<Product> currentProducts = new List<Product>();
                        if (routeMap == null)
                        {
                            currentProducts = products.Where(product => product.RouteMapId == null).ToList();
                        }
                        else
                        {
                            currentProducts = products.Where(product => product.RouteMapId == routeMap.Id).ToList();
                        }

                        List<int> currentProductsIds = currentProducts.Select(product => product.Id).ToList();

                        List<RouteMapProductOperation> genericRouteMap = allProductOperations.Where(rmpo => currentProductsIds.Contains(rmpo.ProductId)).Distinct(new RouteMapProductOperationComparer()).OrderBy(productOperation => productOperation.Priority).ToList();

                        string routeMapName = routeMap != null ? routeMap.Name : "<Без МК>";

                        ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(worksheetName + " " + routeMapName);
                        int startCol = 1, startRow = 2; //["A2"]
                        #region Шапка 
                        worksheet.Cells[startCol + 1, startRow].Value = worksheetHeader;
                        int columnOffset = 0; //Количество колонок, что уже "заняты"

                        CreateHeadLine("Номер изделя", worksheet, startCol + columnOffset++, startRow);
                        CreateHeadLine("Статус", worksheet, startCol + columnOffset++, startRow);
                        CreateHeadLine("Год отгрузки", worksheet, startCol + columnOffset++, startRow);
                        CreateHeadLine("Месяц отгрузки", worksheet, startCol + columnOffset++, startRow);

                        for (int i = columnOffset; i < genericRouteMap.Count + columnOffset; i++)                    
                            CreateOperationHeadLine(genericRouteMap[i - columnOffset], worksheet, startCol + i, startRow);
                    
                        CreateHeadLine("Описание", worksheet, startCol + genericRouteMap.Count + columnOffset, startRow);
                        #endregion
                        #region Данные
                        int rowIndex = 1, colIndex = 1;
                        ExcelRange cell;
                        currentProducts.ToList().ForEach(product =>
                        {
                            //Номер изделия
                            colIndex = 0;
                            cell = worksheet.Cells[startRow + rowIndex, startCol + colIndex++];
                            int numberLength = product.SerialNumber.Count();
                            if (numberLength >= 4)
                            {
                                cell.Value = product.SerialNumber.Substring(0, numberLength - 4);
                                cell.Style.Font.Size = 10;

                                ExcelRichText uniquePart = cell.RichText.Add(product.SerialNumber.Substring(numberLength - 4, 4));
                                uniquePart.Size = 12;
                                uniquePart.Bold = true;
                            }
                            else
                            {
                                cell.Value = product.SerialNumber;
                                cell.Style.Font.Bold = true;
                                cell.Style.Font.Size = 12;
                            }

                            //Статус изделия
                            cell = worksheet.Cells[startRow + rowIndex, startCol + colIndex++];
                            cell.Value = product.ProductStatus.Name;

                            //Год отгрузки
                            if (product.OrderElement != null && product.OrderElement.OrderTree != null && product.OrderElement.OrderTree.Order != null && product.OrderElement.OrderTree.Order.ExecutionDate != null)
                            {
                                cell = worksheet.Cells[startRow + rowIndex, startCol + colIndex];
                                cell.Value = product.OrderElement.OrderTree.Order.ExecutionDate.Year;
                            }
                            colIndex = colIndex + 1;

                            //Месяц отгрузки
                            if (product.OrderElement != null)
                            {
                                cell = worksheet.Cells[startRow + rowIndex, startCol + colIndex];
                                cell.Value = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(product.OrderElement.Month);
                            }
                            colIndex++;

                            List<RouteMapProductOperation> currentproductOperations = allProductOperations.Where(rmpo => currentProductsIds.Contains(rmpo.ProductId)).Where(productOperation => productOperation.ProductId == product.Id).ToList();

                            foreach (RouteMapProductOperation item in currentproductOperations)
                            {
                                int currentIndex = genericRouteMap.IndexOf(genericRouteMap.First(productOperation => productOperation.OperationId == item.OperationId && productOperation.Priority == item.Priority)); 
                                cell = worksheet.Cells[startRow + rowIndex, startCol + currentIndex + columnOffset];
                                cell.Value = item.EndDate;
                                cell.Style.Numberformat.Format = "dd.mm";
                            }

                            cell = worksheet.Cells[startRow + rowIndex, startCol + columnOffset + genericRouteMap.Count()];
                            cell.Value = product.Description;

                            rowIndex++;
                        });
                        #endregion
                        #region Макросы 
                        CreateMacros(package.Workbook, worksheet.Index, GetUnscheduledProductOperationColumns(genericRouteMap, columnOffset));
                        #endregion
                        #region Блок_стилистический
                        CreateButtonStyle(worksheet.Cells[1, 1], "Скрыть/Показать");
                        worksheet.Cells[startRow - 1, startCol + 1, startRow - 1, startCol + 1 + colIndex].Merge = true;
                        worksheet.View.FreezePanes(startRow + 1, startCol + 1);

                        ExcelRange workRange = worksheet.Cells[startRow, startCol, startRow + rowIndex - 1, startCol + colIndex + genericRouteMap.Count()]; //Выбор всей рабочей сетки
                        workRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        workRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        worksheet.Cells[startRow, startCol, startRow, startCol + colIndex].AutoFilter = true;

                        // set some document properties
                        package.Workbook.Properties.Title = "Отчет";
                        package.Workbook.Properties.Author = "bot";
                        package.Workbook.Properties.Comments = "Этот файл был сформирован автоматически из приложения OMVP с помощью библиотеки EPPlus";
                        package.Workbook.Properties.Company = "Исток им. Шокина";

                        #endregion
                        worksheet.Cells.AutoFitColumns();
                    }

                    #region Блок_завершения_экспорта
                    package.SaveAs(fileInfo);
                    Process.Start(folderPath);
                    #endregion
                }
            }
            catch (Exception ex)
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog(ex));
                Dialog.ShowError(ex.Message);
            }
        } //Метод экспорта изделий с его МК

        private static string StartSelectFolderDialog()
        {
            string defaultDirectory = "D:\\";

            CommonOpenFileDialog dlg = new CommonOpenFileDialog
            {
                Title = "Выберите путь для формирования графика-отчета",
                IsFolderPicker = true,
                InitialDirectory = defaultDirectory,

                AddToMostRecentlyUsedList = false,
                AllowNonFileSystemItems = false,
                DefaultDirectory = defaultDirectory,
                EnsureFileExists = true,
                EnsurePathExists = true,
                EnsureReadOnly = false,
                EnsureValidNames = true,
                Multiselect = false,
                ShowPlacesList = true
            };

            if (dlg.ShowDialog() == CommonFileDialogResult.Ok && dlg.FileName != defaultDirectory)           
                return dlg.FileName+"\\";
            
            return defaultDirectory;
        }
        private static string GetUniqueFileName(Product product, string month, string year, string fileType = ".xlsm") => $"{DateTime.Now.ToString("yyyy-MM-dd_hhmmss")}_График по изделиям {product.Revision.Model.Name}_(месяц {month})_(год {year}){fileType}";
        private static string GetUniqueFileName(Product product, string fileType = ".xlsm") => $"{DateTime.Now.ToString("yyyy-MM-dd_hhmmss")}_Полный график по изделиям {product.Revision.Model.Name}{fileType}";

        private static string FormWorksheetDescription(Product product, OrderElement selectedOrderElement)
        {
            string monthInfo;
            string productInfo = $"Модель изделия: {product.Revision.Model.Name}";

            if (selectedOrderElement != null)
            {
                string month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(selectedOrderElement.Month);
                monthInfo = $"Месячный план({month}): {selectedOrderElement.Quantity} изделий. ";
            }
            else
            {
                monthInfo = "Выгрузка данных без выбора месяца. ";
            }

            return string.Concat(monthInfo, productInfo);
        }
        private static string FormWorksheetDescription(Product product) => $"Выгрузка по всем изделиям модели: {product.Revision.Model.Name}";
        private static void CreateHeadLine(string headName, ExcelWorksheet worksheet, int colCoord, int rowCoord)
        {           
            var cell = worksheet.Cells[rowCoord, colCoord];
            cell.Value = headName;
            cell.Style.Font.Bold = true;
        }
        private static void CreateOperationHeadLine(RouteMapProductOperation productOperation, ExcelWorksheet worksheet, int colCoord, int rowCoord)
        {
            var cell = worksheet.Cells[rowCoord, colCoord];
            cell.Value = productOperation.Operation.Name;
            cell.Style.Font.Bold = true;
            if (productOperation.IsAdditional)
            {
                cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                cell.Style.Fill.BackgroundColor.SetColor(Color.LightYellow);
                cell.AddComment("Дополнительная операция", "bot");
            }
            else
            {
                cell.Style.Fill.PatternType = ExcelFillStyle.MediumGray;
                cell.Style.Fill.BackgroundColor.SetColor(Color.LightGreen); //.SetColor(ColorTranslator.FromHtml("HEXCODE"));
                cell.AddComment("Операция, согласно маршруту", "bot");
            }
        }

        private static void CreateMacros(ExcelWorkbook workbook, int worksheetIndex, string unscheduledColums)
        {
            //workbook.CreateVBAProject();
            //workbook.VbaProject.Protection.SetPassword("<скрыто>");
            workbook.Worksheets[worksheetIndex].CodeModule.Code =
                GetVBA_BeforeDoubleClick_Code() + GetVBA_hideUnhide_Code() + GetVBA_getColumNumbers_Code(unscheduledColums);
        }

        private static string GetVBA_BeforeDoubleClick_Code() =>
            "Private Sub Worksheet_BeforeDoubleClick(ByVal Target As Range, Cancel As Boolean)\r\n" +
            "    If Target.Column = 1 And Target.Row = 1 Then\r\n" +
            "        Dim myCells As Collection\r\n" +
            "        Set myCells = getColumNumbers()\r\n" +
            "        For i = 1 To myCells.Count\r\n" +
            "            hide_unHide(myCells(i))\r\n" +
            "        Next i\r\n" +
            "    End If\r\n" +
            "End Sub\r\n";
        private static string GetVBA_hideUnhide_Code() =>
            "Private Sub hide_unHide(ByVal item As Integer)\r\n" +
            "    If Columns(item).EntireColumn.Hidden = True Then\r\n" +
            "            Columns(item).EntireColumn.Hidden = False\r\n" +
            "    Else\r\n" +
            "        Columns(item).EntireColumn.Hidden = True\r\n" +
            "    End If\r\n" +
            "End Sub\r\n";
        private static string GetVBA_getColumNumbers_Code(string unscheduledColums) =>
            "Private Function getColumNumbers() As Collection\r\n" +
            "    Dim getColumNumbers_ As New Collection\r\n" +
            unscheduledColums +
            "    Set getColumNumbers = getColumNumbers_\r\n" +
            "    Set getColumNumbers_ = Nothing\r\n" +
            "End Function\r\n";
        private static string GetUnscheduledProductOperationColumns(List<RouteMapProductOperation> genericRouteMap, int columnOffset)
        {
            string result = "";
            for (int i = 0; i < genericRouteMap.Count; i++)
            {
                if (genericRouteMap[i].IsAdditional)
                    result += $"    getColumNumbers_.Add({i + columnOffset + 1})\r\n";
            }          
            return result;
        }

        private static void CreateButtonStyle(ExcelRange range, string content)
        {
            range.Value = content;
            range.Style.Fill.Gradient.Color1.SetColor(Color.FromArgb(0xC3, 0xEC, 0XB9));
            range.Style.Fill.Gradient.Color2.SetColor(Color.FromArgb(0x44, 0xA5, 0X14));
        }
    } 
}