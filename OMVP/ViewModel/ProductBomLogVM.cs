﻿using OMVP.BL.BLModels;
using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace OMVP.ViewModel
{
    public class ProductBomLogVM : BaseVM
    {
        public ProductBomLogBL ProductBomLogBL { get; set; } = ProductBomLogBL.Instance;
        public RevisionBL RevisionBL { get; set; } = RevisionBL.Instance;
        

        public static readonly Action<BaseVM, bool> ProductBomLogRefresh = (vm, isNavBack) => { ((ProductBomLogVM)vm).GetProductBomLogData(); };

        public ProductBomLogVM()
        {
            LoadProductBomLogPageWithParametersCommand = new SmartCommand(window =>
            {
                ProductBomLogBL.GetProductBomLogWithParameters();
                WindowHelper.CloseWindow(window);
            });

            CancelProductBomLogParametersWindowCommand = new SmartCommand(window => { WindowHelper.CloseWindow(window); });

            ClearSelectedRevisionParameterCommand = new SmartCommand(p => { ProductBomLogBL.ClearSelectedRevisionParameter(); });
            ClearSelectedSlotParameterCommand = new SmartCommand(p => { ProductBomLogBL.ClearSelectedSlotParameter(); });
            ClearSelectedParentProductParameterCommand = new SmartCommand(p => { ProductBomLogBL.ClearSelectedParentProductParameter(); });
            ClearSelectedChildProductParameterCommand = new SmartCommand(p => { ProductBomLogBL.ClearSelectedChildProductParameter(); });
            ClearSelectedStartDateParameterCommand = new SmartCommand(p => { ProductBomLogBL.ClearSelectedStartDateParameter(); });
            ClearSelectedEndDateParameterCommand = new SmartCommand(p => { ProductBomLogBL.ClearSelectedEndDateParameter(); });
        }

        public void GetProductBomLogData()
        {
            RevisionBL.Revisions.Clear();
            SQLServer.Instance.Revision.GetSortedRevisions().ForEach(RevisionBL.Revisions.Add);
        }
    

        #region Commands
        public ICommand LoadProductBomLogPageWithParametersCommand { get; set; }
        public ICommand CancelProductBomLogParametersWindowCommand { get; set; }

        public ICommand ClearSelectedRevisionParameterCommand { get; set; }
        public ICommand ClearSelectedSlotParameterCommand { get; set; }
        public ICommand ClearSelectedParentProductParameterCommand { get; set; }
        public ICommand ClearSelectedChildProductParameterCommand { get; set; }
        public ICommand ClearSelectedStartDateParameterCommand { get; set; }
        public ICommand ClearSelectedEndDateParameterCommand { get; set; }
        #endregion
    }
}
