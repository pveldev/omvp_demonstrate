﻿using OMVP.BL.BLModels;
using OMVP.BL.Models;
using OMVP.Core;
using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace OMVP.ViewModel
{
	public class RevisionNavigationVM : BaseVM
	{
        public ModelBL ModelBL { get; set; } = ModelBL.Instance;
        public RevisionBL RevisionBL { get; set; } = RevisionBL.Instance;
		public ProductBL ProductBL { get; set; } = ProductBL.Instance;
		public MenuBL MenuBL { get; set; } = MenuBL.Instance;
		public OrderBL OrderBL { get; set; } = OrderBL.Instance; // ExcelExportWindow


        public static readonly Action<BaseVM, bool> ThemePageRefresh = async (vm, isNavBack) => { await ((RevisionNavigationVM)vm).GetThemeDataAsync(CodeDictionary.Instance.AuxiliaryModelType.TopLevelModel.Id); };
		public static readonly Action<BaseVM, bool> RevisionChildRefresh = async (vm, isNavBack) => { await ((RevisionNavigationVM)vm).GetChildDataAsync(RevisionBL.Instance.SelectedRevision); }; //Для модулей и узлов/компонентов

        public static readonly Action<BaseVM, bool> ExcelExporterRefresh = (vm, isNavBack) => { ((RevisionNavigationVM)vm).GetExcelExporterData(); };

        public RevisionNavigationVM()
        {
            #region KeyBindings
            ShowChildrenRevisionsCommand = new SmartCommand(_ =>
            {
                if (RevisionBL.SelectedRevision.RevisionBOMs1?.Any() ?? false)
                    PageNavigation.LoadNextPage("ShowRevisionBOMs", RevisionBL.GetCurrentPageTitle(), RevisionBL.SelectedRevision);
            }, p => RevisionBL.SelectedRevision != null, RevisionBL, new string[] { "SelectedRevision" });

            ShowProductsPage = CommandGenerator.Instance.FormExecuteCommandForShowProducts();

            #endregion

            #region ExportProductCommands
            #region ExportWithoutRouteMap
            ExportProductCommand = new SmartCommand(window =>
            {
                if (RevisionBL.FullExport)
                {
                    ExcelExporter.FullExportProductsWithCurrentOperation(SQLServer.Instance.Product.GetProductsForExcelExport(RevisionBL.Instance.SelectedRevision.Id));
                }
                else
                {
                    ExcelExporter.ExportProductsWithCurrentOperation
                    (
                        SQLServer.Instance.Product.GetProductsForExcelExport(RevisionBL.Instance.SelectedRevision.Id, RevisionBL.SelectedOrderElement?.Month, RevisionBL.SelectedYear),
                        RevisionBL.SelectedOrderElement,
                        RevisionBL.SelectedYear
                    );
                }

                WindowHelper.CloseWindow(window);
                RevisionBL.ClearExcelExportProps();

            }, p => RevisionBL.SelectedOrderElement != null || RevisionBL.SelectedYear != null || RevisionBL.FullExport, RevisionBL, new string[] { "SelectedOrderElement", "FullExport", "SelectedYear" });
            #endregion

            ExportProductsWithRouteCommand = new SmartCommand(window =>
            {
                if (RevisionBL.FullExport)
                {
                    ExcelExporter.FullExportProductWithOperations
                    (
                        SQLServer.Instance.Product.GetProductsForExcelExport(RevisionBL.Instance.SelectedRevision.Id),
                        SQLServer.Instance.RouteMapProductOperation.GetRouteMapProductOperationsByRevision(RevisionBL.SelectedRevision)
                    );
                }
                else
                {
                    ExcelExporter.ExportProductWithOperations
                    (
                        SQLServer.Instance.Product.GetProductsForExcelExport(RevisionBL.Instance.SelectedRevision.Id, RevisionBL.SelectedOrderElement?.Month, RevisionBL.SelectedYear),
                        SQLServer.Instance.RouteMapProductOperation.GetRouteMapProductOperationsByRevision(RevisionBL.SelectedRevision),
                        RevisionBL.SelectedOrderElement,
                        RevisionBL.SelectedYear
                    );
                }
                WindowHelper.CloseWindow(window);

            }, p => RevisionBL.SelectedOrderElement != null || RevisionBL.SelectedYear != null || RevisionBL.FullExport, RevisionBL, new string[] { "SelectedOrderElement", "FullExport", "SelectedYear" });

            CancelExportWindowCommand = new SmartCommand(window =>
            {
                WindowHelper.CloseWindow(window);
            });
            ClearSelectedOrderElementCommand = new SmartCommand(_ =>
            {
                RevisionBL.SelectedOrderElement = null;
            });
            ClearSelectedYearCommand = new SmartCommand(_ =>
            {
                RevisionBL.SelectedYear = null;
            });
            #endregion
        }

        public async Task GetThemeDataAsync(int modelTypeId)
        {
            MenuBL.Instance.StartLoadAnimation(0.99f, "Получение списка ревизий");

            RevisionBL.Revisions.Clear();
            (await SQLServer.Instance.Revision.GetRevisionsByModelTypeIdAsync(modelTypeId)).ForEach(RevisionBL.Revisions.Add);

            MenuBL.Instance.StopLoadAnimation();
        }

        public async Task GetChildDataAsync(Revision revision)
        {
            if (revision != null)
            {
                MenuBL.Instance.StartLoadAnimation(0.99f, "Получение списка ревизий-наследников");

                RevisionBL.Revisions.Clear();
                (await SQLServer.Instance.Revision.GetRevisionsChildrenAsync(revision.Id)).ForEach(RevisionBL.Revisions.Add);

                MenuBL.Instance.StopLoadAnimation();
            }
            else
            {
                //ToDo: вывести диалог с ошибкой
            }
		}
        public void GetExcelExporterData()
        {
            OrderBL.OrderElements.Clear();
            SQLServer.Instance.OrderElement.GetValidOrderElements(RevisionBL.SelectedRevision.Id).ForEach(OrderBL.OrderElements.Add);
            OrderBL.OrderElementsView = CollectionViewSource.GetDefaultView(OrderBL.OrderElements);
            OrderBL.OrderElementsView.Filter = item => FilterOrderElement(item, RevisionBL.SelectedYear);

            RevisionBL.DispatchYears = OrderBL.OrderElements.Select(oe => oe.OrderTree.Order.ExecutionDate.Year).Distinct().ToArray();
        }
        private bool FilterOrderElement(object item, int? selectedYear)
        {
            if (selectedYear != null && item is OrderElement orderElement)
            {
                return orderElement.OrderTree.Order.ExecutionDate.Year == selectedYear;
            }
            return true;
        }

        public ICommand ShowChildrenRevisionsCommand { get; set; }
		public ICommand ShowProductsPage { get; set; }

        #region ExportProductCommands
        public ICommand ExportProductCommand { get; set; } //Export with current operation only
        public ICommand ExportProductsWithRouteCommand { get; set; }
        public ICommand CancelExportWindowCommand { get; set; }
        public ICommand ClearSelectedOrderElementCommand { get; set; }
        public ICommand ClearSelectedYearCommand { get; set; }
        #endregion
    }
}