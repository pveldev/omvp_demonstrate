﻿using OMVP.BL.BLModels;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OMVP.ViewModel
{
	public class RouteMapVM : BaseVM
	{
		public RevisionBL RevisionBL { get; set; } = RevisionBL.Instance;
		public RouteMapBL RouteMapBL { get; set; } = RouteMapBL.Instance;
		public EmployeeBL EmployeeBL { get; set; } = EmployeeBL.Instance; //OperationView.xaml
        public RoleBL RoleBL { get; set; } = RoleBL.Instance; //OperationView.xaml
        public ModelBL ModelBL { get; set; } = ModelBL.Instance; //OperationView.xaml
        public MenuBL MenuBL { get; set; } = MenuBL.Instance; //OperationView.xaml

		public AuthorizationModel AuthorizationModel { get; set; } = AuthorizationModel.Instance;

		public static readonly Action<BaseVM, bool> RouteMapRefresh = (vm, isNavBack) => { ((RouteMapVM)vm).GetRouteMapData(); };
		public static readonly Action<BaseVM, bool> OperationRefresh = (vm, isNavBack) => { ((RouteMapVM)vm).GetOperationData(); };

		public RouteMapVM()
		{
            AddNewOperationToRMOsCommand = new SmartCommand(p =>
            {
                RouteMapBL.AddNewOperationToRouteMapOperations();
            }, p => RouteMapBL.SelectedRevision != null && RouteMapBL.SelectedPriority != 0 && !string.IsNullOrEmpty(RouteMapBL.NewOperationName), RouteMapBL, new string[] { "SelectedPriority", "NewOperationName" });

            AddOperationToRMOList = new SmartCommand(p =>
			{
				RouteMapBL.AddOperationToRMOList();
			}, p => RouteMapBL.SelectedPriority != 0 && RouteMapBL.SelectedOperation != null, RouteMapBL, new string[] { "SelectedPriority", "SelectedOperation" });

            SaveNewRouteMapCommand = new SmartCommand(p =>
			{
				if (CanAddRouteMapOperations())
					RouteMapBL.SaveNewRouteMap();
				else
					Dialog.ShowWarning(TextHelper.CreateNewRouteMapWarningMessage(  RouteMapBL.SelectedRevision.Name, 
                                                                                    TextHelper.ListToString(RouteMapBL.SelectedRevision.RevisionBOMs1.Select(bom => bom.Name), "\n")));
            }, p => RouteMapBL.NewRouteMapOperations != null && !string.IsNullOrEmpty(RouteMapBL.NewRouteName) && RouteMapBL.SelectedRevision != null, RouteMapBL, new string[] { "NewRouteMapOperations", "NewRouteName", "SelectedRevision" });

            DeleteRouteMapCommand = new SmartCommand(p =>
            {
                RouteMapBL.DeleteSelectedRouteMap();
            }, p => RouteMapBL.SelectedRouteMap != null && AccessorLevelManager.AccessorLevelCheck("CanDeleteRouteMap", AuthorizationModel.AuthorizedUser.AccessorLevel), RouteMapBL, new string[] { "SelectedRouteMap" });

            AddOperation = new SmartCommand(p =>
			{
				RouteMapBL.AddNewOperation();
            }, p => !string.IsNullOrEmpty(RouteMapBL.NewOperationName), RouteMapBL, new string[] { "NewOperationName" });

            ArchiveSelectedRouteMapCommand = new SmartCommand(_ =>
			{
			    RouteMapBL.ArchiveSelectedRouteMap();
            }, p => RouteMapBL.SelectedRouteMap != null && AccessorLevelManager.AccessorLevelCheck("CanArchiveRouteMap", AuthorizationModel.AuthorizedUser.AccessorLevel), RouteMapBL, new string[] { "SelectedRouteMap" });


            ArchiveSelectedOperationCommand = new SmartCommand(parameter =>
			{
				if (parameter is Operation operation)
				{
					RouteMapBL.ArchiveOperation(operation);
				} else if (parameter is null)
				{
					Dialog.ShowError(TextHelper.SelectedOperationIsNull);
				}
				else
				{
					Dialog.ShowError(TextHelper.ArchiveSelectedOperationError);
				}
			});

            ClaimSelectedRouteMapCommand = new SmartCommand(_=>
            {
                RouteMapBL.ClaimSelectedRouteMap();
            }, p => RouteMapBL.SelectedRouteMap != null && !RouteMapBL.SelectedRouteMap.IsClaimed, RouteMapBL, new string[] { "SelectedRouteMap" });

            LoadGeneralOperationsCommand = new SmartCommand(_ =>
            {
                RouteMapBL.LoadGeneralOperations();
            });

            #region Clear_commands
            ClearSelectedModelCommand = new SmartCommand(p => { RouteMapBL.ClearNewOperationModel(); });
            ClearSelectedRoleCommand = new SmartCommand(p => { RouteMapBL.ClearNewOperationRole(); });
            ClearSelectedWorkingGroupCommand = new SmartCommand(p => { RouteMapBL.ClearNewOperationWorkingGroup(); });           
            #endregion
        }

        #region RefreshMethods
        public void GetRouteMapData()
		{
            RouteMapBL.RouteMapList.Clear();
            SQLServer.Instance.RouteMap.GetSortedRouteMaps().ForEach(RouteMapBL.RouteMapList.Add);

            RevisionBL.Revisions.Clear();
            SQLServer.Instance.Revision.GetSortedRevisions().ForEach(RevisionBL.Revisions.Add);

            GetOperationData();

            RouteMapBL.RefreshPriorityList();

            RouteMapBL.SelectedOperation = null;
			RouteMapBL.SelectedPriority = default;
			RouteMapBL.SelectedRevision = null;
			RouteMapBL.SelectedRouteMap = null;
		}

        public void GetOperationData()
        {
            RouteMapBL.Operations.Clear();

            EmployeeBL.AllWorkingGroups.Clear();
            SQLServer.Instance.WorkingGroup.GetSortedNotArchiveWorkingGroups().ForEach(EmployeeBL.AllWorkingGroups.Add);

            RoleBL.Roles.Clear();
            SQLServer.Instance.Role.GetRoles().ForEach(RoleBL.Roles.Add);

            ModelBL.ModelList.Clear();
            SQLServer.Instance.Model.GetSortedModels().ForEach(ModelBL.ModelList.Add);
        }
        #endregion
        #region CheckRouteMapOperationsMethods
        /*
		 * Проверяет, есть ли у ревизии БОМы
		 * Если есть, то проверяет марштур на наличие операции "комплектация"
		 *	 (При этом "комплектация" должны быть всего одна в маршруте)
		 */
        private bool CanAddRouteMapOperations()	=> RevisionHasBoms() ? RouteMapOperationsHasOneEquipment() : true;		
		private bool RevisionHasBoms() => RouteMapBL.SelectedRevision.RevisionBOMs1?.Any() ?? false;
		private bool RouteMapOperationsHasOneEquipment() =>
			RouteMapBL.NewRouteMapOperations.SingleOrDefault(routeMapOpeartion => routeMapOpeartion.OperationId == CodeDictionary.Instance.AuxiliaryOperation.Equipment.Id) != null;
		private string ConcatenateBomNames() => string.Join("\n", RouteMapBL.SelectedRevision.RevisionBOMs1.Select(bom => bom.Name));
		#endregion
		#region Commands
		public ICommand AddOperationToRMOList { get; set; }
		public ICommand AddNewOperationToRMOsCommand { get; set; }       
        public ICommand SaveNewRouteMapCommand { get; set; }
		public ICommand AddOperation { get; set; }
		public ICommand ArchiveSelectedRouteMapCommand { get; set; }
		public ICommand DeleteRouteMapCommand { get; set; }
		public ICommand ArchiveSelectedOperationCommand { get; set; }
		public ICommand ClaimSelectedRouteMapCommand { get; set; }
        
        public ICommand ClearSelectedModelCommand { get; set; }
        public ICommand ClearSelectedRoleCommand { get; set; }
        public ICommand ClearSelectedWorkingGroupCommand { get; set; }

        public ICommand LoadGeneralOperationsCommand { get; set; }        
        #endregion
    }
}
//ToDo: везде, где есть списки операций - прикрутить конвертер, добавляющий суффикс: op.Name + `[ModelName]`
