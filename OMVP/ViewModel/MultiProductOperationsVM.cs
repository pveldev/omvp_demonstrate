﻿using OMVP.BL.BLModels;
using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace OMVP.ViewModel
{
    public class MultiProductOperationsVM : BaseVM
    {
        public MenuBL MenuBL { get; set; } = MenuBL.Instance;
        public AuthorizationModel AuthorizationModel { get; set; } = AuthorizationModel.Instance;
        public MultiProductBL MultiProductBL { get; set; } = MultiProductBL.Instance;
        public ProductOperationBL ProductOperationBL { get; set; } = ProductOperationBL.Instance;
        public RevisionBL RevisionBL { get; set; } = RevisionBL.Instance;
        public bool CanExecuteGroup { get; set; }

        public static readonly Action<BaseVM, bool> MultiProductOperationsRefresh = (vm, isNavBack) => { ((MultiProductOperationsVM)vm).GetProductOperationsData(); };
        public static readonly Action<BaseVM, bool> SelectProductsForMultiOperationRefresh = (vm, isNavBack) => { ((MultiProductOperationsVM)vm).GetProductsData(); };


        public MultiProductOperationsVM()
        {
            #region Operation_Commands
            GroupStartOperation = new SmartCommand(_ =>
            {
                PageNavigation.LoadNextPage("ShowSelectProductsForMultiOperationView", TextHelper.GroupStartOperationTitle(MultiProductBL.SelectedMultiOperation.Operation.Name));
                MultiProductBL.SelectedGroupAction = MultiProductBL.GroupActions.GroupStart;
                CanExecuteGroup = CanStartNextOperation();
            }, p => CanExecuteGroup, this, new string[] { "CanExecute" });
            GroupEndOperation = new SmartCommand(_ =>
            {
                PageNavigation.LoadNextPage("ShowSelectProductsForMultiOperationView", TextHelper.GroupEndOperationTitle(MultiProductBL.SelectedMultiOperation.Operation.Name));
                MultiProductBL.SelectedGroupAction = MultiProductBL.GroupActions.GroupEnd;
                CanExecuteGroup = CanStartNextOperation();
            });
            GroupCancelOperation = new SmartCommand(_ =>
            {
                PageNavigation.LoadNextPage("ShowSelectProductsForMultiOperationView", TextHelper.GroupCancelOperationTitle(MultiProductBL.SelectedMultiOperation.Operation.Name));
                MultiProductBL.SelectedGroupAction = MultiProductBL.GroupActions.GroupCancel;
                CanExecuteGroup = CanStartNextOperation();
            });
            GroupReturnToSelectedOperation = new SmartCommand(_ =>
            {
                PageNavigation.LoadNextPage("ShowSelectProductsForMultiOperationView", TextHelper.GroupReturnToOperationTitle(MultiProductBL.SelectedMultiOperation.Operation.Name));
                MultiProductBL.SelectedGroupAction = MultiProductBL.GroupActions.GroupReturn;
                CanExecuteGroup = CanStartNextOperation();
            }, p => CanExecuteGroup, this, new string[] { "CanExecute" });
            ExecuteSelectedGroupOperation = new SmartCommand(_ =>
            {
                if (MultiProductBL.Instance.ExecuteSelectedGroupOperation())
                    PageNavigation.LoadPreviusPage();
            }, p => MultiProductBL.SelectedMultiProducts != null && MultiProductBL.SelectedMultiProducts.Count != 0, MultiProductBL, new string[] { "SelectedMultiProducts" });
            #endregion
            ClearSelectedGroupExecuterIdCommand = new SmartCommand(parameter =>
            {
                if (parameter is ComboBox comboBox)
                {
                    comboBox.SelectedIndex = -1; // reset comboBox selected
                    MultiProductBL.ClearSelectedGroupExecuterId();
                }
                else
                {
                    Dialog.ShowError(TextHelper.ClearSelectedOperationExecuterError);
                }
            });
        }

        private void GetProductOperationsData()
        {
            MultiProductBL.SelectedMultiProduct = ProductBL.Instance.SelectedProduct; // Перекидываю SelectedProduct

            ProductOperationBL productOperationBL = ProductOperationBL.Instance;
            productOperationBL.CurrentProductOperationsList.Clear();
            SQLServer.Instance.RouteMapProductOperation.GetSortedRouteMapProductOperations(MultiProductBL.SelectedMultiProduct.Id).ForEach(productOperationBL.CurrentProductOperationsList.Add);

            ProductOperationBL.AllCurrentWorkingGroupEmployees = SQLServer.Instance.WorkingGroupEmployee.GetWorkingGroupEmployees
                (
                    ProductOperationBL.CurrentProductOperationsList
                        .Where(productOperation => productOperation.Operation.WorkingGroup != null)
                            .Select(productOperation => productOperation.Operation.WorkingGroupId).Distinct().ToArray()
                );

            CanExecuteGroup = CanStartNextOperation();
        }

        private void GetProductsData()
        {
            //Выборка изделий из ProductBL с той же текущей операцией
            MultiProductBL.MultiProducts.Clear();
            ProductBL.Instance.Products
                .Where(product => product.CurrentOperationId == MultiProductBL.SelectedMultiProduct.CurrentOperationId && ProductStatusManager.IsNotReclamationAndInstalledInReclamationAndInstalled(product.ProductStatusId))
                    .ToList()
                        .ForEach(MultiProductBL.MultiProducts.Add); 

            //Получение операций изделий (RouteMapProductOperation) для всех изделий, отображенных на экране
            MultiProductBL.ProductOperationViews = SQLServer.Instance.RouteMapProductOperation
                .GetGroupRouteMapProductOperations(MultiProductBL.MultiProducts.Select(product => product.Id), MultiProductBL.SelectedMultiOperation, ConvertDateToBool(MultiProductBL.SelectedMultiOperation.StartDate), ConvertDateToBool(MultiProductBL.SelectedMultiOperation.EndDate));

            //Фильтрация коллекции, что бы отсеить изделия, где операция нахидтся на других этапах
            List<Product> tmpMultiProducts = MultiProductBL.MultiProducts.ToList();
            MultiProductBL.MultiProducts.Clear();
            tmpMultiProducts.Where(product => MultiProductBL.ProductOperationViews.Select(rmpov => rmpov.ProductId).Contains(product.Id)).ToList()
                .ForEach(MultiProductBL.MultiProducts.Add);

            tmpMultiProducts.Clear();

            MultiProductBL.SelectedMultiProducts = null; // CanExecute trigger for {ExecuteSelectedGroupOperation} command
            #region SearchBar
            MultiProductBL.MultiProductsView = CollectionViewSource.GetDefaultView(MultiProductBL.MultiProducts);
            MultiProductBL.MultiProductsView.Filter = item => MultiProductBL.NumberSearchBar == null || ((Product)item).SerialNumber.Contains(MultiProductBL.NumberSearchBar);
            #endregion
        }
        private bool ConvertDateToBool(DateTime? date) => date != null ? true : false;
        private bool CanStartNextOperation()
        {
            return !ColHasInProgressOrEquipmentInProgress(ProductOperationBL.CurrentProductOperationsList);
        } //true, если нет операций со статусом "в процессе" 

        #region CollectionCheck_methods
        private  bool ColHasInProgressOrEquipmentInProgress(IEnumerable<RouteMapProductOperation> col)
        {
            AuxiliaryOperationStatus dict = CodeDictionary.Instance.AuxiliaryOperationStatus;
            return col.Any(productOperation => productOperation.OperationStatusId == dict.EquipmentInProgress.Id || productOperation.OperationStatusId == dict.InProgress.Id);
        }
        #endregion

        #region Operation_Commands
        public ICommand GroupStartOperation { get; set; }
        public ICommand GroupEndOperation { get; set; }
        public ICommand GroupCancelOperation { get; set; }
        public ICommand GroupReturnToSelectedOperation { get; set; }
        public ICommand ExecuteSelectedGroupOperation { get; set; }
        #endregion
        public ICommand ClearSelectedGroupExecuterIdCommand { get; set; }      
    }
}
