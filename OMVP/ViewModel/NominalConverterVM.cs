﻿using OMVP.BL.BLModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.ViewModel
{
    public class NominalConverterVM : BaseVM
    {
        public NominalConverterBL NominalConverterBL { get; set; } = NominalConverterBL.Instance;
    }
}
