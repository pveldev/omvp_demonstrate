﻿using OMVP.DAL;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Configuration;
using OMVP.ApplicationSettings;
using OMVP.BL.BLModels;
using OMVP.BL.CoreBL;
using OMVP.Modal;
using OMVP.Core;
using OMVP.DAL.CoreDAL;

namespace OMVP.ViewModel
{
    public class AuthorizationViewModel : BasicNotifyModel
	{
		public Settings Settings { get; set; } = Settings.Instance;
		public AuthorizationModel AuthorizationModel { get; set; } = AuthorizationModel.Instance;
		public ConnectionStringSettingsCollection Connections { get; set; } = ConfigurationManager.ConnectionStrings;

		private const string _successfulResponce = "-1";

		public AuthorizationViewModel()
		{
			AuthorizationModel.PropertyChanged += async(object sender, PropertyChangedEventArgs e)
				=> await AuthorizationViewModel_PropertyChanged(sender,  e);

			if (Settings.SettingsFileExists() || !string.IsNullOrEmpty(Settings.SettingsData.BaseDbPath))			
				AuthorizationModel.SelectedConnection = Settings.SettingsData.BaseDbPath; 

            LoginCommand = new SmartCommand(pswdBox =>
			{
				try
				{
					if (CheckUser((pswdBox as PasswordBox).Password))
                    {
						AuthorizationModel.IsVisibleAuthView = false;
                        CoreOMVP.PageNavigation.InitHomePage(Core.OmvpConstant.HomePage, TextHelper.RevisionNavigationTitle(), null);
                    }
                    else
						Dialog.ShowError(TextHelper.WrongLoginOrPasswordMessage());

					(pswdBox as PasswordBox).Clear();
                }
                catch (Exception ex)
				{
					Dialog.ShowError(ex.Message);
				}
			}, p => AuthorizationModel.IsEnabledConnection && !string.IsNullOrEmpty(AuthorizationModel.SelectedConnection),
			AuthorizationModel, new string[] { "SelectedConnection", "IsEnabledConnection" });

            LoginRefreshCommand = new SmartCommand(_=>
            {
                AuthorizationModel.LoginRefresh();
            });
        }

		private async Task AuthorizationViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (!string.IsNullOrEmpty(AuthorizationModel.SelectedConnection) && e.PropertyName == nameof(AuthorizationModel.SelectedConnection))
			{
				AuthorizationModel.IsEnabledConnection = false; //block button
				AuthorizationModel.FailConnectionMessage = TextHelper.ConnectionCheckMessage();

				RefreshSettingsFile();

				SQLServer.Instance.db = new OmvpDataModel(AuthorizationModel.SelectedConnection);
                AuthorizationModel.FailConnectionMessage = await SQLServer.Instance.ConnectionCheckAsync();

				AuthorizationModel.IsEnabledConnection = AuthorizationModel.FailConnectionMessage == _successfulResponce; //Управление доступностью кнопки	
			}
		}

		private bool CheckUser(string pass)
		{
			AuthorizationModel.AuthorizedUser = SQLServer.Instance.User.GetUser(AuthorizationModel.LoginName);
			if (AuthorizationModel.AuthorizedUser == null)		
				AuthorizationModel.AuthorizedUser = SQLServer.Instance.User.GetUser(Environment.UserName);
			if (AuthorizationModel.AuthorizedUser == null)
				AuthorizationModel.AuthorizedUser = AuthorizationModel.AddNewUser(Shaper.FormNewUser(Environment.UserName));

			if (string.IsNullOrEmpty(AuthorizationModel.AuthorizedUser.Password) || HashHelper.GenerateSHA256String(pass) == AuthorizationModel.AuthorizedUser.Password)			
				return true; //True для юзеров без пароля и при совпадении хешей паролей		
			return false;
		}
		private void RefreshSettingsFile()
		{
			if (AuthorizationModel.SelectedConnection != Settings.SettingsData.BaseDbPath) // Защита от ненужной перезаписи
			{
				Settings.SettingsData.BaseDbPath = AuthorizationModel.SelectedConnection; //Замена значения
				Settings.SaveSettings(); // Перезапись
			}
		}
		public ICommand LoginCommand { get; set; }
		public ICommand LoginRefreshCommand { get; set; }     
    }
}
