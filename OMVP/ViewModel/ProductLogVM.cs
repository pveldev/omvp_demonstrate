﻿using OMVP.BL.BLModels;
using OMVP.Core;
using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace OMVP.ViewModel
{
	public class ProductLogVM : BaseVM
	{
        public AuthorizationModel AuthorizationModel { get; set; } = AuthorizationModel.Instance;
        public ProductLogBL ProductLogBL { get; set; } = ProductLogBL.Instance;
        public UserAccessorLevelBL UserAccessorLevelBL { get; set; } = UserAccessorLevelBL.Instance; //ProductLogParameters
        public RevisionBL RevisionBL { get; set; } = RevisionBL.Instance; //ProductLogParameters

        public static readonly Action<BaseVM, bool> ProductLogRefresh = async (vm, isNavBack) => { await ((ProductLogVM)vm).GetProductLogDataAsync(); };

        public ProductLogVM()
        {
            DeleteSelectedProductLog = new SmartCommand(_ =>
            {
                MessageBoxResult response = Dialog.ShowYesNo(TextHelper.DoYouWantDeleteRowMessage());
                if ( response == MessageBoxResult.Yes)
                    ProductLogBL.DeleteSelectedProductLog();

            }, p => ProductLogBL.SelectedProductLog != null, ProductLogBL, new string[] { "SelectedProductLog" });

            LoadProductLogPage = new SmartCommand(window =>
            {
                ProductLogBL.GetProductLogsWithParameters();
                ProductLogBL.ProductLogView = CollectionViewSource.GetDefaultView(ProductLogBL.ProductLogs);
                ProductLogBL.BindingLogFilters();

                WindowHelper.CloseWindow(window);
            });

            CancelProductLogParameters = new SmartCommand(window => { WindowHelper.CloseWindow(window); });

            ClearSelectedRevision = new SmartCommand(_ => { ProductLogBL.ClearSelectedRevision(); });
            ClearSelectedUser = new SmartCommand(_ => { ProductLogBL.ClearSelectedUser(); });
            ClearStartDate = new SmartCommand(_ => { ProductLogBL.ClearStartDate(); });
            ClearStopDate = new SmartCommand(_ => { ProductLogBL.ClearStopDate(); });
        }

        public async Task GetProductLogDataAsync()
        {
            MenuBL.Instance.StartLoadAnimation(actionDescription: "Загрузка списка ревизий");

            RevisionBL.Revisions.Clear();
            (await SQLServer.Instance.Revision.GetSortedRevisionsAsync()).ForEach(RevisionBL.Revisions.Add);

            MenuBL.Instance.SetNewProgress(0.5f, actionDescription: "Загрузка списка пользователей");

            UserAccessorLevelBL.Users.Clear();
            (await SQLServer.Instance.User.GetAllSortedUsersAsync()).ForEach(UserAccessorLevelBL.Users.Add);

            MenuBL.Instance.StopLoadAnimation();

            new View.Popup.ProductLogParameters { }.ShowDialog();
        }

        public ICommand DeleteSelectedProductLog { get; set; }

        #region ProductLogPage_commands
        public ICommand LoadProductLogPage { get; set; }
        public ICommand CancelProductLogParameters { get; set; }
        public ICommand ClearSelectedRevision { get; set; }
        public ICommand ClearSelectedUser { get; set; }
        public ICommand ClearStartDate { get; set; }
        public ICommand ClearStopDate { get; set; }
        #endregion       
    }
}
