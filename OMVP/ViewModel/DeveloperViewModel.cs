﻿using OMVP;
using OMVP.BL.BLModels;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace OMVP.ViewModel
{
    public class DeveloperViewModel : OMVP.Core.BasicNotifyModel
    {
        public SQLServer SQLServer { get; set; } = SQLServer.Instance;
        public DataGenerator DataGenerator { get; set; } = DataGenerator.Instance;

        public DeveloperViewModel()
        {
            AddBasicData = new SmartCommand(async _ =>
            {
                MessageBoxResult response = Dialog.ShowYesNo(TextHelper.DoYouWantInitialDbMessage());
                if (response == MessageBoxResult.Yes)
                {
                    await DataGenerator.AddBasicDataAsync();
                    Dialog.ShowOk(TextHelper.InitialCompletedMessage());
                }
            });

			ClearBasicDataCommand = new SmartCommand(async _ =>
			{
                MessageBoxResult response = Dialog.ShowYesNo(TextHelper.DoYouWantDropSystemDbDataMessage());
                if (response == MessageBoxResult.Yes)
                {
				    SQLServer.RemoveRange(await SQLServer.ModelType.GetModelTypesAsync());
				    SQLServer.RemoveRange(await SQLServer.Role.GetRolesAsync());
				    SQLServer.RemoveRange(await SQLServer.OrderStatus.GetOrderStatusesAsync());
				    SQLServer.RemoveRange(await SQLServer.OperationStatus.GetOperationStatusesAsync());
				    //SQLServer.RemoveRange(await SQLServer.Operation.GetOperationsAsync());

				    Dialog.ShowOk(TextHelper.DbCleaningCompletedMessage());
                }
			});
            ProductBomsIsBuggedCheckCommand = new SmartCommand(_ =>
            {
                ProductBL productBL = ProductBL.Instance;

                productBL.Products.Clear();
                SQLServer.Instance.Product.GetBuggedProducts().ForEach(productBL.Products.Add);

                if(productBL.Products?.Any() ?? false)
                {
                    List<string> displayData = productBL.Products.Select(p =>
                    {
                        return string.Concat(p.Revision.Name, ":", p.SerialNumber);
                    }).ToList();

                    Dialog.ShowWarning(TextHelper.BuggedProductsBomsWarningMessage(string.Join("\n", displayData)));
                }
                else
                {
                    Dialog.ShowOk(TextHelper.ProductBomsIsFine);
                }
            });

        }
		public ICommand AddBasicData { get; set; }
        public ICommand ClearBasicDataCommand { get; set; }	
        public ICommand ProductBomsIsBuggedCheckCommand { get; set; }
    }
}