﻿using OMVP.BL.BLModels;
using OMVP.Core;
using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.Models;
using OMVP.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace OMVP.ViewModel
{
	public class MenuVM : BasicNotifyModel
	{ 
		public MenuBL MenuBL { get; set; } = MenuBL.Instance;
		public PageNavigation PageNavigation { get; set; }
		public AuthorizationModel AuthorizationModel { get; set; } = AuthorizationModel.Instance;
        public MenuVM()
		{
			PageNavigation.Change += PageNavigation_Change;

			CallDevView = new SmartCommand(_ =>
			{
				PageNavigation.LoadNextPage("DeveloperPage", "DEVELOPER");
			});

			LoadAllNodePage = new SmartCommand(p =>
			{
				PageNavigation.LoadNextPage(p.ToString(), "Просмотр списка имеющихся узлов");
            }, _ => !MenuBL.CurrentActionInfo.ActionInProcess, MenuBL, new string[] { "CurrentActionInfo" });

            LogOut = new SmartCommand(_ =>
			{
				AuthorizationModel.IsVisibleAuthView = true;
            }, _ => !MenuBL.CurrentActionInfo.ActionInProcess, MenuBL, new string[] { "CurrentActionInfo" });

            LoadPage = new SmartCommand(p =>
			{
                PageNavigation.LoadNextPage(p.ToString());
            }, _ => !MenuBL.CurrentActionInfo.ActionInProcess, MenuBL, new string[] { "CurrentActionInfo" } );

            ShowProductBomLogViewCommand = new SmartCommand(p =>
            {
                PageNavigation.LoadNextPage(p.ToString());
                new View.Popup.ProductBomLogParametersWindow { }.ShowDialog();
            }, _ => !MenuBL.CurrentActionInfo.ActionInProcess, MenuBL, new string[] { "CurrentActionInfo" });

            RefreshPageDataCommand = new SmartCommand(_ =>
            {
                RefreshPageData(MenuBL.CurrentPage.View);
            }, _ => !MenuBL.CurrentActionInfo.ActionInProcess, MenuBL, new string[] { "CurrentActionInfo" });

            ShowSearchedProductViewCommand = new SmartCommand(_ =>
            {
                PageNavigation.LoadNextPage(TextHelper.SearchedProductViewKey, TextHelper.SearchedProductBasicViewTitle, refreshContext:false);
                new View.Popup.SearchProductByNumberWindow { }.ShowDialog();
            }, _ => !MenuBL.CurrentActionInfo.ActionInProcess, MenuBL, new string[] { "CurrentActionInfo" });    
        }

        private void PageNavigation_Change(string key, string PageTitle, Revision selectedRevision, Product selectedProduct, bool refreshContext = true, bool isNavigationBack = false)
		{
			PageInfo page = PageManager.Instance.GetPage(key.ToString(), PageTitle);
			MenuBL.CurrentPage = page;
            
            RevisionBL.Instance.SelectedRevision = null;
			RevisionBL.Instance.SelectedRevision = selectedRevision;

            RefreshPageData(page.View, refreshContext, isNavigationBack);
        }

        public void RefreshPageData(UserControl view, bool refreshContext = true, bool isNavigationBack = false)
        {
            SQLServer.Instance.UpdateDbContexts(refreshContext);
            (view as BaseView)?.Refresh(isNavigationBack);

            if (refreshContext)
                MenuBL.DataRefreshTime = DateTime.Now;
        }

		public ICommand CallDevView { get; set; }
		public ICommand LoadAllNodePage { get; set; }
		public ICommand LoadPage { get; set; }
		public ICommand LogOut { get; set; }
		public ICommand ShowProductBomLogViewCommand { get; set; }
		public ICommand RefreshPageDataCommand { get; set; }
		public ICommand ShowSearchedProductViewCommand { get; set; }    
    }
}
