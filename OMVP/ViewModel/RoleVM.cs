﻿using OMVP.BL.BLModels;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OMVP.ViewModel
{
	public class RoleVM : BaseVM
	{
		public RoleBL RoleBL { get; set; } = RoleBL.Instance;

		public static readonly Action<BaseVM, bool> RoleRefresh = (vm, isNavBack) => { ((RoleVM)vm).GetData(); };

		public RoleVM()
		{
			AddNewRole = new SmartCommand(_ =>
			{
				RoleBL.AddRole();
			});
		}

		public void GetData()
		{
            RoleBL.Roles.Clear();
            SQLServer.Instance.Role.GetRoles().ForEach(RoleBL.Roles.Add);
		}

		public ICommand AddNewRole { get; set; }
	}
}