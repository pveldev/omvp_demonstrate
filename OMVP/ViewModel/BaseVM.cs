﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OMVP.ViewModel
{
	public class BaseVM : Core.BasicNotifyModel
	{
		public Action<BaseVM, bool> Refresh;
        public BaseVM()
        {
            CloseWindowCommand = new SmartCommand(window =>
            {
                WindowHelper.CloseWindow(window);
            });
        }

        public void CallRefresh(bool isNavigationBack)
		{
			Refresh?.Invoke(this, isNavigationBack);
		}
       
        public ICommand CloseWindowCommand { get; set; }
    }
}
