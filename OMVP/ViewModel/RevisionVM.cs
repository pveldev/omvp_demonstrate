﻿using OMVP.BL.BLModels;
using OMVP.Core;
using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace OMVP.ViewModel
{
	public class RevisionVM : BaseVM
	{
        public MenuBL MenuBL { get; set; } = MenuBL.Instance;
        public AuthorizationModel AuthorizationModel { get; set; } = AuthorizationModel.Instance;
		public RevisionBL RevisionBL { get; set; } = RevisionBL.Instance;
        public ModelBL ModelBL { get; set; } = ModelBL.Instance;
        public RevisionBOMBL RevisionBOMBL { get; set; } = RevisionBOMBL.Instance;

        public static readonly Action<BaseVM, bool> RevisionsPageRefresh = (vm, isNavBack) => { ((RevisionVM)vm).GetRevisionData(); };
		public static readonly Action<BaseVM, bool> ModelsPageRefresh = (vm, isNavBack) => { ((RevisionVM)vm).GetModelData(); };
		public static readonly Action<BaseVM, bool> SlotsPageRefresh = (vm, isNavBack) => { ((RevisionVM)vm).GetRevisionBomData(); };

        public RevisionVM()
		{
            #region ModelView
            AddModel = new SmartCommand(_ =>
			{
				ModelBL.AddModelWithRevision();

			}, p => !string.IsNullOrEmpty(ModelBL.NewModelName) && !string.IsNullOrEmpty(ModelBL.NewModelKRPG) && ModelBL.NewModelType != null && !string.IsNullOrEmpty(ModelBL.NewRevisionName),
					ModelBL, new string[] { "NewModelName", "NewModelKRPG", "NewModelType", "NewRevisionName" });
            #endregion

            #region RevisionsView
            AddNewRevision = new SmartCommand(_ =>
			{
				RevisionBL.AddRevision();

			}, p => !string.IsNullOrEmpty(RevisionBL.NewRevisionName) && RevisionBL.NewRevisionModel != null, RevisionBL, new string[] { "NewRevisionName", "NewRevisionModel" });

            ArchiveSelectedRevisionCommand = new SmartCommand(_ =>
			{
                if (!RevisionBL.ArchiveSelectedRevision())
                    Dialog.ShowError(TextHelper.ArchiveSelectedRevisionError);
			});

            DeleteSelectedRevisionCommand = new SmartCommand(_ =>
            {
                if (!RevisionBL.DeleteSelectedRevision())
                    Dialog.ShowError(TextHelper.DeleteSelectedRevisionError);
            }, p => RevisionBL.SelectedRevision != null && !RevisionBL.SelectedRevision.BomsIsClaimed, RevisionBL, new string[] { "SelectedRevision" });

            ClaimSelectedRevisionCommand = new SmartCommand(_ =>
            {
                string slotNames = string.Join(", ", SQLServer.Instance.RevisionBom.GetRevisionBOMs(RevisionBL.SelectedRevision.Id).Select(slot => slot.Name));
                MessageBoxResult result = Dialog.ShowYesNoCancel(TextHelper.DoYouWantToClaimSelectedRevision(RevisionBL.SelectedRevision.Name, slotNames));
                if (result == MessageBoxResult.Yes)
                {
                    RevisionBL.ClaimSelectedRevision();
                }
            }, p => RevisionBL.SelectedRevision != null && !RevisionBL.SelectedRevision.BomsIsClaimed, RevisionBL, new string[] { "SelectedRevision" });
            #endregion

            #region RevisionBomsView
            AddNewRevisionBOM = new SmartCommand(_ =>
            {
                RevisionBOMBL.AddRevisionBOM();
            }, p => !string.IsNullOrEmpty(RevisionBOMBL.NewSlotName) && RevisionBOMBL.NewSelectedSlotModel != null && RevisionBOMBL.NewSelectedSlotBomRevision != null && RevisionBOMBL.SelectedModelRevision != null,
                    RevisionBOMBL, new string[] { "NewSlotName", "NewSelectedSlotModel", "NewSelectedSlotBomRevision", "SelectedModelRevision" });

            DeleteSelectedSlotCommand = new SmartCommand(_ =>
            {
                RevisionBOMBL.DeleteSelectedSlot();
            }, p => RevisionBOMBL.SelectedRevisionBom != null && !RevisionBOMBL.SelectedRevisionBom.Revision1.BomsIsClaimed, RevisionBOMBL, new string[] { "SelectedRevisionBom" });

            ClaimSelectedRevisionBomsCommand = new SmartCommand(_ =>
            {
                string slotNames = string.Join(", ", RevisionBOMBL.RevisionBOMList.Select(slot => slot.Name));
                MessageBoxResult result = Dialog.ShowYesNoCancel(TextHelper.DoYouWantToClaimSelectedRevision(RevisionBOMBL.SelectedModelRevision.Name, slotNames));
                if (result == MessageBoxResult.Yes)
                {
                    RevisionBOMBL.ClaimSelectedRevisionBoms();
                }
            }, p => RevisionBOMBL.SelectedModelRevision != null && !RevisionBOMBL.SelectedModelRevision.BomsIsClaimed, RevisionBOMBL, new string[] { "SelectedModelRevision" });

            #endregion

            RevisionBL.PropertyChanged += RevisionBL_SelectionChanged;  
        }

        private void RevisionBL_SelectionChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if (e.PropertyName == nameof(RevisionBL.SelectedRevision) && NavigationStack.SelectedRevisions.Count == 1)
			{
				NavigationStack.SelectedRevisions.Pop();
				NavigationStack.SelectedRevisions.Push(RevisionBL.SelectedRevision);
			}
		} // Подмена Revision-пустышки в стеке

		public void GetRevisionData()
		{
            RevisionBL.Revisions.Clear();
            SQLServer.Instance.Revision.GetSortedRevisions().ForEach(RevisionBL.Revisions.Add);

            GetAllModels();
        }

        public void GetModelData()
		{
            RevisionBL.ModelTypeList.Clear();
            SQLServer.Instance.ModelType.GetModelTypes().ForEach(RevisionBL.ModelTypeList.Add);

            GetAllModels();
        }
        public void GetRevisionBomData()
		{
            RevisionBL.Revisions.Clear();
            SQLServer.Instance.Revision.GetNoComponentRevisions().ForEach(RevisionBL.Revisions.Add);

            GetAllModels();
        }

        private void GetAllModels()
        {
            ModelBL.AllModelList.Clear();
            SQLServer.Instance.Model.GetSortedModels().ForEach(ModelBL.AllModelList.Add);
        }
		

		#region RevisionView_commands
		public ICommand AddNewRevision { get; set; }
		public ICommand ArchiveSelectedRevisionCommand { get; set; }
		public ICommand ClaimSelectedRevisionCommand { get; set; }
		public ICommand DeleteSelectedRevisionCommand { get; set; }
        #endregion

        #region Model_commands
        public ICommand AddModel { get; set; }
		#endregion

		#region Revision_RevisionBOM_commands
		public ICommand AddNewRevisionBOM { get; set; }
		public ICommand DeleteSelectedSlotCommand { get; set; }
		public ICommand ClaimSelectedRevisionBomsCommand { get; set; }
        #endregion
    }
}
