﻿using OMVP.BL.BLModels;
using OMVP.Core;
using OMVP.CoreOMVP;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OMVP.ViewModel
{
    public class NavigationBarVM : BasicNotifyModel
    {
        public MenuBL MenuBL { get; set; } = MenuBL.Instance;

        public NavigationBarVM()
        {
            ReturnToPreviusPage = new SmartCommand(_ =>
            {
                PageNavigation.LoadPreviusPage();
            }, p => !NavigationStack.CurrentPageIsHomePage(), MenuBL, new string[] { "CurrentPage" });

            ReturnToHomePage = new SmartCommand(_ =>
            {
                PageNavigation.LoadHomePage(OmvpConstant.HomePage, TextHelper.RevisionNavigationTitle());
            }, p => !NavigationStack.CurrentPageIsHomePage(), MenuBL, new string[] { "CurrentPage" });

            ShowHelpWindowCommand = new SmartCommand(_ =>
            { 
                if (MenuBL.CurrentPage.HelperWindow is null)
                {
                    Dialog.ShowWarning($"Не существует окна-помошника для данной страницы :(\nСтраницы с помошниками:\n{PageManager.Instance.GetHelpersInfo()}");
                    return;
                }

                MenuBL.CurrentPage.HelperWindow.ShowHelper();
            });
        }

        public ICommand ReturnToPreviusPage { get; set; }
        public ICommand ReturnToHomePage { get; set; }
        public ICommand ShowHelpWindowCommand { get; set; }        
    }
}
