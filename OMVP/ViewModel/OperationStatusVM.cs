﻿using OMVP.BL.BLModels;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OMVP.ViewModel
{
	public class OperationStatusVM : BaseVM
	{
		public OperationStatusBL OperationStatusBL { get; set; } = OperationStatusBL.Instance;

		public static readonly Action<BaseVM, bool> OperationStatusRefresh = (vm, isNavBack) => { ((OperationStatusVM)vm).GetData(); };

		public OperationStatusVM()
		{
			AddNewOperationStatus = new SmartCommand(_ =>
			{
				OperationStatusBL.AddOperationStatus();
			});
		}

		public void GetData()
		{
            OperationStatusBL.OperationStatusList.Clear();
            SQLServer.Instance.OperationStatus.GetOperationStatuses().ForEach(OperationStatusBL.OperationStatusList.Add);
		}

		public ICommand AddNewOperationStatus { get; set; }
	}
}