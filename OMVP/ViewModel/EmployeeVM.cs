﻿using OMVP.BL.BLModels;
using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace OMVP.ViewModel
{
	public class EmployeeVM : BaseVM
	{
		public EmployeeBL EmployeeBL { get; set; } = EmployeeBL.Instance;
		public RoleBL RoleBL { get; set; } = RoleBL.Instance;
		public ModelBL ModelBL { get; set; } = ModelBL.Instance;
		public AuthorizationModel AuthorizationModel { get; set; } = AuthorizationModel.Instance;
		

		public static readonly Action<BaseVM, bool> EmployeeRefresh = (vm, isNavBack) => { ((EmployeeVM)vm).GetEmployeeData(); };
		public static readonly Action<BaseVM, bool> WorkingGroupRefresh = (vm, isNavBack) => { ((EmployeeVM)vm).GetWorkingGroupData(); };
		
		public EmployeeVM()
		{
			AddNewEmployee = new SmartCommand(_ =>
			{
				EmployeeBL.AddEmployee();
			}, p => !string.IsNullOrEmpty(EmployeeBL.NewEmployeeFirstName) && !string.IsNullOrEmpty(EmployeeBL.NewEmployeeLastName) && EmployeeBL.SelectedRole != null,
			EmployeeBL, new string[] { "NewEmployeeFirstName", "NewEmployeeLastName", "SelectedRole" });

			ArchiveEmployee = new SmartCommand(parameter =>
			{
                if (parameter is Employee employee)
                    EmployeeBL.ArchiveEmployee(employee);
                else
                    Dialog.ShowError(TextHelper.EmployeeArchivingFailCozInvalidCast);
			});

			ArchiveWorkingGroup = new SmartCommand(parameter =>
            {
                if (parameter is WorkingGroup workingGroup)
                    EmployeeBL.ArchiveWorkingGroup(workingGroup);
                else
                    Dialog.ShowError(TextHelper.WorkingGroupArchivingFailCozInvalidCast);
			});

			AddEmployeeToWorkingGroupEmployee = new SmartCommand(_ =>
			{
				EmployeeBL.AddEmployeeToWorkingGroupEmployee();
            }, p => EmployeeBL.SelectedEmployee != null && EmployeeBL.SelectedWorkingGroup != null, EmployeeBL, new string[] { "SelectedEmployee", "SelectedWorkingGroup" });

            CreateNewWorkingGroup = new SmartCommand(_ => 
            {
                EmployeeBL.CreateNewWorkingGroup();
            }, p => !string.IsNullOrEmpty(EmployeeBL.NewWorkingGroupName), EmployeeBL, new string[] { "NewWorkingGroupName" });

            SaveWorkingGroupEmployee = new SmartCommand(_ =>
			{
				EmployeeBL.SaveWorkingGroupEmployee();
			});

            SaveModelPlusWorkingGroupCompositionCommand = new SmartCommand(_ =>
            {
                EmployeeBL.SaveModelPlusWorkingGroupComposition();
            }, p => EmployeeBL.SelectedModel != null && EmployeeBL.SelectedWorkingGroup != null, EmployeeBL, new string[] { "SelectedModel", "SelectedWorkingGroup" });

            DeleteSelectedEmployeeFromSelectedGroupCommand = new SmartCommand(_ =>
            {
                if (CheckBL.WorkingGroupEmployee_EmployeeNotNull(EmployeeBL.SelectedWorkingGroupEmployee) && EmployeeBL.SelectedWorkingGroup != null
                && Dialog.ShowYesNo(TextHelper.DoYouWantToExcludeEmployeeFromGroup(EmployeeBL.SelectedWorkingGroupEmployee.Employee.FirstName, EmployeeBL.SelectedWorkingGroupEmployee.Employee.LastName, EmployeeBL.SelectedWorkingGroup.Name)) == MessageBoxResult.Yes)              
                    EmployeeBL.RemoveSelectedEmployeeFromSelectedGroup();           
            }, p => EmployeeBL.SelectedWorkingGroupEmployee != null, EmployeeBL, new string[] { "SelectedWorkingGroupEmployee" });

            DeleteModelXWorkingGroupRelationCommand = new SmartCommand(_=>
            {
                EmployeeBL.DeleteModelXWorkingGroupRelation();
            }, p => EmployeeBL.SelectedModelXWorkingGroupRelation != null, EmployeeBL, new string[] { "SelectedModelXWorkingGroupRelation" });
        }

        public void GetEmployeeData()
		{
            RoleBL.Roles.Clear();
            SQLServer.Instance.Role.GetRoles().ForEach(RoleBL.Roles.Add);

            EmployeeBL.EmployeeList.Clear();
            SQLServer.Instance.Employee.GetAllSortedEmployees().ForEach(EmployeeBL.EmployeeList.Add);
		}

		public void GetWorkingGroupData()
        {
            EmployeeBL.WorkingGroupEmployeeList.Clear();

            EmployeeBL.AllWorkingGroups.Clear();
            SQLServer.Instance.WorkingGroup.GetAllSordtedWorkingGroups().ForEach(EmployeeBL.AllWorkingGroups.Add);

            EmployeeBL.NotArchiveWorkingGroups.Clear();
            SQLServer.Instance.WorkingGroup.GetSortedNotArchiveWorkingGroups().ForEach(EmployeeBL.NotArchiveWorkingGroups.Add);

            ModelBL.ModelList.Clear();
            SQLServer.Instance.Model.GetSortedModels().ForEach(ModelBL.ModelList.Add);

            EmployeeBL.EmployeeList.Clear();
            SQLServer.Instance.Employee.GetSortedNotArchivedEmployees().ForEach(EmployeeBL.EmployeeList.Add);

            EmployeeBL.ModelsXWorkingGroups.Clear();
            SQLServer.Instance.WorkingGroup.GetAllModelXWorkingGroup().ForEach(EmployeeBL.ModelsXWorkingGroups.Add);
        }

		public ICommand AddNewEmployee { get; set; }
		public ICommand ArchiveEmployee { get; set; }
		public ICommand ArchiveWorkingGroup { get; set; }
		public ICommand AddEmployeeToWorkingGroupEmployee { get; set; }
		public ICommand SaveWorkingGroupEmployee { get; set; }
		public ICommand CreateNewWorkingGroup { get; set; }
		public ICommand SaveModelPlusWorkingGroupCompositionCommand { get; set; }
		public ICommand DeleteSelectedEmployeeFromSelectedGroupCommand { get; set; }
		public ICommand DeleteModelXWorkingGroupRelationCommand { get; set; }      
    }
}    