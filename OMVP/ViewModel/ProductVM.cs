﻿using OMVP.BL.BLModels;
using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace OMVP.ViewModel
{
    public class ProductVM : BaseVM
    {
        public int BaseTabIndex { get; set; } = -1;
        public ProductBL ProductBL { get; set; } = ProductBL.Instance;
        public RevisionBL RevisionBL { get; set; } = RevisionBL.Instance;
        public RouteMapBL RouteMapBL { get; set; } = RouteMapBL.Instance;
        public OrderBL OrderBL { get; set; } = OrderBL.Instance;
        public MenuBL MenuBL { get; set; } = MenuBL.Instance;
        public AuthorizationModel AuthorizationModel { get; set; } = AuthorizationModel.Instance;

        public static readonly Action<BaseVM, bool> ProductRefresh = (vm, isNavBack) => { ((ProductVM)vm).GetData(); };

        public ProductVM()
        {
            #region productCRUD
            AddRangeProduct = new SmartCommandAsync(async _ =>
            {
                if (SelectedRouteIsNotNull())
                {
                    if (ProductBL.NewProductQuantity > 1)
                    {
                        MenuBL.Instance.StartLoadAnimation();
                        bool result = ProductBL.ShapeAndPreviewNewProducts();
                        MenuBL.Instance.StopLoadAnimation();

                        if (result)
                        {
                            new View.Popup.PreviewNewProductsWindow { }.ShowDialog();
                        }
                    }
                    else
                    {
                        MenuBL.Instance.StartLoadAnimation();
                        await ProductBL.AddSingleProductAsync();
                        MenuBL.Instance.StopLoadAnimation();
                    }
                }    
            }, p => ProductBL.NewProductQuantity > 0 && !string.IsNullOrEmpty(ProductBL.StartNumber) && LetterIsNotNull(),
            ProductBL, new string[] { "NewProductQuantity", "StartNumber", "NewProductLetter" });

            SavePreviewedProductsCommand = new SmartCommandAsync(async window =>
            {
                //Начинаем не с нулевого прогресса, т.к. в теле SavePreviewedProducts происходит только "шаг 3"
                MenuBL.Instance.StartLoadAnimation(OmvpConstant.Step1 + OmvpConstant.Step2);

                await ProductBL.SavePreviewedProducts();
                WindowHelper.CloseWindow(window);

                MenuBL.Instance.StopLoadAnimation();

            });
            CancelPreviewProductsWindowCommand = new SmartCommand(window =>
            {
                if (WindowHelper.CloseWindow(window))               
                    ProductBL.PreviewNewProducts.Clear();                           
            });
            EditProductCommand = new SmartCommand(window => // ==SaveProductChangesCommand
            {
                if (!ProductBL.UpdateSelectedProduct())         
                    Dialog.ShowWarning(TextHelper.InformationSaveErrorMessage());

                WindowHelper.CloseWindow(window);

            }, p => CheckBL.AccessorLevelCheck(2) && ProductBL.SelectedProduct != null, ProductBL, new string[] { "SelectedProduct", "EditProductWorkingGroupEmployee", "EditProductWorkingGroup" });

            CloseEditWindowCommand = new SmartCommand(window =>
            {
                ProductBL.ClearEditProductWindowProps();
                WindowHelper.CloseWindow(window);
            });
            #endregion

            #region ClearSelecteds_ProductView
            ClearSelectedWorkingGroupCommand = new SmartCommand(_ =>
            {
                ProductBL.ClearSelectedWorkingGroup();
            }, p => ProductBL.SelectedWorkingGroup != null, ProductBL, new string[] { "SelectedWorkingGroup" });
            ClearSelectedEngineerCommand = new SmartCommand(_ =>
            {
                ProductBL.ClearSelectedEngineer();
            }, p => ProductBL.SelectedEngineer != null, ProductBL, new string[] { "SelectedEngineer" });
            ClearSelectedTechnologistCommand = new SmartCommand(_ =>
            {
                ProductBL.ClearSelectedTechnologist();
            }, p => ProductBL.SelectedTechnologist != null, ProductBL, new string[] { "SelectedTechnologist" });
            ClearSelectedRouteMapCommand = new SmartCommand(_ =>
            {
                ProductBL.ClearSelectedRouteMap();
            }, p => ProductBL.SelectedRouteMap != null, ProductBL, new string[] { "SelectedRouteMap" });
            #endregion

            AddGenericNumberCodeCommand = new SmartCommand(_ =>
            {
                ProductBL.StartNumber = "127" + (DateTime.Now.Year % 2000).ToString();
            });

            #region ClearSelecteds_EditProductWindow

            ClearEditProductWorkingGroupCommand = new SmartCommand(_ =>
            {
                ProductBL.ClearEditProductWorkingGroup();
            }, p => ProductBL.EditProductWorkingGroup != null, ProductBL, new string[] { "EditProductWorkingGroup" });

            ClearEditProductEngineerCommand = new SmartCommand(_ =>
            {
                ProductBL.ClearEditProductEngineer();
            }, p => ProductBL.EditProductEngineer != null, ProductBL, new string[] { "EditProductEngineer" });

            ClearEditProductTechnologistCommand = new SmartCommand(_ =>
            {
                ProductBL.ClearEditProductTechnologist();
            }, p => ProductBL.EditProductTechnologist != null, ProductBL, new string[] { "EditProductTechnologist" });

            ClearEditProductRouteMapCommand = new SmartCommand(_ =>
            {
                ProductBL.ClearEditProductRouteMap();
            }, p => ProductBL.EditProductRouteMap != null, ProductBL, new string[] { "EditProductRouteMap" });

            #endregion

            UnrejectProductCommand = new SmartCommand(_ =>
            {
                ProductBL.UnrejectProduct(ProductBL.SelectedProduct);
            }, p => ProductBL.SelectedProduct != null, ProductBL, new string[] { "SelectedProduct" });

            DispatchProductCommand = new SmartCommand(async _ =>
            {
                MenuBL.Instance.StartLoadAnimation();
                await ProductBL.DispatchProductsAsync();
                MenuBL.Instance.StopLoadAnimation();
            }, p => CanExecuteDispatchButton() && CheckBL.AccessorLevelCheck(2), ProductBL, new string[] { "SelectedProduct", "SelectedOrderElement" });

            PseudoDispatchProductCommand = new SmartCommand(async _ =>
            {
                if (Dialog.ShowOKCancel(TextHelper.DoYouWantToPseudoDispatch) == MessageBoxResult.OK)
                {
                    MenuBL.Instance.StartLoadAnimation();
                    await ProductBL.DispatchProductsAsync(isPseudoDispatch: true);
                    MenuBL.Instance.StopLoadAnimation();
                }
            }, p => CanExecuteSpeudoDispatchButton() && CheckBL.AccessorLevelCheck(2), ProductBL, new string[] { "SelectedProduct", "SelectedOrderElement" });

            #region PreviewNewProductsWindow_commands
            ClearRouteMapForSelectedPreviewProductCommand = new SmartCommand(_ =>
            {
                ProductBL.ClearRouteMapForSelectedPreviewProduct(ProductBL.SelectedProduct);
            }, p => ProductBL.SelectedProduct != null, ProductBL, new string[] { "SelectedProduct" });
            DeleteSelectedPreviewedProductCommand = new SmartCommand(parameter=>
            {
                if (parameter is Product product)
                {
                    ProductBL.DeleteSelectedPreviewedProduct(product);
                }
                else
                {
                    Dialog.ShowError(TextHelper.InvalidCastErrorMessage);
                    SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog( exceptionName: "CustomInvalidCast", 
                                                                                message: "Не удалось преобразовать параметр к типу Product",
                                                                                innerExceptionMessage: parameter is null ? "" : parameter.ToString(),
                                                                                methodName: "DeleteSelectedPreviewedProductCommand",
                                                                                projectName: "OMVP.VM"));
                }
            });
            #endregion

            #region DispatchedProductsParameters_commands
            ShowDispatchedProductsParametersWindowCommand = new SmartCommand(_=>
            {
                new View.Popup.DispatchedProductsParametersWindow().ShowDialog();
            });
            ShowPseudoDispatchedProductsCommand = new SmartCommand(_ =>
            {
                MenuBL.Instance.StartLoadAnimation();
                ProductBL.ShowPseudoDispatchedProducts();
                MenuBL.Instance.StopLoadAnimation();
            });

            LoadDispatchedProductsWithParametersCommand = new SmartCommand(window=>
            {
                ProductBL.LoadDispatchedProductsWithSelectedParameters();
                WindowHelper.CloseWindow(window);
                MenuBL.Instance.DataRefreshTime = DateTime.Now;
            });
            CancelDispatchedProductsWindowCommand = new SmartCommand(window =>
            {
                WindowHelper.CloseWindow(window);
            });
            #endregion

            #region SearchProductByNumberWindow_commands
            ShowSearchProductsCommand = new SmartCommand(async window =>
            {
                MenuBL.Instance.StartLoadAnimation();

                WindowHelper.CloseWindow(window);
                await ProductBL.GetProductsBySerialNumberAsync();
                MenuBL.SetCurrentPageTitle(TextHelper.SearchedProductViewTitle(ProductBL.Instance.IsAdvancedSearch, ProductBL.Instance.GlobalSearchPrductBySerialNumber));
                MenuBL.Instance.DataRefreshTime = DateTime.Now;

                MenuBL.Instance.StopLoadAnimation();
            }, p => !string.IsNullOrEmpty(ProductBL.GlobalSearchPrductBySerialNumber), ProductBL, new string[] { "GlobalSearchPrductBySerialNumber" });
            #endregion
        }

        public void GetData()
        {
            BaseTabIndex = 0; //Открытие первой вкладки, ибо именно для нее (и только для нее) будут обновляться данные в этом методе
            ProductBL.SelectedDispatchedDate = DateTime.Now; //Дата по умолчанию
            ProductBL.InWorkProductsTabDataRefresh();

            ProductBL.ProductsView  = CollectionViewSource.GetDefaultView(ProductBL.Products);
            ProductBL.ProductsView.Filter = item => ProductBL.NumberSearchBar == null || ((Product)item).SerialNumber.Contains(ProductBL.NumberSearchBar);

            #region Dispatch_OrderElements_Initial
            OrderBL.OrderElements.Clear();
            SQLServer.Instance.OrderElement.GetActualAndClaimedOrderElements(RevisionBL.Instance.SelectedRevision).ForEach(OrderBL.OrderElements.Add);
            OrderBL.OrderElementsView = CollectionViewSource.GetDefaultView(OrderBL.OrderElements);

            OrderBL.OrderElementsView.Filter = item => ProductBL.SelectedProduct == null || FilterOrderElement(item);
            #endregion

            ProductBL.CurrentWorkingGroupList = new List<WorkingGroup>(SQLServer.Instance.WorkingGroup.GetSordetWorkingGroups(RevisionBL.Instance.SelectedRevision.ModelId));

            RouteMapBL.RouteMapList.Clear();
            SQLServer.Instance.RouteMap.GetNotArchiveNClaimedRouteMaps(RevisionBL.Instance.SelectedRevision.Id).ForEach(RouteMapBL.RouteMapList.Add);

            ProductBL.ClearProductProps(); // Нужно, что бы при переходе от одного изделия к другому не просачивались скрытые (по visibility) свойства
        }

        private bool FilterOrderElement(object item)
        {
            if (item is OrderElement orderElement)
            {
                return ProductBL.SelectedProduct.Revision.Model.HasLetter ?
                    orderElement.OrderTree.Order.Letter == ProductBL.SelectedProduct.Letter && orderElement.Quantity > orderElement.CompletedQuantity
                    :
                    orderElement.Quantity > orderElement.CompletedQuantity;
            }
            return false;
        }

        private bool LetterIsNotNull() => !RevisionBL.SelectedRevision.Model.HasLetter || !string.IsNullOrEmpty(ProductBL.NewProductLetter);

        private bool SelectedRouteIsNotNull()
        {
            if (ProductBL.SelectedRouteMap != null)
            {
                return true;
            }
            else
            {
                return !(RouteMapBL.RouteMapList?.Any() ?? false) || Dialog.ShowOKCancel(TextHelper.DoYouWantAddProductWithoutRouteMapMessage()) == MessageBoxResult.OK;
            }
        }

        #region Displach_methods
        private bool CanExecuteSpeudoDispatchButton() //for single SelectedProduct mode
        {
            return DispatchPropsIsNotNull() &&
                ProductStatusManager.CanExecutePseudoDispatchStatusCheck(ProductBL.SelectedProduct.ProductStatusId) &&
                DispatchProductLetterCheck() &&
                SelectedOrderElementQuantityCheck();
        }

        private bool CanExecuteDispatchButton() //for single SelectedProduct mode
        {
            return DispatchPropsIsNotNull() && 
                ProductStatusManager.CanExecuteDispatchStatusCheck(ProductBL.SelectedProduct.ProductStatusId) && 
                DispatchProductLetterCheck() &&
                SelectedOrderElementQuantityCheck();
        }
        private bool DispatchPropsIsNotNull() => ProductBL.SelectedOrderElement != null && ProductBL.SelectedProduct != null;
        private bool DispatchProductLetterCheck() =>
         !ProductBL.SelectedProduct.Revision.Model.HasLetter || ProductBL.SelectedProduct.Letter == ProductBL.SelectedOrderElement.OrderTree.Order.Letter;
        private bool SelectedOrderElementQuantityCheck() => ProductBL.SelectedOrderElement.Quantity - ProductBL.SelectedOrderElement.CompletedQuantity > 0;


        #endregion

        #region ProductCRUD_commands
        public ICommand AddGenericNumberCodeCommand { get; set; }
        public ICommand AddRangeProduct { get; set; }
        public ICommand SavePreviewedProductsCommand { get; set; }
        public ICommand CancelPreviewProductsWindowCommand { get; set; }
        public ICommand EditProductCommand { get; set; }
        public ICommand UnrejectProductCommand { get; set; }
        public ICommand CloseEditWindowCommand { get; set; }
        #endregion
        #region ProductView_ClearButtons_commands
        public ICommand ClearSelectedWorkingGroupCommand { get; set; }
        public ICommand ClearSelectedEngineerCommand { get; set; }
        public ICommand ClearSelectedTechnologistCommand { get; set; }
        public ICommand ClearSelectedRouteMapCommand { get; set; }
        #endregion
        #region EditProductWindow_ClearButtons_commands
        public ICommand ClearEditProductWorkingGroupCommand { get; set; }
        public ICommand ClearEditProductEngineerCommand { get; set; }
        public ICommand ClearEditProductTechnologistCommand { get; set; }
        public ICommand ClearEditProductRouteMapCommand { get; set; }
        #endregion
        #region PreviewNewProductsWindow_commands
        public ICommand ClearRouteMapForSelectedPreviewProductCommand { get; set; }
        public ICommand DeleteSelectedPreviewedProductCommand { get; set; }
        #endregion
        #region DispatchedTab_commands
        public ICommand ShowDispatchedProductsParametersWindowCommand { get; set; }
        public ICommand ShowPseudoDispatchedProductsCommand { get; set; }
        
        public ICommand LoadDispatchedProductsWithParametersCommand { get; set; }
        public ICommand CancelDispatchedProductsWindowCommand { get; set; }

        public ICommand DispatchProductCommand { get; set; }
        public ICommand PseudoDispatchProductCommand { get; set; }// Отгрузить изделие, готовое к установке (на случай некорректного ведения графиков)
        #endregion
        #region SearchProductByNumberWindow_commands
        public ICommand ShowSearchProductsCommand { get; set; }
        #endregion
    }
}
