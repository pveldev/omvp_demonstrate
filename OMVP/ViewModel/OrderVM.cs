﻿using OMVP.BL.BLModels;
using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.DataViews;
using OMVP.DAL.Models;
using OMVP.Modal;
using OMVP.View.Popup;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace OMVP.ViewModel
{
	public class OrderVM : BaseVM
	{
		public MenuBL MenuBL { get; set; } = MenuBL.Instance;
		public OrderBL OrderBL { get; set; } = OrderBL.Instance;
		public RevisionBL RevisionBL { get; set; } = RevisionBL.Instance;
		public AuthorizationModel AuthorizationModel { get; set; } = AuthorizationModel.Instance;

		public static readonly Action<BaseVM, bool> OrderRefresh = (vm, isNavBack) => { ((OrderVM)vm).GetData(); };
		public static readonly Action<BaseVM, bool> GroupOrderRefresh = (vm, isNavBack) => { ((OrderVM)vm).GetGroupData(); };

        public OrderVM()
		{
			OrderBL.PropertyChanged += OrderBL_PropertyChanged;

            AddNewOrder = new SmartCommand(_ =>
			{
				OrderBL.AddOrder();
			}, p => OrderBL.NewOrderQuantity > 0 && OrderBL.SelectedRevision != null &&
					OrderBL.NewOrderExecutionDate != null && !string.IsNullOrEmpty(OrderBL.NewOrderCustomer) && 
					!string.IsNullOrEmpty(OrderBL.NewOrderSHPZCode) && !string.IsNullOrEmpty(OrderBL.NewOrderCode),
					OrderBL, new string[] { "NewOrderQuantity", "SelectedRevision", "NewOrderExecutionDate", "NewOrderCustomer", "NewOrderSHPZCode", "NewOrderCode" });

			AddNewOrderElementCommand = new SmartCommand(async _ =>
			{
                if (OrderBL.SelectedOrderTreeFromComboBox.Order.IsClaimed)
                {
                    MenuBL.StartLoadAnimation(0.99f);
                    await OrderBL.UpsertOrderElementsAsync();
                    MenuBL.StopLoadAnimation();
                }
                else
                {
                    MenuBL.StartLoadAnimation(0.99f);
                    List<User> users = await SQLServer.Instance.User.GetUsersAsync(AccessorLevelManager.GetAccessorLevel("CanClaimOrders"));
                    MenuBL.StopLoadAnimation();

                    Dialog.ShowWarning(TextHelper.OrderIsNotClaimedWarningMessage(users.ToStringColumn()));
                }
            }, p => OrderBL.SelectedOrderTreeFromComboBox != null, OrderBL, new[] { "SelectedOrderTreeFromComboBox" });

            DeleteSelectedOrderCommand = new SmartCommand(_=>
            {
                if (Dialog.ShowYesNo(TextHelper.DoYouWantToDoIt("удалить")) == MessageBoxResult.Yes)
                    OrderBL.DeleteSelectedOrder();

            }, p => OrderBL.SelectedOrderTree != null && OrderBL.SelectedOrderTree.Order != null && !OrderBL.SelectedOrderTree.Order.IsClaimed, OrderBL, new[] { "SelectedOrderTree" });

            #region Navigation
            LoadGroupOrderViewCommand = new SmartCommand(_=>
            {
                PageNavigation.LoadNextPage("ShowGroupOrderView", refreshContext: false);
            });

            LoadGroupOrderElementViewCommand = new SmartCommand(_ =>
            {
                new View.Popup.OrderElementGroupParameters { }.ShowDialog();
            });
            #endregion
            GetOrderElementGroupDataCommand = new SmartCommand(window =>
            {
                PageNavigation.LoadNextPage("ShowGroupOrderElementView", refreshContext: false, pageTitle: $"Сгруппированный по ежемесячной отгрузке годовой план [{OrderBL.SelectedRevisionParameter.Name}] для [{OrderBL.SelectedYear}] года");
                OrderBL.GetOrderElementGroupData(OrderBL.SelectedRevisionParameter, OrderBL.SelectedYear);
                WindowHelper.CloseWindow(window);
            }, p => OrderBL.SelectedRevisionParameter != null, OrderBL, new[] { "SelectedRevisionParameter" });

            CancelOrderElementGroupParametersWindowCommand = new SmartCommand(window =>
            {
                WindowHelper.CloseWindow(window);
                PageNavigation.LoadPreviusPage();
            });
            ClaimSelectedOrderCommand = new SmartCommand(async _ =>
            {
                if (AccessorLevelManager.AccessorLevelCheck("CanClaimOrders", AuthorizationModel.AuthorizedUser.AccessorLevel))
                {
                    MenuBL.StartLoadAnimation(0.99f);
                    OrderBL.ClaimSelectedOrder();
                    MenuBL.StopLoadAnimation();
                }
                else
                {
                    MenuBL.StartLoadAnimation(0.99f);
                    List<User> users = await SQLServer.Instance.User.GetUsersAsync(AccessorLevelManager.GetAccessorLevel("CanClaimOrders"));
                    MenuBL.StopLoadAnimation();

                    Dialog.ShowWarning(TextHelper.CannotClaimOrderWarningMessage(users.ToStringColumn()));
                }


            }, p => OrderBL.SelectedOrderTree != null && OrderBL.SelectedOrderTree.Order != null && !OrderBL.SelectedOrderTree.Order.IsClaimed, OrderBL, new[] { "SelectedOrderTree" });

            ArchiveSelectedOrderCommand = new SmartCommand(_ =>
            {
                if (Dialog.ShowYesNo(TextHelper.DoYouWantToDoIt("архивировать")) == MessageBoxResult.Yes)               
                    OrderBL.ArchiveSelectedOrder();
                
            }, p => OrderBL.SelectedOrderTree != null 
                    && OrderBL.SelectedOrderTree.Order != null 
                    && OrderBL.SelectedOrderTree.Order.OrderStatusId != CodeDictionary.Instance.AuxiliaryOrderStatus.Completed.Id
                    && OrderBL.SelectedOrderTree.Order.OrderStatusId != CodeDictionary.Instance.AuxiliaryOrderStatus.Archive.Id, OrderBL, new[] { "SelectedOrderTree" });

            #region Edit_order_comamnds
            OpenEditOrderWindowCommand = new SmartCommand(_ =>
            {
                OrderBL.EditableOrder = Shaper.FormEditableOrder(OrderBL.SelectedOrderTree.Order);
                new EditOrderWindow { }.ShowDialog();
            }, p => OrderBL.SelectedOrderTree != null && OrderBL.SelectedOrderTree.Order != null, OrderBL, new string[] { "SelectedOrderTree" });

            SaveEditOrderChangesCommand = new SmartCommand(window =>
            {
                OrderBL.SaveEditOrderChanges();
                WindowHelper.CloseWindow(window);

            }, p => OrderBL.EditableOrder != null, OrderBL, new string[] { "EditableOrder" });

            CloseOrderEditWindowCommand = new SmartCommand(window =>
            {
                OrderBL.ClearEditOrderProps();
                WindowHelper.CloseWindow(window);
            });
            #endregion
        }

        private void OrderBL_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if (e.PropertyName == (nameof(OrderBL.HighLevelOrderElements)) && OrderBL.HighLevelOrderElements != null && (OrderBL.HighLevelOrderElements?.Any() ?? false))
			    OrderBL.SummQuantity = OrderBL.HighLevelOrderElements.Sum(orderElement => orderElement != null ? orderElement.Quantity : 0);					
		}

		public void GetData()
		{
            OrderBL.DispatchYears = GetDispatchYears();
            OrderBL.SelectedYear = OrderBL.DispatchYears.Last();

            RevisionBL.Revisions.Clear();
            SQLServer.Instance.Revision.GetRevisionsByModelTypeId(CodeDictionary.Instance.AuxiliaryModelType.TopLevelModel.Id, true).ForEach(RevisionBL.Revisions.Add);

            OrderBL.OrderTrees.Clear();
            SQLServer.Instance.OrderTree.GetOrderTrees(CodeDictionary.Instance.AuxiliaryModelType.TopLevelModel.Id, OrderBL.SelectedYear).ForEach(OrderBL.OrderTrees.Add); //GetHighLevelOrderTrees
        }

        public void GetGroupData()
        {
            OrderBL.OrderTrees.Clear();
            SQLServer.Instance.OrderTree.GetOrderTrees(CodeDictionary.Instance.AuxiliaryModelType.TopLevelModel.Id, OrderBL.SelectedYear).ForEach(OrderBL.OrderTrees.Add); //GetHighLevelOrderTrees

            OrderBL.GroupOrderTrees.Clear();
            OrderBL.OrderTrees.GroupBy(orderTree => orderTree.RevisionId).Select(orderTree => new GroupOrderTrees()
            {
                RevisionName = orderTree.First().Revision.Name,
                GroupQuantity = orderTree.Sum(ot => ot.Order.Quantity),
                DispatchedQuantity = orderTree.Sum(ot => ot.DispatchedQuantity),
                Year = orderTree.First().Order.ExecutionDate,
                DetailedInformation = orderTree.DetailedInformation()
            }).ToList().ForEach(OrderBL.GroupOrderTrees.Add);
        }

        private int[] GetDispatchYears()
        {
            int count = DateTime.Now.Year - OmvpConstant.FirstDispatchYear + 1;
            if (count < 0) count = 1;

            return Enumerable.Range(OmvpConstant.FirstDispatchYear, count).ToArray();
        }

        #region commands
        public ICommand AddNewOrder { get; set; }
		public ICommand AddNewOrderElementCommand { get; set; }
		public ICommand ClaimSelectedOrderCommand { get; set; }
		public ICommand ArchiveSelectedOrderCommand { get; set; }      
        public ICommand DeleteSelectedOrderCommand { get; set; }
        
        public ICommand LoadGroupOrderViewCommand { get; set; }     

        //OrderElementGroupView_commands
        public ICommand LoadGroupOrderElementViewCommand { get; set; }       
		public ICommand GetOrderElementGroupDataCommand { get; set; }       
		public ICommand CancelOrderElementGroupParametersWindowCommand { get; set; }

        //Edit_order_commands
		public ICommand OpenEditOrderWindowCommand { get; set; }
		public ICommand SaveEditOrderChangesCommand { get; set; }
		public ICommand CloseOrderEditWindowCommand { get; set; }
        #endregion
    }
}