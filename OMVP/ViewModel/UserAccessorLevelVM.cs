﻿using OMVP.BL.BLModels;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OMVP.ViewModel
{
	public class UserAccessorLevelVM : BaseVM
	{
        public AuthorizationModel AuthorizationModel { get; set; } = AuthorizationModel.Instance;
		public UserAccessorLevelBL UserAccessorLevelBL { get; set; } = UserAccessorLevelBL.Instance;
        public EmployeeBL EmployeeBL { get; set; } = EmployeeBL.Instance;


		public static readonly Action<BaseVM, bool> UserRefresh = (vm, isNavBack) => { ((UserAccessorLevelVM)vm).GetUserData(); };

		public UserAccessorLevelVM()
		{
			UpdateUser = new SmartCommand(_ =>
			{
				UserAccessorLevelBL.UpdateUser();
            }, p => UserAccessorLevelBL.SelectedUser != null, UserAccessorLevelBL, new string[] { "SelectedUser" });
        }

        public void GetUserData()
		{
            UserAccessorLevelBL.Users.Clear();
            SQLServer.Instance.User.GetAllSortedUsers().ForEach(UserAccessorLevelBL.Users.Add);

            EmployeeBL.EmployeeList.Clear();
            SQLServer.Instance.Employee.GetAllSortedEmployees().ForEach(EmployeeBL.EmployeeList.Add);
		}

		public ICommand UpdateUser { get; set; }
	}
}
