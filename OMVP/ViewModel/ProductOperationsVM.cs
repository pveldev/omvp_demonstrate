﻿using OMVP.BL.BLModels;
using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace OMVP.ViewModel
{
    public class ProductOperationsVM : BaseVM
	{
		public ProductOperationBL ProductOperationBL { get; set; } = ProductOperationBL.Instance;
		public AuthorizationModel AuthorizationModel { get; set; } = AuthorizationModel.Instance;
		public RouteMapBL RouteMapBL { get; set; } = RouteMapBL.Instance;
		public MenuBL MenuBL { get; set; } = MenuBL.Instance;       

        public bool CanExecute { get; set; }


        public static readonly Action<BaseVM, bool> ProductOperationsRefresh = (vm, isNavBack) => {((ProductOperationsVM)vm).GetData(); };

		public ProductOperationsVM()
		{
            #region Operation_commands
            StartOperation = new SmartCommand(_ =>
			{
				if (CanStartSelectedOperation())
				{
					//Операция"Комплектация" обрабатывается особым образом
					if (ProductOperationBL.SelectedProductOperation.OperationId == CodeDictionary.Instance.AuxiliaryOperation.Equipment.Id)
					{
                        if (ProductOperationBL.StartOrReternToSelectedOperation(CodeDictionary.Instance.AuxiliaryOperationStatus.EquipmentInProgress, TextHelper.StartOperation()))                       
                            PageNavigation.LoadNextPage(
                                "ShowInstallationProductView", 
                                TextHelper.InstallationProductTitle(ProductOperationBL.SelectedProductOperation.Product.Revision.Model.Name, ProductOperationBL.SelectedProductOperation.Product.SerialNumber),
                                selectedProduct: ProductOperationBL.SelectedProductOperation.Product,
                                refreshContext: false);                     
					}
					else
					{
						ProductOperationBL.StartOrReternToSelectedOperation(CodeDictionary.Instance.AuxiliaryOperationStatus.InProgress, TextHelper.StartOperation());
					}
					CanExecute = CanStartNextOperation();
				}
				else
				{
					Dialog.ShowError(TextHelper.CantStartOperationMessage());
				}

			}, p => CanExecute, this, new string[] { "CanExecute" });

			EndOperation = new SmartCommand(_ =>
			{
				ProductOperationBL.EndSelectedOperation();
				CanExecute = CanStartNextOperation();
			});

			ReturnToSelectedOperation = new SmartCommand(_ =>
			{
				if (ProductOperationBL.SelectedProductOperation.OperationId == CodeDictionary.Instance.AuxiliaryOperation.Equipment.Id)
				{
                    PageNavigation.LoadNextPage(
                        "ShowInstallationProductView", 
                        TextHelper.InstallationProductTitle(ProductOperationBL.SelectedProductOperation.Product.Revision.Model.Name, ProductOperationBL.SelectedProductOperation.Product.SerialNumber),
                        selectedProduct: ProductOperationBL.SelectedProductOperation.Product,
                        refreshContext: false);

                    ProductOperationBL.StartOrReternToSelectedOperation(CodeDictionary.Instance.AuxiliaryOperationStatus.Completed, TextHelper.RepeatEquipment, true);
				}
				else
				{
					ProductOperationBL.StartOrReternToSelectedOperation(CodeDictionary.Instance.AuxiliaryOperationStatus.InProgress, TextHelper.ReturnOperation(), true);
				}
				CanExecute = CanStartNextOperation();
			}, p => CanExecute, this, new string[] { "CanExecute" });

			CancelOperation = new SmartCommand(_ =>
			{
				ProductOperationBL.CancelSelectedOperation();
				CanExecute = CanStartNextOperation();
			});

			ContinueOperation = new SmartCommand(_ =>
			{
                PageNavigation.LoadNextPage(
                    "ShowInstallationProductView",
                    TextHelper.InstallationProductTitle(ProductOperationBL.SelectedProductOperation.Product.Revision.Model.Name, ProductOperationBL.SelectedProductOperation.Product.SerialNumber),
                    selectedProduct: ProductOperationBL.SelectedProductOperation.Product,
                    refreshContext: false);

                CanExecute = CanStartNextOperation();			
			});

            ClearSelectedProductOperationCommand = new SmartCommand(_=>
            {
                ProductOperationBL.ClearSelectedProductOperation();
            }); //CanExecute - настраивается конвертором
            #endregion

			AddUnscheduledOperationCommand = new SmartCommand(_ =>
			{
                if (!ProductOperationBL.CurrentProductOperationsList.Any(productOperation => productOperation.OperationId == ProductOperationBL.SelectedOperation.Id))
                {
                    if (ProductOperationBL.SelectedProductOperation != null)
                    {
				        if (CanAddOperationAfterSelected())
					        ProductOperationBL.AddUnscheduledOperation(ProductOperationBL.SelectedProductOperation.Priority);
				        else				
					        Dialog.ShowWarning(TextHelper.CantAddUnsheduledOperationMessage()); //Операцию можно добавить только в "текущее положение и далее. Нельзя добавить в "прошлое".
                    }
                    else //Добавление операции в чистый маршрут
                    {
                        ProductOperationBL.AddUnscheduledOperation(priotity: 1);                  
                    }
                }
                else
                {
                    Dialog.ShowWarning(TextHelper.OperationIsADuplicate(ProductOperationBL.SelectedOperation.Name));
                }

            }, p => ProductOperationBL.SelectedOperation != null && CanAddUnscheduledOperationCheck(), ProductOperationBL, new string[] { "SelectedProductOperation" , "SelectedOperation" });

            OpenEditProductOperationWindow = new SmartCommand(_ =>
            {
                new View.Popup.EditProductOperationWindow{ DataContext = this }.ShowDialog();
            }, p => ProductOperationBL.SelectedProductOperation != null, ProductOperationBL, new string[] { "SelectedProductOperation" });

            SaveProductOperationDescription = new SmartCommand(window =>
            {            
                if (!ProductOperationBL.SaveProductOperationDescription())
                    Dialog.ShowWarning(TextHelper.SaveErrorDescriptionMessage());

                WindowHelper.CloseWindow(window);            
            });

            CancelProductOperationDescription = new SmartCommand(window =>
            {
                try
                {
                    ((Window)window).Close();
                }
                catch (InvalidCastException ex)
                {
                    SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog(ex));
                    Dialog.ShowError(ex.Message);
                }
            });

            PushProductUntilReady = new SmartCommand(_=>
            {
                try
                {
                    MessageBoxResult result = Dialog.ShowYesNoCancel(TextHelper.DoYouWantPushWithoutRouteMessage(ProductOperationBL.SelectedProduct.Revision.Model.Name, ProductOperationBL.SelectedProduct.SerialNumber));

                    if (result == MessageBoxResult.Yes && ProductBL.Instance.PushProductUntilReady(ProductOperationBL.SelectedProduct))                   
                        PageNavigation.LoadPreviusPage();                                     
                    else if (result == MessageBoxResult.No && ProductBL.Instance.PushProductOperationsUntilReady(ProductOperationBL.SelectedProduct))                 
                        PageNavigation.LoadPreviusPage();
                    
                    //result == MessageBoxResult.Cancel не обрабатывается. Используется для закрытия модального окна
                }
                catch (Exception ex)
                {
                    SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog(ex));
                    Dialog.ShowError(ex.Message);
                }
            });

            ClearSelectedEmployeeCommand = new SmartCommand(parameter =>
            {
                if (parameter is ComboBox comboBox)
                {
                    comboBox.SelectedIndex = -1; // reset comboBox selected
                    ProductOperationBL.ClearSelectedEmployee();
                }
                else
                {
                    Dialog.ShowError(TextHelper.ClearSelectedOperationExecuterError);
                }
            });

            EndReclamationWorkCommand = new SmartCommand(_ =>
            {
                Product product = ProductOperationBL.EndReclamationWork();
                if (product != null)
                {
                    PageNavigation.LoadPreviusPage();
                    Dialog.ShowOk(TextHelper.ProductStatusSuccessfullyChanged(product.SerialNumber, product.ProductStatus.Name));
                    ProductOperationBL.SelectedProduct = null;
                }
            }, p => ProductOperationBL.SelectedProduct != null && ProductStatusManager.IsReclamationOrInstalledInReclamation(ProductOperationBL.SelectedProduct.ProductStatusId), ProductOperationBL, new string[] { "SelectedProduct" });
        }
        
        private void GetData()
		{
            ProductOperationBL.ResetBLProps();

            ProductOperationBL.SelectedProduct = ProductBL.Instance.SelectedProduct; // Перекидываю Selected, что бы не лезть потом за ним в другую БЛ-модель

            if (ProductOperationBL.SelectedProduct is null)
            {
                ProductOperationBL.CurrentProductOperationsList.Clear();
                RouteMapBL.Operations.Clear();
                Dialog.ShowError(TextHelper.RefreshDataErrorMessage);
                return;
            }

            ProductOperationBL.CurrentProductOperationsList.Clear();
            SQLServer.Instance.RouteMapProductOperation.GetSortedRouteMapProductOperations(ProductOperationBL.SelectedProduct.Id).ForEach(ProductOperationBL.CurrentProductOperationsList.Add);

            ProductOperationBL.AllCurrentWorkingGroupEmployees = 
                SQLServer.Instance.WorkingGroupEmployee.GetWorkingGroupEmployees(ProductOperationBL.CurrentProductOperationsList
                    .Where(productOperation => productOperation.Operation.WorkingGroup != null)
                        .Select(productOperation => productOperation.Operation.WorkingGroupId).Distinct().ToArray());

            RouteMapBL.Operations.Clear();
            SQLServer.Instance.Operation.GetSortedNotArchivedOperations(ProductOperationBL.SelectedProduct.Revision.Model).ForEach(RouteMapBL.Operations.Add);

            CanExecute = CanStartNextOperation();
        }
      
        private bool CanStartNextOperation()
		{
            if (ProductOperationBL.CurrentProductOperationsList.All(productOperation => productOperation.OperationStatus != null))
            {
			    int inProgressId = CodeDictionary.Instance.AuxiliaryOperationStatus.InProgress.Id;
			    int EquipmentInProgressId = CodeDictionary.Instance.AuxiliaryOperationStatus.EquipmentInProgress.Id;

			    return ProductOperationBL.CurrentProductOperationsList.FirstOrDefault(productOperation =>
				    productOperation.OperationStatusId == EquipmentInProgressId ||
				    productOperation.OperationStatusId == inProgressId) != null ? false : true;

            }
            return false;
		} //true, если нет операций со статусом "в процессе" и "в процессе (сборка)"

		#region CanStartSelectedOperation
		/* 
         * Проверяет, есть ли операция "комплектация" в МК. 
		 * Если есть, то проверяет где выбранная операция находится по отношению к "комплектации".
		 * Если выбранная стоит после "комплектации", то проверяет заполнены ли все "слоты" в изделии.
         */
		private bool CanStartSelectedOperation()
		{
			RouteMapProductOperation equipment = GetEquipmentOperationFromRouteMap();
            return equipment == null ? 
                true 
                :
                CurrentOperationBeforeEquipment(equipment) ?
                    true 
                    :
                    InstallationProductBL.Instance.InstallationCompletedCheck(equipment.Product);
		} 	

		private RouteMapProductOperation GetEquipmentOperationFromRouteMap() =>
			 ProductOperationBL.CurrentProductOperationsList.ToList().SingleOrDefault(rmpo => rmpo.OperationId == CodeDictionary.Instance.AuxiliaryOperation.Equipment.Id);

		private bool CurrentOperationBeforeEquipment(RouteMapProductOperation productOperation) =>
			 ProductOperationBL.CurrentProductOperationsList.IndexOf(ProductOperationBL.SelectedProductOperation) <= ProductOperationBL.CurrentProductOperationsList.IndexOf(productOperation);		
		#endregion

		private bool CanAddOperationAfterSelected()
		{
			RouteMapProductOperation notCompletedOperation = ProductOperationBL.CurrentProductOperationsList.FirstOrDefault(productOperation => productOperation.OperationStatusId != CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id);

            if (notCompletedOperation != null)
				return ProductOperationBL.SelectedProductOperation.Priority >= notCompletedOperation.Priority;
			else			
				return ProductOperationBL.SelectedProductOperation.Priority >= ProductOperationBL.CurrentProductOperationsList.Last().Priority;	
		} //true, если выбранная операция имеет приоритет выше или такой же по отношению к первой операции из МК со статусом != completed.
        private bool CurrentProductOperationIsNotNull() => ProductOperationBL.CurrentProductOperationsList?.Any() ?? false;

        private bool CanAddUnscheduledOperationCheck() =>
            CurrentProductOperationIsNotNull() ? (ProductOperationBL.SelectedProductOperation != null ? true : false) : true;

        #region Operation_Commands
        public ICommand StartOperation { get; set; }
		public ICommand ContinueOperation { get; set; }
		public ICommand EndOperation { get; set; }
		public ICommand CancelOperation { get; set; }
		public ICommand ReturnToSelectedOperation { get; set; }

        public ICommand ClearSelectedProductOperationCommand { get; set; }
        #endregion
        #region UnscheduledOperation_Commands
        public ICommand AddUnscheduledOperationCommand { get; set; }
        #endregion
        #region EditProductOperationWindow_Commands
        public ICommand OpenEditProductOperationWindow { get; set; }     
        public ICommand SaveProductOperationDescription { get; set; }     
        public ICommand CancelProductOperationDescription { get; set; }
        #endregion
        public ICommand PushProductUntilReady { get; set; }
        public ICommand ClearSelectedEmployeeCommand { get; set; }
        public ICommand EndReclamationWorkCommand { get; set; }
    }
}
