﻿using OMVP.BL.BLModels;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;


namespace OMVP.ViewModel
{
	public class ProductStatusVM : BaseVM
	{
		public ProductStatusBL ProductStatusBL { get; set; } = ProductStatusBL.Instance;

		public static readonly Action<BaseVM, bool> ProductStatusRefresh = (vm, isNavBack) => { ((ProductStatusVM)vm).GetData(); };

		public ProductStatusVM()
		{
			AddNewProductStatus = new SmartCommand(_ =>
			{
				ProductStatusBL.AddProductStatus();
			});
		}

		public void GetData()
		{
            ProductStatusBL.ProductStatusList.Clear();
            SQLServer.Instance.ProductStatus.GetAllProductStatuses().ForEach(ProductStatusBL.ProductStatusList.Add);
		}

		public ICommand AddNewProductStatus { get; set; }
	}
}