﻿using OMVP.BL.BLModels;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OMVP.ViewModel
{
    public class ErrorLogVM : BaseVM
    {
        public ErrorLogBL ErrorLogBL { get; set; } = ErrorLogBL.Instance;
        public static readonly Action<BaseVM, bool> ErrorLogtRefresh = (vm, isNavBack) => { ((ErrorLogVM)vm).GetData(); };


        public ErrorLogVM()
        {
            DeleteSelectedErrorLog = new SmartCommand(_=>
            {
                ErrorLogBL.DeleteSelectedErrorLog();
            }, p => ErrorLogBL.SelectedErrorLog != null, ErrorLogBL, new string[] { "SelectedErrorLog" });
            DeleteAllErrorLogs = new SmartCommand(_=>
            {
                ErrorLogBL.DeleteAllErrorLogs();
            });
        }
        private void GetData()
        {
            ErrorLogBL.ErrorLogs.Clear();
            SQLServer.Instance.ErrorLog.GetErrorLogs().ForEach(ErrorLogBL.ErrorLogs.Add);
        }

        public ICommand DeleteSelectedErrorLog { get; set; }
        public ICommand DeleteAllErrorLogs { get; set; }
    }
}
