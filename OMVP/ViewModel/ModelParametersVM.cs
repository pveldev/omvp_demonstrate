﻿using OMVP.BL.BLModels;
using OMVP.BL.CoreBL;
using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.View.Popup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OMVP.ViewModel
{
    //VM for db.ModelParameters, db.ParameterTypes tables
    public class ModelParametersVM : BaseVM
    {
        public ModelParametersBL ModelParametersBL { get; set; } = ModelParametersBL.Instance;
        public ModelBL ModelBL { get; set; } = ModelBL.Instance;

        public static readonly Action<BaseVM, bool> ParameterTypesRefresh = (vm, isNavBack) =>  { ((ModelParametersVM)vm).GetParameterTypesData(); };
        public static readonly Action<BaseVM, bool> ModelParametersRefresh = (vm, isNavBack) =>  { ((ModelParametersVM)vm).GetModelParametersData(); };

        public ModelParametersVM()
        {
            #region ParameterTypesView_commands
            AddNewParameterTypeCommand = new SmartCommand(async _ =>
            {
                MenuBL.Instance.StartLoadAnimation(actionDescription: "Добавление нового типа");
                await ModelParametersBL.AddNewParameterTypeAsync();
                MenuBL.Instance.StopLoadAnimation();
            }, _ => !string.IsNullOrEmpty(ModelParametersBL.NewParameterTypeName), ModelParametersBL, new string[] { "NewParameterTypeName" });

            OpenEditSelectedParamTypeWindowCommand = new SmartCommand(_ =>
            {
                ModelParametersBL.EditableParameterType = Shaper.FormEditableParameterType(ModelParametersBL.SelectedParameterType);
                new EditParameterTypeWindow { }.ShowDialog();
            }, p => ModelParametersBL.SelectedParameterType != null, ModelParametersBL, new string[] { "SelectedParameterType" });

            ParamTypeSaveChangesCommand = new SmartCommand(window =>
            {
                ModelParametersBL.SaveParamTypeChanges();
                WindowHelper.CloseWindow(window);

            }, p => ModelParametersBL.EditableParameterType != null, ModelParametersBL, new string[] { "EditableParameterType" });

            ParamTypeCloseEditWindowCommand = new SmartCommand(window =>
            {
                ModelParametersBL.ClearEditableParamTypeProps();
                WindowHelper.CloseWindow(window);
            });
            #endregion

            #region ModelParam_Commands
            AddNewModelParameterToModelCommand = new SmartCommand(async _ =>
            {
                MenuBL.Instance.StartLoadAnimation(actionDescription: "Добавление параметра");
                await ModelParametersBL.AddNewModelParameterToModel();
                MenuBL.Instance.StopLoadAnimation();
            }, _=> ModelParametersBL.SelectedModel != null && ModelParametersBL.SelectedParameterType != null && !string.IsNullOrEmpty(ModelParametersBL.NewModelParameterName), ModelParametersBL, new string[] { "SelectedModel", "SelectedParameterType" , "NewModelParameterName" });

            RemoveModelParameterFromModelCommand = new SmartCommand(_ =>
            {
                MenuBL.Instance.StartLoadAnimation(actionDescription: "Отвязывание параметра");
                ModelParametersBL.RemoveModelParameterFromModel();
                MenuBL.Instance.StopLoadAnimation();
            }, _ => ModelParametersBL.SelectedModel != null && ModelParametersBL.SelectedModelParameter != null, ModelParametersBL, new string[] { "SelectedModel", "SelectedModelParameter" });

            OpenEditSelectedModelParamWindowCommand = new SmartCommand(_ =>
            {
                ModelParametersBL.EditableModelParameter = Shaper.FormEditableModelParameter(ModelParametersBL.SelectedModelParameter);
                new EditModelParameterWindow { }.ShowDialog();
            }, p => ModelParametersBL.SelectedModelParameter != null, ModelParametersBL, new string[] { "SelectedModelParameter" });

            ModelParamSaveChangesCommand = new SmartCommand(window =>
            {
                ModelParametersBL.SaveModelParameterChanges();
                WindowHelper.CloseWindow(window);

            }, p => ModelParametersBL.EditableModelParameter != null, ModelParametersBL, new string[] { "EditableModelParameter"});

            ModelParamCloseEditWindowCommand = new SmartCommand(window =>
            {
                ModelParametersBL.ClearEditableModelParameter();
                WindowHelper.CloseWindow(window);
            });
            #endregion
        }
        private void GetParameterTypesData()
        {
            MenuBL.Instance.StartLoadAnimation(actionDescription: "Загрузка списка типов параметров");
            RefreshParameterTypes();
            MenuBL.Instance.StopLoadAnimation();
        }
        private void GetModelParametersData()
        {
            MenuBL.Instance.StartLoadAnimation(actionDescription: "Загрузка списка типов параметров");

            ModelBL.ModelList.Clear();
            SQLServer.Instance.Model.GetSortedModels().ForEach(ModelBL.ModelList.Add);

            RefreshParameterTypes();
            ClearDataAfterOpenPage();
            MenuBL.Instance.StopLoadAnimation();
        }
        private void RefreshParameterTypes()
        {
            ModelParametersBL.ParameterTypes.Clear();
            SQLServer.Instance.ParameterType.GetAllParameterTypes().ForEach(ModelParametersBL.ParameterTypes.Add);
        }
        private void ClearDataAfterOpenPage()
        {
            ModelParametersBL.SelectedModel = null;
            ModelParametersBL.CurrentModelParameters.Clear();
            ModelParametersBL.NewModelParameterName = default;
            ModelParametersBL.NewModelParameterDescription = default;
        }

        #region ParameterTypesView_commands
        public ICommand AddNewParameterTypeCommand { get; set; }
        public ICommand OpenEditSelectedParamTypeWindowCommand { get; set; }

        //EditParamTypWindow_commands
        public ICommand ParamTypeSaveChangesCommand { get; set; }
        public ICommand ParamTypeCloseEditWindowCommand { get; set; }
        #endregion


        #region ModelParam_Commands
        public ICommand AddNewModelParameterToModelCommand { get; set; }
        public ICommand RemoveModelParameterFromModelCommand { get; set; }
        public ICommand OpenEditSelectedModelParamWindowCommand { get; set; }


        //EditModelParamWindow_commands
        public ICommand ModelParamSaveChangesCommand { get; set; }
        public ICommand ModelParamCloseEditWindowCommand { get; set; }
        #endregion
    }
}
