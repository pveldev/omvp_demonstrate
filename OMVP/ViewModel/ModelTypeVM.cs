﻿using OMVP.BL.BLModels;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OMVP.ViewModel
{
	public class ModelTypeVM : BaseVM
	{
		public ModelTypeBL ModelTypeBL { get; set; } = ModelTypeBL.Instance;

		public static readonly Action<BaseVM, bool> ModelTypesRefresh = (vm, isNavBack) => {((ModelTypeVM)vm).GetData(); };

		public ModelTypeVM()
		{
			AddNewModelType = new SmartCommand(_ =>
			{
				ModelTypeBL.AddModelType();
            }, p => !string.IsNullOrEmpty(ModelTypeBL.NewModelTypeName), ModelTypeBL, new string[] { "NewModelTypeName" });
        }

        public void GetData()
		{
            ModelTypeBL.ModelTypeList.Clear();
            SQLServer.Instance.ModelType.GetModelTypes().ForEach(ModelTypeBL.ModelTypeList.Add);
		}

		public ICommand AddNewModelType { get; set; }
	}
}
