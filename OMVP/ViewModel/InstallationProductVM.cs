﻿using OMVP.BL.BLModels;
using OMVP.BL.CoreBL;
using OMVP.Core;
using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OMVP.ViewModel
{
    public class InstallationProductVM : BaseVM
    {
        public AuthorizationModel AuthorizationModel { get; set; } = AuthorizationModel.Instance;
        public InstallationProductBL InstallationProductBL { get; set; } = InstallationProductBL.Instance;
        public ProductOperationBL ProductOperationBL { get; set; } = ProductOperationBL.Instance;

        public static readonly Action<BaseVM, bool> InstallationProductRefresh = (vm, isNavBack) => { ((InstallationProductVM)vm).GetData(isNavBack); };
        public static readonly Action<BaseVM, bool> ProductParentRefresh = (vm, isNavBack) => { ((InstallationProductVM)vm).GetParentData(); };
        public bool IsEditableView { get; set; } = false; // для всех конверторов вью (InstallationProductView)


        public InstallationProductVM()
        {
            SaveComposition = new SmartCommand(_ =>
            {               
                InstallationProductBL.SaveComposition(NavigationStack.GetToStringStack());
                
            }, p => InstallationProductBL.ChangedProductBoms?.Any() ?? false, InstallationProductBL, new string[] { "ChangedProductBoms" });
            
            ComboBoxSelectionChanged = new SmartCommand(productBom =>
            {
                try
                {
                    ProductBOM bom = productBom as ProductBOM;
                    InstallationProductBL.UpdateChangeDictionary(bom, true, InstallationProductBL.SelectedProduct);
                    InstallationProductBL.SelectedProductBom = bom; //Триггер CollectionToCurtainVisibility. Вызывается после обновления ChangeDictionary
                }
                catch (InvalidCastException ex)
                {
                    SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog(ex));
                    Dialog.ShowError(ex.Message);
                }
            });

            DeinstallProduct = new SmartCommand(productBom =>
            {
                try
                {
                    ProductBOM bom = productBom as ProductBOM;
                    InstallationProductBL.SelectedProduct = null; //deinstall
                    InstallationProductBL.UpdateChangeDictionary(bom, false, bom.Product1);
                    InstallationProductBL.SelectedProductBom = bom; //Trigger for MultiProductBomToCurtainVisibility & MultiProductBomToDescriptionVisibility (обновляется после обновления коллекций)
                }
                catch (InvalidCastException ex)
                {
                    SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog(ex));
                    Dialog.ShowError(ex.Message);
                }
            });
            
            ShowProductsBySlotCommand = new SmartCommand(_=>
            {
                PageNavigation.LoadNextPage(TextHelper.ProductsViewKey, TextHelper.ProductsViewTitle(InstallationProductBL.SelectedBomRow.RevisionBOM.Name), InstallationProductBL.SelectedBomRow.RevisionBOM.Revision, refreshContext: false);
                //ProductBL.Instance.SelectedProduct = InstallationProductBL.SelectedBomRow.Product; //Перенос изделия-родителя для успешного срабатывания возврата "назад" по навигации
            }, p => InstallationProductBL.SelectedBomRow != null, InstallationProductBL, new string[] { "SelectedBomRow" });
        }

        private void GetData(bool isNavBack)
        {
            Product product = GetCurrentProduct();

            if (product is null)
            {
                Dialog.ShowError(TextHelper.RefreshDataErrorMessage);
                return;
            }
            
            InstallationProductBL.ResetProductBomLogProps();

            InstallationProductBL.ProductBOMList.Clear();
            SQLServer.Instance.ProductBom.GetProductBOMs(product.Id).ForEach(InstallationProductBL.ProductBOMList.Add);

            if (isNavBack) //refresh product operation and selected operation for current DbContext
            {
                ProductOperationBL productOperationBL = ProductOperationBL.Instance;
                var tmp = productOperationBL.SelectedProductOperation;

                productOperationBL.CurrentProductOperationsList.Clear();
                SQLServer.Instance.RouteMapProductOperation.GetSortedRouteMapProductOperations(product.Id).ForEach(productOperationBL.CurrentProductOperationsList.Add);
                
                productOperationBL.SelectedProductOperation = tmp;
            }            

            IsEditableView = !ProductStatusManager.GetInstallationProductViewIsReadOnlyStatusIds().Contains(product.ProductStatusId);
        }
        private void GetParentData()
        {
            InstallationProductBL.ResetProductBomLogProps();

            ProductBOM bom = SQLServer.Instance.ProductBom.GetProductBOM(GetCurrentProduct().Id); // Получаем слот, куда установлен данное изделие
            InstallationProductBL.ProductBOMList.Clear();
            SQLServer.Instance.ProductBom.GetProductBOMs(bom.ProductId).ForEach(InstallationProductBL.ProductBOMList.Add); // Зная слот, получаю список всех изделий-детей

            MenuBL.Instance.SetCurrentPageTitle(TextHelper.ProductBomTitle(bom.Product.Revision.Name, bom.Product.SerialNumber));   

            IsEditableView = false; // При открытии родительской комплектации - bom.Product.ProductStatus.Id != ProductBL.Instance.SelectedProduct.Id;
        }

      
        private Product GetCurrentProduct()
        {
            //Если можно взять изделие из стека - используем его (необходимо для успешной навигации "назад")
            //В противном случае это должна быть навигация "вперед" и ProductBL.Instance.SelectedProduct обязать быть не null

            Product productFromStack = NavigationStack.GetCurrentProduct();
            if (productFromStack != null)
            {
                ProductBL.Instance.SelectedProduct = productFromStack;
                return productFromStack;
            }
            else
            {
                return ProductBL.Instance.SelectedProduct;
            }
        }
        public ICommand SaveComposition { get; set; }
        public ICommand ComboBoxSelectionChanged { get; set; }
        public ICommand DeinstallProduct { get; set; }
        #region Navigation_commands
        public ICommand ShowProductsBySlotCommand { get; set; }
        #endregion
    }
}
