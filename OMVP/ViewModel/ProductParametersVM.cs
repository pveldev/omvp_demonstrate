﻿using OMVP.BL.BLModels;
using OMVP.Core;
using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using OMVP.Modal;
using OMVP.View.Popup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OMVP.ViewModel
{
    public class ProductParametersVM : BaseVM
    {
        public ProductParametersBL ProductParametersBL { get; set; } = ProductParametersBL.Instance;
        public ModelParametersBL ModelParametersBL { get; set; } = ModelParametersBL.Instance;

        public static readonly Action<BaseVM, bool> ProductParametersRefresh = (vm, isNavBack) => { ((ProductParametersVM)vm).GetProductParametersData(ProductBL.Instance.SelectedProduct); };

        public ProductParametersVM()
        {
            CreateNewProductParamCommand = new SmartCommand(async _ =>
            {
                MenuBL.Instance.StartLoadAnimation(actionDescription: "Добавление нового параметра");
                await ProductParametersBL.AddNewParameterToProduct();
                MenuBL.Instance.StopLoadAnimation();

            }, _ => ProductParametersBL.SelectedProduct != null && ProductParametersBL.SelectedModelParameter != null && !string.IsNullOrEmpty(ProductParametersBL.NewProductParameterValue), ProductParametersBL, new string[] { "SelectedProduct", "SelectedModelParameter", "NewProductParameterValue" });


            OpenEditProductParameterWindowCommand = new SmartCommand(_ =>
            {
                ProductParametersBL.EditableProductParameter = Shaper.FormEditableProductParameter(ProductParametersBL.SelectedProductParameter);
                new EditProductParameterWindow{ }.ShowDialog();
            }, p => ProductParametersBL.SelectedProductParameter != null, ProductParametersBL, new string[] { "SelectedProductParameter" });

            RemoveSelectedProductParameterCommand = new SmartCommand(async _ =>
            {
                if (Dialog.ShowYesNo(TextHelper.DoYouWantToDeleteParameter) == System.Windows.MessageBoxResult.Yes)
                    await ProductParametersBL.RemoveSelectedProductParameter();

            }, p => ProductParametersBL.SelectedProductParameter != null, ProductParametersBL, new string[] { "SelectedProductParameter" });

            ProductParamsSaveChangesCommand = new SmartCommand(window =>
            {
                ProductParametersBL.ProductParamSaveChanges();
                WindowHelper.CloseWindow(window);

            }, p => ProductParametersBL.EditableProductParameter != null, ProductParametersBL, new string[] { "EditableProductParameter" });

            ProductParamsCloseEditWindowCommand = new SmartCommand(window =>
            {
                ProductParametersBL.ClearEditableProductParamsProps();
                WindowHelper.CloseWindow(window);
            });
        }

        private void GetProductParametersData(Product selectedProduct)
        {
            ProductParametersBL.ClearProductParamBufferProps();

            if (selectedProduct is null || selectedProduct.Revision is null || selectedProduct.Revision.Model is null)
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(
                    Shaper.NewErrorLog(
                        exceptionName: "the custom null reference ex",
                        message: "the selectedProduc is null",
                        innerExceptionMessage: "exception inside the refresh method",
                        methodName: "ProductParametersVM.GetProductParametersData",
                        projectName: "OMVP"));

                return;
            }

            ProductParametersBL.SelectedProduct = ProductBL.Instance.SelectedProduct; // Переброс

            //refresh combobox collection
            ModelParametersBL.CurrentModelParameters.Clear();
            SQLServer.Instance.ModelParameter.GetModelParameters(selectedProduct.Revision.Model.Id).ForEach(ModelParametersBL.CurrentModelParameters.Add);

            //refresh datagrid collection
            ProductParametersBL.CurrentProductParameters.Clear();
            SQLServer.Instance.ProductParameter.GetProductParameters(selectedProduct.Id).ForEach(ProductParametersBL.CurrentProductParameters.Add);
        }

        //ProductParametersView_commands
        public ICommand CreateNewProductParamCommand { get; set; }
        public ICommand OpenEditProductParameterWindowCommand { get; set; }
        public ICommand RemoveSelectedProductParameterCommand { get; set; }

        //EditProductParametersWindow_commands
        public ICommand ProductParamsSaveChangesCommand { get; set; }
        public ICommand ProductParamsCloseEditWindowCommand { get; set; }       
    }
}
