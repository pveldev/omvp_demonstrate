﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace OMVP.Converters
{
    class OperationNameConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
            if (value != null && value is Operation operation)
            {
                string additionalInfo = "";

                if (!string.IsNullOrEmpty(operation.RoutingNumber))
                    additionalInfo += " [" + operation.RoutingNumber + "]";

                return operation.Name + additionalInfo;
            }
            else
            {
                return "<Ошибка преобразования!>";
            }
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}