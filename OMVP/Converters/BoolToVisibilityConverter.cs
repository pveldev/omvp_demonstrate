﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace OMVP.Converters
{
    public class BoolToVisibilityConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
            if (value != null && value is bool IsVisible)
                return IsVisible ? Visibility.Visible : Visibility.Collapsed;
            else            
                return Visibility.Collapsed;
        }

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
