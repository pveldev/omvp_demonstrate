﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace OMVP.Converters
{
    class OrderTreesToDisplayNameConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
            if (value != null && (string.IsNullOrEmpty(value.ToString())))
            {
                return "";
            }


            if (value is OrderTree orderTree && orderTree != null)
            {
                if (string.IsNullOrEmpty(orderTree.Order.Letter))
                    return orderTree.Revision.Name;

                return orderTree.Revision.Name + " (литер " + orderTree.Order.Letter + ")" + $" [{orderTree.Order.Quantity} шт]" + $"[ код:{orderTree.Order.OrderCode}]";
            }
            else
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomInvalidCastException", "Не удалось привести к указанному типу", "", "OrderTreesToDisplayNameConverter", "OMVP"));
                return "Ошибка преобразования";
            }           
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
