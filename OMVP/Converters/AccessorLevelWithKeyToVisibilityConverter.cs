﻿using OMVP.Core;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace OMVP.Converters
{
    //новый конвертер (аналог AccessorLevelToVisibilityConverter). Работает сконтантами из коллекции AccessorLevelManager._accessorLevels
    class AccessorLevelWithKeyToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int accessorLevel && parameter is string key)
            {
                return accessorLevel >= AccessorLevelManager.GetAccessorLevel(key) ? Visibility.Visible : Visibility.Collapsed;
            }
            else
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomInvalidCastException", "Не удалось привести к указанному типу", "", "AccessorLevelWithKeyToVisibilityConverter", "OMVP"));
                return false;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
