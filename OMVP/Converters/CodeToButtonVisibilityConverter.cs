﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace OMVP.Converters
{
    //RevisionView button visibility conveter. Заменяется на CommandGenerator
    public class CodeToButtonVisibilityConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
            if (!string.IsNullOrEmpty(value.ToString()))
            {
                return value.ToString().Equals(CodeDictionary.Instance.AuxiliaryModelType.TopLevelModel.Code) ? Visibility.Visible : Visibility.Hidden;
            }
            else
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomInvalidCastException", "Не удалось привести к указанному типу", "", "CodeToButtonVisibilityConverter", "OMVP"));
                return Visibility.Visible;
            }          
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
