﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

namespace OMVP.Converters
{
    public class BoolToRowDetailsVisibilityConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
            if (value != null && value is bool IsVisible)
            {
                return IsVisible ? DataGridRowDetailsVisibilityMode.Visible : DataGridRowDetailsVisibilityMode.Collapsed;
            }
            else
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomInvalidCastException", "Не удалось привести к указанному типу", "", "BoolToRowDetailsVisibilityConverter", "OMVP"));
                return DataGridRowDetailsVisibilityMode.Visible;
            }
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
