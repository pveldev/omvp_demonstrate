﻿using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;


namespace OMVP.Converters
{
    class OperationStatusToBoolConverter : IValueConverter
    {
        //Специальный конвертер для комбобокса исполнителей и ее кнопки очистки выбранного исполнителя
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is OperationStatus operationStatus)           
                return operationStatus.Id != CodeDictionary.Instance.AuxiliaryOperationStatus.NotDone.Id;
        
            return false;          
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
