﻿using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace OMVP.Converters
{
    public class DispatchBarIsEnabledConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string tabName = value.ToString();
            if (!string.IsNullOrEmpty(tabName))
            {
                return tabName == "readyToDispatchTab" || tabName == "inWorkProductsTab";
            }
            else
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomInvalidCastException", $"Не удалось привести [{value}] к типу string", "", "DispatchBarVisibilityConverter", "OMVP"));
                return true;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
