﻿using OMVP.BL.CoreBL;
using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace OMVP.Converters
{
    class MultiProductToBoolConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
            string allowedStatusCodeOneLine = values[0] as string;
            if (values[1] != null && !string.IsNullOrEmpty(allowedStatusCodeOneLine))
            {
                string[] allowedStatusCodes = allowedStatusCodeOneLine.Split(',');
                if (values[1] is Product product)
                {
                    return (ProductStatusManager.ConvertCodesToIds(allowedStatusCodes).Contains(product.ProductStatusId));
                }
                else
                {
                    SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomInvalidCastException", "Не удалось привести к указанному типу", "", "MultiProductToBoolConverter", "OMVP"));
                    return false;
                }
            }
            return true;         
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}