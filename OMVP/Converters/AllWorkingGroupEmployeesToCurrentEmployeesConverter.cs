﻿using OMVP.BL.BLModels;
using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace OMVP.Converters
{
    class AllWorkingGroupEmployeesToCurrentEmployeesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is RouteMapProductOperation productOperation && (ProductOperationBL.Instance.AllCurrentWorkingGroupEmployees?.Any() ?? false))
            {
                List<Employee> employees = ProductOperationBL.Instance.AllCurrentWorkingGroupEmployees
                        .Where(currentWge => currentWge.WorkingGroupId == productOperation.Operation.WorkingGroupId)
                        .Select(currentWge => currentWge.Employee)
                        .ToList<Employee>();

                return employees;
            }
            else
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
