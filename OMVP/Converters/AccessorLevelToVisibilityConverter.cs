﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace OMVP.Converters
{
    class AccessorLevelToVisibilityConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
            try
            {
				int accessorLevel = System.Convert.ToInt32(value);
				int visibilityThreshold = System.Convert.ToInt32(parameter);
				
				return accessorLevel >= visibilityThreshold ? Visibility.Visible : Visibility.Collapsed;
			}
			catch (InvalidCastException ex)
			{
				SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog(ex, "AccessorLevelToVisibility"));
				return Visibility.Visible;
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
