﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace OMVP.Converters
{
    public class RevisionIdToOrderConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
            if (value is Revision revision && revision != null)
            {
                List<OrderTree> orderTrees = SQLServer.Instance.OrderTree.GetActualAndClaimedOrderTrees(revision.Id);
                return $"{orderTrees.Sum(orderTree => orderTree.DispatchedQuantity)}/{orderTrees.Sum(orderTree => orderTree.Order.Quantity)}";
            }
            else
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomInvalidCastException", "Не удалось привести к указанному типу", "", "RevisionIdToOrderConverter", "OMVP"));
                return 0;
            }
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}