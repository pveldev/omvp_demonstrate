﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace OMVP.Converters
{
    class OrderElementToMonthWithLetterConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
            if (value != null && (string.IsNullOrEmpty(value.ToString())))         
                return "";

            if (value is OrderElement orderElement && orderElement.Month >= 1 && orderElement.Month <= 12)
            {
                string additionalInfo = string.IsNullOrEmpty(orderElement.OrderTree.Order.Letter) ? "" : $" ({orderElement.OrderTree.Order.Letter} литер)";
                return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(orderElement.Month) + additionalInfo + $" ({orderElement.CompletedQuantity}/{orderElement.Quantity}) [заказ {orderElement.OrderTree.Order.ExecutionDate.Year}]";
            }
            else
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomInvalidCastException", "Ошибка преобазования элемента заказа в формат [месяц:литер]", "", "OrderElementToMonthWithLetterConverter", "OMVP"));
                return "Ошибка преобразования";
            }
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}