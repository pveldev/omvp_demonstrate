﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace OMVP.Converters
{
    class ProgressToPercentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double doubleProgress)
            {
                if (!double.IsNaN(doubleProgress))
                {
                    return CalculateProgress(doubleProgress);
                }
                return "";
            }
            else if (value is float floatProgress)
            {
                return CalculateProgress((double)floatProgress);
            }
            else
            {
                return "Ошибка преобразования!";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        private string CalculateProgress(double progress) => string.Concat(Math.Round((progress* 100), 0), "%");

        
    }
}
