﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace OMVP.Converters
{
    class EmployeeNameConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
            if (value != null && value is Employee employee)
                return employee.LastName + " " + employee.FirstName;
            else           
                return "<Нет данных>";           
        }

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
