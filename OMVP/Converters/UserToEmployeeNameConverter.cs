﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace OMVP.Converters
{
    class UserToEmployeeNameConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
            if (value is User user && user != null)
            {
                if (user.Employee != null)
                    return user.Employee.FirstName + " " + user.Employee.LastName;
                return user.PCName;
            }
            else
            {
                return "пользователь не найден";
            }			
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
