﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace OMVP.Converters
{
    //ProductOperationView
    public class OperationStatusToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string firstStatus && parameter is string secondStatus)
            {
                return firstStatus.Equals(secondStatus) ? Visibility.Visible : Visibility.Collapsed;
            }
            else
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomInvalidCastException", "Не удалось привести к указанному типу", "", "OperationStatusToVisibilityConverter", "OMVP"));
                return Visibility.Collapsed;
            }
        } 

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
