﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace OMVP.Converters
{
    public class RevisionToTabVisibilityConverter : IValueConverter
	{
		//parameter содержит в себе коды моделей, для которых будет возвращено Visibility.Visible
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
            if (value is Revision revision && revision != null && parameter is string castedParameter && !string.IsNullOrEmpty(castedParameter))
            {
                string[] splited = castedParameter.Split(new char[] { '_' });
                return splited.Contains(revision.Model.ModelType.Code) 
                    ? Visibility.Visible : Visibility.Collapsed;
            }
            else
            {
                return Visibility.Visible;
            }
        }

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
