﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using System;
using System.Globalization;
using System.Windows.Data;

namespace OMVP.Converters
{
    class BoolToLetterStatusConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
            if (value != null && value is bool status)
            {
                return status ? "Литерный" : "";
            }
            else
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomInvalidCastException", "Не удалось привести к указанному типу", "", "BoolToLetterStatusConverter", "OMVP"));
                return "Ошибка преобразования";
            }         
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
