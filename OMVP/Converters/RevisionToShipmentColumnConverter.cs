﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace OMVP.Converters
{
    public class RevisionToShipmentColumnConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
            if (value is Revision revision && revision != null)
            {
                List<OrderElement> orderElements = SQLServer.Instance.OrderElement.GetActualAndClaiemdOrderElementsByRevisionId(revision);
                if (orderElements?.Any() ?? false)
                {
                    List<OrderElement> actualOrderElements = orderElements.Where(orderElement => orderElement.CompletedQuantity != orderElement.Quantity).ToList();
                    if (actualOrderElements.Count == 0)
                       return "Заказ выполнен!";
                    
                    int minMonth = actualOrderElements.Where(orderElement => orderElement.CompletedQuantity != orderElement.Quantity).Min(orderElement => orderElement.Month);
                    int sumQuantity = actualOrderElements.Where(item => item.Month == minMonth).Sum(item => item.Quantity);
                    int sumCompletedQuantity = actualOrderElements.Where(item => item.Month == minMonth).Sum(item => item.CompletedQuantity);
                    string month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(System.Convert.ToInt32(minMonth));

                    return $"{month} ({sumCompletedQuantity}/{sumQuantity})";
                }
                return $"Ежемесячная отгрузка на {DateTime.Now.Year} не заполнена!";
            }
            else
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomInvalidCastException", "Не удалось привести к указанному типу", "", "RevisionToShipmentColumnConverter", "OMVP"));
                return "Ошибка преоразования";
            }      
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
    }
}
