﻿using OMVP.BL.CoreBL;
using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace OMVP.Converters
{
    class ProductToCanChangeRouteMapSpecialConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                if (value is Product product)
                {
                    return product.RouteMap == null
                        && ProductStatusManager.CanChangeRouteMapCheck(product.ProductStatusId)
                        && product.RouteMapProductOperations.Count == 0;
                }
                else
                {
                    SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomInvalidCastException", "Не удалось привести value к Product", $"value:{value}", "ProductToCanChangeRouteMapSpecialConverter", "OMVP"));
                    return false;
                }
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
