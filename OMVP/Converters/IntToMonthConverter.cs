﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using System;
using System.Globalization;
using System.Windows.Data;

namespace OMVP.Converters
{
    public class IntToMonthConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
            if (value == null)
                return value;          

            if (value is byte monthNumber && monthNumber > 0 && monthNumber < 13)           
                return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(monthNumber);           

            SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomArgumentOutOfRangeException_or_InvalidCast", "Номер месяца вне допустимого диапазона или не удалось привести value к byte", "", "IntToMonthConverter", "OMVP"));
            return value;
        }

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
