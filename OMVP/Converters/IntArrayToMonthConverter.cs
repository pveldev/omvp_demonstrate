﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace OMVP.Converters
{
	public class IntArrayToMonthConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
            if (value is byte[] monthNumbers)
            {
                if (monthNumbers.All(month => month < 1 && month > 12))
                {
                    SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomArgumentOutOfRangeException", "Номер месяца вне допустимого диапазона", "", "IntArrayToMonthConverter", "OMVP"));
                    return monthNumbers;
                }

                return monthNumbers.Select(monthNumber => CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(monthNumber));
            }
            else
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomInvalidCastException", "Не удалось привести к указанному типу", "", "IntArrayToMonthConverter", "OMVP"));
                return value;
            }         
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
