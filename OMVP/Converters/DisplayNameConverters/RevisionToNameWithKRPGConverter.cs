﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace OMVP.Converters.DisplayNameConverters
{
    class RevisionToNameWithKRPGConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && (string.IsNullOrEmpty(value.ToString())))         
                return "";         

            if (value is Revision revision)
            {
                if (!string.IsNullOrEmpty(revision.Model.KRPG))
                    return revision.Name + " [КРПГ." + revision.Model.KRPG + "]";
                return revision.Name;
            }
            else
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomInvalidCastException", "Не удалось привести к указанному типу", "", "RevisionToNameWithKRPGConverter", "OMVP"));
                return "<Ошибка преобразования!>";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}