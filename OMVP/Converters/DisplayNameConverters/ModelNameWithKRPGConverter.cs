﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace OMVP.Converters.DisplayNameConverters
{
    class ModelNameWithKRPGConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
            if (value != null && (string.IsNullOrEmpty(value.ToString())))
                return "";
            

            if (value is Model model)
            {                
                if (!string.IsNullOrEmpty(model.KRPG))
                    return model.Name + " [КРПГ." + model.KRPG + "]";
                return model.Name;
            }
            else
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomInvalidCastException", "Не удалось привести к указанному типу", "", "ModelNameWithKRPGConverter", "OMVP"));
                return "<Ошибка преобразования!>";
            }
        }

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}