﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace OMVP.Converters
{
    public class RevisionToActionsConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
            if (value is Revision revision && revision != null && revision.Model != null)
                return CommandGenerator.Instance.GetNavigationCommands(revision.Model.ModelTypeId);
            else
                return null;                    
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
