﻿using OMVP.BL.BLModels;
using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.Models;
using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace OMVP.Converters
{
    //Личный конвертор для Description-TextBox (InstallationProductView)
    //Возвращает Visible, если происходит изменение в посадочном месте вне зависимости от того установка эти или демонтаж
    class MultiProductBomToDescriptionVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] is ProductBOM changedBom && values[1] is ProductBOM selectedBom)
            {
                if (changedBom != null && selectedBom != null && ProductBomLogsContainsChangedBom(selectedBom.Id) && ChangedProductBomsContainsKey(changedBom.Id))
                    return Visibility.Visible;
            }
            return Visibility.Hidden;       
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private bool ChangedProductBomsContainsKey(int key) => InstallationProductBL.Instance.ChangedProductBoms.ContainsKey(key);

        private bool ProductBomLogsContainsChangedBom(int changedBomId)
        {
            if (ProductBomLogsNotNull())
                return InstallationProductBL.Instance.ProductBomLogs.Any(bomLog => bomLog.ProductBomId == changedBomId); //!= null ? true : false;
            else
                return false;
        }
        private bool ProductBomLogsNotNull() => InstallationProductBL.Instance.ProductBomLogs?.Any() ?? false;
    }
}
