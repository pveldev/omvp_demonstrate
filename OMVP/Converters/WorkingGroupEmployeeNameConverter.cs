﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace OMVP.Converters
{
    //Конвертирует работника в "Имя работника" + "фамилия работника"
    public class WorkingGroupEmployeeNameConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
            if (value != null && value is WorkingGroupEmployee workingGroupEmployee && workingGroupEmployee.Employee != null)
            {
                return workingGroupEmployee.Employee.FirstName + " " + workingGroupEmployee.Employee.LastName;
            }
            else
            {
                return "<Ошибка преобразования>";
            }         
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
