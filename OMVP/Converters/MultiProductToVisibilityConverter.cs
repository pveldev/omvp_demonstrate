﻿using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace OMVP.Converters
{
    public class MultiProductToVisibilityConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
            if (parameter is string allowedStatusCodeOneLine)
            {
                if (!string.IsNullOrEmpty(allowedStatusCodeOneLine))
                {
                    string[] allowedStatusCodes = allowedStatusCodeOneLine.Split(',');
                    if (values[0] != null && values[0] is Product product && product.ProductStatus != null)
                        return (allowedStatusCodes.Contains(product.ProductStatus.Code)) ? Visibility.Visible : Visibility.Collapsed;
                    return Visibility.Collapsed;
                }
                return Visibility.Visible;
            }
            else
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomInvalidCastException", "Не удалось привести к указанному типу", "", "MultiProductToVisibilityConverter", "OMVP"));
                return Visibility.Collapsed;
            }          
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}