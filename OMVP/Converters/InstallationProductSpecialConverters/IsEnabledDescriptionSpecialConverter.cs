﻿using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace OMVP.Converters.InstallationProductSpecialConverters
{
    class IsEnabledDescriptionSpecialConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {       
            if (values[0] is string castedValue && values[1] is bool isEnabled)
            {
                return !string.IsNullOrEmpty(castedValue) && isEnabled;
            }
            else
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomInvalidCastException", "Не удалось привести к указанному типу", "", "StringToInversionBoolConverter", "OMVP"));
                return true;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
