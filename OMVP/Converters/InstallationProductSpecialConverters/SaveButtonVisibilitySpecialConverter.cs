﻿using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace OMVP.Converters.InstallationProductSpecialConverters
{
    class SaveButtonVisibilitySpecialConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool isEnabled)
            {
                return isEnabled ? Visibility.Visible : Visibility.Collapsed;
            }
            else
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomInvalidCastException", "Не удалось привести value к bool", "", "SaveButtonVisibilitySpecialConverter", "OMVP"));
                return Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
