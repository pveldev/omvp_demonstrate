﻿using OMVP.BL.CoreBL;
using OMVP.DAL;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace OMVP.Converters.InstallationProductSpecialConverters
{
    class IsEnabledComboboxSpecialConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] is string castedValue && values[1] is bool IsEnabledParameter && values[2] is ProductBOM slot)
            {
                return string.IsNullOrEmpty(castedValue) && IsEnabledParameter && ProductStatusManager.CanChangeSlotCheck(slot.Product.ProductStatusId);
            }
            else
            {
                SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog("CustomInvalidCastException", "Не удалось привести к указанному типу", "", "StringToBoolConverter", "OMVP"));
                return false;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
