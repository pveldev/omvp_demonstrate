﻿using OMVP.CoreOMVP;
using OMVP.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OMVP.View
{
    /// <summary>
    /// Interaction logic for ModelTypeView.xaml
    /// </summary>
    public partial class ModelTypeView : BaseView
	{
		public ModelTypeView(Action<BaseVM, bool> refresh)
		{
			InitializeComponent();
			SetContext(refresh);
		}
	}
}
