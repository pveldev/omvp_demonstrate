﻿using OMVP.Core;

namespace OMVP.View.HelpWindow
{
    /// <summary>
    /// Логика взаимодействия для ProductParametersHelper.xaml
    /// </summary>
    public partial class ProductParametersHelper : BaseWindowHelper
    {
        public ProductParametersHelper(bool isEmptyInit = false)
        {
            if (!isEmptyInit)
                InitializeComponent();
        }

        public override void ShowHelper() => new ProductParametersHelper().Show();
    }
}
