﻿using OMVP.Core;

namespace OMVP.View.HelpWindow
{
    /// <summary>
    /// Логика взаимодействия для RouteMapHelper.xaml
    /// </summary>
    public partial class RouteMapHelper : BaseWindowHelper
    {
        public RouteMapHelper(bool isEmptyInit = false)
        {
            if (!isEmptyInit)
                InitializeComponent();
        }

        public override void ShowHelper() => new RouteMapHelper().Show();
    }
}
