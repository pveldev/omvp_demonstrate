﻿using OMVP.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OMVP.View.HelpWindow
{
    /// <summary>
    /// Логика взаимодействия для ModelParametersHelper.xaml
    /// </summary>
    public partial class ModelParametersHelper : BaseWindowHelper
    {

        public ModelParametersHelper(bool isEmptyInit = false)
        {
            if (!isEmptyInit)
                InitializeComponent();
        }

        public override void ShowHelper() => new ModelParametersHelper().Show();
    }
}
