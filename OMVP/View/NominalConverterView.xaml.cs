﻿using OMVP.CoreOMVP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OMVP.View
{
    /// <summary>
    /// Логика взаимодействия для NominalConverterView.xaml
    /// </summary>
    public partial class NominalConverterView : UserControl
    {
        public NominalConverterView()
        {
            InitializeComponent();
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            foreach (char ch in e.Text)
            {
                e.Handled = !Char.IsDigit(ch);
            }           
        }
    }
}
