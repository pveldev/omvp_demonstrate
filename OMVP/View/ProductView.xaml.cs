﻿using OMVP.BL.BLModels;
using OMVP.CoreOMVP;
using OMVP.DAL;
using OMVP.DAL.Models;
using OMVP.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OMVP.View
{
	/// <summary>
	/// Interaction logic for ProductView.xaml
	/// </summary>
	public partial class ProductView : BaseView
	{
		public ProductView(Action<BaseVM, bool> refresh)
		{
			InitializeComponent();
			SetContext(refresh);
        }

		private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			e.Handled = MyValidation.IsNumber(e.Text);
		}

        private void TextBox_IsLetter(object sender, TextCompositionEventArgs e)
        {
            e.Handled = MyValidation.IsLetter(e.Text);
        }
    }
}
