﻿using OMVP.CoreOMVP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OMVP.View.Popup
{
	/// <summary>
	/// Логика взаимодействия для EditProductWindow.xaml
	/// </summary>
	public partial class EditProductWindow : Window
	{
		public EditProductWindow()
		{
			InitializeComponent();
		}
        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = MyValidation.IsSerialNumber(e.Text);
        }

        private void TextBox_IsLetter(object sender, TextCompositionEventArgs e)
        {
            e.Handled = MyValidation.IsLetter(e.Text);
        }
    }
}
