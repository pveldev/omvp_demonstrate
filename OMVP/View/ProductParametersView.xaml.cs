﻿using OMVP.CoreOMVP;
using OMVP.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OMVP.View
{
    /// <summary>
    /// Логика взаимодействия для ProductParametersView.xaml
    /// </summary>
    public partial class ProductParametersView : BaseView
    {
        public ProductParametersView(Action<BaseVM, bool> refresh)
        {
            InitializeComponent();
            SetContext(refresh);
        }
    }
}
