﻿using OMVP.Core;

namespace OMVP.DAL.Models
{
    public partial class Employee : BasicNotifyModel { }
    public partial class ErrorLog : BasicNotifyModel { }
    public partial class Model : BasicNotifyModel { }
    public partial class ModelXWorkingGroup : BasicNotifyModel { }
    public partial class ModelType : BasicNotifyModel { }
    public partial class Operation : BasicNotifyModel { }
    public partial class OperationStatus : BasicNotifyModel { }
    public partial class OrderElement : BasicNotifyModel { }
    public partial class Order : BasicNotifyModel { }
    public partial class OrderTree : BasicNotifyModel { }
    public partial class OrderStatus : BasicNotifyModel { }
    public partial class ProductBOM : BasicNotifyModel { }
    public partial class ProductBomLog : BasicNotifyModel { }
    public partial class ProductLog : BasicNotifyModel { }
    public partial class Product : BasicNotifyModel { }
    public partial class ProductStatus : BasicNotifyModel { }
    public partial class RevisionBOM : BasicNotifyModel { }
    public partial class Revision : BasicNotifyModel { }
    public partial class Role : BasicNotifyModel { }
    public partial class RouteMapOperation : BasicNotifyModel { }
    public partial class RouteMapProductOperation : BasicNotifyModel { }
    public partial class RouteMap : BasicNotifyModel { }
    public partial class User : BasicNotifyModel { }
    public partial class WorkingGroupEmployee : BasicNotifyModel { }
    public partial class WorkingGroup : BasicNotifyModel { }

    public partial class ModelParameter : BasicNotifyModel { }
    public partial class ParameterType : BasicNotifyModel { }
    public partial class ProductParameter : BasicNotifyModel { }
}
