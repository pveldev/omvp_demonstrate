﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.Models
{
	//Для отработки LINQ .Except в OrderBL
	public partial class OrderElement
	{
		public override bool Equals(object obj)
		{
			return (Month == (obj as OrderElement)?.Month);
		}
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}
}
