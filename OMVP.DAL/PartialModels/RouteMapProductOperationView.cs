﻿using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.PartialModels
{
    public partial class RouteMapProductOperationComparer : IEqualityComparer<RouteMapProductOperation>
    {      
        public bool Equals(RouteMapProductOperation x, RouteMapProductOperation y)
        {
            if (string.Equals(x.OperationId.ToString(), y.OperationId.ToString(), StringComparison.OrdinalIgnoreCase) && x.Priority == y.Priority)
            {
                return true;
            }
            return false;
        }

        public int GetHashCode(RouteMapProductOperation obj)
        {
            return (obj.OperationId.ToString() + obj.Priority.ToString()).GetHashCode();
        }       
    }
}
