﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL
{
    public class HandleQuery
    {
        public static string GetRevisionsChildren => @"	SELECT r.* FROM RevisionBOMs bom 
													    INNER JOIN Revisions r ON r.Id = bom.BOMRevisionId
													    WHERE bom.RevisionId = @revisionId";
		public static string GetProductsChildren => @"	SELECT p.* FROM ProductBOMs bom 
												        INNER JOIN Products p ON p.Id = bom.BOMProductId
												        WHERE bom.ProductId = @productId";
		public static string SelectOne => "SELECT 1";

        public static string UpdateOrderElements => @"  UPDATE OrderElements
                                                        SET Quantity = @quantity
                                                        WHERE Id = @id";

        public static string GetProductCount(int inWorkId, int readyToInstallId, int installedId, int readyToDispatchId, 
            int reclamationId, int installedInReclamationId, int readyToReclamationDispatchId, int readyToReclamationInstallId) 
            => $@"
                SELECT 
	                 COUNT(case WHEN ProductStatusId = {inWorkId} AND Progress >= 0.9 then 1 else NULL end) AS InWorkAtTheExitCount
	                ,COUNT(case WHEN ProductStatusId = {inWorkId} AND Progress < 0.9 then 1 else NULL end) AS InWorkCount
	                ,COUNT(case ProductStatusId WHEN {readyToInstallId} THEN 1 
								                WHEN {installedId} THEN 1 
								                WHEN {readyToDispatchId} THEN 1 else NULL end) AS ReadyCount
                    ,COUNT(case ProductStatusId WHEN {reclamationId} THEN 1 else NULL end) AS ReclamationWorkCount 
                    ,COUNT(case ProductStatusId WHEN {installedInReclamationId} THEN 1 else NULL end) AS InstalledInReclamatioCount 
	                ,COUNT(case ProductStatusId WHEN {readyToReclamationDispatchId} THEN 1 
								                WHEN {readyToReclamationInstallId} THEN 1 else NULL end) AS ReclamationReadyCount
                FROM Products p
                Where p.RevisionId = @revisionId ";
        public static string ProductRouteMapIsComplete(int productId, int completedStatusId)
            => $@"
                SELECT CASE WHEN EXISTS
                (
	                SELECT *
	                FROM RouteMapProductOperations
	                WHERE ProductId = {productId} AND OperationStatusId != {completedStatusId}
                )
                THEN CAST (0 AS BIT)
                ELSE CAST (1 AS BIT) END
                ";

        //Рассчитывает прогресс изделия после начала операции для группы изделий

        #region CalculateNewProgress_methods
        public static string CalculateProgressAfterStartOperation(string selectedProductIds, int completeStatusId, int inProgressStatusId, int equipmentInProgressStatusId,
            float operationStartWeight, float stepWeight)
            => CalculateProgress("+", selectedProductIds, completeStatusId, inProgressStatusId, equipmentInProgressStatusId, operationStartWeight, stepWeight);
        public static string CalculateProgressAfterReExecuteOrCancelOperation(string selectedProductIds, int completeStatusId, int inProgressStatusId, int equipmentInProgressStatusId,
         float operationStartWeight, float stepWeight)
         => CalculateProgress("-", selectedProductIds, completeStatusId, inProgressStatusId, equipmentInProgressStatusId, operationStartWeight, stepWeight);

        //action == "+" => для рассчета прогресса после начала операции
        //action == "-" => для рассчета прогресса после повторного выполнения операции
        public static string CalculateProgress(string action, string selectedProductIds, int completeStatusId, int inProgressStatusId, int equipmentInProgressStatusId,
           float operationStartWeight, float stepWeight)
           => $@"
                SELECT 
	            p.Id AS [ProductId]
	            ,(
		            (COUNT(CASE OperationStatusId	WHEN {completeStatusId} THEN 1.0 ELSE NULL END) * {stepWeight}) 
		            +
		            (COUNT(CASE OperationStatusId	WHEN {inProgressStatusId} THEN 1.0
										            WHEN {equipmentInProgressStatusId} THEN 1.0 ELSE NULL END) * {operationStartWeight})
		            {action} {operationStartWeight}
	            )
		            / 																												   
	            (
		            (CAST( COUNT(*) AS FLOAT(24)) * {stepWeight})
	            )
		            AS [NewProductProgress]

                FROM RouteMapProductOperations rmpo
                INNER JOIN Products p ON p.Id = rmpo.ProductId
                WHERE rmpo.ProductId IN ({selectedProductIds})
                GROUP BY p.Id
                ";
        #endregion

        #region ProductLogs_ProductBomLogs
        public static string SelectFromProductLogs(int count) => $"SELECT TOP ({count}) pl.* FROM ProductLogs pl";
        public const string JoinProductsForProductLog   = " INNER JOIN Products p ON p.Id = pl.ProductId";
        public const string JoinUsersForProductLog      = " INNER JOIN Users u ON u.Id = pl.UserId";
        public const string RevisionEquality            = " p.RevisionId = " + RevisioIdParam; //Общее для ProductLogs и ProductBomLogs
        public const string PCEqualilty                 = " u.Id = " + UserIdParam;
        public static string StartDateInequality(string tableAlias) => $" {tableAlias}.Date > " + StartDateParam; //Общее для ProductLogs и ProductBomLogs
        public static string StopDateInequality(string tableAlias) => $" {tableAlias}.Date < " + EndDateParam; //Общее для ProductLogs и ProductBomLogs
               
        public const string RevisioIdParam  = "@revisionId"; //Общее для ProductLogs и ProductBomLogs
        public const string UserIdParam     = "@Id";
        public const string StartDateParam  = "@startDate"; //Общее для ProductLogs и ProductBomLogs
        public const string EndDateParam    = "@stopDate"; //Общее для ProductLogs и ProductBomLogs
              
              
              
        public static string SelectFromProductBomLogs(int count) => $"SELECT TOP ({count}) pbl.* FROM ProductBomLogs pbl";
        public const string JoinProductsForProductBomLog    = " INNER JOIN Products p ON bom.ProductId = p.Id";
        public const string JoinProductBOMsForProductBomLog = " INNER JOIN ProductBOMs bom ON pbl.ProductBomId = bom.Id";
               
        public const string SlotIdParam         = "@slotId";
        public const string ParentProductParam  = "@parentProductId";
        public const string ChildProductParam   = "@childProductId";
               
        public const string SlotIdEquality          = " bom.RevisionBomId = " + SlotIdParam;
        public const string ParentProductIdEquality = " bom.ProductId = " + ParentProductParam;
        public const string ChildProductIdEquality  = " pbl.ProductId = " + ChildProductParam;
        public static string OrderByDateDesc(string table)  => $" ORDER BY {table}.Date DESC "; //Общее для ProductLogs и ProductBomLogs

        #endregion

        #region GetProductTree
        //Так же возвращает и изделие-родителя, полученное на входе (для уверенности, что весь список изделий будет из одного контекста данных)
        public static string GetProductTree(string parentProductIds, bool withParent = true) 
            => withParent ?
                string.Concat(FormSlotsTree(parentProductIds), RefreshParentProduct(parentProductIds), ConversionSlotsToProducts())
                :
                string.Concat(FormSlotsTree(parentProductIds), ConversionSlotsToProducts());
        private static string FormSlotsTree(string parentProductIds) 
            => $@"
                        --form slots tree
                        ;WITH bomView AS 
                        (
	                        SELECT depth = 0, highLevelBom.*
	                        FROM ProductBOMs highLevelBom
	                        WHERE ProductId IN ({parentProductIds})

	                        UNION ALL

	                        SELECT depth = bView.depth+1, bom.*
	                        FROM bomView bView
	                        join ProductBOMs bom 
		                        ON bom.ProductId = bView.BOMProductId

	                        WHERE depth<99
                        )
                    ";
        private static string RefreshParentProduct(string parentProductIds)
            => $@"   
                SELECT * 
                FROM Products p 
                WHERE p.Id IN ({parentProductIds})
                UNION ALL
            ";
        private static string ConversionSlotsToProducts()
            => @"
                SELECT p.*
                FROM bomView
                INNER JOIN Products p 
	                ON p.Id = bomView.BOMProductId 
                ORDER BY p.Id
                OPTION (maxrecursion 99)
                ";
        #endregion

        //Метод возвращает список изделий, которые установлены в несколько посадочных мест одновременно (иными словами: для которых нарушена целостность данных)
        public static string GetBuggedProducts()
            => @"
                SELECT * FROM Products p 
                WHERE p.Id IN
                (
	                SELECT (plog.BOMProductId) AS [BOMProductId]
	                FROM [ProductBOMs] plog
	                GROUP BY plog.BOMProductId
	                HAVING COUNT (plog.BOMProductId) > 1
                )
                ";
    }
}
