namespace OMVP.DAL
{
	using System;
	using System.Data.Entity;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;
	using OMVP.DAL.Models;

	public partial class OmvpDataModel : DbContext
	{
		private static readonly string _baseConnectionString = "OmvpDataModel";
		public OmvpDataModel()
			: base($"name={_baseConnectionString}")
		{
		}
		public OmvpDataModel(string connectionString)
		: base(connectionString)
		{
        }

        public static string GetDefaultConnectionString() => _baseConnectionString;

		public virtual DbSet<Employee> Employees { get; set; }
		public virtual DbSet<ErrorLog> ErrorLogs { get; set; }
		public virtual DbSet<Model> Models { get; set; }
		public virtual DbSet<ModelXWorkingGroup> ModelsXWorkingGroups { get; set; }
        public virtual DbSet<ModelType> ModelTypes { get; set; }
		public virtual DbSet<Operation> Operations { get; set; }
		public virtual DbSet<OperationStatus> OperationStatuses { get; set; }
		public virtual DbSet<OrderElement> OrderElements { get; set; }
		public virtual DbSet<Order> Orders { get; set; }
		public virtual DbSet<OrderTree> OrderTrees { get; set; }
		public virtual DbSet<OrderStatus> OrderStatuses { get; set; }
		public virtual DbSet<ProductBOM> ProductBOMs { get; set; }
		public virtual DbSet<ProductBomLog> ProductBomLogs { get; set; }
        public virtual DbSet<ProductLog> ProductLogs { get; set; }
		public virtual DbSet<Product> Products { get; set; }
		public virtual DbSet<ProductStatus> ProductStatuses { get; set; }
		public virtual DbSet<RevisionBOM> RevisionBOMs { get; set; }
		public virtual DbSet<Revision> Revisions { get; set; }
		public virtual DbSet<Role> Roles { get; set; }
		public virtual DbSet<RouteMapOperation> RouteMapOperations { get; set; }
		public virtual DbSet<RouteMapProductOperation> RouteMapProductOperations { get; set; }
		public virtual DbSet<RouteMap> RouteMaps { get; set; }
		public virtual DbSet<User> Users { get; set; }
		public virtual DbSet<WorkingGroupEmployee> WorkingGroupEmployees { get; set; }
		public virtual DbSet<WorkingGroup> WorkingGroups { get; set; }

		public virtual DbSet<ModelParameter> ModelParameters { get; set; }
		public virtual DbSet<ParameterType> ParameterTypes { get; set; }
		public virtual DbSet<ProductParameter> ProductParameters { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Employee>()
				.Property(e => e.FirstName)
				.IsUnicode(false);

			modelBuilder.Entity<Employee>()
				.Property(e => e.LastName)
				.IsUnicode(false);

			modelBuilder.Entity<Employee>()
				.HasMany(e => e.WorkingGroupEmployees)
				.WithRequired(e => e.Employee)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<Model>()
				.Property(e => e.Name)
				.IsUnicode(false);

			modelBuilder.Entity<Model>()
				.Property(e => e.KRPG)
				.IsUnicode(false);

			modelBuilder.Entity<Model>()
				.HasMany(e => e.Revisions)
				.WithRequired(e => e.Model)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<Model>()
				.HasMany(e => e.RevisionBOMs)
				.WithRequired(e => e.Model)
				.WillCascadeOnDelete(false);

            modelBuilder.Entity<ModelType>()
				.Property(e => e.Name)
				.IsUnicode(false);

			modelBuilder.Entity<ModelType>()
				.Property(e => e.Code)
				.IsUnicode(false);

			modelBuilder.Entity<ModelType>()
				.Property(e => e.Description)
				.IsUnicode(false);

			modelBuilder.Entity<ModelType>()
				.HasMany(e => e.Models)
				.WithRequired(e => e.ModelType)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<Operation>()
				.Property(e => e.Name)
				.IsUnicode(false);

			modelBuilder.Entity<Operation>()
				.Property(e => e.Description)
				.IsUnicode(false);

			modelBuilder.Entity<Operation>()
				.Property(e => e.Code)
				.IsUnicode(false);

			modelBuilder.Entity<Operation>()
				.HasMany(e => e.Products)
				.WithRequired(e => e.Operation)
				.HasForeignKey(e => e.CurrentOperationId)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<Operation>()
				.HasMany(e => e.RouteMapOperations)
				.WithRequired(e => e.Operation)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<OperationStatus>()
				.Property(e => e.Code)
				.IsUnicode(false);

			modelBuilder.Entity<OperationStatus>()
				.Property(e => e.Name)
				.IsUnicode(false);

			modelBuilder.Entity<OperationStatus>()
				.HasMany(e => e.RouteMapProductOperations)
				.WithRequired(e => e.OperationStatus)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<OrderElement>()
				.Property(e => e.Description)
				.IsUnicode(false);

			modelBuilder.Entity<Order>()
				.Property(e => e.Contract)
				.IsUnicode(false);

			modelBuilder.Entity<Order>()
				.Property(e => e.Customer)
				.IsUnicode(false);

			modelBuilder.Entity<Order>()
				.Property(e => e.SHPZCode)
				.IsUnicode(false);

			modelBuilder.Entity<Order>()
				.Property(e => e.OrderCode)
				.IsUnicode(false);

			modelBuilder.Entity<Order>()
				.Property(e => e.Description)
				.IsUnicode(false);

			modelBuilder.Entity<OrderStatus>()
				.Property(e => e.Name)
				.IsUnicode(false);

			modelBuilder.Entity<OrderStatus>()
				.Property(e => e.Code)
				.IsUnicode(false);

			modelBuilder.Entity<OrderStatus>()
				.HasMany(e => e.Orders)
				.WithRequired(e => e.OrderStatus)
				.WillCascadeOnDelete(false);	

			modelBuilder.Entity<ProductBOM>()
				.Property(e => e.Description)
				.IsUnicode(false);

			modelBuilder.Entity<Product>()
				.Property(e => e.SerialNumber)
				.IsUnicode(false);

			modelBuilder.Entity<Product>()
				.Property(e => e.Description)
				.IsUnicode(false);

			modelBuilder.Entity<Product>()
				.HasMany(e => e.ProductBOMs)
				.WithRequired(e => e.Product)
				.HasForeignKey(e => e.ProductId)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<Product>()
				.HasMany(e => e.ProductBOMs1)
				.WithOptional(e => e.Product1)
				.HasForeignKey(e => e.BOMProductId);

			modelBuilder.Entity<Product>()
				.HasMany(e => e.ProductLogs)
				.WithRequired(e => e.Product)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<Product>()
				.HasMany(e => e.RouteMapProductOperations)
				.WithRequired(e => e.Product)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ProductStatus>()
				.Property(e => e.Name)
				.IsUnicode(false);

			modelBuilder.Entity<ProductStatus>()
				.Property(e => e.Code)
				.IsUnicode(false);

            modelBuilder.Entity<ProductStatus>()
                .HasMany(e => e.Products)
                .WithRequired(e => e.ProductStatus)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RevisionBOM>()
				.Property(e => e.Name)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<RevisionBOM>()
				.HasMany(e => e.ProductBOMs)
				.WithRequired(e => e.RevisionBOM)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<Revision>()
				.Property(e => e.Name)
				.IsUnicode(false);
	
			modelBuilder.Entity<Revision>()
				.HasMany(e => e.Products)
				.WithRequired(e => e.Revision)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<Revision>()
				.HasMany(e => e.RevisionBOMs)
				.WithRequired(e => e.Revision)
				.HasForeignKey(e => e.BOMRevisionId)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<Revision>()
				.HasMany(e => e.RevisionBOMs1)
				.WithRequired(e => e.Revision1)
				.HasForeignKey(e => e.RevisionId)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<Revision>()
				.HasMany(e => e.RouteMaps)
				.WithRequired(e => e.Revision)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<Role>()
				.Property(e => e.Name)
				.IsUnicode(false);

			modelBuilder.Entity<Role>()
				.Property(e => e.Code)
				.IsUnicode(false);

			modelBuilder.Entity<Role>()
				.HasMany(e => e.Employees)
				.WithRequired(e => e.Role)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<RouteMap>()
				.Property(e => e.Name)
				.IsUnicode(false);

			modelBuilder.Entity<RouteMap>()
				.HasMany(e => e.RouteMapOperations)
				.WithRequired(e => e.RouteMap)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<WorkingGroup>()
				.Property(e => e.Name)
				.IsUnicode(false);

			modelBuilder.Entity<WorkingGroup>()
				.HasMany(e => e.WorkingGroupEmployees)
				.WithRequired(e => e.WorkingGroup)
				.WillCascadeOnDelete(false);
		}
	}
}
