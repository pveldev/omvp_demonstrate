namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoringRevisionBomClaim : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Revisions", "BomsIsClaimed", c => c.Boolean(nullable: false));
            Sql(@"
                    UPDATE Revisions
                    SET BomsIsClaimed = 1
                ");

            DropColumn("dbo.RevisionBOMs", "IsClaimed");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RevisionBOMs", "IsClaimed", c => c.Boolean(nullable: false));
            DropColumn("dbo.Revisions", "BomsIsClaimed");
        }
    }
}
