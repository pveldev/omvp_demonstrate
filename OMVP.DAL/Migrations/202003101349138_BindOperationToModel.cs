namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BindOperationToModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Operations", "ModelId", c => c.Int());
            CreateIndex("dbo.Operations", "ModelId");
            AddForeignKey("dbo.Operations", "ModelId", "dbo.Models", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Operations", "ModelId", "dbo.Models");
            DropIndex("dbo.Operations", new[] { "ModelId" });
            DropColumn("dbo.Operations", "ModelId");
        }
    }
}
