namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProductBomLogTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductBomLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductBomId = c.Int(nullable: false),
                        ProductId = c.Int(),
                        Description = c.String(),
                        Date = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        IsInstallation = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId)
                .ForeignKey("dbo.ProductBOMs", t => t.ProductBomId, cascadeDelete: true)
                .Index(t => t.ProductBomId)
                .Index(t => t.ProductId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductBomLogs", "ProductBomId", "dbo.ProductBOMs");
            DropForeignKey("dbo.ProductBomLogs", "ProductId", "dbo.Products");
            DropIndex("dbo.ProductBomLogs", new[] { "ProductId" });
            DropIndex("dbo.ProductBomLogs", new[] { "ProductBomId" });
            DropTable("dbo.ProductBomLogs");
        }
    }
}
