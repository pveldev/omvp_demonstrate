namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BindWorkGroupToOperation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Operations", "WorkingGroupId", c => c.Int());
            CreateIndex("dbo.Operations", "WorkingGroupId");
            AddForeignKey("dbo.Operations", "WorkingGroupId", "dbo.WorkingGroups", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Operations", "WorkingGroupId", "dbo.WorkingGroups");
            DropIndex("dbo.Operations", new[] { "WorkingGroupId" });
            DropColumn("dbo.Operations", "WorkingGroupId");
        }
    }
}
