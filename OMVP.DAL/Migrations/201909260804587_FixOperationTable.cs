namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixOperationTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Operations", "Code", c => c.String(maxLength: 4, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Operations", "Code", c => c.String(nullable: false, maxLength: 4, unicode: false));
        }
    }
}
