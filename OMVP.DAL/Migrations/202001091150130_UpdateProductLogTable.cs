namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateProductLogTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductLogs", "UserId", c => c.Int(nullable: false));
            Sql(@"
                UPDATE ProductLogs
                SET UserId = u.Id
                FROM ProductLogs pl
                INNER JOIN Users u ON u.PCName = pl.PCUserName
                ");
            CreateIndex("dbo.ProductLogs", "UserId");
            AddForeignKey("dbo.ProductLogs", "UserId", "dbo.Users", "Id", cascadeDelete: true);
            DropColumn("dbo.ProductLogs", "PCUserName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProductLogs", "PCUserName", c => c.String(nullable: false, maxLength: 30, unicode: false));
            DropForeignKey("dbo.ProductLogs", "UserId", "dbo.Users");
            DropIndex("dbo.ProductLogs", new[] { "UserId" });
            DropColumn("dbo.ProductLogs", "UserId");
        }
    }
}
