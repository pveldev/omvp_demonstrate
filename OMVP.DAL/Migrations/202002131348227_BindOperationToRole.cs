namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BindOperationToRole : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Operations", "ExecutorRoleId", c => c.Int());
            CreateIndex("dbo.Operations", "ExecutorRoleId");
            AddForeignKey("dbo.Operations", "ExecutorRoleId", "dbo.Roles", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Operations", "ExecutorRoleId", "dbo.Roles");
            DropIndex("dbo.Operations", new[] { "ExecutorRoleId" });
            DropColumn("dbo.Operations", "ExecutorRoleId");
        }
    }
}
