namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OperationIsArchive : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Operations", "IsArchive", c => c.Boolean(nullable: false));

            Sql(@"
                    UPDATE Operations
                    SET IsArchive = 0
                ");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Operations", "IsArchive");
        }
    }
}
