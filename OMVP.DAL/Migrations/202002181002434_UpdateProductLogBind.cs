namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateProductLogBind : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductLogs", "OperationId", "dbo.Operations");
            DropIndex("dbo.ProductLogs", new[] { "OperationId" });

            AddColumn("dbo.ProductLogs", "ProductOperationId", c => c.Int());
            CreateIndex("dbo.ProductLogs", "ProductOperationId");
            AddForeignKey("dbo.ProductLogs", "ProductOperationId", "dbo.RouteMapProductOperations", "Id");

            Sql(@"
                    UPDATE ProductLogs
                    SET ProductOperationId = rmpo.Id
                    FROM ProductLogs pl
                    JOIN RouteMapProductOperations rmpo 
                    ON pl.ProductId = rmpo.ProductId AND pl.OperationId = rmpo.OperationId
                ");

            DropColumn("dbo.ProductLogs", "OperationId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProductLogs", "OperationId", c => c.Int(nullable: false));
            DropForeignKey("dbo.ProductLogs", "ProductOperationId", "dbo.RouteMapProductOperations");
            DropIndex("dbo.ProductLogs", new[] { "ProductOperationId" });
            DropColumn("dbo.ProductLogs", "ProductOperationId");
            CreateIndex("dbo.ProductLogs", "OperationId");
            AddForeignKey("dbo.ProductLogs", "OperationId", "dbo.Operations", "Id");
        }
    }
}
