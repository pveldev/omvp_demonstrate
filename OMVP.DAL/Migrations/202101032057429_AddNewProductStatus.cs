namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewProductStatus : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                    INSERT INTO ProductStatuses (Name, Code)
                    VALUES('������ ��������', '5017')
                ");
        }
        
        public override void Down()
        {
            Sql(@"
                    DELETE FROM ProductStatuses
                    WHERE Name = '������ ��������'
                "); 
        }
    }
}
