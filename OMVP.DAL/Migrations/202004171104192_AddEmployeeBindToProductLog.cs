namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEmployeeBindToProductLog : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductLogs", "OperationExecuterId", c => c.Int());
            CreateIndex("dbo.ProductLogs", "OperationExecuterId");
            AddForeignKey("dbo.ProductLogs", "OperationExecuterId", "dbo.Employees", "Id");

            Sql(@"
                    UPDATE ProductLogs
                    SET OperationExecuterId = rmpo.OperationExecuterId
                    FROM ProductLogs pl
                    INNER JOIN RouteMapProductOperations rmpo
                    ON rmpo.Id = pl.ProductOperationId
                ");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductLogs", "OperationExecuterId", "dbo.Employees");
            DropIndex("dbo.ProductLogs", new[] { "OperationExecuterId" });
            DropColumn("dbo.ProductLogs", "OperationExecuterId");
        }
    }
}
