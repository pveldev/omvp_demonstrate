namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.Employees",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            FirstName = c.String(nullable: false, maxLength: 20, unicode: false),
            //            LastName = c.String(nullable: false, maxLength: 20, unicode: false),
            //            IsArchive = c.Boolean(nullable: false),
            //            RoleId = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Roles", t => t.RoleId)
            //    .Index(t => t.RoleId);
            
            //CreateTable(
            //    "dbo.Products",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            SerialNumber = c.String(nullable: false, maxLength: 20, unicode: false),
            //            CurrentOperationId = c.Int(nullable: false),
            //            ProductStatusId = c.Int(nullable: false),
            //            RevisionId = c.Int(nullable: false),
            //            RouteMapId = c.Int(),
            //            OrderElementId = c.Int(),
            //            EmployeeId = c.Int(),
            //            WorkingGroupId = c.Int(),
            //            Description = c.String(unicode: false),
            //            Letter = c.String(maxLength: 4),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Employees", t => t.EmployeeId)
            //    .ForeignKey("dbo.Operations", t => t.CurrentOperationId)
            //    .ForeignKey("dbo.RouteMaps", t => t.RouteMapId)
            //    .ForeignKey("dbo.WorkingGroups", t => t.WorkingGroupId)
            //    .ForeignKey("dbo.Revisions", t => t.RevisionId)
            //    .ForeignKey("dbo.OrderElements", t => t.OrderElementId)
            //    .ForeignKey("dbo.ProductStatuses", t => t.ProductStatusId)
            //    .Index(t => t.CurrentOperationId)
            //    .Index(t => t.ProductStatusId)
            //    .Index(t => t.RevisionId)
            //    .Index(t => t.RouteMapId)
            //    .Index(t => t.OrderElementId)
            //    .Index(t => t.EmployeeId)
            //    .Index(t => t.WorkingGroupId);
            
            //CreateTable(
            //    "dbo.Operations",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Name = c.String(nullable: false, maxLength: 30, unicode: false),
            //            Description = c.String(unicode: false),
            //            Code = c.String(nullable: false, maxLength: 4, unicode: false),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.ProductLogs",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            PCUserName = c.String(nullable: false, maxLength: 30, unicode: false),
            //            ProductId = c.Int(nullable: false),
            //            OperationId = c.Int(nullable: false),
            //            OperationAction = c.String(nullable: false, maxLength: 15),
            //            Date = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Operations", t => t.OperationId)
            //    .ForeignKey("dbo.Products", t => t.ProductId)
            //    .Index(t => t.ProductId)
            //    .Index(t => t.OperationId);
            
            //CreateTable(
            //    "dbo.RouteMapOperations",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Priority = c.Byte(nullable: false),
            //            OperationId = c.Int(nullable: false),
            //            RouteMapId = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.RouteMaps", t => t.RouteMapId)
            //    .ForeignKey("dbo.Operations", t => t.OperationId)
            //    .Index(t => t.OperationId)
            //    .Index(t => t.RouteMapId);
            
            //CreateTable(
            //    "dbo.RouteMaps",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Name = c.String(nullable: false, maxLength: 20, unicode: false),
            //            IsArchive = c.Boolean(nullable: false),
            //            RevisionId = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Revisions", t => t.RevisionId)
            //    .Index(t => t.RevisionId);
            
            //CreateTable(
            //    "dbo.Revisions",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Name = c.String(nullable: false, maxLength: 20, unicode: false),
            //            ModelId = c.Int(nullable: false),
            //            IsArchive = c.Boolean(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Models", t => t.ModelId)
            //    .Index(t => t.ModelId);
            
            //CreateTable(
            //    "dbo.Models",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Name = c.String(nullable: false, maxLength: 20, unicode: false),
            //            KRPG = c.String(maxLength: 20, unicode: false),
            //            ModelTypeId = c.Int(nullable: false),
            //            HasLetter = c.Boolean(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.ModelTypes", t => t.ModelTypeId)
            //    .Index(t => t.ModelTypeId);
            
            //CreateTable(
            //    "dbo.ModelTypes",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Name = c.String(nullable: false, maxLength: 20, unicode: false),
            //            Code = c.String(nullable: false, maxLength: 4, unicode: false),
            //            Description = c.String(unicode: false),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.RevisionBOMs",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Name = c.String(maxLength: 20, fixedLength: true, unicode: false),
            //            ModelId = c.Int(nullable: false),
            //            RevisionId = c.Int(nullable: false),
            //            BOMRevisionId = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Models", t => t.ModelId)
            //    .ForeignKey("dbo.Revisions", t => t.BOMRevisionId)
            //    .ForeignKey("dbo.Revisions", t => t.RevisionId)
            //    .Index(t => t.ModelId)
            //    .Index(t => t.RevisionId)
            //    .Index(t => t.BOMRevisionId);
            
            //CreateTable(
            //    "dbo.ProductBOMs",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            RevisionBOMId = c.Int(nullable: false),
            //            ProductId = c.Int(nullable: false),
            //            BOMProductId = c.Int(),
            //            Description = c.String(unicode: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.RevisionBOMs", t => t.RevisionBOMId)
            //    .ForeignKey("dbo.Products", t => t.ProductId)
            //    .ForeignKey("dbo.Products", t => t.BOMProductId)
            //    .Index(t => t.RevisionBOMId)
            //    .Index(t => t.ProductId)
            //    .Index(t => t.BOMProductId);
            
            //CreateTable(
            //    "dbo.WorkingGroups",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Name = c.String(nullable: false, maxLength: 30, unicode: false),
            //            ModelId = c.Int(nullable: false),
            //            IsArchive = c.Boolean(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Models", t => t.ModelId)
            //    .Index(t => t.ModelId);
            
            //CreateTable(
            //    "dbo.WorkingGroupEmployees",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            WorkingGroupId = c.Int(nullable: false),
            //            EmployeeId = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.WorkingGroups", t => t.WorkingGroupId)
            //    .ForeignKey("dbo.Employees", t => t.EmployeeId)
            //    .Index(t => t.WorkingGroupId)
            //    .Index(t => t.EmployeeId);
            
            //CreateTable(
            //    "dbo.RouteMapProductOperations",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            ProductId = c.Int(nullable: false),
            //            OperationStatusId = c.Int(nullable: false),
            //            RouteMapOperationId = c.Int(nullable: false),
            //            StartDate = c.DateTime(precision: 7, storeType: "datetime2"),
            //            EndDate = c.DateTime(precision: 7, storeType: "datetime2"),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.OperationStatuses", t => t.OperationStatusId)
            //    .ForeignKey("dbo.RouteMapOperations", t => t.RouteMapOperationId)
            //    .ForeignKey("dbo.Products", t => t.ProductId)
            //    .Index(t => t.ProductId)
            //    .Index(t => t.OperationStatusId)
            //    .Index(t => t.RouteMapOperationId);
            
            //CreateTable(
            //    "dbo.OperationStatuses",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Code = c.String(nullable: false, maxLength: 4, unicode: false),
            //            Name = c.String(nullable: false, maxLength: 30, unicode: false),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.OrderElements",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            OrderTreeId = c.Int(nullable: false),
            //            Month = c.Byte(nullable: false),
            //            Quantity = c.Int(nullable: false),
            //            Description = c.String(unicode: false),
            //            CompletedQuantity = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.OrderTrees", t => t.OrderTreeId, cascadeDelete: true)
            //    .Index(t => t.OrderTreeId);
            
            //CreateTable(
            //    "dbo.OrderTrees",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            OrderId = c.Int(nullable: false),
            //            RevisionId = c.Int(nullable: false),
            //            DispatchedQuantity = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
            //    .ForeignKey("dbo.Revisions", t => t.RevisionId, cascadeDelete: true)
            //    .Index(t => t.OrderId)
            //    .Index(t => t.RevisionId);
            
            //CreateTable(
            //    "dbo.Orders",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Letter = c.String(maxLength: 4),
            //            Quantity = c.Int(nullable: false),
            //            OrderStatusId = c.Int(nullable: false),
            //            ExecutionDate = c.DateTime(nullable: false),
            //            Contract = c.String(maxLength: 70, unicode: false),
            //            Customer = c.String(maxLength: 50, unicode: false),
            //            SHPZCode = c.String(maxLength: 30, unicode: false),
            //            OrderCode = c.String(maxLength: 20, unicode: false),
            //            Description = c.String(unicode: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.OrderStatuses", t => t.OrderStatusId)
            //    .Index(t => t.OrderStatusId);
            
            //CreateTable(
            //    "dbo.OrderStatuses",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Name = c.String(nullable: false, maxLength: 30, unicode: false),
            //            Code = c.String(nullable: false, maxLength: 4, unicode: false),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.ProductStatuses",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Name = c.String(nullable: false, maxLength: 30, unicode: false),
            //            Code = c.String(nullable: false, maxLength: 4, unicode: false),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.Roles",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Name = c.String(nullable: false, maxLength: 30, unicode: false),
            //            Code = c.String(nullable: false, maxLength: 4, unicode: false),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.Users",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            PCName = c.String(nullable: false),
            //            Password = c.String(),
            //            EmployeeId = c.Int(),
            //            AccessorLevel = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Employees", t => t.EmployeeId)
            //    .Index(t => t.EmployeeId);
            
            //CreateTable(
            //    "dbo.ErrorLogs",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            ExceptionName = c.String(),
            //            Message = c.String(),
            //            InnerExceptionMessage = c.String(),
            //            MethodName = c.String(),
            //            ProjectName = c.String(),
            //            Date = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
            //        })
            //    .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkingGroupEmployees", "EmployeeId", "dbo.Employees");
            DropForeignKey("dbo.Users", "EmployeeId", "dbo.Employees");
            DropForeignKey("dbo.Employees", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.RouteMapProductOperations", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Products", "ProductStatusId", "dbo.ProductStatuses");
            DropForeignKey("dbo.ProductLogs", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductBOMs", "BOMProductId", "dbo.Products");
            DropForeignKey("dbo.ProductBOMs", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Products", "OrderElementId", "dbo.OrderElements");
            DropForeignKey("dbo.OrderTrees", "RevisionId", "dbo.Revisions");
            DropForeignKey("dbo.OrderElements", "OrderTreeId", "dbo.OrderTrees");
            DropForeignKey("dbo.OrderTrees", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.Orders", "OrderStatusId", "dbo.OrderStatuses");
            DropForeignKey("dbo.RouteMapOperations", "OperationId", "dbo.Operations");
            DropForeignKey("dbo.RouteMapProductOperations", "RouteMapOperationId", "dbo.RouteMapOperations");
            DropForeignKey("dbo.RouteMapProductOperations", "OperationStatusId", "dbo.OperationStatuses");
            DropForeignKey("dbo.RouteMapOperations", "RouteMapId", "dbo.RouteMaps");
            DropForeignKey("dbo.RouteMaps", "RevisionId", "dbo.Revisions");
            DropForeignKey("dbo.RevisionBOMs", "RevisionId", "dbo.Revisions");
            DropForeignKey("dbo.RevisionBOMs", "BOMRevisionId", "dbo.Revisions");
            DropForeignKey("dbo.Products", "RevisionId", "dbo.Revisions");
            DropForeignKey("dbo.WorkingGroups", "ModelId", "dbo.Models");
            DropForeignKey("dbo.WorkingGroupEmployees", "WorkingGroupId", "dbo.WorkingGroups");
            DropForeignKey("dbo.Products", "WorkingGroupId", "dbo.WorkingGroups");
            DropForeignKey("dbo.Revisions", "ModelId", "dbo.Models");
            DropForeignKey("dbo.RevisionBOMs", "ModelId", "dbo.Models");
            DropForeignKey("dbo.ProductBOMs", "RevisionBOMId", "dbo.RevisionBOMs");
            DropForeignKey("dbo.Models", "ModelTypeId", "dbo.ModelTypes");
            DropForeignKey("dbo.Products", "RouteMapId", "dbo.RouteMaps");
            DropForeignKey("dbo.Products", "CurrentOperationId", "dbo.Operations");
            DropForeignKey("dbo.ProductLogs", "OperationId", "dbo.Operations");
            DropForeignKey("dbo.Products", "EmployeeId", "dbo.Employees");
            DropIndex("dbo.Users", new[] { "EmployeeId" });
            DropIndex("dbo.Orders", new[] { "OrderStatusId" });
            DropIndex("dbo.OrderTrees", new[] { "RevisionId" });
            DropIndex("dbo.OrderTrees", new[] { "OrderId" });
            DropIndex("dbo.OrderElements", new[] { "OrderTreeId" });
            DropIndex("dbo.RouteMapProductOperations", new[] { "RouteMapOperationId" });
            DropIndex("dbo.RouteMapProductOperations", new[] { "OperationStatusId" });
            DropIndex("dbo.RouteMapProductOperations", new[] { "ProductId" });
            DropIndex("dbo.WorkingGroupEmployees", new[] { "EmployeeId" });
            DropIndex("dbo.WorkingGroupEmployees", new[] { "WorkingGroupId" });
            DropIndex("dbo.WorkingGroups", new[] { "ModelId" });
            DropIndex("dbo.ProductBOMs", new[] { "BOMProductId" });
            DropIndex("dbo.ProductBOMs", new[] { "ProductId" });
            DropIndex("dbo.ProductBOMs", new[] { "RevisionBOMId" });
            DropIndex("dbo.RevisionBOMs", new[] { "BOMRevisionId" });
            DropIndex("dbo.RevisionBOMs", new[] { "RevisionId" });
            DropIndex("dbo.RevisionBOMs", new[] { "ModelId" });
            DropIndex("dbo.Models", new[] { "ModelTypeId" });
            DropIndex("dbo.Revisions", new[] { "ModelId" });
            DropIndex("dbo.RouteMaps", new[] { "RevisionId" });
            DropIndex("dbo.RouteMapOperations", new[] { "RouteMapId" });
            DropIndex("dbo.RouteMapOperations", new[] { "OperationId" });
            DropIndex("dbo.ProductLogs", new[] { "OperationId" });
            DropIndex("dbo.ProductLogs", new[] { "ProductId" });
            DropIndex("dbo.Products", new[] { "WorkingGroupId" });
            DropIndex("dbo.Products", new[] { "EmployeeId" });
            DropIndex("dbo.Products", new[] { "OrderElementId" });
            DropIndex("dbo.Products", new[] { "RouteMapId" });
            DropIndex("dbo.Products", new[] { "RevisionId" });
            DropIndex("dbo.Products", new[] { "ProductStatusId" });
            DropIndex("dbo.Products", new[] { "CurrentOperationId" });
            DropIndex("dbo.Employees", new[] { "RoleId" });
            DropTable("dbo.ErrorLogs");
            DropTable("dbo.Users");
            DropTable("dbo.Roles");
            DropTable("dbo.ProductStatuses");
            DropTable("dbo.OrderStatuses");
            DropTable("dbo.Orders");
            DropTable("dbo.OrderTrees");
            DropTable("dbo.OrderElements");
            DropTable("dbo.OperationStatuses");
            DropTable("dbo.RouteMapProductOperations");
            DropTable("dbo.WorkingGroupEmployees");
            DropTable("dbo.WorkingGroups");
            DropTable("dbo.ProductBOMs");
            DropTable("dbo.RevisionBOMs");
            DropTable("dbo.ModelTypes");
            DropTable("dbo.Models");
            DropTable("dbo.Revisions");
            DropTable("dbo.RouteMaps");
            DropTable("dbo.RouteMapOperations");
            DropTable("dbo.ProductLogs");
            DropTable("dbo.Operations");
            DropTable("dbo.Products");
            DropTable("dbo.Employees");
        }
    }
}
