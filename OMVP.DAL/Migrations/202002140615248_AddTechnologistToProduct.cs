namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTechnologistToProduct : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "TechologistId", c => c.Int());
            CreateIndex("dbo.Products", "TechologistId");
            AddForeignKey("dbo.Products", "TechologistId", "dbo.Employees", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "TechologistId", "dbo.Employees");
            DropIndex("dbo.Products", new[] { "TechologistId" });
            DropColumn("dbo.Products", "TechologistId");
        }
    }
}
