namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UnionRMPOwithUPO : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UnscheduledProductOperations", "OperationId", "dbo.Operations");
            DropForeignKey("dbo.UnscheduledProductOperations", "OperationStatusId", "dbo.OperationStatuses");
            DropForeignKey("dbo.UnscheduledProductOperations", "ProductId", "dbo.Products");
            DropIndex("dbo.UnscheduledProductOperations", new[] { "ProductId" });
            DropIndex("dbo.UnscheduledProductOperations", new[] { "OperationStatusId" });
            DropIndex("dbo.UnscheduledProductOperations", new[] { "OperationId" });
            AddColumn("dbo.RouteMapProductOperations", "IsAdditional", c => c.Boolean(nullable: false));

            Sql(@"
                    UPDATE RouteMapProductOperations
                    SET IsAdditional = 0
                "); // �������� ����������� ����

            Sql(@"                 
                    INSERT INTO dbo.RouteMapProductOperations
                    SELECT	ProductId AS ProductId, 
		                    OperationStatusId AS OperationStatusId,
		                    StartDate AS StartDate,
		                    EndDate AS EndDate,
		                    Description AS Description,
		                    OperationId AS OperationId,
		                    Priority AS Priority,
                            '1' AS IsAdditional
                    FROM dbo.UnscheduledProductOperations
                "); // �������� ������ �� UPO � RMPO

            DropTable("dbo.UnscheduledProductOperations");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.UnscheduledProductOperations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(nullable: false),
                        OperationStatusId = c.Int(nullable: false),
                        OperationId = c.Int(nullable: false),
                        Priority = c.Byte(nullable: false),
                        StartDate = c.DateTime(precision: 7, storeType: "datetime2"),
                        EndDate = c.DateTime(precision: 7, storeType: "datetime2"),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.RouteMapProductOperations", "IsAdditional");
            CreateIndex("dbo.UnscheduledProductOperations", "OperationId");
            CreateIndex("dbo.UnscheduledProductOperations", "OperationStatusId");
            CreateIndex("dbo.UnscheduledProductOperations", "ProductId");
            AddForeignKey("dbo.UnscheduledProductOperations", "ProductId", "dbo.Products", "Id", cascadeDelete: true);
            AddForeignKey("dbo.UnscheduledProductOperations", "OperationStatusId", "dbo.OperationStatuses", "Id", cascadeDelete: true);
            AddForeignKey("dbo.UnscheduledProductOperations", "OperationId", "dbo.Operations", "Id", cascadeDelete: true);
        }
    }
}
