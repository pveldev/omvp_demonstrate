namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ClaimRMnOrdersnrBom : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RevisionBOMs", "IsClaimed", c => c.Boolean(nullable: false));
            Sql(@"
                    UPDATE RevisionBOMs
                    SET IsClaimed = 1
                ");

            AddColumn("dbo.RouteMaps", "IsClaimed", c => c.Boolean(nullable: false));
            Sql(@"
                    UPDATE RouteMaps
                    SET IsClaimed = 1
                ");

            AddColumn("dbo.Orders", "IsClaimed", c => c.Boolean(nullable: false));
            Sql(@"
                    UPDATE Orders
                    SET IsClaimed = 1
                ");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "IsClaimed");
            DropColumn("dbo.RouteMaps", "IsClaimed");
            DropColumn("dbo.RevisionBOMs", "IsClaimed");
        }
    }
}
