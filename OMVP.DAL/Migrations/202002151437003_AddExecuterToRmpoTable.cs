namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddExecuterToRmpoTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RouteMapProductOperations", "OperationExecuterId", c => c.Int());
            CreateIndex("dbo.RouteMapProductOperations", "OperationExecuterId");
            AddForeignKey("dbo.RouteMapProductOperations", "OperationExecuterId", "dbo.Employees", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RouteMapProductOperations", "OperationExecuterId", "dbo.Employees");
            DropIndex("dbo.RouteMapProductOperations", new[] { "OperationExecuterId" });
            DropColumn("dbo.RouteMapProductOperations", "OperationExecuterId");
        }
    }
}
