namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductProgress : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Progress", c => c.Single(nullable: false));

            Sql(@"
                    UPDATE Products
                    SET Progress = 0
                "); // 0 ��� ����

            Sql(@"
                    UPDATE Products
                    SET Progress = 1
                    WHERE ProductStatusId IN (4, 5, 7, 8)
                "); // 1 ������ ��� "�������"
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "Progress");
        }
    }
}
