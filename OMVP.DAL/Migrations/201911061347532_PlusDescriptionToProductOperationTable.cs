namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PlusDescriptionToProductOperationTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RouteMapProductOperations", "Description", c => c.String());
            AddColumn("dbo.UnscheduledProductOperations", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UnscheduledProductOperations", "Description");
            DropColumn("dbo.RouteMapProductOperations", "Description");
        }
    }
}
