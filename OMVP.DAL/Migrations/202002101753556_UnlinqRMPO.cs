namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UnlinqRMPO : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RouteMapProductOperations", "OperationId", c => c.Int(nullable: false));
            AddColumn("dbo.RouteMapProductOperations", "Priority", c => c.Byte(nullable: false));

            Sql(@"
                    UPDATE RouteMapProductOperations 
                    SET OperationId = rmo.OperationId, Priority = rmo.Priority
                    FROM RouteMapProductOperations rmpo
                    INNER JOIN RouteMapOperations rmo ON rmo.Id = rmpo.RouteMapOperationId
                ");

            DropForeignKey("dbo.RouteMapProductOperations", "RouteMapOperationId", "dbo.RouteMapOperations");
            DropIndex("dbo.RouteMapProductOperations", new[] { "RouteMapOperationId" });
            CreateIndex("dbo.RouteMapProductOperations", "OperationId");
            AddForeignKey("dbo.RouteMapProductOperations", "OperationId", "dbo.Operations", "Id", cascadeDelete: true);
            DropColumn("dbo.RouteMapProductOperations", "RouteMapOperationId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RouteMapProductOperations", "RouteMapOperationId", c => c.Int(nullable: false));
            DropForeignKey("dbo.RouteMapProductOperations", "OperationId", "dbo.Operations");
            DropIndex("dbo.RouteMapProductOperations", new[] { "OperationId" });
            DropColumn("dbo.RouteMapProductOperations", "Priority");
            DropColumn("dbo.RouteMapProductOperations", "OperationId");
            CreateIndex("dbo.RouteMapProductOperations", "RouteMapOperationId");
            AddForeignKey("dbo.RouteMapProductOperations", "RouteMapOperationId", "dbo.RouteMapOperations", "Id");
        }
    }
}
