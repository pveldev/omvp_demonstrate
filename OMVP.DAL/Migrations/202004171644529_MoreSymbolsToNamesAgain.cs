namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MoreSymbolsToNamesAgain : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ModelTypes",       "Name", c => c.String(nullable: false, maxLength: 50, unicode: false));
            AlterColumn("dbo.RouteMaps",        "Name", c => c.String(nullable: false, maxLength: 50, unicode: false));
            AlterColumn("dbo.WorkingGroups",    "Name", c => c.String(nullable: false, maxLength: 50, unicode: false));
        }
      
        public override void Down()
        {
            AlterColumn("dbo.ModelTypes",       "Name", c => c.String(nullable: false, maxLength: 20, unicode: false));
            AlterColumn("dbo.RouteMaps",        "Name", c => c.String(nullable: false, maxLength: 20, unicode: false));
            AlterColumn("dbo.WorkingGroups",    "Name", c => c.String(nullable: false, maxLength: 30, unicode: false));
        }
    }
}
