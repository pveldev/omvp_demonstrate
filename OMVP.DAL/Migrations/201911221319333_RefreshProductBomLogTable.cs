namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefreshProductBomLogTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductBomLogs", "ProductId", "dbo.Products");
            DropIndex("dbo.ProductBomLogs", new[] { "ProductId" });
            AlterColumn("dbo.ProductBomLogs", "ProductId", c => c.Int(nullable: false));
            CreateIndex("dbo.ProductBomLogs", "ProductId");
            AddForeignKey("dbo.ProductBomLogs", "ProductId", "dbo.Products", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductBomLogs", "ProductId", "dbo.Products");
            DropIndex("dbo.ProductBomLogs", new[] { "ProductId" });
            AlterColumn("dbo.ProductBomLogs", "ProductId", c => c.Int());
            CreateIndex("dbo.ProductBomLogs", "ProductId");
            AddForeignKey("dbo.ProductBomLogs", "ProductId", "dbo.Products", "Id");
        }
    }
}
