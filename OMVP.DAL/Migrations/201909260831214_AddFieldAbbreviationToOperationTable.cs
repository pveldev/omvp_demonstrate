namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFieldAbbreviationToOperationTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Operations", "AbbreviationName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Operations", "AbbreviationName");
        }
    }
}
