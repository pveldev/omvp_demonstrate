namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameProductEmployee : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Products", name: "EmployeeId", newName: "EngineerId");
            RenameIndex(table: "dbo.Products", name: "IX_EmployeeId", newName: "IX_EngineerId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Products", name: "IX_EngineerId", newName: "IX_EmployeeId");
            RenameColumn(table: "dbo.Products", name: "EngineerId", newName: "EmployeeId");
        }
    }
}
