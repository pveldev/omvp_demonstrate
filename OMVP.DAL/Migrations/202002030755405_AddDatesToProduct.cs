namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDatesToProduct : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "CreationDate", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AddColumn("dbo.Products", "UpdateDate", c => c.DateTime(precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "UpdateDate");
            DropColumn("dbo.Products", "CreationDate");
        }
    }
}
