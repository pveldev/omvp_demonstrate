namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_field_names : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                    UPDATE ModelTypes
                    SET Name = '������� �������', Description = '������� (���� ����� ��), ������� ���������� ���������'
                    WHERE Code = '1001'

                    UPDATE ModelTypes
                    SET Name = '������� �������', Description = '�������, ������� ����� ������� � ������ ������� �������� ������ � ����� ����������� ������� ������'
                    WHERE Code = '1002'

                    UPDATE ModelTypes
                    SET Name = '������ �������', Description = '�������, ������� ����� ������� � ������ ������� �������� ����� � �� ����� ����� �����������'
                    WHERE Code = '1004'

                    UPDATE OrderStatuses
                    SET Name = '�����������'
                    WHERE Code = '3003'
                ");
        }
        
        public override void Down()
        {
        }
    }
}
