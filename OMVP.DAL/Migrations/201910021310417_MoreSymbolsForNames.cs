namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MoreSymbolsForNames : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Revisions", "Name", c => c.String(nullable: false, maxLength: 30, unicode: false));
            AlterColumn("dbo.Models", "Name", c => c.String(nullable: false, maxLength: 30, unicode: false));
            AlterColumn("dbo.Models", "KRPG", c => c.String(maxLength: 30, unicode: false));
            AlterColumn("dbo.RevisionBOMs", "Name", c => c.String(nullable: false, maxLength: 30, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RevisionBOMs", "Name", c => c.String(maxLength: 20, unicode: false));
            AlterColumn("dbo.Models", "KRPG", c => c.String(maxLength: 20, unicode: false));
            AlterColumn("dbo.Models", "Name", c => c.String(nullable: false, maxLength: 20, unicode: false));
            AlterColumn("dbo.Revisions", "Name", c => c.String(nullable: false, maxLength: 20, unicode: false));
        }
    }
}
