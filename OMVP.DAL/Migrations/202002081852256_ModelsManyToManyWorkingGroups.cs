namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelsManyToManyWorkingGroups : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.WorkingGroups", "ModelId", "dbo.Models");
            DropIndex("dbo.WorkingGroups", new[] { "ModelId" });
            CreateTable(
                "dbo.ModelXWorkingGroups",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    ModelId = c.Int(nullable: false),
                    WorkingGroupId = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Models", t => t.ModelId, cascadeDelete: true)
                .ForeignKey("dbo.WorkingGroups", t => t.WorkingGroupId, cascadeDelete: true)
                .Index(t => t.ModelId)
                .Index(t => t.WorkingGroupId);

            DropColumn("dbo.WorkingGroups", "ModelId");
        }

        public override void Down()
        {
            AddColumn("dbo.WorkingGroups", "ModelId", c => c.Int(nullable: false));
            DropForeignKey("dbo.ModelXWorkingGroups", "WorkingGroupId", "dbo.WorkingGroups");
            DropForeignKey("dbo.ModelXWorkingGroups", "ModelId", "dbo.Models");
            DropIndex("dbo.ModelXWorkingGroups", new[] { "WorkingGroupId" });
            DropIndex("dbo.ModelXWorkingGroups", new[] { "ModelId" });
            DropTable("dbo.ModelXWorkingGroups");
            CreateIndex("dbo.WorkingGroups", "ModelId");
            AddForeignKey("dbo.WorkingGroups", "ModelId", "dbo.Models", "Id");
        }
    }
}
