namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UnitForParamType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ParameterTypes", "Unit", c => c.String(maxLength: 10));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ParameterTypes", "Unit");
        }
    }
}
