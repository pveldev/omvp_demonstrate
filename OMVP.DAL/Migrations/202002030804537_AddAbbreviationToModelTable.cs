namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAbbreviationToModelTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Models", "AbbreviationName", c => c.String(maxLength: 10));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Models", "AbbreviationName");
        }
    }
}
