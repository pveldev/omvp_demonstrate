namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTableUnscheduledProductOperation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UnscheduledProductOperations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(nullable: false),
                        OperationStatusId = c.Int(nullable: false),
                        OperationId = c.Int(nullable: false),
                        Priority = c.Byte(nullable: false),
                        StartDate = c.DateTime(precision: 7, storeType: "datetime2"),
                        EndDate = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Operations", t => t.OperationId, cascadeDelete: true)
                .ForeignKey("dbo.OperationStatuses", t => t.OperationStatusId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.OperationStatusId)
                .Index(t => t.OperationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UnscheduledProductOperations", "ProductId", "dbo.Products");
            DropForeignKey("dbo.UnscheduledProductOperations", "OperationStatusId", "dbo.OperationStatuses");
            DropForeignKey("dbo.UnscheduledProductOperations", "OperationId", "dbo.Operations");
            DropIndex("dbo.UnscheduledProductOperations", new[] { "OperationId" });
            DropIndex("dbo.UnscheduledProductOperations", new[] { "OperationStatusId" });
            DropIndex("dbo.UnscheduledProductOperations", new[] { "ProductId" });
            DropTable("dbo.UnscheduledProductOperations");
        }
    }
}
