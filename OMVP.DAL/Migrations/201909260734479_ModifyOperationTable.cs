namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyOperationTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Operations", "RoutingNumber", c => c.String());
            AlterColumn("dbo.Operations", "Name", c => c.String(nullable: false, maxLength: 60, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Operations", "Name", c => c.String(nullable: false, maxLength: 30, unicode: false));
            DropColumn("dbo.Operations", "RoutingNumber");
        }
    }
}
