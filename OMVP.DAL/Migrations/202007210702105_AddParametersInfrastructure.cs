namespace OMVP.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddParametersInfrastructure : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ModelParameters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ModelId = c.Int(nullable: false),
                        ParameterTypeId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 60),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Models", t => t.ModelId, cascadeDelete: true)
                .ForeignKey("dbo.ParameterTypes", t => t.ParameterTypeId, cascadeDelete: true)
                .Index(t => t.ModelId)
                .Index(t => t.ParameterTypeId);
            
            CreateTable(
                "dbo.ParameterTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 60),
                        Code = c.String(nullable: false, maxLength: 4),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductParameters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(nullable: false),
                        ModelParameterId = c.Int(nullable: false),
                        Value = c.String(nullable: false, maxLength: 150),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ModelParameters", t => t.ModelParameterId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.ModelParameterId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductParameters", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductParameters", "ModelParameterId", "dbo.ModelParameters");
            DropForeignKey("dbo.ModelParameters", "ParameterTypeId", "dbo.ParameterTypes");
            DropForeignKey("dbo.ModelParameters", "ModelId", "dbo.Models");
            DropIndex("dbo.ProductParameters", new[] { "ModelParameterId" });
            DropIndex("dbo.ProductParameters", new[] { "ProductId" });
            DropIndex("dbo.ModelParameters", new[] { "ParameterTypeId" });
            DropIndex("dbo.ModelParameters", new[] { "ModelId" });
            DropTable("dbo.ProductParameters");
            DropTable("dbo.ParameterTypes");
            DropTable("dbo.ModelParameters");
        }
    }
}
