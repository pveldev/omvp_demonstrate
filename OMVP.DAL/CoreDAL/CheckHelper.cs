﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.CoreDAL
{
    public class CheckHelper
    {
        public static string QueryParametersNullCheck(string queryParameters) => string.IsNullOrEmpty(queryParameters) ? " WHERE" : " AND"; // return "WHERE" for first parameter, return "AND" for second parameters
        public static string AddJoinIfFirstTime(string newJoin, List<string> joins) => joins.Contains(newJoin) ? "" : newJoin; //return newJoin, если его еще нет в join-списке
    }
}
