﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.CoreDAL
{
    public class BaseQO
    {
        public OmvpDataModel _db;
        public void RefreshContext(OmvpDataModel db)
        {
            _db = db;
        }
    }
}
