﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.CoreDAL
{
    //Обертки для работы с БД
    public static class ContextExtensions
    {
        #region sync
        public static bool ExecuteWithTransactionAndSave(this OmvpDataModel context, Action<OmvpDataModel> action)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    action(context);
                    context.SaveChanges();
                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return SQLServerHelper.ExceptionProcessing(ex);
                }
            }
        }
        public static bool ExecuteWithTransactionAndSaveAndReturnBool(this OmvpDataModel context, Func<OmvpDataModel, bool> func)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    bool result = func(context);
                    context.SaveChanges();
                    transaction.Commit();
                    return result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return SQLServerHelper.ExceptionProcessing(ex);
                }
            }
        }
        public static bool ExecuteWithoutTransactionWithSaveAndReturnBool(this OmvpDataModel context, Func<OmvpDataModel, bool> func)
        {        
            try
            {
                bool result = func(context);
                context.SaveChanges();
                return result;
            }
            catch (Exception ex)
            {
                return SQLServerHelper.ExceptionProcessing(ex);
            }           
        }
        public static T ExecuteWithTransactionAndReturnValue<T>(this OmvpDataModel context, Func<OmvpDataModel, T> func) where T : class
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    T entity = func(context);
                    context.SaveChanges();
                    transaction.Commit();
                    return entity;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    SQLServerHelper.ExceptionProcessing(ex);
                    return null;
                }
            }
        }
        public static void ExecuteWithTransactionAndReturnVoid(this OmvpDataModel context, Action<OmvpDataModel> action)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    action(context);
                    context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    SQLServerHelper.ExceptionProcessing(ex);
                }
            }
        }
        public static T ExecuteWithoutTransactionAndReturnValue<T>(this OmvpDataModel context, Func<OmvpDataModel, T> func) where T : class
        {           
            try
            {
                T entity = func(context);
                context.SaveChanges();
                return entity;
            }
            catch (Exception ex)
            {
                SQLServerHelper.ExceptionProcessing(ex);
                return null;
            }    
        }
        #endregion

        #region async
        public static async Task<bool> ExecuteWithTransactionAndSaveAsync(this OmvpDataModel context, Action<OmvpDataModel> action)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    action(context);
                    await context.SaveChangesAsync();
                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return SQLServerHelper.ExceptionProcessing(ex);
                }
            }
        }
        public static async Task<bool> ExecuteWithTransactionAndSaveAndReturnBoolAsync(this OmvpDataModel context, Func<OmvpDataModel, bool> func)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    bool result = func(context);
                    await context.SaveChangesAsync();
                    transaction.Commit();
                    return result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return SQLServerHelper.ExceptionProcessing(ex);
                }
            }
        }
        public static async Task<bool> ExecuteWithoutTransactionWithSaveAndReturnBoolAsync(this OmvpDataModel context, Func<OmvpDataModel, bool> func)
        {
            try
            {
                bool result = func(context);
                await context.SaveChangesAsync();
                return result;
            }
            catch (Exception ex)
            {
                return SQLServerHelper.ExceptionProcessing(ex);
            }
        }
        public static async Task<T> ExecuteWithTransactionAndReturnValueAsync<T>(this OmvpDataModel context, Func<OmvpDataModel, T> func) where T : class
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    T entity = func(context);
                    await context.SaveChangesAsync();
                    transaction.Commit();
                    return entity;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    SQLServerHelper.ExceptionProcessing(ex);
                    return null;
                }
            }
        }
        public static async Task ExecuteWithTransactionAndReturnVoidAsync(this OmvpDataModel context, Action<OmvpDataModel> action)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    action(context);
                    await context.SaveChangesAsync();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    SQLServerHelper.ExceptionProcessing(ex);
                }
            }
        }
        public static async Task<T> ExecuteWithoutTransactionAndReturnValueAsync<T>(this OmvpDataModel context, Func<OmvpDataModel, T> func) where T : class
        {
            try
            {
                T entity = func(context);
                await context.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                SQLServerHelper.ExceptionProcessing(ex);
                return null;
            }
        }
        #endregion
    }
}
