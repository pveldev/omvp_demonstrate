﻿using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OMVP.DAL.CoreDAL
{
    public static class SQLServerHelper
    {
        private static readonly OmvpDataModel db;

        static SQLServerHelper()
        {
            db = SQLServer.Instance.db;
        }

        //Проверяет статуса операций на готовность в входящей коллекции операций
        public static bool ProductIsComplete(IEnumerable<RouteMapProductOperation> productOperations) =>
           !productOperations.Any(productOperation => productOperation.OperationStatusId != CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id);

        //Проверяет статуса операций на готовность в входящей коллекции операций, за исключением завершаемой в данный момент операции
        public static bool ProductIsComplete(IEnumerable<RouteMapProductOperation> productOperations, RouteMapProductOperation beingEndedProductOperation) =>
           !productOperations.Any(productOperation => productOperation.Id != beingEndedProductOperation.Id && productOperation.OperationStatusId != CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id);

        public static bool ProductIsComplete(Product product)
        {
            if (product.RouteMapProductOperations.Count != 0)
                return ProductIsComplete(SQLServer.Instance.RouteMapProductOperation.GetRouteMapProductOperations(product.Id));

            return true;
        }



        public static bool ExceptionProcessing(Exception ex)
        {
            SQLServer.Instance.ErrorLog.AddErrorLog(Shaper.NewErrorLog(ex));
            Dialog.ShowError(ex.Message);
            return false;
        }//Общий метод обработки исключений
    }
}