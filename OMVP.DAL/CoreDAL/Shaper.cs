﻿using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.CoreDAL
{
    public class Shaper
    {
        #region ErrorLog
        public static ErrorLog NewErrorLog(Exception ex)
        {
            string innerExMessage = "";
            if (ex.InnerException != null)
                innerExMessage = ex.InnerException.Message;

            return new ErrorLog()
            {
                ExceptionName = ex.GetType().ToString(),
                Message = ex.Message,
                InnerExceptionMessage = innerExMessage,
                MethodName = ex.TargetSite.Name,
                ProjectName = ex.Source,
                Date = DateTime.Now
            };
        }

        public static ErrorLog NewErrorLogWithCustomInner(Exception ex, string customInnerExMessage = "")
        {
            return new ErrorLog()
            {
                ExceptionName = ex.GetType().ToString(),
                Message = ex.Message,
                InnerExceptionMessage = customInnerExMessage,
                MethodName = ex.TargetSite.Name,
                ProjectName = ex.Source,
                Date = DateTime.Now
            };
        }

        public static ErrorLog NewErrorLog(Exception ex, string realMethodName)
        {
            string innerExMessage = "";
            if (ex.InnerException != null)
                innerExMessage = ex.InnerException.Message;

            return new ErrorLog()
            {
                ExceptionName = ex.GetType().ToString(),
                Message = ex.Message,
                InnerExceptionMessage = innerExMessage,
                MethodName = ex.TargetSite.Name + $"_[{realMethodName}]",
                ProjectName = ex.Source,
                Date = DateTime.Now
            };
        }

        public static ErrorLog NewErrorLog(string exceptionName, string message, string innerExceptionMessage, string methodName, string projectName)
        {
            return new ErrorLog()
            {
                ExceptionName = exceptionName,
                Message = message,
                InnerExceptionMessage = innerExceptionMessage,
                MethodName = methodName,
                ProjectName = projectName,
                Date = DateTime.Now
            };
        }
        #endregion

        #region ModelXWorkingGroup_WorkingGroup_WorkingGroupEmployee_Employee
        public static ModelXWorkingGroup FormNewModelXWorkingGroup(WorkingGroup workingGroup, Model model) => new ModelXWorkingGroup()
        {
            WorkingGroupId = workingGroup.Id,
            ModelId = model.Id
        };
        public static Employee FormNewEmployee(string firstName, string lastName, Role role, bool isArchive = false) => new Employee()
        {
            FirstName = firstName,
            LastName = lastName,
            RoleId = role.Id,
            IsArchive = isArchive
        };
        public static WorkingGroup FormNewWorkingGroup(string name, bool isArchive = false) => new WorkingGroup()
        {
            Name = name,
            IsArchive = isArchive
        };
        public static WorkingGroupEmployee FormWorkingGroupEmployee(WorkingGroup workingGroup, Employee employee) => new WorkingGroupEmployee()
        {
            WorkingGroup = workingGroup,
            Employee = employee, //Допускается установка навигационного свойсва для сущностей из одного контекста. Необходимо для отображения данных на экране
            WorkingGroupId = workingGroup.Id,
            EmployeeId = employee.Id
        };
        #endregion

        #region ProductBomLog
        public static ProductBomLog FormNewProductBomLog(ProductBOM productBom, Product product, string description, bool isInstallation) => new ProductBomLog()
        {
            ProductBomId = productBom.Id,
            ProductId = product.Id,
            Description = description,
            Date = DateTime.Now,
            IsInstallation = isInstallation
        };
        #endregion

        #region ProductLog_ProductLogs
        public static ProductLog FormNewLog(string action, int productId, RouteMapProductOperation productOperation, User user) => new ProductLog()
        {
            UserId = user.Id,
            ProductId = productId,
            OperationAction = action,
            ProductOperationId = productOperation?.Id,
            OperationExecuterId = productOperation?.OperationExecuterId,
            Date = DateTime.Now,
        };

        public static ProductLog FormNewLog(RouteMapProductOperation productOperation, string action, User user) => new ProductLog()
        {
            UserId = user.Id,
            ProductId = productOperation.Product.Id,
            OperationAction = action,
            ProductOperationId = productOperation.Id,
            OperationExecuterId = productOperation.OperationExecuterId,
            Date = DateTime.Now,
        };
        public static IEnumerable<ProductLog> NewGroupLogs(IEnumerable<RouteMapProductOperation> productOperationViews, User user, string action)
            => productOperationViews.Select(productOperation 
                => new ProductLog()
                {
                    UserId = user.Id,
                    ProductId = productOperation.Product.Id,
                    OperationAction = action,
                    ProductOperationId = productOperation.Id,
                    OperationExecuterId = productOperation.OperationExecuterId,
                    Date = DateTime.Now
                });

        public static List<ProductLog> NewGroupLogs(List<Product> products, User user, string action)
        => products.Select(product
            => new ProductLog()
            {
                UserId = user.Id,
                ProductId = product.Id,
                OperationAction = action,
                ProductOperationId = null,
                OperationExecuterId = null,
                Date = DateTime.Now
            }).ToList();
        #endregion

        #region Product
        public static Product FormNewProduct(string serialNumber, int statusId, float progress, string letter, int currentOperationId,
            int revisionId, int? routeMapId, int? engineerId, int? technologistId, int? workingGroupId, string description, bool isFavorite = false) 
                => new Product()
                    {
                        SerialNumber = serialNumber,
                        CreationDate = DateTime.Now,
                        UpdateDate = DateTime.Now,
                        ProductStatusId = statusId,
                        Progress = progress,
                        Letter = DataNormalizer.NormalizeLetter(letter),
                        CurrentOperationId = currentOperationId,
                        RevisionId = revisionId,
                        RouteMapId = routeMapId,
                        EngineerId = engineerId,
                        TechologistId = technologistId,
                        IsFavorite = isFavorite,
                        WorkingGroupId = workingGroupId,
                        Description = description
                    };
        public static Product FormNewProduct(string serialNumber, ProductStatus status, float progress, string letter, Operation currentOperation,
           Revision revision, RouteMap routeMap, Employee engineer, Employee technologist, WorkingGroup workingGroup, string description, bool isFavorite = false)
               => new Product()
               {
                   SerialNumber = serialNumber,
                   CreationDate = DateTime.Now,
                   UpdateDate = DateTime.Now,
                   ProductStatusId = status.Id,
                   Progress = progress,
                   Letter = DataNormalizer.NormalizeLetter(letter),
                   CurrentOperationId = currentOperation.Id,
                   RevisionId = revision.Id,
                   RouteMapId = routeMap?.Id,
                   EngineerId = engineer?.Id,
                   TechologistId = technologist?.Id,
                   IsFavorite = isFavorite,
                   WorkingGroupId = workingGroup?.Id,
                   Description = description,

                   Revision = revision,
                   RouteMap = routeMap,
                   Engineer = engineer,
                   Techologist = technologist,
                   WorkingGroup = workingGroup
               }; //С заполнением навигационных свойств для отображения их в PreviewNewProductsWindow.xaml
        #endregion

        #region ProductBom
        public static ProductBOM ProductBomFromRevision(int productId, RevisionBOM revisionBOM) => new ProductBOM()
        {
            ProductId = productId,
            RevisionBOMId = revisionBOM.Id,
        };
        public static ProductBOM ProductBomFromRevision(Product product, RevisionBOM revisionBOM) => new ProductBOM()
        {
            Product = product, //Необходимо для корректного добавления группы изделий
            ProductId = product.Id,
            RevisionBOMId = revisionBOM.Id,
        };
        #endregion

        #region Model
        public static Model FormNewModel(string name, string abbreviationName, string krpg, ModelType type, bool hasLetter) => new Model()
        {
            Name = name,
            AbbreviationName = abbreviationName,
            KRPG = krpg,
            ModelTypeId = type.Id,
            HasLetter = hasLetter
        };
        #endregion

        #region OperationStatus_ProductStatus_ModelType_Role
        public static OperationStatus FormNewOperationStatus(string name) => new OperationStatus() { Name = name };
        public static ProductStatus FormNewProductStatus(string name) => new ProductStatus() { Name = name };
        public static ModelType FormNewModelType(string name, string description) => new ModelType() { Name = name, Description = description };
        public static Role FormNewRole(string name) => new Role() { Name = name };

        #endregion

        #region Order_OrderTree_OrderElement
        public static Order FormNewOrder(int quantity, string letter, OrderStatus status, DateTime executionDate, string contract, string customer, string shpz, string orderCode, string description) => new Order()
        {
            Quantity = quantity,
            Letter = DataNormalizer.NormalizeLetter(letter),
            OrderStatusId = status.Id,
            ExecutionDate = executionDate,
            Contract = contract,
            Customer = customer,
            SHPZCode = shpz,
            OrderCode = orderCode,
            Description = description
        }; //Формируем общую чатсь заказа (таблица Order)

        public static List<OrderTree> FormNewOrderTrees(List<Revision> revisionsTree, Order order)
        {
            List<OrderTree> ordersTree = new List<OrderTree>();
            revisionsTree.ForEach(revision => ordersTree.Add(new OrderTree()
            {
                OrderId = order.Id,
                RevisionId = revision.Id,
                DispatchedQuantity = 0
            }));
            return ordersTree;
        } //Формирование OrderTrees для дерева ревизий, разбивая Order
        public static List<OrderElement> FormNewOrderElementsForNewMonth(OrderTree orderTree, List<OrderElement> addedOrderElements)
        {
            List<OrderElement> revisionOrderElements = new List<OrderElement>();

            addedOrderElements.ForEach(addedOrderElement => revisionOrderElements.Add(new OrderElement()
            {
                Id = addedOrderElement.Id,
                OrderTreeId = orderTree.Id,
                Quantity = addedOrderElement.Quantity,
                CompletedQuantity = 0,
                Month = addedOrderElement.Month,
                Description = addedOrderElement.Description
            }));
            return revisionOrderElements;
        }
        public static Order FormEditableOrder(Order order) => new Order()
        {
            Contract = order.Contract,
            Customer = order.Customer,
            Description = order.Description,
            IsClaimed = order.IsClaimed,
            Letter = order.Letter,
            OrderCode = order.OrderCode,
            Quantity = order.Quantity,
            SHPZCode = order.SHPZCode        
        };
        #endregion

        #region RouteMapProductOperation
        public static RouteMapProductOperation FormNewUnscheduledProductOperation(int productId, int statusId, int operationId, byte priority, string description) => new RouteMapProductOperation()
        {
            IsAdditional = true,
            ProductId = productId,
            OperationStatusId = statusId, //Обязательно заполнение для сравнение ИД при выборе нового статуса для изделия
            OperationId = operationId,
            OperationExecuterId = null,
            Priority = priority,
            Description = description

        };
        public static IEnumerable<RouteMapProductOperation> FormNewProductOperation(IEnumerable<RouteMapOperation> routeMapOperations, Product product) =>
            routeMapOperations.Select(routeMapTemplate =>
                new RouteMapProductOperation()
                {
                    IsAdditional = false,
                    Product = product, //Необходимо для корректного добавления группы изделий
                    //При этом нельзя завадать "ProductId = product.Id", т.к. Id == 0
                    OperationStatusId = GetStatusBasedOnPriority(routeMapTemplate.Priority).Id,
                    OperationId = routeMapTemplate.OperationId,
                    OperationExecuterId = GetDefaultOperationExcecuterId(product, routeMapTemplate.Operation),
                    Priority = routeMapTemplate.Priority
                });

        private static OperationStatus GetStatusBasedOnPriority(byte priority) =>
            priority == 1 ? CodeDictionary.Instance.AuxiliaryOperationStatus.ReadyToStart : CodeDictionary.Instance.AuxiliaryOperationStatus.NotDone;

        public static int? GetDefaultOperationExcecuterId(Product product, Operation operation)
        {
            if (operation.ExecutorRoleId is null)
                return null;

            if (operation.ExecutorRoleId == CodeDictionary.Instance.AuxiliaryRole.Engineer.Id)
                return product.EngineerId;

            if (operation.ExecutorRoleId == CodeDictionary.Instance.AuxiliaryRole.Technologist.Id)
                return product.TechologistId;

            return null;
        }

        #endregion

        #region Revision
        public static Revision FormNewRevision(Model model, string name, bool isArchive = false, bool bomsIsClaimed = false) => new Revision()
        {
            BomsIsClaimed = bomsIsClaimed,
            ModelId = model.Id,
            Name = name,
            IsArchive = isArchive
        };
        #endregion

        #region RevisionBOM
        public static RevisionBOM FormNewRevisionBOM(string slotName, Model model, Revision revision, Revision BomRevision) => new RevisionBOM()
        {            
            Name = slotName,
            ModelId = model.Id,
            RevisionId = revision.Id,
            BOMRevisionId = BomRevision.Id
        };
        #endregion

        #region RouteMap_RouteMapOperation_Operation
        public static RouteMap FormNewRouteMap(string name, Revision revision, bool isArchive = false) => new RouteMap()
        {
            Name = name,
            IsArchive = isArchive,
            RevisionId = revision.Id
        };
        public static RouteMapOperation FormRouteMapOperation(Operation operation, byte priotity) => new RouteMapOperation()
        {
            Operation = operation, //Допускается установка навигационного свойсва для сущностей из одного контекста. Необходимо для отображения данных на экране
            OperationId = operation.Id,
            Priority = priotity,
        };
        public static Operation FormNewOperation(string name, string abbreviationName, Model model, Role role, WorkingGroup workingGroup, string routingNumber, string description, bool isArchive = false) => new Operation()
        {
            Name = name,
            AbbreviationName = abbreviationName,
            ModelId = model?.Id,
            ExecutorRoleId = role?.Id,
            WorkingGroupId = workingGroup?.Id,
            RoutingNumber = routingNumber,
            IsArchive = isArchive,
            Description = description
        };
        #endregion

        #region User
        public static User FormNewUser(string pcName, int accessorLevel = Core.OmvpConstant.MinimumAccessorLevel) => new User()
        {
            PCName = pcName,
            AccessorLevel = accessorLevel
        };
        #endregion

        #region ParameterType_ModelParameter_ProductParameter

        public static ParameterType FormNewParameterType(string name, string unit, string description) => new ParameterType()
        {
            Name = name,
            Description = description,
            Unit = unit,
            Code = "0000" // [Required]
        };

        public static ModelParameter FormNewModelParameter(int modelId, int paramTypeId, string name, string description = "") => new ModelParameter()
        {
            ModelId = modelId,
            ParameterTypeId = paramTypeId,
            Name = name,
            Description = description
        };

        public static ModelParameter FormEditableModelParameter(ModelParameter selectedModelParam) => new ModelParameter()
        {
            Name = selectedModelParam.Name,
            Description = selectedModelParam.Description,
            ParameterType = FormEditableParameterType(selectedModelParam.ParameterType)
        };
        public static ParameterType FormEditableParameterType(ParameterType selectedParameterType) => new ParameterType()
        {
            Name = selectedParameterType.Name,
            Description = selectedParameterType.Description,
            Unit = selectedParameterType.Unit
        };

        public static ProductParameter FormProductParameter(int productId, int modelParamId, string value, string description) => new ProductParameter()
        {
            ProductId = productId,
            ModelParameterId = modelParamId,
            Value = value,
            Description = description
        };
        public static ProductParameter FormEditableProductParameter(ProductParameter productParameter) => new ProductParameter()
        {
            ModelParameter = FormEditableModelParameter(productParameter.ModelParameter),
            Value = productParameter.Value,
            Description = productParameter.Description            
        };
        
        #endregion
    }
}
