﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.CoreDAL
{
    public class DataNormalizer
    {
        public static string NormalizeLetter(string letter)
        {
            if (string.IsNullOrEmpty(letter))           
                return string.Empty;
            

            return letter.Replace(" ", string.Empty)
                            .Replace(",", ".")
                                .Replace("..", ".");
        }       
    }
}
