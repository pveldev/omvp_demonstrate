﻿using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.CoreDAL
{
    public static class OrderTreeExtention
    {
        public static string DetailedInformation(this IGrouping<int, OrderTree> orderTrees)
        {
            if (!orderTrees?.Any() ?? true)
                return "";

            if (!string.IsNullOrEmpty(orderTrees.First().Order.Letter))
            {
                string result = "";
                IEnumerable<string> letters = orderTrees.Select(ot => ot.Order.Letter).Distinct();
                foreach (string letter in letters)
                {
                    IEnumerable<OrderTree> currentOrderTrees = orderTrees.Where(ot => ot.Order.Letter == letter);
                    int sumQuantity = currentOrderTrees.Sum(ot => ot.Order.Quantity);
                    int sumDispatchedQuantity = currentOrderTrees.Sum(ot => ot.DispatchedQuantity);

                    result += string.Concat("[л.", letter, ":", sumDispatchedQuantity, "/", sumQuantity, "] ");
                }
                return result;
            }
            else
            {
                return string.Join(", ", orderTrees.Select(ot => string.Concat(ot.DispatchedQuantity, "/", ot.Order.Quantity)));
            }
        }
    }
}
