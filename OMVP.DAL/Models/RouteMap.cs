namespace OMVP.DAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RouteMap
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RouteMap()
        {
            Products = new HashSet<Product>();
            RouteMapOperations = new HashSet<RouteMapOperation>();
        }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

		[Required]
		public bool IsArchive { get; set; }
        public bool IsClaimed { get; set; }

        public int RevisionId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Product> Products { get; set; }

        public virtual Revision Revision { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RouteMapOperation> RouteMapOperations { get; set; }
    }
}
