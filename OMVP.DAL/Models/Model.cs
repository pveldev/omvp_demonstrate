namespace OMVP.DAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Model
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Model()
        {
            Revisions = new HashSet<Revision>();
            RevisionBOMs = new HashSet<RevisionBOM>();
            ModelsXWorkingGroups = new HashSet<ModelXWorkingGroup>();
            Operations = new HashSet<Operation>();
            ModelParameters = new HashSet<ModelParameter>();
        }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(10)]
        public string AbbreviationName { get; set; }

        [StringLength(50)]
        public string KRPG { get; set; }

        public int ModelTypeId { get; set; }
        public bool HasLetter { get; set; }

		public virtual ModelType ModelType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Revision> Revisions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RevisionBOM> RevisionBOMs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ModelXWorkingGroup> ModelsXWorkingGroups { get; set; }
        public virtual ICollection<Operation> Operations { get; set; }
        public virtual ICollection<ModelParameter> ModelParameters { get; set; }
    }
}
