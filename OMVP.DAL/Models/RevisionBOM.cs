namespace OMVP.DAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RevisionBOM
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RevisionBOM()
        {
            ProductBOMs = new HashSet<ProductBOM>();
        }

		[Required]
		[StringLength(30)]
        public string Name { get; set; }

        public int ModelId { get; set; }

        public int RevisionId { get; set; }

        public int BOMRevisionId { get; set; }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductBOM> ProductBOMs { get; set; }

        public virtual Model Model { get; set; }

        public virtual Revision Revision { get; set; } //������� ����������� �����

        public virtual Revision Revision1 { get; set; } //��������
    }
}
