﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.Models
{ 
    //[Many to many table]
    public partial class ModelXWorkingGroup
    {
        public int ModelId { get; set; }
        public int WorkingGroupId { get; set; }

        public virtual Model Model { get; set; }
        public virtual WorkingGroup WorkingGroup { get; set; }
    }
}
