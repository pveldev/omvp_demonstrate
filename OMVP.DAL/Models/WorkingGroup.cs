namespace OMVP.DAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class WorkingGroup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public WorkingGroup()
        {
            Products = new HashSet<Product>();
            WorkingGroupEmployees = new HashSet<WorkingGroupEmployee>();
            ModelsXWorkingGroups = new HashSet<ModelXWorkingGroup>();
            Operations = new HashSet<Operation>();
        }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
		public bool IsArchive { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Product> Products { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WorkingGroupEmployee> WorkingGroupEmployees { get; set; }
        public virtual ICollection<ModelXWorkingGroup> ModelsXWorkingGroups { get; set; }
        public virtual ICollection<Operation> Operations { get; set; }
    }
}
