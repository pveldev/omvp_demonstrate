﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.Models
{
    public partial class ProductParameter
    {
        public int ProductId { get; set; }
        public int ModelParameterId { get; set; }

        [Required]
        [StringLength(150)]
        public string Value { get; set; }
        public string Description { get; set; }

        public virtual Product Product { get; set; }
        public virtual ModelParameter ModelParameter { get; set; } //parent-template
    }
}
