﻿namespace OMVP.DAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ProductBOM
    {
        public ProductBOM()
        {
            ProductBomLogs = new HashSet<ProductBomLog>();
        }

        public int RevisionBOMId { get; set; }

        public int ProductId { get; set; }//ИД родителя

        public int? BOMProductId { get; set; }//ИД изделия в слоте

        public string Description { get; set; }

        public virtual Product Product { get; set; } //Изделие-родитель

        public virtual Product Product1 { get; set; } //Изделие вставленное в слот

        public virtual RevisionBOM RevisionBOM { get; set; }

        [ForeignKey("ProductBomId")]
        public virtual ICollection<ProductBomLog> ProductBomLogs { get; set; }
    }
}
