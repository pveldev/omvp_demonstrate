﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.Models
{
    public partial class ProductBomLog
    {
        public int ProductBomId { get; set; } //Id слота
        public int ProductId { get; set; } // Id устанавливаемого/извлекаемого изделия
        public string Description { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime Date { get; set; }
        public bool IsInstallation { get; set; }  //1=true=installation, 0=false=deInstallation

        public virtual ProductBOM ProductBom { get; set; }
        public virtual Product Product { get; set; }
    }
}
