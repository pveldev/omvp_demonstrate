﻿namespace OMVP.DAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Operation
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Operation()
        {
            Products = new HashSet<Product>();
            RouteMapOperations = new HashSet<RouteMapOperation>();
            RouteMapProductOperations = new HashSet<RouteMapProductOperation>();
        }

        [Required]
        [StringLength(60)]
        public string Name { get; set; }
        public string AbbreviationName { get; set; }
		public string RoutingNumber { get; set; } //Номер ТК, КК
		public string Description { get; set; }
        public int? WorkingGroupId { get; set; }
        public int? ExecutorRoleId { get; set; } // Определяет тип операции (в зависимости от роли, к которой относится)
        public int? ModelId { get; set; } // Определяет принадлежность операции к конкретной модели
        public bool IsArchive { get; set; }

        [StringLength(4)]
        public string Code { get; set; }

        public virtual WorkingGroup WorkingGroup { get; set; }
        public virtual Role ExecutorRole { get; set; }
        public virtual Model Model { get; set; }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Product> Products { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RouteMapOperation> RouteMapOperations { get; set; }
        public virtual ICollection<RouteMapProductOperation> RouteMapProductOperations { get; set; }
    }
}
