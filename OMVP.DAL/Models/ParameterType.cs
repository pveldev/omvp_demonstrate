﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.Models
{
    public partial class ParameterType
    {
        public ParameterType()
        {
            ModelParameters = new HashSet<ModelParameter>();
        }

        [Required]
        [StringLength(60)]
        public string Name { get; set; }

        [StringLength(10)]
        public string Unit { get; set; }

        [Required]
        [StringLength(4)]
        public string Code { get; set; }
        public string Description { get; set; }


        public virtual ICollection<ModelParameter> ModelParameters { get; set; }
    }
}
