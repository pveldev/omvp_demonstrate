﻿namespace OMVP.DAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Product
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Product()
        {
            ProductBOMs = new HashSet<ProductBOM>();
            ProductBOMs1 = new HashSet<ProductBOM>();
            ProductLogs = new HashSet<ProductLog>();
            RouteMapProductOperations = new HashSet<RouteMapProductOperation>();
            ProductBomLogs = new HashSet<ProductBomLog>();
            ProductParameters = new HashSet<ProductParameter>();
        }

        [Required]
        [StringLength(20)]
        public string SerialNumber { get; set; }

        
        [Column(TypeName = "DateTime2")]
        public DateTime? CreationDate { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime? UpdateDate { get; set; } //Дата последнего изменения модели (Изменеия во внешних ключах не учитываются)
        public int CurrentOperationId { get; set; }
        public float Progress { get; set; } // 1 - готовое, 0 - на начальной стадии
        public int ProductStatusId { get; set; }
        public int RevisionId { get; set; }
        public int? RouteMapId { get; set; }
        public int? OrderElementId { get; set; }
        public int? EngineerId { get; set; }
        public int? TechologistId { get; set; }
        public int? WorkingGroupId { get; set; }
        public bool IsFavorite { get; set; }
        public string Description { get; set; }


		[StringLength(4)]
		public string Letter { get; set; }                                                              
                                                                                                          
        public virtual Employee Engineer { get; set; }                                                                                                
                                                                                                         
        public virtual Employee Techologist { get; set; }
        public virtual Operation Operation { get; set; }

		public virtual OrderElement OrderElement { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductBOM> ProductBOMs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductBOM> ProductBOMs1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductLog> ProductLogs { get; set; }

        public virtual ProductStatus ProductStatus { get; set; }

        public virtual Revision Revision { get; set; }

        public virtual RouteMap RouteMap { get; set; }

        public virtual WorkingGroup WorkingGroup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RouteMapProductOperation> RouteMapProductOperations { get; set; }

        [ForeignKey("ProductId")]
        public virtual ICollection<ProductBomLog> ProductBomLogs { get; set; }
        public virtual ICollection<ProductParameter> ProductParameters { get; set; }
    }
}