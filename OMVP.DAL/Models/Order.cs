namespace OMVP.DAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Order
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Order()
        {
            OrderTrees = new HashSet<OrderTree>();
		}

		[StringLength(4)]
		public string Letter { get; set; }

		public int Quantity { get; set; }

        public int OrderStatusId { get; set; }

        public DateTime ExecutionDate { get; set; }


		[StringLength(70)]
        public string Contract { get; set; }

        [StringLength(50)]
        public string Customer { get; set; }

        [StringLength(30)]
        public string SHPZCode { get; set; }

        [StringLength(20)]
        public string OrderCode { get; set; }
        public bool IsClaimed { get; set; }

        public string Description { get; set; }


		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public virtual ICollection<OrderTree> OrderTrees { get; set; }

		public virtual OrderStatus OrderStatus { get; set; }

    }
}
