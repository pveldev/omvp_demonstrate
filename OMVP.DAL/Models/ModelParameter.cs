﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.Models
{
    public partial class ModelParameter
    {
        public ModelParameter()
        {
            ProductParameters = new HashSet<ProductParameter>();
        }
        public int ModelId { get; set; }
        public int ParameterTypeId { get; set; }

        [Required]
        [StringLength(60)]
        public string Name { get; set; } //Название параметра
        public string Description { get; set; } //(необязательное) описание

        public virtual Model Model { get; set; }
        public virtual ParameterType ParameterType { get; set; }


        public virtual ICollection<ProductParameter> ProductParameters { get; set; }
    }
}
