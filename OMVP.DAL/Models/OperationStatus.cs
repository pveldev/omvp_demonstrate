namespace OMVP.DAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OperationStatuses")]
    public partial class OperationStatus
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OperationStatus()
        {
            RouteMapProductOperations = new HashSet<RouteMapProductOperation>();
        }

        [Required]
        [StringLength(4)]
        public string Code { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RouteMapProductOperation> RouteMapProductOperations { get; set; }
    }
}
