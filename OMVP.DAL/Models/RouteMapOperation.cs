namespace OMVP.DAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RouteMapOperation
    {
        public byte Priority { get; set; }

        public int OperationId { get; set; }

        public int RouteMapId { get; set; }

        public virtual Operation Operation { get; set; }

        public virtual RouteMap RouteMap { get; set; }
    }
}
