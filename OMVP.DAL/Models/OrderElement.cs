namespace OMVP.DAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class OrderElement
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OrderElement()
        {
            Products = new HashSet<Product>();
        }

        public int OrderTreeId { get; set; }

        public byte Month { get; set; }

        public int Quantity { get; set; }

        public string Description { get; set; }

        public int CompletedQuantity { get; set; }

		[ForeignKey(nameof(OrderTreeId))]
		public virtual OrderTree OrderTree { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Product> Products { get; set; }
    }
}
