﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.Models
{
	public partial class OrderTree
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
		public OrderTree()
		{
			OrderElements = new HashSet<OrderElement>();
		}
        public int OrderId { get; set; }
		public int RevisionId { get; set; }
		public int DispatchedQuantity { get; set; }

		[ForeignKey(nameof(OrderId))]
		public virtual Order Order { get; set; }

		[ForeignKey(nameof(RevisionId))]
		public virtual Revision Revision { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public virtual ICollection<OrderElement> OrderElements { get; set; }
	}
}
