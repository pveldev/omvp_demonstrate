namespace OMVP.DAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RouteMapProductOperation
    {
        public RouteMapProductOperation()
        {
            ProductLogs = new HashSet<ProductLog>();
        }
        public bool IsAdditional { get; set; } //true - UnscheduledProductOperation; false - ScheduledProductOperation

        public int ProductId { get; set; }

        public int OperationStatusId { get; set; }
        public int OperationId { get; set; }
        public int? OperationExecuterId { get; set; }
        public byte Priority { get; set; }

        [Column(TypeName = "DateTime2")]
		public DateTime? StartDate { get; set; }

		[Column(TypeName = "DateTime2")]
		public DateTime? EndDate { get; set; }

        public string  Description { get; set; }

        [ForeignKey(nameof(OperationStatusId))]
        public virtual OperationStatus OperationStatus { get; set; }
        public virtual Employee OperationExecuter { get; set; }
        public virtual Product Product { get; set; }
        public virtual Operation Operation { get; set; }

        [ForeignKey("ProductOperationId")]
        public virtual ICollection<ProductLog> ProductLogs { get; set; }

    }
}
