namespace OMVP.DAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ProductLog
    {
        public int UserId { get; set; }
        public int ProductId { get; set; }
        public int? ProductOperationId { get; set; }
        public int? OperationExecuterId { get; set; }

        [Required]
		[StringLength(15)]
		public string OperationAction { get; set; }
	
		[Column(TypeName = "DateTime2")]
		public DateTime Date { get; set; }


        public virtual Employee OperationExecuter { get; set; }
        public virtual RouteMapProductOperation ProductOperation { get; set; }
        public virtual User User { get; set; }
        public virtual Product Product { get; set; }
    }
}
