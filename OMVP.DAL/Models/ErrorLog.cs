﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.Models
{
	public partial class ErrorLog
	{
        public string ExceptionName { get; set; }
		public string Message { get; set; }
		public string InnerExceptionMessage { get; set; }
		public string MethodName { get; set; }
		public string ProjectName { get; set; }

		[Required]
		[Column(TypeName = "DateTime2")]
		public DateTime Date { get; set; }
	}
}
