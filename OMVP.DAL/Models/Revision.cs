namespace OMVP.DAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Revision
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Revision()
        {
            Products = new HashSet<Product>();
            RevisionBOMs = new HashSet<RevisionBOM>();
            RevisionBOMs1 = new HashSet<RevisionBOM>();
			RouteMaps = new HashSet<RouteMap>();
        }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public int ModelId { get; set; }

		[Required]
		public bool IsArchive { get; set; }
        public bool BomsIsClaimed { get; set; } // ������������� ������������ ���������� ����

        public virtual Model Model { get; set; }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Product> Products { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RevisionBOM> RevisionBOMs { get; set; } // ������ �� ������� ��������

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RevisionBOM> RevisionBOMs1 { get; set; } //����� �������


		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RouteMap> RouteMaps { get; set; }
    }
}
