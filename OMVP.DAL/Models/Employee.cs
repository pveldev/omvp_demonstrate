namespace OMVP.DAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Employee
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Employee()
        {
            TechologistProducts = new HashSet<Product>();
            EngineerProducts = new HashSet<Product>();
            WorkingGroupEmployees = new HashSet<WorkingGroupEmployee>();
            Users = new HashSet<User>();
            ProductLogs = new HashSet<ProductLog>();
        }

        [Required]
        [StringLength(20)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(20)]
        public string LastName { get; set; }

        public bool IsArchive { get; set; }

        public int RoleId { get; set; }

        public virtual Role Role { get; set; }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [ForeignKey("EngineerId")]
        public virtual ICollection<Product> EngineerProducts { get; set; }

        [ForeignKey("TechologistId")]
        public virtual ICollection<Product> TechologistProducts { get; set; }

        [ForeignKey("OperationExecuterId")]
        public virtual ICollection<RouteMapProductOperation> RouteMapProductOperations { get; set; }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WorkingGroupEmployee> WorkingGroupEmployees { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<ProductLog> ProductLogs { get; set; }      
    }
}