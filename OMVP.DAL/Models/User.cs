﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.Models
{
	public partial class User
	{
        public User()
        {
            ProductLogs = new HashSet<ProductLog>();
        }

        [Required]
		public string PCName { get; set; }
		public string Password { get; set; }
		public int? EmployeeId { get; set; }

		[Required]
		public int AccessorLevel { get; set; }

		public virtual Employee Employee { get; set; }

        public virtual ICollection<ProductLog> ProductLogs { get; set; }
    }
}
