﻿using OMVP.Core;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Data;
using OMVP.DAL.Models;
using OMVP.DAL.QOs;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OMVP.DAL
{
    public class SQLServer : BasicNotifyModel
	{
        private OmvpDataModel _db;
        public OmvpDataModel db
        {
            get { return _db; }
            set
            {
                _db = value;
                if (value != null)
                {
                    UpdateDbContexts();
                }
            }
        }

        public EmployeeQO Employee { get; set; } = new EmployeeQO();
        public ErrorLogQO ErrorLog { get; set; } = new ErrorLogQO();
        public ModelQO Model { get; set; } = new ModelQO();
        public ModelTypeQO ModelType { get; set; } = new ModelTypeQO();
        public OperationQO Operation { get; set; } = new OperationQO();
        public OperationStatusQO OperationStatus { get; set; } = new OperationStatusQO();
        public OrderQO Order { get; set; } = new OrderQO();
        public OrderStatusQO OrderStatus { get; set; } = new OrderStatusQO();
        public OrderElementQO OrderElement { get; set; } = new OrderElementQO();
        public OrderTreeQO OrderTree { get; set; } = new OrderTreeQO();
        public ProductBomQO ProductBom { get; set; } = new ProductBomQO();
        public ProductBomLogQO ProductBomLog { get; set; } = new ProductBomLogQO();
        public ProductLogQO ProductLog { get; set; } = new ProductLogQO();
        public ProductQO Product { get; set; } = new ProductQO();
        public ProductStatusQO ProductStatus { get; set; } = new ProductStatusQO();
        public RevisionBomQO RevisionBom { get; set; } = new RevisionBomQO();
        public RevisionQO Revision { get; set; } = new RevisionQO();
        public RoleQO Role { get; set; } = new RoleQO();
        public RouteMapOperationQO RouteMapOperation { get; set; } = new RouteMapOperationQO();
        public RouteMapProductOperationQO RouteMapProductOperation { get; set; } = new RouteMapProductOperationQO();
        public RouteMapQO RouteMap { get; set; } = new RouteMapQO();
        public UserQO User { get; set; } = new UserQO();
        public WorkingGroupEmployeeQO WorkingGroupEmployee { get; set; } = new WorkingGroupEmployeeQO();
        public WorkingGroupQO WorkingGroup { get; set; } = new WorkingGroupQO();
        public ParameterTypeQO ParameterType { get; set; } = new ParameterTypeQO();
        public ModelParameterQO ModelParameter { get; set; } = new ModelParameterQO();
        public ProductParameterQO ProductParameter { get; set; } = new ProductParameterQO();

        #region Singleton_SQLServer
        private static readonly Lazy<SQLServer> _lazyModel = new Lazy<SQLServer>(() => new SQLServer());
        public static SQLServer Instance => _lazyModel.Value;
        private SQLServer(){}
        #endregion

        public void UpdateDbContexts(bool refreshContext = true)
        {
            if (refreshContext)
            {
                _db = new OmvpDataModel(db.Database.Connection.ConnectionString);

                Employee.RefreshContext(_db);
                ErrorLog.RefreshContext(_db);
                Model.RefreshContext(_db);
                ModelType.RefreshContext(_db);
                Operation.RefreshContext(_db);
                OperationStatus.RefreshContext(_db);
                OrderElement.RefreshContext(_db);
                Order.RefreshContext(_db);
                OrderStatus.RefreshContext(_db);
                OrderTree.RefreshContext(_db);
                ProductBom.RefreshContext(_db);
                ProductBomLog.RefreshContext(_db);
                ProductLog.RefreshContext(_db);
                ProductStatus.RefreshContext(_db);
                RevisionBom.RefreshContext(_db);
                Revision.RefreshContext(_db);
                Role.RefreshContext(_db);
                RouteMapOperation.RefreshContext(_db);
                RouteMapProductOperation.RefreshContext(_db);
                RouteMap.RefreshContext(_db);
                User.RefreshContext(_db);
                WorkingGroupEmployee.RefreshContext(_db);
                WorkingGroup.RefreshContext(_db);
                Product.RefreshContext(_db);
                ParameterType.RefreshContext(_db);
                ModelParameter.RefreshContext(_db);
                ProductParameter.RefreshContext(_db);
            }
        }
        public async Task<string> ConnectionCheckAsync()
		{
			return await Task.Run(() =>
			{
				try
				{
                    return db.Database.ExecuteSqlCommand(HandleQuery.SelectOne).ToString();
				}
				catch (System.Data.Entity.Core.EntityException ex)
				{
					return $"{ex.Message}\n" + $"{ex.InnerException.Message}";
				}
				catch (SqlException ex)                
				{
                    if (ex.Number == 18452)
                    {
                        return TextHelper.NetConnectionError(TextHelper.SQLServerLoginFailed(ex.Message));
                    }
                    else
                    {
					    return TextHelper.NetConnectionError(ex.Message);
                    }
				}
				catch (InvalidOperationException ex)
				{
                    return TextHelper.OmvpVersionError(ex.Message);
                }
				catch (Exception ex)
				{
					return $"{ex.Message}\n";
				}
			});
		}	

		#region Add_Data<T>
		public T AddEntityToDB<T>(T entity) where T : class => db.ExecuteWithoutTransactionAndReturnValue<T>((db) =>
        {
            return entity = db.Set<T>().Add(entity);
        }); //Одиночное добавление без транзакции

        public IEnumerable<T> AddRange<T>(IEnumerable<T> entities) where T : class => db.ExecuteWithTransactionAndReturnValue<IEnumerable<T>>((db) =>
        {
            return db.Set<T>().AddRange(entities);
        });

        public IEnumerable<T> InsertOrUpdate<T>(IEnumerable<T> entities) where T : BasicModel => db.ExecuteWithTransactionAndReturnValue<IEnumerable<T>>((db) =>
        {
            foreach (T entity in entities)
            {
                db.Entry<T>(entity).State = entity.Id == 0 ?
                                    EntityState.Added :
                                    EntityState.Modified;
            }
            return entities;
        });
        public IEnumerable<T> Insert<T>(IEnumerable<T> entities) where T : BasicModel => db.ExecuteWithTransactionAndReturnValue<IEnumerable<T>>((db) =>
        {
            foreach (T entity in entities)
            {
                db.Entry<T>(entity).State = entity.Id == 0 ?
                                    EntityState.Added :
                                    EntityState.Unchanged;
            }
            return entities;
        }); // State .Added or .Unchanged

        public bool InsertAndReturnBool<T>(IEnumerable<T> entities) where T : BasicModel => db.ExecuteWithTransactionAndSaveAndReturnBool((db) =>
        {
            foreach (T entity in entities)
            {
                db.Entry<T>(entity).State = entity.Id == 0 ?
                                    EntityState.Added :
                                    EntityState.Unchanged;
            }
            return true;
        }); // State .Added or .Unchanged

        public async Task<IEnumerable<T>> AddRangeAsync<T>(IEnumerable<T> entityList) where T : class
		{
			using (var transaction = db.Database.BeginTransaction())
			{
				try
				{
					entityList = db.Set<T>().AddRange(entityList);
					await db.SaveChangesAsync();
					transaction.Commit();
					return entityList;
				}
				catch (Exception ex)
				{
					transaction.Rollback();
                    SQLServerHelper.ExceptionProcessing(ex);
					return null;
				}
			}
		}
        #endregion

        #region Remove_Data<T>
        public void RemoveRange<T>(IEnumerable<T> entitys) where T : class => db.ExecuteWithTransactionAndReturnVoid((db) =>
        {
            db.Set<T>().RemoveRange(entitys);
            //(DEVELOPER-method without return) 
        });
        public bool Remove<T>(T entity) where T : class => db.ExecuteWithoutTransactionWithSaveAndReturnBool((db) =>
        {
            db.Set<T>().Remove(entity);
            return true;
        });
        public bool RemoveRangeAndReturnBool<T>(IEnumerable<T> entitys) where T : class => db.ExecuteWithTransactionAndSave((db) =>
        {
            db.Set<T>().RemoveRange(entitys);
        });
        #endregion
        public bool ModifyEntity<T>(T entity) where T : class => _db.ExecuteWithoutTransactionWithSaveAndReturnBool((db) =>
        {
            db.Entry(entity).State = EntityState.Modified;
            return true;
        });
        #region ProductOperationView

        public bool StartOperation(RouteMapProductOperation productOperation, OperationStatus newStatus, ProductStatus newProductStatus, float newProgress, ProductLog log) => db.ExecuteWithTransactionAndSaveAndReturnBool((db) =>
        {
            RouteMapProductOperation.UpdateProductOperation(productOperation, newStatus, newProgress, newProductStatus, DateTime.Now);

            if (productOperation.OperationStatus == null)
                db.Entry(productOperation).Reference(rmpo => rmpo.OperationStatus).Load(); //Explicitly fix-up the relationship

            db.ProductLogs.Add(log);

            return true;
        });
     
        public bool StartOperation(RouteMapProductOperation productOperation, OperationStatus newStatus, ProductStatus newProductStatus, ProductLog log) => db.ExecuteWithTransactionAndSaveAndReturnBool((db) =>
        {
            RouteMapProductOperation.UpdateProductOperation(productOperation, newStatus, newProductStatus, DateTime.Now);

            db.ProductLogs.Add(log);

            return true;
        });

        public bool EndOperation(RouteMapProductOperation productOperation, OperationStatus newOperationStatus, ProductStatus newProductStatus, float newProgress, IEnumerable<RouteMapProductOperation> productOperations, bool routeMapIsComplete, ProductLog log = null) => 
            db.ExecuteWithTransactionAndSaveAndReturnBool((db) =>
            {
                //productOperationView != null (coz button's CanExecute)
                productOperation.OperationStatusId = newOperationStatus.Id;

                RouteMapProductOperation.UpdateEndProductOperation(productOperation, newOperationStatus, newProgress, newProductStatus, DateTime.Now);

                if (productOperation.OperationStatus == null)
                    db.Entry(productOperation).Reference(rmpo => rmpo.OperationStatus).Load(); //Explicitly fix-up the relationship

                //Если в текущей приоритет-группе все операции "выполнены", то у следующей приоритет-группы выставляется статус "готовы к выполнению"
                if (!productOperations.Any(rmpoView => rmpoView.Priority == productOperation.Priority && rmpoView.OperationStatusId != CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id))
                {
                    foreach (RouteMapProductOperation item in productOperations)
                    {
                        if (item.Priority == productOperation.Priority + 1 && item.OperationStatusId != CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id)
                        {
                            OperationStatus readyToStart = CodeDictionary.Instance.AuxiliaryOperationStatus.ReadyToStart;
                            item.OperationStatusId = readyToStart.Id;

                            RouteMapProductOperation.UpdateProductOperation(item, readyToStart);
                        }
                    }
                }

                if (log != null)
                    db.ProductLogs.Add(log);

                return true;
            });

        public bool EndOperationWithoutTransaction(Product product, RouteMapProductOperation productOperation, OperationStatus newOperationStatus, ProductStatus newProductStatus, float newProgress, IEnumerable<RouteMapProductOperation> productOperations, ProductLog log = null)     
        {
            RouteMapProductOperation.UpdateEndProductOperation(product, productOperation, newOperationStatus, newProgress, newProductStatus, DateTime.Now);

            //Если в текущей приоритет-группе все операции "выполнены", то у следующей приоритет-группы выставляется статус "готовы к выполнению"
            if (!productOperations.Any(rmpoView => rmpoView.Priority == productOperation.Priority && rmpoView.OperationStatusId != CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id))
            {
                foreach (RouteMapProductOperation item in productOperations)
                {
                    if (item.Priority == productOperation.Priority + 1 && item.OperationStatusId != CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id)
                    {
                        RouteMapProductOperation.UpdateProductOperation(item, CodeDictionary.Instance.AuxiliaryOperationStatus.ReadyToStart);
                    }
                }
            }

            if (log != null && log.ProductOperation != null)
                db.ProductLogs.Add(log);

            productOperation.EndDate = DateTime.Now;
            return true;
        }
        public bool CancelOperation(RouteMapProductOperation productOperation, OperationStatus newOperationStatus, ProductStatus newProductStatus, float newProgress, ProductLog log) => db.ExecuteWithTransactionAndSaveAndReturnBool((db) =>
        { 
            RouteMapProductOperation.UpdateProductOperation(productOperation, newOperationStatus, newProgress, newProductStatus);

            db.ProductLogs.Add(log);

            return true;
        });
        #endregion

        #region GroupProductOperationView
        public bool GroupStartOperation(IEnumerable<RouteMapProductOperation> productOperations, int newProductStatusId, int? selectedGroupExecuterId, List<CalculatedProductInfo> calculatedInfos, IEnumerable<ProductLog> logs) => db.ExecuteWithTransactionAndSaveAndReturnBool((db) =>
        {
            if (!productOperations?.Any() ?? true)
            {
                Dialog.ShowError(TextHelper.WrongParameters());
                return false;
            }
            RouteMapProductOperation.UpdateProductOperation(productOperations, selectedGroupExecuterId, CodeDictionary.Instance.AuxiliaryOperationStatus.InProgress, calculatedInfos, newProductStatusId, DateTime.Now);      

            db.ProductLogs.AddRange(logs);
            return true;
        });

        public bool GroupEndOperation(IEnumerable<RouteMapProductOperation> selectedProductOperations, IEnumerable<IEnumerable<RouteMapProductOperation>> groupedProductOperations, 
            IEnumerable<CalculatedProductInfo> calculatedProductInfos, int? selectedGroupExecuterId, IEnumerable<ProductLog> logs) => db.ExecuteWithTransactionAndSaveAndReturnBool((db) =>
        {               
            if ((!selectedProductOperations?.Any() ?? true) || (!logs?.Any() ?? true) || !AllViewsWithSamePriority(selectedProductOperations))
            {
                Dialog.ShowError(TextHelper.WrongParameters());
                return false;
            } //Входные проверки

            OperationStatus completed = CodeDictionary.Instance.AuxiliaryOperationStatus.Completed;

            foreach (IEnumerable<RouteMapProductOperation> currentProductOperations in groupedProductOperations)
            {
                //Product currentProduct = productOperations.First().Product;
                int currentProductId = currentProductOperations.First().Product.Id;
                RouteMapProductOperation currentProductOperation = selectedProductOperations.Single(productOperation => productOperation.ProductId == currentProductId);
                currentProductOperation.OperationStatusId = completed.Id;
                CalculatedProductInfo currentProductInfo = calculatedProductInfos.Single(productWithStatus => productWithStatus.ProductId == currentProductId);

                //Если в текущей приоритет-группе все операции "выполнены", то у следующей приоритет-группы выставляется статус "готовы к выполнению"
                if (currentProductOperations.Where(productOperation => productOperation.Priority == currentProductOperation.Priority && productOperation.OperationStatusId != CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id).Count() == 0)
                {
                    foreach (RouteMapProductOperation item in currentProductOperations)
                    {
                        if (item.Priority == selectedProductOperations.First().Priority + 1 && item.OperationStatusId != CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id)
                        {
                            OperationStatus readyToStart = CodeDictionary.Instance.AuxiliaryOperationStatus.ReadyToStart;
                            item.OperationStatusId = readyToStart.Id;

                            RouteMapProductOperation.UpdateProductOperation(item, readyToStart);
                        }
                    }
                }

                //Обновление данных в БД
                currentProductOperation = RouteMapProductOperation.UpdateEndProductOperation(currentProductOperation, selectedGroupExecuterId, completed, currentProductInfo.NewProductProgress, currentProductInfo.NewProductStatus, DateTime.Now);
            }

            db.ProductLogs.AddRange(logs);
            return true;
        });

        private bool AllViewsWithSamePriority(IEnumerable<RouteMapProductOperation> productOperations) =>       
            productOperations.All(selectedProductOperationView => selectedProductOperationView.Priority == productOperations.First().Priority);
        
        public bool GroupCancelOperation(IEnumerable<RouteMapProductOperation> productOperations, int newProductStatusId, List<CalculatedProductInfo> afterCancelInfos, IEnumerable<ProductLog> logs) => db.ExecuteWithTransactionAndSaveAndReturnBool((db) =>
        {
            if ((!productOperations?.Any() ?? true) || (!logs?.Any() ?? true) || !productOperations.All(selectedProductOperationView => selectedProductOperationView.Priority == productOperations.First().Priority))
            {
                Dialog.ShowError(TextHelper.WrongParameters());
                return false;
            } //Входные проверки

            OperationStatus newOperationStatus = CodeDictionary.Instance.AuxiliaryOperationStatus.ReadyToStart;

            //Обновление данных в БД
            RouteMapProductOperation.UpdateProductOperation(productOperations, newOperationStatus, afterCancelInfos, newProductStatusId);

            db.ProductLogs.AddRange(logs);
            return true;
        });
        #endregion
    }
}
