﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL
{
	//Класс, заполняющий чистую БД всеми небходимыми для работы базовыми данными
	public class DataGenerator
	{
		public SQLServer SQLServer { get; set; } = SQLServer.Instance;

		#region Singletion
		private static readonly object _syncRoot = new object();
		private static DataGenerator _instance;
		public static DataGenerator Instance
		{
			get
			{
				if (_instance == null)
				{
					lock (_syncRoot)
					{
						if (_instance == null)
							_instance = new DataGenerator();
					}
				}
				return _instance;
			}
		}
		private DataGenerator()
		{

		}
		#endregion

		public async Task AddBasicDataAsync()
		{
			if ((await SQLServer.ModelType.GetModelTypesAsync())?.Any() ?? false)
			{
				await SQLServer.AddRangeAsync(new List<ModelType>()
				{
                    new ModelType() { Name = "Верхний уровень",          Code="1001", Description = "Комплексированное изделие" },
                    new ModelType() { Name = "Средний уровень",      Code="1002", Description = "Модуль, входящий КИ"},
                    new ModelType() { Name = "Нижний уровень",        Code="1004", Description = "Составляющая модуля"}
                });
			}

			if ((await SQLServer.Role.GetRolesAsync())?.Any() ?? false)
			{
				await SQLServer.AddRangeAsync(new List<Role>() //30 символов
				{
					new Role() {Name = "Начальник лаборатории",     Code="2001" }, 
                    new Role() {Name = "Инженер",                   Code="2002" },
					new Role() {Name = "Технолог",                  Code="2003" },
					new Role() {Name = "Испытатель",                Code="2004" },
					new Role() {Name = "Инженер-Технолог",          Code="2005" },
					new Role() {Name = "Внешняя",                   Code="2006" }
                });
			}

			if ((await SQLServer.OrderStatus.GetOrderStatusesAsync())?.Any() ?? false)
			{
				await SQLServer.AddRangeAsync(new List<OrderStatus>()
				{
					new OrderStatus() { Name = "Выполняется",           Code="3001"},
					new OrderStatus() { Name = "Выполнен",              Code="3002"},
					new OrderStatus() { Name = "Архивирован",           Code="3003"}, 
                    new OrderStatus() { Name = "Рекламационные работы", Code="3004"}
				});
			}

			if ((await SQLServer.OperationStatus.GetOperationStatusesAsync())?.Any() ?? false)
			{
				await SQLServer.AddRangeAsync(new List<OperationStatus>()
				{
					new OperationStatus() {Name = "Не выполнена",               Code="4001" },
					new OperationStatus() {Name = "Готова к выполнению",        Code="4002" },
					new OperationStatus() {Name = "В процессе",                 Code="4003" },
					new OperationStatus() {Name = "Выполнена",                  Code="4004" },
					new OperationStatus() {Name = "Пропущена/Не выполнялась",   Code="4005" },
					new OperationStatus() {Name = "В процессе (Сборка)",		Code="4006" }
				});
			}

			if ((await SQLServer.ProductStatus.GetProductStatusesAsync())?.Any() ?? false)
			{
				await SQLServer.AddRangeAsync(new List<ProductStatus>() //30 символов
				{
                    new ProductStatus() {Name = "В процессе сборки",                Code="5001" },
                    new ProductStatus() {Name = "В работе",                         Code="5002" },
					new ProductStatus() {Name = "Готов к установке",                Code="5004" },
					new ProductStatus() {Name = "Установлен",                       Code="5005" },
					new ProductStatus() {Name = "Готов к отгрузке",                 Code="5007" },
					new ProductStatus() {Name = "Отгружен",                         Code="5008" },
					new ProductStatus() {Name = "Забракован",                       Code="5009" },
					new ProductStatus() {Name = "Утилизирован",                     Code="5010" },
					new ProductStatus() {Name = "Рекламационные работы",            Code="5013" },
					new ProductStatus() {Name = "Установлен в рекламационный",      Code="5014" },
					new ProductStatus() {Name = "Готов к отгрузке (рекламация)",    Code="5015" },
					new ProductStatus() {Name = "Готов к установке (рекламация)",   Code="5016" },
					new ProductStatus() {Name = "Псевдо отгружен",                  Code="5017" }
                });
			}

			if ((await SQLServer.Operation.GetOperationsAsync())?.Any() ?? false)
			{
				await SQLServer.AddRangeAsync(new List<Operation>() //60 символов
				{
					new Operation() {Name = "Комплектация", Code="6001", Description="Присвоение модулям узлов/ присвоение КИ-ям модулей" }
				});
			}

			if (await SQLServer.User.GetUserAsync("admin") == null)
			{
				await SQLServer.AddRangeAsync(new List<User>()
				{
					new User() {PCName = "admin", Password="<СКРЫТО>", AccessorLevel=1337}
				});
			}
		}
	}
}