﻿using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL
{
	public class CodeDictionary
	{
		public AuxiliaryRole AuxiliaryRole { get; set; } = AuxiliaryRole.Instance;
		public AuxiliaryModelType AuxiliaryModelType { get; set; } = AuxiliaryModelType.Instance;
		public AuxiliaryOrderStatus AuxiliaryOrderStatus { get; set; } = AuxiliaryOrderStatus.Instance;
		public AuxiliaryOperationStatus AuxiliaryOperationStatus { get; set; } = AuxiliaryOperationStatus.Instance;
		public AuxiliaryOperation AuxiliaryOperation { get; set; } = AuxiliaryOperation.Instance;

        #region Singleton
        private static readonly Lazy<CodeDictionary> _lazyModel = new Lazy<CodeDictionary>(() => new CodeDictionary());
        public static CodeDictionary Instance => _lazyModel.Value;

        private CodeDictionary() { }

		#endregion
	}

	public class AuxiliaryModelType
	{
		public readonly List<ModelType> _modelTypes = SQLServer.Instance.ModelType.GetModelTypes();

		private readonly Func<string, Func<ModelType, bool>> _searchByCode =
			(string code) =>
				(ModelType entity) =>
					entity.Code == code;

		#region Consts
		readonly string _topLevelModelCode = "1001";
		readonly string _middleLevelModel = "1002";
        readonly string _lowerLevelModel = "1004";
		#endregion

		#region Singleton
		private static readonly object _syncRoot = new object();

		private static AuxiliaryModelType _instance = null;
		public static AuxiliaryModelType Instance
		{
			get
			{
				if (_instance == null)
				{
					lock (_syncRoot)
					{
						if (_instance == null)
							_instance = new AuxiliaryModelType();
					}
				}
				return _instance;
			}
		}

		private AuxiliaryModelType() { }
		#endregion

		public ModelType TopLevelModel => _modelTypes.Single(_searchByCode(_topLevelModelCode));
		public ModelType MiddleLevelModel => _modelTypes.Single(_searchByCode(_middleLevelModel));
		public ModelType LowerLevelModel => _modelTypes.Single(_searchByCode(_lowerLevelModel));		
	}
	public class AuxiliaryRole
	{
		public readonly List<Role> _roles = SQLServer.Instance.Role.GetRoles();

		private readonly Func<string, Func<Role, bool>> _searchByCode =
		(string code) =>
			(Role entity) =>
				entity.Code == code;

		#region Consts
		readonly string BossCode = "2001";
		readonly string EngineerCode = "2002";
		readonly string TechnologistCode = "2003";
		readonly string TesterCode = "2004";
		readonly string EngineerTechnologistCode = "2005";
		readonly string ExternalCode = "2006";
        #endregion

        #region Singleton
        private static readonly object _syncRoot = new object();

		private static AuxiliaryRole _instance = null;
		public static AuxiliaryRole Instance
		{
			get
			{
				if (_instance == null)
				{
					lock (_syncRoot)
					{
						if (_instance == null)
							_instance = new AuxiliaryRole();
					}
				}
				return _instance;
			}
		}

		private AuxiliaryRole() { }

		#endregion

		public Role Boss => _roles.Single(_searchByCode(BossCode));
		public Role Engineer => _roles.Single(_searchByCode(EngineerCode));
		public Role Technologist => _roles.Single(_searchByCode(TechnologistCode));
		public Role Tester => _roles.Single(_searchByCode(TesterCode));
		public Role EngineerTechnologist => _roles.Single(_searchByCode(EngineerTechnologistCode));
		public Role External => _roles.Single(_searchByCode(ExternalCode));

	}
	public class AuxiliaryOrderStatus
	{
		public readonly List<OrderStatus> _orderStatuses = SQLServer.Instance.OrderStatus.GetOrderStatuses();

		private readonly Func<string, Func<OrderStatus, bool>> _searchByCode =
			(string code) =>
				(OrderStatus entity) =>
					entity.Code == code;

		#region Consts
		readonly string InProgressCode = "3001";
		readonly string CompletedCode = "3002";
		readonly string ArchiveCode = "3003";
		readonly string ReclamationWorkCode = "3004";
		#endregion

		#region Singleton
		private static readonly object _syncRoot = new object();

		private static AuxiliaryOrderStatus _instance = null;
		public static AuxiliaryOrderStatus Instance
		{
			get
			{
				if (_instance == null)
				{
					lock (_syncRoot)
					{
						if (_instance == null)
							_instance = new AuxiliaryOrderStatus();
					}
				}
				return _instance;
			}
		}

		private AuxiliaryOrderStatus() { }

		#endregion

		public OrderStatus InProgress => _orderStatuses.Single(_searchByCode(InProgressCode));
		public OrderStatus Completed => _orderStatuses.Single(_searchByCode(CompletedCode));
		public OrderStatus Archive => _orderStatuses.Single(_searchByCode(ArchiveCode));
		public OrderStatus ReclamationWork => _orderStatuses.Single(_searchByCode(ReclamationWorkCode));
    }
    public class AuxiliaryOperationStatus
	{
		public readonly List<OperationStatus> _operationStatuses = SQLServer.Instance.OperationStatus.GetOperationStatuses();
		private readonly Func<string, Func<OperationStatus, bool>> _searchByCode =
			(string code) =>
				(OperationStatus entity) =>
					entity.Code == code;

		#region Consts
		readonly string NotDoneCode = "4001";
		readonly string ReadyToStartCode = "4002";
		readonly string InProgressCode = "4003";
		readonly string CompletedCode = "4004";
		readonly string MissedCode = "4005";
		readonly string EquipmentInProgressCode = "4006";
		#endregion

		#region Singleton
		private static readonly object _syncRoot = new object();

		private static AuxiliaryOperationStatus _instance = null;
		public static AuxiliaryOperationStatus Instance
		{
			get
			{
				if (_instance == null)
				{
					lock (_syncRoot)
					{
						if (_instance == null)
							_instance = new AuxiliaryOperationStatus();
					}
				}
				return _instance;
			}
		}

		private AuxiliaryOperationStatus() { }
		#endregion
		public OperationStatus NotDone => _operationStatuses.Single(_searchByCode(NotDoneCode));
		public OperationStatus ReadyToStart => _operationStatuses.Single(_searchByCode(ReadyToStartCode));
		public OperationStatus InProgress => _operationStatuses.Single(_searchByCode(InProgressCode));
		public OperationStatus Completed => _operationStatuses.Single(_searchByCode(CompletedCode));
		public OperationStatus Missed => _operationStatuses.Single(_searchByCode(MissedCode));
		public OperationStatus EquipmentInProgress => _operationStatuses.Single(_searchByCode(EquipmentInProgressCode));
	}

	public class AuxiliaryOperation
	{
		public readonly List<Operation> _operations = SQLServer.Instance.Operation.GetOperationsWithCode();

		private readonly Func<string, Func<Operation, bool>> _searchByCode =
			(string code) =>
				(Operation entity) =>
					entity.Code == code;
		#region Consts
		readonly string EquipmentCode = "6001";
		#endregion

		#region Singleton
		private static readonly object _syncRoot = new object();

		private static AuxiliaryOperation _instance = null;
		public static AuxiliaryOperation Instance
		{
			get
			{
				if (_instance == null)
				{
					lock (_syncRoot)
					{
						if (_instance == null)
							_instance = new AuxiliaryOperation();
					}
				}
				return _instance;
			}
		}

		private AuxiliaryOperation() { }
		#endregion

		public Operation Equipment => _operations.Single(_searchByCode(EquipmentCode));
	}
}	