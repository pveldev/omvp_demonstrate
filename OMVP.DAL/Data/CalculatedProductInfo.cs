﻿using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.Data
{
    public class CalculatedProductInfo
    {
        public int ProductId { get; set; }
        public ProductStatus NewProductStatus { get; set; }
        public float NewProductProgress { get; set; }
        public bool RouteMapIsComplete { get; set; }

        public CalculatedProductInfo(int productId, ProductStatus newStatus)
        {
            ProductId = productId;
            NewProductStatus = newStatus;
        }

        public CalculatedProductInfo(int productId, ProductStatus newStatus, float newProgress, bool routeMapIsComplete)
            : this(productId, newStatus)
        {
            NewProductProgress = newProgress;
            RouteMapIsComplete = routeMapIsComplete;
        }


        //Упрощенный конструктор для запросов на рассчет готовности после выполнения групповых операций
        public CalculatedProductInfo()
        {
            NewProductStatus = null;
            RouteMapIsComplete = false;
        }
    }
}
