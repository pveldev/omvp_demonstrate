﻿using OMVP.Core;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class UserQO : BaseQO
    {
        #region Get_Methods
        public User GetUser(string userName) => _db.Users.SingleOrDefault(user => user.PCName == userName);
        public async Task<User> GetUserAsync(string userName) => await _db.Users.SingleOrDefaultAsync(user => user.PCName == userName);
        public async Task<List<User>> GetUsersAsync(int accessorLevel) => await _db.Users.Where(user => user.AccessorLevel >= accessorLevel).ToListAsync();

        public List<User> GetAllSortedUsers(int count = OmvpConstant.MaximumRowsByOneQuery) => _db.Users.OrderBy(user => user.AccessorLevel).Take(count).ToList();
        public async Task<List<User>> GetAllSortedUsersAsync(int count = OmvpConstant.MaximumRowsByOneQuery) => await _db.Users.OrderBy(user => user.AccessorLevel).Take(count).ToListAsync();
        public async Task<List<User>> GetUsersAsync() => await _db.Users.ToListAsync();
        #endregion

        public User UpdateUser(User user, Employee newEmployee, int newAccessorLevel) => _db.ExecuteWithoutTransactionAndReturnValue<User>((db) =>
        {
            user.EmployeeId = newEmployee.Id;
            user.AccessorLevel = newAccessorLevel;

            db.Entry(user).State = EntityState.Modified;
            return user;
        });
    }
}
