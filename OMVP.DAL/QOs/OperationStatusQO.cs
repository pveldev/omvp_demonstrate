﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class OperationStatusQO : BaseQO
    {
        public List<OperationStatus> GetOperationStatuses() => _db.OperationStatuses.ToList();
        public OperationStatus GetOperationStatus(int statusId) => _db.OperationStatuses.Single(status => status.Id == statusId);
        public async Task<List<OperationStatus>> GetOperationStatusesAsync() => await _db.OperationStatuses.ToListAsync();
    }
}
