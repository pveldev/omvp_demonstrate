﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class WorkingGroupQO : BaseQO
    {       
        #region Get_WorkingGroups
        public List<WorkingGroup> GetAllSordtedWorkingGroups(int count = 1000) => _db.WorkingGroups.OrderBy(workingGroup => workingGroup.Name).Take(count).ToList();
        public List<WorkingGroup> GetSortedNotArchiveWorkingGroups(int count = 1000) => _db.WorkingGroups.Where(workingGroup => workingGroup.IsArchive == false).OrderBy(workingGroup => workingGroup.Name).Take(count).ToList();

        public List<WorkingGroup> GetSordetWorkingGroups(int modelId, int count = 1000) => _db.ModelsXWorkingGroups
            .Where(modelXgroup => modelXgroup.ModelId == modelId && modelXgroup.WorkingGroup.IsArchive == false)
                .Select(modelXgroup => modelXgroup.WorkingGroup)
                    .OrderBy(workingGroup => workingGroup.Name)
                        .Take(count).ToList();

        public List<ModelXWorkingGroup> GetAllModelXWorkingGroup() => _db.ModelsXWorkingGroups.ToList();
        #endregion
        #region Get_Async_WorkingGroups
        public async Task<List<WorkingGroup>> GetWorkingGroupsAsync() => await _db.WorkingGroups.ToListAsync();

        public async Task<List<WorkingGroup>> GetNotArchiveWorkingGroupsAsync() => await _db.WorkingGroups.Where(workingGroup => workingGroup.IsArchive == false).ToListAsync();
        #endregion
        public bool ArchiveWorkingGroup(WorkingGroup selectedWorkingGroup) => _db.ExecuteWithoutTransactionWithSaveAndReturnBool((db) =>
        {
            db.Entry(selectedWorkingGroup).State = EntityState.Modified;
            return true;
        }); //Архивация/разархивация рабочей группы	
    }
}
