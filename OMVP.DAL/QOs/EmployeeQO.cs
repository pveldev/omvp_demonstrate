﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class EmployeeQO : BaseQO
    {
        public async Task<List<Employee>> GetSortedEmployeesAsync() => await _db.Employees.OrderBy(employee => employee.LastName).ToListAsync();
        public async Task<List<Employee>> GetNotArchiveEmployeesAsync() => await _db.Employees.Where(employee => employee.IsArchive == false).ToListAsync();
        public List<Employee> GetAllSortedEmployees() => _db.Employees.OrderBy(employee => employee.LastName).ToList();
        public List<Employee> GetSortedNotArchivedEmployees() => _db.Employees.Where(employee => !employee.IsArchive).OrderBy(employee => employee.LastName).ToList();
        public List<Employee> GetSortedNotArchivedEmployees(int workingGroupId) => _db.WorkingGroupEmployees
            .Where(workingGroupEmployee => workingGroupEmployee.WorkingGroupId == workingGroupId && !workingGroupEmployee.Employee.IsArchive)
                .Select(workingGroupEmployee => workingGroupEmployee.Employee)
                    .OrderBy(employee => employee.LastName).ToList();


        public bool ArchiveEmployee(Employee selectedEmployee) => _db.ExecuteWithoutTransactionWithSaveAndReturnBool((db) =>
        {
            db.Entry(selectedEmployee).State = EntityState.Modified;
            return true;
        }); //Архивация/разархивация сотрудника
    }
}
