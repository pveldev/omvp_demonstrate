﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class ModelQO : BaseQO
    {
        #region Get_Models
        public List<Model> GetSortedModels() => _db.Models.OrderBy(model => model.Name).ToList();
        public async Task<List<Model>> GetSortedModelsAsync() => await _db.Models.OrderBy(model => model.ModelType.Code).ToListAsync();
        #endregion
        public Model AddModelWithRevision(Model model, Revision revision) => _db.ExecuteWithTransactionAndReturnValue<Model>((db) =>
        {
            model = db.Models.Add(model);
            revision.ModelId = model.Id;
            db.Revisions.Add(revision);

            return model;
        });       
    }
}
