﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class ParameterTypeQO : BaseQO
    {
        public async Task<List<ParameterType>> GetAllParameterTypesAsync() => await _db.ParameterTypes.ToListAsync();
        public List<ParameterType> GetAllParameterTypes() => _db.ParameterTypes.ToList();
        public async Task<ParameterType> AddNewParameterTypeAsync(ParameterType parameterType) => await _db.ExecuteWithoutTransactionAndReturnValueAsync<ParameterType>((db) =>
        {
           return _db.ParameterTypes.Add(parameterType);
        });

        public ParameterType SaveParameterTypeChanges(ParameterType param) => _db.ExecuteWithoutTransactionAndReturnValue<ParameterType>(db =>
        {
            db.Entry(param).State = EntityState.Modified;
            return param;
        });
    }
}
