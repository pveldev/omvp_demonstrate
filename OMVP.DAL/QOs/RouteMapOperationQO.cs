﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class RouteMapOperationQO : BaseQO
    {      
        public List<RouteMapOperation> GetRouteMapOperations(int routeMapId) => _db.RouteMapOperations.Where(routeMapOperation => routeMapOperation.RouteMapId == routeMapId).ToList();
        public List<RouteMapOperation> GetRouteMapOperations(int? routeMapId) => _db.RouteMapOperations.Where(routeMapOperation => routeMapOperation.RouteMapId == routeMapId).ToList();
        public RouteMapOperation GetFirstOperation(int routeMapId) => _db.RouteMapOperations.FirstOrDefault(routeMapOperation => routeMapOperation.RouteMapId == routeMapId);
    }
}
