﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class OrderQO : BaseQO
    {
        public List<Order> GetOrders() => _db.Orders.ToList();

        public bool DeleteOrderWithTreeAndElements(int orderId, int orderTreeId) => _db.ExecuteWithTransactionAndSaveAndReturnBool((db) =>
        {
            IQueryable<OrderElement> orderElements = db.OrderElements.Where(orderElement => orderElement.OrderTreeId == orderTreeId);
            db.OrderElements.RemoveRange(orderElements);
            IQueryable<OrderTree> orderTrees = db.OrderTrees.Where(orderTree => orderTree.OrderId == orderId);
            db.OrderTrees.RemoveRange(orderTrees);
            Order order = db.Orders.SingleOrDefault(ord => ord.Id == orderId);
            db.Orders.Remove(order);

            return true;
        });

        public Order ArchiveSelectedOrder(Order order, int newOrderStatusId) => _db.ExecuteWithoutTransactionAndReturnValue<Order>((db) =>
        {
            order.OrderStatusId = newOrderStatusId;
            db.Entry(order).State = EntityState.Modified;

            return order;
        });
    }
}
