﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace OMVP.DAL.QOs
{
    public class ProductStatusQO : BaseQO
    {
        public List<ProductStatus> GetAllProductStatuses() => _db.ProductStatuses.ToList();
        public async Task<List<ProductStatus>> GetProductStatusesAsync() => await _db.ProductStatuses.ToListAsync();
    }
}
