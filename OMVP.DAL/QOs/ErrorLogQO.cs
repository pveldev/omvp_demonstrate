﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class ErrorLogQO : BaseQO
    {
        public void AddErrorLog(ErrorLog log)
        {
            try
            {
                _db.ErrorLogs.Add(log);
                _db.SaveChanges();
            }
            catch (Exception)
            {
                //Обработка не требуется
            }
        }

        public List<ErrorLog> GetErrorLogs() => _db.ErrorLogs.OrderByDescending(errorLog => errorLog.Id).ToList();

        public bool DeleteErrorLog(ErrorLog log) => _db.ExecuteWithTransactionAndSave((db) =>
        {
            db.ErrorLogs.Remove(log);
        });
    }
}
