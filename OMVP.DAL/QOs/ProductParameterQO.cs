﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class ProductParameterQO : BaseQO
    {
        public List<ProductParameter> GetProductParameters(int productId) => _db.ProductParameters.Where(parameter => parameter.ProductId == productId).ToList();

        public async Task<ProductParameter> AddNewProductParameterAsync(ProductParameter param) => await _db.ExecuteWithoutTransactionAndReturnValueAsync<ProductParameter>((db) =>
        {
            return db.ProductParameters.Add(param);
        });

        public async Task<bool> RemoveProductParameter(ProductParameter productParameter) => await _db.ExecuteWithoutTransactionWithSaveAndReturnBoolAsync( db =>
        {
            db.ProductParameters.Remove(productParameter);
            return true;
        });


        public ProductParameter ProductParameterSaveChanges(ProductParameter productParameter) => _db.ExecuteWithoutTransactionAndReturnValue(db =>
        {
            db.Entry(productParameter).State = System.Data.Entity.EntityState.Modified;
            return productParameter;
        });
    }
}
