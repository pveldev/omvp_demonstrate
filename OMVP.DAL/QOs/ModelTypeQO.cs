﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class ModelTypeQO : BaseQO
    {
        public async Task<List<ModelType>> GetModelTypesAsync() => await _db.ModelTypes.ToListAsync();
        public List<ModelType> GetModelTypes() => _db.ModelTypes.ToList();
    }
}
