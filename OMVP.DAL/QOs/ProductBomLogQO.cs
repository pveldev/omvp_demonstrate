﻿using OMVP.Core;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class ProductBomLogQO : BaseQO
    {
        public List<ProductBomLog> GetProductBomLogs(int count = 1000) => _db.ProductBomLogs.Include("ProductBom").Include("Product").Take(count).ToList();

   
        #region GetWithParameters
        public List<ProductBomLog> GetProductBomLogs(Revision revision, RevisionBOM slot, Product parentProduct, Product childProduct, DateTime startDate, DateTime endDate, int count = 1000)
        {
            List<SqlParameter> parameters = FillSQLParameters(revision, slot, parentProduct, childProduct, startDate, endDate);
            string query = PrepareQuery(parameters, count);

            return (parameters?.Any() ?? false) ? _db.ProductBomLogs.SqlQuery(query, parameters.ToArray()).ToList() : GetProductBomLogs(count);
        }
        private List<SqlParameter> FillSQLParameters(Revision revision, RevisionBOM slot, Product parentProduct, Product childProduct, DateTime startDate, DateTime endDate)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>();

            if (revision != null)
                sqlParameters.Add(new SqlParameter() { ParameterName = HandleQuery.RevisioIdParam, SqlDbType = SqlDbType.Int, Value = revision.Id });

            if (slot != null)
                sqlParameters.Add(new SqlParameter() { ParameterName = HandleQuery.SlotIdParam, SqlDbType = SqlDbType.Int, Value = slot.Id });

            if (parentProduct != null)
                sqlParameters.Add(new SqlParameter() { ParameterName = HandleQuery.ParentProductParam, SqlDbType = SqlDbType.Int, Value = parentProduct.Id });

            if (childProduct != null)
                sqlParameters.Add(new SqlParameter() { ParameterName = HandleQuery.ChildProductParam, SqlDbType = SqlDbType.Int, Value = childProduct.Id });


            //Параметры для дат всегда присутствуют
            sqlParameters.Add(new SqlParameter() { ParameterName = HandleQuery.StartDateParam, SqlDbType = SqlDbType.DateTime2, Value = startDate });
            sqlParameters.Add(new SqlParameter() { ParameterName = HandleQuery.EndDateParam, SqlDbType = SqlDbType.DateTime2, Value = endDate });

            return sqlParameters;
        }
        private string PrepareQuery(List<SqlParameter> parameters, int count)
        {
            List<string> queryJoins = new List<string>();
            string queryParameters = "";

            if (parameters.Any(param => param.ParameterName == HandleQuery.RevisioIdParam))
            {
                queryJoins.Add(CheckHelper.AddJoinIfFirstTime(HandleQuery.JoinProductBOMsForProductBomLog, queryJoins));
                queryJoins.Add(CheckHelper.AddJoinIfFirstTime(HandleQuery.JoinProductsForProductBomLog, queryJoins));
                queryParameters += CheckHelper.QueryParametersNullCheck(queryParameters) + HandleQuery.RevisionEquality;
            }
            if (parameters.Any(param => param.ParameterName == HandleQuery.SlotIdParam))
            {
                queryJoins.Add(CheckHelper.AddJoinIfFirstTime(HandleQuery.JoinProductBOMsForProductBomLog, queryJoins));
                queryParameters += CheckHelper.QueryParametersNullCheck(queryParameters) + HandleQuery.SlotIdEquality;
            }
            if (parameters.Any(param => param.ParameterName == HandleQuery.ParentProductParam))
            {
                queryJoins.Add(CheckHelper.AddJoinIfFirstTime(HandleQuery.JoinProductBOMsForProductBomLog, queryJoins));
                queryParameters += CheckHelper.QueryParametersNullCheck(queryParameters) + HandleQuery.ParentProductIdEquality;
            }
            if (parameters.Any(param => param.ParameterName == HandleQuery.ChildProductParam))
            {
                queryParameters += CheckHelper.QueryParametersNullCheck(queryParameters) + HandleQuery.ChildProductIdEquality;
            }

            //Параметры дат всегда присутсивуют
            queryParameters += CheckHelper.QueryParametersNullCheck(queryParameters) + HandleQuery.StartDateInequality("pbl");
            queryParameters += CheckHelper.QueryParametersNullCheck(queryParameters) + HandleQuery.StopDateInequality("pbl");

            return HandleQuery.SelectFromProductBomLogs(count) + TextHelper.ListToString(queryJoins)+ queryParameters + HandleQuery.OrderByDateDesc("pbl");
        }
        #endregion

        public List<ProductBomLog> GetBomLogsByProductId(int productId) => _db.ProductBomLogs.Where(bomLog => bomLog.ProductId == productId).ToList();
        public int GetBomLogsCountByBomProduct(int productId) => _db.ProductBomLogs.Where(bomLog => bomLog.ProductId == productId).Count(); // Количество установок/демонтажей изделия{productId} в посадочное место
        public int GetBomLogsCountByParentProduct(int productId) => _db.ProductBomLogs.Where(bomLog => bomLog.ProductBom.ProductId == productId).Count(); // Количество установок/демонтажей в посадочные места изделия{productId}
        public int GetBomLogsCountByParentOrBomProduct(int productId) => _db.ProductBomLogs.Where(bomLog => bomLog.ProductBom.ProductId == productId || bomLog.ProductId == productId).Count(); // Любые упоминания изделия {productId} (в качестве родителя или ребенка) в таблице истории
    }
}
