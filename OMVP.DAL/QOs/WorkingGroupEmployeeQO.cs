﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class WorkingGroupEmployeeQO : BaseQO
    {        
        #region Get_Methods
        public List<WorkingGroupEmployee> GetWorkingGroupEmployees(int workingGroupId) =>
            _db.WorkingGroupEmployees.Where(workingGroupEmployee => workingGroupEmployee.WorkingGroupId == workingGroupId).ToList();
        public List<WorkingGroupEmployee> GetSortedWorkingGroupEmployees(int workingGroupId) =>
            GetWorkingGroupEmployees(workingGroupId).OrderBy(workingGroupEmployee => workingGroupEmployee.Employee.LastName).ToList();

        public List<WorkingGroupEmployee> GetWorkingGroupEmployees(int?[] workingGroupIds) =>
            _db.WorkingGroupEmployees.Where(workingGroupEmployee => workingGroupIds.Contains(workingGroupEmployee.WorkingGroupId) && !workingGroupEmployee.Employee.IsArchive).OrderBy(workingGroupEmployee => workingGroupEmployee.Employee.LastName).ToList();
        #endregion

        public WorkingGroupEmployee RemoveEmployeeFromWorkingGroup(WorkingGroupEmployee workingGroupEmployee) => _db.WorkingGroupEmployees.Remove(workingGroupEmployee);       
    }
}
