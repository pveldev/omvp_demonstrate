﻿using OMVP.Core;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class OrderElementQO : BaseQO
    {
        #region Get_OrderElements
        //Срез для текущей ревизии
        public List<OrderElement> GetOrderElements(Revision revision, int executionYear) => _db.OrderElements.Where(orderElement =>
            orderElement.OrderTree.Revision.Model.ModelTypeId == revision.Model.ModelTypeId
        &&  orderElement.OrderTree.RevisionId == revision.Id
        &&  orderElement.OrderTree.Order.ExecutionDate.Year == executionYear).ToList();
        
        public List<OrderElement> GetActualAndClaimedOrderElements(Revision revision)
        {
            List<OrderElement> orderElements = new List<OrderElement>();
            List<OrderTree> actualOrderTrees = GetActualOrderTrees(revision.Id);
            actualOrderTrees.ForEach(orderTree => orderElements.AddRange(GetOrderElements(orderTree.Id)
                .Where(orderElement => orderElement.Quantity > orderElement.CompletedQuantity)));

            return orderElements.OrderBy(orderElement => orderElement.Month).ToList();
        } // Возвращает полный список OrderElements для данной ревизии
        public async Task<List<OrderElement>> GetOrderElementsAsync(Revision revision)
        {
            List<OrderElement> orderElements = new List<OrderElement>();
            List<OrderTree> actualOrderTrees = await GetActualOrderTreesAsync(revision.Id);
            actualOrderTrees.ForEach(orderTree => orderElements.AddRange(GetOrderElements(orderTree.Id)
                .Where(orderElement => orderElement.Quantity > orderElement.CompletedQuantity)));

            return orderElements.OrderBy(orderElement => orderElement.Month).ToList();
        } // Асинхронно возвращает полный список OrderElements для данной ревизии

        public List<OrderElement> GetActualAndClaiemdOrderElementsByRevisionId(Revision revision)
        {
            if (revision is null)
            {
                return null;
            }

            return _db.OrderElements.Where(orderElemet =>
                orderElemet.OrderTree.Order.OrderStatusId != CodeDictionary.Instance.AuxiliaryOrderStatus.Archive.Id &&
                orderElemet.OrderTree.Order.ExecutionDate.Year == DateTime.Now.Year &&
                orderElemet.OrderTree.RevisionId == revision.Id &&
                orderElemet.OrderTree.Order.IsClaimed
                ).ToList();
        }

        private List<OrderTree> GetActualOrderTrees(int revisionId) => _db.OrderTrees.Where(orderTree =>
            orderTree.RevisionId == revisionId &&
            orderTree.Order.OrderStatusId != CodeDictionary.Instance.AuxiliaryOrderStatus.Archive.Id &&
            orderTree.Order.IsClaimed == true
        ).ToList();
        private async Task<List<OrderTree>> GetActualOrderTreesAsync(int revisionId) => await _db.OrderTrees.Where(orderTree => orderTree.RevisionId == revisionId && orderTree.Order.OrderStatusId == CodeDictionary.Instance.AuxiliaryOrderStatus.InProgress.Id).ToListAsync();

        private List<OrderElement> GetOrderElements(int orderTreeId) => _db.OrderElements.Where(orderElement => orderElement.OrderTreeId == orderTreeId).OrderBy(orderElement => orderElement.Month).ToList();
        public List<OrderElement> GetOrderElements(int orderId, int month) => _db.OrderElements.Where(orderElement => orderElement.OrderTree.OrderId == orderId && orderElement.Month == month).ToList(); // Получение дерева отгрузочных строк заданного месяца в рамках одного заказа
        public List<OrderElement> GetOrderElements(int orderId, int month, int[] revisionIds) => _db.OrderElements
            .Where(orderElement => orderElement.OrderTree.OrderId == orderId && orderElement.Month == month && revisionIds.Contains(orderElement.OrderTree.RevisionId)).ToList(); // Получение (неполного. в рамках выбранного дерева ревизий) дерева отгрузочных строк заданного месяца в рамках одного заказа
        public List<OrderElement> GetOrderElementsByOrderId(int orderId) => _db.OrderElements.Where(orderElement => orderElement.OrderTree.OrderId == orderId).OrderBy(orderElement => orderElement.Month).ToList();

        public List<OrderElement> GetValidOrderElements(int revisionId) => _db.Products
            .Where(product => product.RevisionId == revisionId && product.OrderElementId != null)
                .Select(product => product.OrderElement)
                    .Distinct().ToList();

        #endregion

        public async Task<bool> InsertOrUpdateOrderElementsAsync(List<OrderElement> orderElements) => await _db.ExecuteWithTransactionAndSaveAsync((db) =>
        {
            foreach (OrderElement orderElement in orderElements)
            {
                if (orderElement.Quantity > 0)
                {
                    if (orderElement.Id == 0)
                    {
                        db.OrderElements.Add(orderElement);
                    }
                    else
                    {
                        db.OrderElements.SqlQuery(HandleQuery.UpdateOrderElements, new[] { new SqlParameter(@"quantity", orderElement.Quantity), new SqlParameter(@"id", orderElement.Id) });
                    }
                }
                else if (orderElement.Quantity == 0)
                {
                    db.OrderElements.Remove(orderElement);
                }
                else
                {
                    throw new ArgumentException(TextHelper.OrderElementQuantityValidationErrorMessage(orderElement.Quantity));
                }
            }
        });
    }
}
