﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace OMVP.DAL.QOs
{
    public class RevisionQO : BaseQO
    {
        #region Get_Revisions     
        public List<Revision> GetRevisions(int modelId) => _db.Revisions.Where(revision => revision.ModelId == modelId).ToList();
        public Revision GetRevision(int revisioId) => _db.Revisions.Single(revision => revision.Id == revisioId);

        public List<Revision> GetSortedRevisions() => _db.Revisions.OrderBy(revision => revision.Name).ToList();
        public async Task<List<Revision>> GetSortedRevisionsAsync() => await _db.Revisions.OrderBy(revision => revision.Name).ToListAsync();
        public async Task<List<Revision>> GetRevisionsAsync() => await _db.Revisions.OrderBy(revision => revision.Name).ToListAsync();

        public List<Revision> GetNoComponentRevisions() =>
            _db.Revisions.Where(revision => revision.Model.ModelTypeId == CodeDictionary.Instance.AuxiliaryModelType.TopLevelModel.Id || revision.Model.ModelTypeId == CodeDictionary.Instance.AuxiliaryModelType.MiddleLevelModel.Id)
                .OrderBy(revision => revision.Model.ModelType.Code).ToList();
        public async Task<List<Revision>> GetNoComponentRevisionsAsync() =>
          await _db.Revisions.Where(revision => revision.Model.ModelTypeId == CodeDictionary.Instance.AuxiliaryModelType.TopLevelModel.Id || revision.Model.ModelTypeId == CodeDictionary.Instance.AuxiliaryModelType.MiddleLevelModel.Id)
              .OrderBy(revision => revision.Model.ModelType.Code).ToListAsync();

        public async Task<List<Revision>> GetRevisionsByModelTypeIdAsync(int modelTypeId) => await _db.Revisions.Where(revision => revision.Model.ModelType.Id == modelTypeId).ToListAsync();
        public List<Revision> GetRevisionsByModelTypeId(int modelTypeId, bool bomsIsClaimed) => _db.Revisions.Where(revision => revision.Model.ModelType.Id == modelTypeId && revision.BomsIsClaimed == bomsIsClaimed).ToList();
        public async Task<List<Revision>> GetRevisionsByModelTypeCodeAsync(int modelTypeId) => await _db.Revisions.Where(revision => revision.Model.ModelTypeId == modelTypeId).ToListAsync();
        
        public List<Revision> GetRevisionsChildren(int revisionId) => _db.Revisions.SqlQuery(HandleQuery.GetRevisionsChildren, new SqlParameter(@"revisionId", revisionId)).ToList();
        public async Task<List<Revision>> GetRevisionsChildrenAsync(int revisionId) => await _db.Revisions.SqlQuery(HandleQuery.GetRevisionsChildren, new SqlParameter(@"revisionId", revisionId)).ToListAsync();
        #endregion
        public bool DeleteRevision(Revision revision) => _db.ExecuteWithoutTransactionWithSaveAndReturnBool((db) =>
        {
            db.Revisions.Remove(revision);
            return true;
        });
        public Revision ClaimRevision(Revision revision) => _db.ExecuteWithoutTransactionAndReturnValue((db) =>
        {
            revision.BomsIsClaimed = true;
            db.Entry(revision).State = EntityState.Modified;
            return revision;
        });
    }
}
