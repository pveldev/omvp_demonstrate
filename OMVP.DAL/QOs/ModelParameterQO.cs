﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class ModelParameterQO : BaseQO
    {
        public List<ModelParameter> GetModelParameters(int modelId) => _db.ModelParameters.Where(modelParameter => modelParameter.ModelId == modelId).ToList();
        public async Task<ModelParameter> AddNewModelParameterAsync(ModelParameter modelParam) => await _db.ExecuteWithoutTransactionAndReturnValueAsync<ModelParameter>((db) =>
        {
            return _db.ModelParameters.Add(modelParam);
        });

        public bool RemoveModelParameterFromModel(ModelParameter param) => _db.ExecuteWithTransactionAndSaveAndReturnBool( db =>
        {
            db.ModelParameters.Remove(param);
            return true;
        });

        public ModelParameter SaveModelParameterChanges(ModelParameter param) => _db.ExecuteWithoutTransactionAndReturnValue<ModelParameter>( db =>
        {
            db.Entry(param).State = System.Data.Entity.EntityState.Modified;
            return param;
        });
    }
}
