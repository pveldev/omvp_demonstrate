﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class OrderStatusQO : BaseQO
    {
        public List<OrderStatus> GetOrderStatuses() => _db.OrderStatuses.ToList();
        public async Task<List<OrderStatus>> GetOrderStatusesAsync() => await _db.OrderStatuses.ToListAsync();
    }
}
