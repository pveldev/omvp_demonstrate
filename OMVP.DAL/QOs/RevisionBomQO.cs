﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class RevisionBomQO : BaseQO
    {
        public List<RevisionBOM> GetRevisionBOMs(int revisionId) => _db.RevisionBOMs.Where(bom => bom.RevisionId == revisionId).ToList();
        public List<RevisionBOM> GetRevisionBOMs(int[] revisionIds) => _db.RevisionBOMs.Where(bom => revisionIds.Contains(bom.RevisionId)).ToList();
    }
}
