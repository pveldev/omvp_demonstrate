﻿using System;
using OMVP.DAL.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OMVP.DAL.CoreDAL;

namespace OMVP.DAL.QOs
{
    public class OrderTreeQO : BaseQO
    {
        #region Get_OrderTrees
        public List<OrderTree> GetOrderTrees(int modelTypeId, int executionYear) => _db.OrderTrees.Where(orderTree => orderTree.Revision.Model.ModelTypeId == modelTypeId && orderTree.Order.ExecutionDate.Year == executionYear).ToList();
        public async Task<List<OrderTree>> GetOrderTreesByModelTypeCodeAsync(int modelTypeId) => await _db.OrderTrees.Where(orderTree => orderTree.Revision.Model.ModelTypeId == modelTypeId).ToListAsync();

        public List<OrderTree> GetActualAndClaimedOrderTrees(int revisionId) =>
            _db.OrderTrees.Where(orderTree => 
                                    orderTree.RevisionId == revisionId && 
                                    orderTree.Order.OrderStatusId != CodeDictionary.Instance.AuxiliaryOrderStatus.Archive.Id &&
                                    orderTree.Order.ExecutionDate.Year == DateTime.Now.Year &&
                                    orderTree.Order.IsClaimed == true
                                ).ToList();
        public async Task<List<OrderTree>> GetActualOrderTreesAsync(int revisionId) => await _db.OrderTrees.Where(orderTree => orderTree.RevisionId == revisionId && orderTree.Order.OrderStatusId == CodeDictionary.Instance.AuxiliaryOrderStatus.InProgress.Id).ToListAsync();

        public List<OrderTree> GetActualOrderTreesByRevisionId(int oderId) => _db.OrderTrees.Where(orderTree => orderTree.OrderId == oderId && orderTree.Order.OrderStatusId == CodeDictionary.Instance.AuxiliaryOrderStatus.InProgress.Id).ToList();
        public List<OrderTree> GetActualOrderTreesByOrderTree(OrderTree selectedOrderTree) => _db.OrderTrees.Where(orderTree => orderTree.OrderId == selectedOrderTree.OrderId && orderTree.Order.OrderStatusId == CodeDictionary.Instance.AuxiliaryOrderStatus.InProgress.Id).ToList();
        #endregion
       
        public OrderTree AddOrder(Order order, List<OrderTree> orderTrees) => _db.ExecuteWithTransactionAndReturnValue<OrderTree>((db) =>
        {
            order = db.Orders.Add(order);
            orderTrees = db.OrderTrees.AddRange(orderTrees).ToList();
            return orderTrees.SingleOrDefault(orderTree => orderTree.Revision.Model.ModelTypeId == CodeDictionary.Instance.AuxiliaryModelType.TopLevelModel.Id); // Обновление коллекции для вывода на экран
        }); // Добавление заказа в таблицы Order, OrderTree единой транзакцией
    }
}
