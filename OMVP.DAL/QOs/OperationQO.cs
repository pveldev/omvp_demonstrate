﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class OperationQO : BaseQO
    {
        #region Get_methods
        public Operation GetOperation(int operationId) => _db.Operations.Single(operation => operation.Id == operationId);
        public List<Operation> GetSortedNotArchivedOperations() => _db.Operations.OrderBy(operation => operation.Name).Where(operation => operation.IsArchive == false).ToList();
        public List<Operation> GetSortedGeneralOperations(int count = 1000) => _db.Operations.Where(operation => operation.ModelId == null).OrderBy(operation => operation.Name).Take(count).ToList();
        public List<Operation> GetSortedOperations(int modelId, int count = 1000) => _db.Operations.Where(operation => operation.ModelId == modelId).OrderBy(operation => operation.Name).Take(count).ToList();
        public List<Operation> GetSortedNotArchivedOperations(Model model) => _db.Operations.Where(operation => (operation.ModelId == model.Id || operation.ModelId == null) && operation.IsArchive == false).OrderBy(operation => operation.Name).ToList();
        public List<Operation> GetOperationsWithCode() => _db.Operations.Where(operation => !string.IsNullOrEmpty(operation.Code)).ToList();

        #endregion
        #region GetAsync
        public async Task<List<Operation>> GetSortedOperationsAsync() => await _db.Operations.OrderBy(operation => operation.Name).ToListAsync();
        public async Task<List<Operation>> GetOperationsAsync() => await _db.Operations.OrderBy(operation => operation.Name).ToListAsync();

        #endregion
    }
}
