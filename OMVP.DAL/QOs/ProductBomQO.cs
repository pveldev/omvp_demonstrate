﻿using System;
using System.Collections.Generic;
using OMVP.DAL.Models;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OMVP.DAL.CoreDAL;
using OMVP.Modal;
using OMVP.DAL.Data;

namespace OMVP.DAL.QOs
{
    public class ProductBomQO : BaseQO
    {
        #region Get_Methods
        public List<ProductBOM> GetProductBOMs(int productId) => _db.ProductBOMs.Where(bom => bom.ProductId == productId).ToList();
        public async Task<List<ProductBOM>> GetProductBOMsAsync(int productId) => await _db.ProductBOMs.Where(bom => bom.ProductId == productId).ToListAsync();

        public ProductBOM GetProductBOM(int bomProductId) => _db.ProductBOMs.Single(bom => bom.BOMProductId == bomProductId);
        public async Task<ProductBOM> GetProductBomAsync(int bomProductId) => await _db.ProductBOMs.SingleAsync(bom => bom.BOMProductId == bomProductId);
        #endregion

        #region SaveSlotChanges
        public bool SaveSlotChanges(Product product, List<Product> installedProducts, List<Product> deinstalledProducts, List<CalculatedProductInfo> productInfos, List<ProductBomLog> productBomLogs, IEnumerable<RouteMapProductOperation> currentProductOperations, RouteMapProductOperation selectedProductOperaion, bool installationIsCompleted, float newProgress, ProductStatus newProductStatus, ProductLog log = null) => _db.ExecuteWithTransactionAndSave((db) =>
        {
            SaveSlotChanges(installedProducts, deinstalledProducts, productBomLogs, productInfos);

            if (installationIsCompleted)
            {
                SQLServer.Instance.EndOperationWithoutTransaction(product, selectedProductOperaion, CodeDictionary.Instance.AuxiliaryOperationStatus.Completed, newProductStatus, newProgress, currentProductOperations, log);
            }
            else
            {
                selectedProductOperaion.OperationStatusId = CodeDictionary.Instance.AuxiliaryOperationStatus.EquipmentInProgress.Id;
                selectedProductOperaion.Product.Progress = newProgress;
                selectedProductOperaion.Product.ProductStatusId = newProductStatus.Id;
            }
        });

        public bool SaveSlotChanges(List<Product> installedProducts, List<Product> deinstalledProducts, List<CalculatedProductInfo> productInfos, List<ProductBomLog> productBomLogs, Product parentProduct, bool installationIsCompleted, ProductStatus newProductStatus, ProductLog log = null) => _db.ExecuteWithTransactionAndSave((db) =>
        {
            SaveSlotChanges(installedProducts, deinstalledProducts, productBomLogs, productInfos);

            if (installationIsCompleted) //Для изделий без МК завершение комплектации эквивалетно готовности изделия. Так же сюда попадают изделий с пройденым маршрутом
            {
                SQLServer.Instance.Product.EndInstallationWithoutTransaction(parentProduct, newProductStatus, 1, log); // Для изделий без маршрута, но с БОМ-ами   
            }
            else
            {
                parentProduct.ProductStatusId = newProductStatus.Id;
                parentProduct.Progress = 0.5F; //для изделий без МК - открыть "комплектацию" 0.5 прогресса
            }
        });

        public bool InstallationIsCompleted(Product product) =>        
            _db.ProductBOMs.Where(bom => product.Id == bom.ProductId).All(bom => bom.BOMProductId != null);      

        private void SaveSlotChanges(List<Product> installedProducts, List<Product> deinstalledProducts, List<ProductBomLog> productBomLogs, List<CalculatedProductInfo> productInfos)
        {     
            foreach (Product installedProduct in installedProducts)
            {
                installedProduct.ProductStatusId = productInfos.Single(p => p.ProductId == installedProduct.Id).NewProductStatus.Id;
                //_db.Entry(installedProduct).State = EntityState.Modified;
            }

            foreach (Product deinstalledProduct in deinstalledProducts)
            {
                deinstalledProduct.ProductStatusId = productInfos.Single(p => p.ProductId == deinstalledProduct.Id).NewProductStatus.Id;
                //_db.Entry(deinstalledProduct).State = EntityState.Modified;
            }

            _db.ProductBomLogs.AddRange(productBomLogs);         
        } //Внутренний метод без транзакции
        #endregion
    }
}
