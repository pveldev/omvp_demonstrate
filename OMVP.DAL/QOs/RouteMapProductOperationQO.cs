﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Data;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class RouteMapProductOperationQO : BaseQO
    {
        #region DEVELOPER_fix_empty_navigation_props  
        //public RouteMapProductOperation AddUnscheduledOperation1(float newProgress, RouteMapProductOperation unscheduledProductOperation)
        //{
        //    int routeMapProductOperationId = AddUnscheduledOperation_bugged(newProgress, unscheduledProductOperation).Id;
        //    return GetSingleProductOperationWithIncludes(routeMapProductOperationId);

        //} //Костыль-обертка
        //public RouteMapProductOperation AddUnscheduledOperation3(float newProgress, RouteMapProductOperation unscheduledProductOperation)
        //{
        //    int routeMapProductOperationId = AddUnscheduledOperation_bugged3(newProgress, unscheduledProductOperation).Id;
        //    return GetSingleProductOperationWithIncludes(routeMapProductOperationId);
        //} //Костыль-обертка
        //public RouteMapProductOperation AddUnscheduledOperation_bugged3(float newProgress, RouteMapProductOperation unscheduledProductOperation) => _db.ExecuteWithTransactionAndReturnValue<RouteMapProductOperation>((db) =>
        //{
        //    unscheduledProductOperation = db.RouteMapProductOperations.Add(unscheduledProductOperation);
        //    db.Entry(unscheduledProductOperation).State = EntityState.Added;

        //    if (unscheduledProductOperation.Product != null && (unscheduledProductOperation.Product.ProductStatusId != CodeDictionary.Instance.AuxiliaryProductStatus.Assembly.Id || unscheduledProductOperation.Product.ProductStatusId != CodeDictionary.Instance.AuxiliaryProductStatus.InWork.Id))
        //    {
        //        unscheduledProductOperation.Product.ProductStatusId = CodeDictionary.Instance.AuxiliaryProductStatus.InWork.Id;
        //        unscheduledProductOperation.Product.Progress = newProgress;

        //        db.Entry(unscheduledProductOperation.Product).State = EntityState.Modified;
        //    }
        //    db.RouteMapProductOperations.Attach(unscheduledProductOperation);
        //    return unscheduledProductOperation;

        //}); // Добавление внеплановой операции в текущий маршрут изделия
        //public RouteMapProductOperation AddUnscheduledOperation_bugged(float newProgress, RouteMapProductOperation unscheduledProductOperation) => _db.ExecuteWithTransactionAndReturnValue<RouteMapProductOperation>((db) =>
        //{

        //    //unscheduledProductOperation = SQLServer.Instance.AddEntityToDB(unscheduledProductOperation);

        //    unscheduledProductOperation = db.RouteMapProductOperations.Add(unscheduledProductOperation);
        //    //db.Entry(unscheduledProductOperation).State = EntityState.Added;
        //    //db.SaveChanges();


        //    if (unscheduledProductOperation.Product != null && (unscheduledProductOperation.Product.ProductStatusId != CodeDictionary.Instance.AuxiliaryProductStatus.Assembly.Id || unscheduledProductOperation.Product.ProductStatusId != CodeDictionary.Instance.AuxiliaryProductStatus.InWork.Id))
        //    {
        //        unscheduledProductOperation.Product.ProductStatusId = CodeDictionary.Instance.AuxiliaryProductStatus.InWork.Id;
        //        unscheduledProductOperation.Product.Progress = newProgress;

        //        db.Entry(unscheduledProductOperation.Product).State = EntityState.Modified;
        //    }
        //    //EF сходит с ума
        //    //БАГ: Навигационные свойства пустые, если добавять к изделию с пустой МК
        //    //Пока не разберусь - не пользоваться ретерном
        //    // return unscheduledProductOperation;

        //    return unscheduledProductOperation;

        //}); // Добавление внеплановой операции в текущий маршрут изделия
        #endregion

        public List<RouteMapProductOperation> AddUnscheduledOperation(float newProgress, RouteMapProductOperation unscheduledProductOperation, ProductStatus newProductStatus, bool IsAssembly, RouteMapProductOperation equipment) => _db.ExecuteWithTransactionAndReturnValue<List<RouteMapProductOperation>>((db) =>
        {
            List<RouteMapProductOperation> unscheduleds = new List<RouteMapProductOperation>();
            if (equipment != null && unscheduledProductOperation.OperationId != CodeDictionary.Instance.AuxiliaryOperation.Equipment.Id)           
                unscheduleds.Add(db.RouteMapProductOperations.Add(equipment));
            
            unscheduleds.Add(db.RouteMapProductOperations.Add(unscheduledProductOperation));

            #region fix
            foreach (RouteMapProductOperation unscheduled in unscheduleds)
            {
                if (unscheduled.OperationStatus == null)
                    db.Entry(unscheduled).Reference(rmpo => rmpo.OperationStatus).Load(); //Explicitly fix-up the relationship
                if (unscheduled.Product == null)
                    db.Entry(unscheduled).Reference(rmpo => rmpo.Product).Load(); //Explicitly fix-up the relationship
            }
            #endregion

            if (unscheduledProductOperation.Product != null && !IsAssembly)
            {
                unscheduledProductOperation.Product.ProductStatusId = newProductStatus.Id;
                unscheduledProductOperation.Product.Progress = newProgress;

                db.Entry(unscheduledProductOperation.Product).State = EntityState.Modified;
            }

            return unscheduleds;

        }); // Добавление внеплановой операции в текущий маршрут изделия  

        #region Get_RouteMapProductOperations     			
        public List<RouteMapProductOperation> GetRouteMapProductOperationsByRevision(Revision revision)
        {
            return _db.RouteMapProductOperations.Where(routeMapProductOperation => routeMapProductOperation.Product.RevisionId == revision.Id).ToList();
        }

        public List<RouteMapProductOperation> GetSortedRouteMapProductOperations(int productId) => _db.RouteMapProductOperations
            .Where(routeMapProductOperation => routeMapProductOperation.ProductId == productId).OrderBy(rmpoView => rmpoView.Priority).ToList();
        public RouteMapProductOperation GetSingleProductOperation(int id) =>
            _db.RouteMapProductOperations.Single(productOperation => productOperation.Id == id);
        public RouteMapProductOperation GetSingleProductOperation(int id, int productId) =>
            _db.RouteMapProductOperations.Single(productOperation => productOperation.Id == id && productOperation.ProductId == productId);
        public RouteMapProductOperation GetSingleProductOperationWithIncludes(int id) =>
            _db.RouteMapProductOperations.Include("OperationStatus").Include("Product").Include("Operation").Include("OperationExecuter").Single(productOperation => productOperation.Id == id);
        public List<RouteMapProductOperation> GetRouteMapProductOperations(int productId) => _db.RouteMapProductOperations.Where(productOperations => productOperations.ProductId == productId).ToList();
        public RouteMapProductOperation GetEquipmentOrNull(int productId)
            => _db.RouteMapProductOperations.SingleOrDefault(productOperation
                => productOperation.ProductId == productId && productOperation.OperationId == CodeDictionary.Instance.AuxiliaryOperation.Equipment.Id);

        #region MultiProducts_methods        
        public IEnumerable<RouteMapProductOperation> GetGroupRouteMapProductOperations(IEnumerable<int> productIds, RouteMapProductOperation productOperation, bool withStartDate, bool withStopDate)
            => _db.RouteMapProductOperations.Where(routeMapProductOperation => productIds.Contains(routeMapProductOperation.ProductId)
                     && routeMapProductOperation.OperationId == productOperation.Operation.Id
                     && routeMapProductOperation.OperationStatusId == productOperation.OperationStatus.Id
                     && routeMapProductOperation.Priority == productOperation.Priority
                     && (!withStartDate || routeMapProductOperation.StartDate != null)
                     && (!withStopDate || routeMapProductOperation.EndDate != null)).ToList();
        //Получить группу одинакорвых операций для разных изделий {products} по выбарнной операции {rmpoView} с заданными параметрами

        public IEnumerable<RouteMapProductOperation> GetAllProductOperations(IEnumerable<int> productIds) => _db.RouteMapProductOperations
            .Where(routeMapProductOperation => productIds.Contains(routeMapProductOperation.ProductId)).ToList();
        //Получить все операций {RouteMapProductOperations} для разных изделий по их идентификатору {productIds}

        #endregion

        public async Task<IEnumerable<RouteMapProductOperation>> GetSortedRouteMapProductOperationsAsync(int productId) => await _db.RouteMapProductOperations
            .Where(routeMapProductOperation => routeMapProductOperation.ProductId == productId).OrderBy(rmpoView => rmpoView.Priority).ToListAsync();


        #endregion

        public bool ProductRouteMapIsComplete(int productId, int completeStatusId) => _db.Database.SqlQuery<bool>(HandleQuery.ProductRouteMapIsComplete(productId, completeStatusId)).ToList().First();

        #region UpdateProductOperationDescription_Methods
        public bool UpdateProductOperationDescription(RouteMapProductOperation productOperation) => _db.ExecuteWithTransactionAndSaveAndReturnBool((db) =>
        {
            if (productOperation != null)
            {
                UpdateProductOperation(productOperation, productOperation.Description);
                return true;
            }
            return false;
        });

        public void UpdateProductOperation(RouteMapProductOperation productOperation, OperationStatus newOperationStatus, float newProgress, ProductStatus newProductStatus, DateTime dateTime)
        {
            productOperation.StartDate = dateTime;
            productOperation.OperationStatusId = newOperationStatus.Id;
            productOperation.Product.ProductStatusId = newProductStatus.Id;
            productOperation.Product.CurrentOperationId = productOperation.OperationId;
            productOperation.Product.Progress = newProgress;

            _db.Entry(productOperation).State = EntityState.Modified;
        } //Update: StartDate, OperationStatus, Product.ProductStatus, Product.Operation, Product.Progress

        public void UpdateProductOperation(RouteMapProductOperation productOperation, OperationStatus newOperationStatus, ProductStatus newProductStatus, DateTime dateTime)
        {
            productOperation.StartDate = dateTime;
            productOperation.OperationStatusId = newOperationStatus.Id;
            productOperation.Product.ProductStatusId = newProductStatus.Id;
            productOperation.Product.CurrentOperationId = productOperation.OperationId;

            _db.Entry(productOperation).State = EntityState.Modified;
        } //Update: StartDate, OperationStatus, Product.ProductStatus, Product.Operation
        public void UpdateEndProductOperation(RouteMapProductOperation productOperation, OperationStatus newOperationStatus, float newProgress, ProductStatus newProductStatus, DateTime dateTime)
        {
            productOperation.EndDate = dateTime;
            productOperation.OperationStatusId = newOperationStatus.Id;
            productOperation.Product.ProductStatusId = newProductStatus.Id;
            productOperation.Product.CurrentOperationId = productOperation.OperationId;
            productOperation.Product.Progress = newProgress;

            _db.Entry(productOperation).State = EntityState.Modified;
        } //Update: EndDate, OperationStatus, Product.ProductStatus, Product.Operation, Product.Progress
        public void UpdateEndProductOperation(Product product, RouteMapProductOperation productOperation, OperationStatus newOperationStatus, float newProgress, ProductStatus newProductStatus, DateTime dateTime)
        {
            productOperation.EndDate = dateTime;
            productOperation.OperationStatusId = newOperationStatus.Id;
            product.ProductStatusId = newProductStatus.Id;
            product.CurrentOperationId = productOperation.OperationId;
            product.Progress = newProgress;

            //_db.Entry(product).State = EntityState.Modified;
           // _db.Entry(productOperation).State = EntityState.Modified; fix ex
        } //Update: EndDate, OperationStatus, Product.ProductStatus, Product.Operation, Product.Progress
        public void UpdateProductOperation(RouteMapProductOperation productOperation, OperationStatus newOperationStatus, float newProgress, ProductStatus newProductStatus)
        {
            productOperation.OperationStatusId = newOperationStatus.Id;
            productOperation.Product.ProductStatusId = newProductStatus.Id;
            productOperation.Product.CurrentOperationId = productOperation.OperationId;
            productOperation.Product.Progress = newProgress;

            _db.Entry(productOperation).State = EntityState.Modified;
        } //Update: OperationStatus, Product.ProductStatus, Product.Operation, Product.Progress
        public void UpdateProductOperation(RouteMapProductOperation productOperation, OperationStatus newOperationStatus)
        {
            productOperation.OperationStatusId = newOperationStatus.Id;
            //_db.Entry(productOperation).State = EntityState.Modified;
        } //Update: OperationStatus
        public void UpdateProductOperation(RouteMapProductOperation productOperation, string description)
        {
            productOperation.Description = description;
            _db.Entry(productOperation).State = EntityState.Modified;
        } //Update: Description

        public void UpdateStartProductOperation(RouteMapProductOperation productOperation, OperationStatus newOperationStatus, DateTime endDate)
        {
            productOperation.EndDate = endDate;
            productOperation.OperationStatusId = newOperationStatus.Id;
            _db.Entry(productOperation).State = EntityState.Modified;
        } //Update: EndDate, OperationStatus

        public void UpdateProductOperation(RouteMapProductOperation productOperation, OperationStatus newOperationStatus, DateTime startDate, DateTime endDate)
        {
            productOperation.StartDate = startDate;
            productOperation.EndDate = endDate;
            productOperation.OperationStatusId = newOperationStatus.Id;
            _db.Entry(productOperation).State = EntityState.Modified;
        } //Update: StartDate, EndDate, OperationStatus
        #endregion

        #region UpdateProductOperationDescription_forCollection_Methods
        public void UpdateProductOperation(IEnumerable<RouteMapProductOperation> productOperations, int? selectedOperationExecuterId, OperationStatus newOperationStatus, List<CalculatedProductInfo> calculatedInfos, int newProductStatusId, DateTime dateTime)
        {
            foreach (RouteMapProductOperation productOperation in productOperations)
            {
                productOperation.StartDate = dateTime;
                productOperation.OperationExecuterId = selectedOperationExecuterId;
                productOperation.OperationStatusId = newOperationStatus.Id;
                productOperation.Product.ProductStatusId = newProductStatusId;
                productOperation.Product.CurrentOperationId = productOperations.First().OperationId;
                productOperation.Product.Progress = calculatedInfos.Single(info => info.ProductId == productOperation.ProductId).NewProductProgress;

                _db.Entry(productOperation).State = EntityState.Modified;
            }
        } //Update: StartDate, OperationExecuterId, OperationStatus, Product.ProductStatus, Product.Operation Product.Progress; For collection
        public void UpdateEndProductOperation(IEnumerable<RouteMapProductOperation> productOperations, int? selectedGroupExecuterId, OperationStatus newOperationStatus, float newProgress, ProductStatus newProductStatus, DateTime dateTime)
        {
            foreach (RouteMapProductOperation productOperation in productOperations)
            {
                productOperation.EndDate = dateTime;
                productOperation.OperationExecuterId = selectedGroupExecuterId;
                productOperation.OperationStatusId = newOperationStatus.Id;
                productOperation.Product.ProductStatusId = newProductStatus.Id;
                productOperation.Product.CurrentOperationId = productOperations.First().OperationId;
                productOperation.Product.Progress = newProgress;

                _db.Entry(productOperation).State = EntityState.Modified;
            }
        } //Update: EndDate, OperationExecuterId, OperationStatus, Product.ProductStatus, Product.Operation, Product.Progress; For collection
        public RouteMapProductOperation UpdateEndProductOperation(RouteMapProductOperation productOperation, int? selectedGroupExecuterId, OperationStatus newOperationStatus, float newProgress, ProductStatus newProductStatus, DateTime dateTime)
        {
            productOperation.EndDate = dateTime;
            productOperation.OperationExecuterId = selectedGroupExecuterId;
            productOperation.OperationStatusId = newOperationStatus.Id;
            productOperation.Product.ProductStatusId = newProductStatus.Id;
            productOperation.Product.CurrentOperationId = productOperation.OperationId;
            productOperation.Product.Progress = newProgress;

            _db.Entry(productOperation).State = EntityState.Modified;
            return productOperation;
        } //Update: EndDate, OperationExecuterId, OperationStatus, Product.ProductStatus, Product.Operation, Product.Progress;
        public void UpdateProductOperation(IEnumerable<RouteMapProductOperation> productOperations, OperationStatus newOperationStatus, List<CalculatedProductInfo> afterCancelInfos, int newProductStatusId)
        {
            foreach (RouteMapProductOperation productOperation in productOperations)
            {
                productOperation.OperationStatusId = newOperationStatus.Id;
                productOperation.Product.ProductStatusId = newProductStatusId;
                productOperation.Product.CurrentOperationId = productOperations.First().OperationId;
                productOperation.Product.Progress = afterCancelInfos.Single(info => info.ProductId == productOperation.ProductId).NewProductProgress;

                _db.Entry(productOperation).State = EntityState.Modified;
            }
        } //Update: OperationStatus, Product.ProductStatus, Product.Operation, Product.Progress; For collection
        #endregion
    }
}
