﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class RouteMapQO : BaseQO
    {
        #region Get_RouteMaps
        public List<RouteMap> GetSortedRouteMaps() => _db.RouteMaps.OrderBy(routeMap => routeMap.IsArchive).ThenBy(routeMap => routeMap.Name).ToList();
        public async Task<List<RouteMap>> GetSortedRouteMapsAsync() => await _db.RouteMaps.OrderBy(routeMap => routeMap.Name).ToListAsync();

        public List<RouteMap> GetNotArchiveNClaimedRouteMaps(int revisionId) => _db.RouteMaps.Where(routeMap => routeMap.RevisionId == revisionId && !routeMap.IsArchive && routeMap.IsClaimed).ToList();
        public async Task<List<RouteMap>> GetNotArchiveRouteMapsAsync(int revisionId) => await _db.RouteMaps.Where(routeMap => routeMap.RevisionId == revisionId && !routeMap.IsArchive).ToListAsync();
        #endregion
        public int AmountOfUseRouteMap(RouteMap routeMap) => _db.Products.Where(product => product.RouteMapId == routeMap.Id).Count();
        public bool ArchiveRouteMap(RouteMap selectedRouteMap) => _db.ExecuteWithoutTransactionWithSaveAndReturnBool((db) =>
        {
            db.Entry(selectedRouteMap).State = EntityState.Modified;
            return true;
        }); //Архивация/разархивация маршрута
        public RouteMap SaveNewRouteMap(RouteMap newRouteMap, IEnumerable<RouteMapOperation> routeMapOperations) => _db.ExecuteWithTransactionAndReturnValue<RouteMap>(p =>
        {
            newRouteMap = _db.RouteMaps.Add(newRouteMap);
            routeMapOperations.Select(routeMapOperation => routeMapOperation.RouteMapId = newRouteMap.Id);
            _db.RouteMapOperations.AddRange(routeMapOperations);
            return newRouteMap;
        }); //Одной транзакцией добавляет RouteMap и список операций RouteMapOperations, привязанных к нему

        public bool DeleteRouteMap(RouteMap routeMap, IEnumerable<RouteMapOperation> routeMapOperation) => _db.ExecuteWithTransactionAndSaveAndReturnBool(db =>
        {
            db.RouteMapOperations.RemoveRange(routeMapOperation);
            db.RouteMaps.Remove(routeMap);
            return true;
        });

    }
}
