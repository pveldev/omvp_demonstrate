﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class RoleQO : BaseQO
    {
        public List<Role> GetRoles() => _db.Roles.ToList();
        public async Task<List<Role>> GetRolesAsync() => await _db.Roles.ToListAsync();
    }
}
