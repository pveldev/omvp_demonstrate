﻿using OMVP.DAL.CoreDAL;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.QOs
{
    public class ProductLogQO : BaseQO
    {
        public async Task<List<ProductLog>> GetProductLogsAsync(int count = 1000) => await _db.ProductLogs.Take(count).ToListAsync();
        public List<ProductLog> GetProductLogs(int count = 1000) => _db.ProductLogs.Take(count).ToList();
        public List<ProductLog> GetCurrentProductLogs(int productId) => _db.ProductLogs.Where(log => log.ProductId == productId).OrderByDescending(log => log.Date).ToList();
        public List<ProductLog> GetProductLogs(Revision revision, User user, DateTime startDate, DateTime stopDate, int count = 1000)
        {
            List<SqlParameter> parameters = FillSQLParameters(revision, user, startDate, stopDate);
            string query = PrepareQuery(parameters, count);

            return (parameters?.Any() ?? false) ? _db.ProductLogs.SqlQuery(query, parameters.ToArray()).ToList() : GetProductLogs(count);
        }

        public ProductLog RemoveProductLog(ProductLog log) => _db.ExecuteWithTransactionAndReturnValue<ProductLog>((db) =>
        {
            return db.ProductLogs.Remove(log);
        });

        private List<SqlParameter> FillSQLParameters(Revision revision, User user, DateTime startDate, DateTime stopDate)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>();

            if (revision != null)
                sqlParameters.Add(new SqlParameter() { ParameterName = HandleQuery.RevisioIdParam, SqlDbType = SqlDbType.Int, Value = revision.Id });

            if (user != null)        
                sqlParameters.Add(new SqlParameter() { ParameterName = HandleQuery.UserIdParam, SqlDbType = SqlDbType.Int, Value = user.Id });          

            //Параметры для дат всегда присутствуют
            sqlParameters.Add(new SqlParameter() { ParameterName = HandleQuery.StartDateParam, SqlDbType = SqlDbType.DateTime2, Value = startDate });
            sqlParameters.Add(new SqlParameter() { ParameterName = HandleQuery.EndDateParam, SqlDbType = SqlDbType.DateTime2, Value = stopDate });

            return sqlParameters;
        }

        private string PrepareQuery(List<SqlParameter> parameters, int count)
        {
            string queryJoins = "";
            string queryParameters = "";

            if (parameters.Any(param => param.ParameterName == HandleQuery.RevisioIdParam))
            {
                queryJoins += HandleQuery.JoinProductsForProductLog;
                queryParameters += CheckHelper.QueryParametersNullCheck(queryParameters) + HandleQuery.RevisionEquality;
            }

            if (parameters.Any(param => param.ParameterName == HandleQuery.UserIdParam))
            {
                queryJoins += HandleQuery.JoinUsersForProductLog;
                queryParameters += CheckHelper.QueryParametersNullCheck(queryParameters) + HandleQuery.PCEqualilty;
            }

            //Параметры дат всегда присутсивуют
            queryParameters += CheckHelper.QueryParametersNullCheck(queryParameters) + HandleQuery.StartDateInequality("pl");
            queryParameters += CheckHelper.QueryParametersNullCheck(queryParameters) + HandleQuery.StopDateInequality("pl");

            return HandleQuery.SelectFromProductLogs(count) + queryJoins + queryParameters + HandleQuery.OrderByDateDesc("pl");
        }        
    }
}