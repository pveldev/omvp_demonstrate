﻿using OMVP.Core;
using OMVP.DAL.CoreDAL;
using OMVP.DAL.Data;
using OMVP.DAL.DataViews;
using OMVP.DAL.Models;
using OMVP.Modal;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;

namespace OMVP.DAL.QOs
{
    public class ProductQO : BaseQO
    {
        #region Get_Products
        public List<Product> GetProductsForExcelExport(int revisionId, byte? month, int? year) => _db.Products
            .Where(product => product.RevisionId == revisionId 
            && (month == null || product.OrderElement.Month == month)
            && (year == null || product.OrderElement.OrderTree.Order.ExecutionDate.Year == year))
                .OrderBy(product => product.SerialNumber).ToList();

        public List<Product> GetProductsForExcelExport(int revisionId) => _db.Products
            .Where(product => product.RevisionId == revisionId)
                .OrderByDescending(product => product.Progress)
                    .ThenByDescending(product => product.ProductStatus.Code).ToList();

        public List<Product> GetProducts(int revisionId, int[] productStatusIds) => _db.Products
            .Where(product => product.RevisionId == revisionId && productStatusIds.Contains(product.ProductStatusId)).ToList();

        public List<Product> GetSortedByFavoriteAndNumberProducts(int revisionId, int productStatusId) => _db.Products
            .Where(product => product.RevisionId == revisionId && product.ProductStatusId == productStatusId)
                .OrderByDescending(product => product.IsFavorite)
                    .ThenByDescending(product => product.SerialNumber).ToList();

        public List<Product> GetDispatchedProducts(int revisionId, int dispatchedYear, int[] dispatchedStatusIds) => _db.Products
           .Where(product => product.RevisionId == revisionId
                                && dispatchedStatusIds.Contains(product.ProductStatusId)
                                && product.OrderElement.OrderTree.Order.ExecutionDate.Year == dispatchedYear).ToList();

        public List<Product> GetPseudoDispatchedProducts(int revisionId, int pseudoDispatchedStatusId) => _db.Products
           .Where(product => product.RevisionId == revisionId && product.ProductStatusId == pseudoDispatchedStatusId).ToList();

        public List<Product> GetSortedByFavoriteAndNumberProducts(int revisionId, int[] productStatusIds) => _db.Products
            .Where(product => product.RevisionId == revisionId && productStatusIds.Contains(product.ProductStatusId))
                .OrderByDescending(product => product.IsFavorite)
                    .ThenByDescending(product => product.SerialNumber).ToList();

        public List<Product> GetSortedByNumberProducts(int revisionId) => _db.Products.Where(product => product.RevisionId == revisionId).OrderBy(product => product.SerialNumber).ToList();

        public async Task<List<Product>> GetProductsAsync(int revisionId) => await _db.Products.Where(product => product.RevisionId == revisionId).ToListAsync();

        public List<Product> GetSortedProductsByStatusIds(int revisionId, int[] statusIds) => _db.Products.Where(product => product.RevisionId == revisionId && statusIds.Contains(product.ProductStatus.Id)).OrderBy(product => product.SerialNumber).ToList();
        public List<Product> GetProductsByStatusId(int revisionId, int statusId) => _db.Products.Where(product => product.RevisionId == revisionId && product.ProductStatus.Id == statusId).ToList();
        public List<Product> GetProductsByStatusCode(int revisionId, string statusCode) => _db.Products.Where(product => product.RevisionId == revisionId && product.ProductStatus.Code == statusCode).ToList();
        public List<Product> GetProductsByStatusCodeWithCurrentLetter(ProductBOM bom, int statusId)
            => _db.Products.Where(product => product.RevisionId == bom.RevisionBOM.BOMRevisionId && product.Letter == bom.Product.Letter && product.ProductStatusId == statusId).ToList();
        public Product GetProductById(int? productId) => _db.Products.Single(product => product.Id == productId);
        public List<Product> GetProductsChildren(int productId) => _db.Products.SqlQuery(HandleQuery.GetProductsChildren, new SqlParameter(@"productId", productId)).ToList();

        public List<Product> GetProductTree(string parentProductIds) => _db.Products.SqlQuery(HandleQuery.GetProductTree(parentProductIds)).ToList();
        public async Task<List<Product>> GetProductsBySerialNumberAsync(string serialNumber, int count = OmvpConstant.MaximumRowsByOneQuery) => await _db.Products.Where(product => product.SerialNumber == serialNumber).Take(count).ToListAsync();
        public async Task<List<Product>> GetProductsContainsSerialNumberAsync(string serialNumber, int count = OmvpConstant.MaximumRowsByOneQuery) => await _db.Products.Where(product => product.SerialNumber.Contains(serialNumber)).Take(count).ToListAsync();
        #endregion

        #region Other_get_methods       
        //Возвращает отсортированный список "готовностей" для изделий выбранной ревизии
        public List<ProductProgressAndLetter> GetLetterProgresses(int revisionId, int[] putToWorkStatusIds)
        {
            //Выборка по всем "запущенным" в работу изделиям.
            //"Запущенным" считается изделие не учтенное в количестве отгруженных изделий (в orderElement.CompletedQuantity и в order.CompletedQuantity)

            return _db.Products
                .Where(product => product.RevisionId == revisionId && putToWorkStatusIds.Contains(product.ProductStatusId))
                    .OrderByDescending(product => product.Progress)
                        .Select(product => new ProductProgressAndLetter()
                        {
                            Progress = product.Progress,
                            Letter = product.Letter
                        }).ToList();
        }
        public List<float> GetProgresses(int revisionId, int[] putToWorkStatusIds)
        {
            return _db.Products
                .Where(product => product.RevisionId == revisionId && putToWorkStatusIds.Contains(product.ProductStatusId))
                    .OrderByDescending(product => product.Progress)
                        .Select(product => product.Progress).ToList();
        }
        public List<CalculatedProductInfo> CalculateProgressAfterStartOperation(int[] selectedProductIds, int completeStatusId, int inProgressStatusId, int equipmentInProgressStatusId)
        {
            string oneLineIds = string.Join(",", selectedProductIds);
            return _db.Database.SqlQuery<CalculatedProductInfo>(HandleQuery.CalculateProgressAfterStartOperation(oneLineIds, completeStatusId, inProgressStatusId, equipmentInProgressStatusId, OmvpConstant.OperationStartWeight, OmvpConstant.StepWeight)).ToList();
        }
        public List<CalculatedProductInfo> CalculateProgressAfterReExecuteOrCancelOperation(int[] selectedProductIds, int completeStatusId, int inProgressStatusId, int equipmentInProgressStatusId)
        {
            string oneLineIds = string.Join(",", selectedProductIds);
            return _db.Database.SqlQuery<CalculatedProductInfo>(HandleQuery.CalculateProgressAfterReExecuteOrCancelOperation(oneLineIds, completeStatusId, inProgressStatusId, equipmentInProgressStatusId, OmvpConstant.OperationStartWeight, OmvpConstant.StepWeight)).ToList();
        }

        public ProductCounter GetProductCounter(int revisionId, int inWorkId, int readyToInstallId, int installedId, int readyToDispatchId, int reclamationId, int installedInReclamationId, int readyToReclamDispatchId, int readyToReclamInstallId)
        {
            return _db.Database.SqlQuery<ProductCounter>(HandleQuery.GetProductCount(inWorkId,
                                                                                        readyToInstallId,
                                                                                        installedId,
                                                                                        readyToDispatchId,
                                                                                        reclamationId,
                                                                                        installedInReclamationId,
                                                                                        readyToReclamDispatchId,
                                                                                        readyToReclamInstallId
                                                                                    ), new SqlParameter(@"revisionId", revisionId)).First();
        }
       
        public List<string> GetDuplicateSerialNumbers(List<string> serialNumbers, int revisionId) => 
            _db.Products.Where(product => product.RevisionId == revisionId)
                            .Select(product => product.SerialNumber)
                            .Intersect(serialNumbers)
                            .ToList();

        public bool SerialNumberIsDuplicateCheck(string serialNumber, int revisionId) =>
            _db.Products.Any(product => product.RevisionId == revisionId && product.SerialNumber == serialNumber);

        public List<Product> GetBuggedProducts() => _db.Products.SqlQuery(HandleQuery.GetBuggedProducts()).ToList();
        #endregion

        #region Add_Products
        public Task<List<Product>> AddRangeProducts(IEnumerable<Product> products, bool hasBoms) => _db.ExecuteWithTransactionAndReturnValueAsync<List<Product>>((db) =>
        {
            if (!products?.Any() ?? true) return null;
           
            products = db.Products.AddRange(products);

            if (products.Any(product => product.ProductStatus is null))
            {
                foreach (Product product in products.Where(product => product.ProductStatus is null))
                {
                    db.Entry(product).Reference(p => p.ProductStatus).Load(); //Explicitly fix-up the relationship
                }
            }

            if (products.Any(product => product.RouteMapId != null))           
                AddRouteMapProductOperationNoTransaction(db, products.Where(product => product.RouteMapId != null).ToList());
 
            if (hasBoms) // Если ревизия имеет слоты, то добавляю ProductBOM-пустышки
                AddProductBomNoTransaction(db, products.ToList());

            return products.ToList();
        });

        public Task<Product> AddNewProductAsync(Product product, bool hasBoms) => _db.ExecuteWithTransactionAndReturnValueAsync<Product>((db) =>
        {
            product = db.Products.Add(product);

            if (product is null)
                return null;

            if (product.RouteMapId != null) //Компоненты имеют пустой RouteMap
                AddRouteMapProductOperationNoTransaction(db, product);

            if (hasBoms) // Если ревизия имеет слоты, то добавляю ProductBOM-пустышки
                AddProductBomNoTransaction(db, product);

            return product;
        });

        private List<RouteMapProductOperation> AddRouteMapProductOperationNoTransaction(OmvpDataModel db, Product product)
        {
            List<RouteMapOperation> routeMapOperations = SQLServer.Instance.RouteMapOperation.GetRouteMapOperations(product.RouteMapId);
            List<RouteMapProductOperation> routeMapProductOperations = new List<RouteMapProductOperation>();

            routeMapProductOperations.AddRange(Shaper.FormNewProductOperation(routeMapOperations, product));

            return db.RouteMapProductOperations.AddRange(routeMapProductOperations).ToList();
        } //Добавляет пачку операций идзелию, согласно его маршруту. Внутрений метод, работающий без открытия новой транзакции.
        private void AddRouteMapProductOperationNoTransaction(OmvpDataModel db, List<Product> products)
        {      
            //products null-check outside this method
            Product firstProduct = products.First();

            bool allProductsHasOneRouteMapId = products.All(product => product.RouteMapId == firstProduct.RouteMapId);

            if (allProductsHasOneRouteMapId)
            {
                List<RouteMapOperation> routeMapOperations = SQLServer.Instance.RouteMapOperation.GetRouteMapOperations(firstProduct.RouteMapId);
                List<RouteMapProductOperation> routeMapProductOperations = new List<RouteMapProductOperation>();

                foreach (Product product in products)
                {
                    routeMapProductOperations.AddRange(Shaper.FormNewProductOperation(routeMapOperations, product));
                }

                db.RouteMapProductOperations.AddRange(routeMapProductOperations).ToList();

            }
            else //Если коллекция изделий имеет разные маршрутные карты, то для каждого изделия идет запрос к БД
            {
                List<RouteMapProductOperation> routeMapProductOperations = new List<RouteMapProductOperation>();

                foreach (Product product in products)
                {
                    List<RouteMapOperation> routeMapOperations = SQLServer.Instance.RouteMapOperation.GetRouteMapOperations(firstProduct.RouteMapId);
                    routeMapProductOperations.AddRange(Shaper.FormNewProductOperation(routeMapOperations, product));
                }

                db.RouteMapProductOperations.AddRange(routeMapProductOperations).ToList();
            }
            
        } //Добавляет пачку операций идзелию, согласно его маршруту. Внутрений метод, работающий без открытия новой транзакции.
        private void AddProductBomNoTransaction(OmvpDataModel db, Product product)
        {
            List<RevisionBOM> revisionBOMList = SQLServer.Instance.RevisionBom.GetRevisionBOMs(product.RevisionId);
            List<ProductBOM> productBOMList = new List<ProductBOM>();

            productBOMList.AddRange(revisionBOMList.Select(revisionBOM => Shaper.ProductBomFromRevision(product.Id, revisionBOM)));

            db.ProductBOMs.AddRange(productBOMList);
        } // Добавляет пустышки в слоты изделия  

        private void AddProductBomNoTransaction(OmvpDataModel db, List<Product> products)
        {
            if (products?.Any() ?? false)
            {
                List<RevisionBOM> revisionBoms = SQLServer.Instance.RevisionBom.GetRevisionBOMs(products.First().RevisionId);
                List<ProductBOM> productBoms = new List<ProductBOM>();

                foreach (Product product in products)
                {
                    productBoms.AddRange(revisionBoms.Select(revisionBOM => Shaper.ProductBomFromRevision(product, revisionBOM)));
                }

                db.ProductBOMs.AddRange(productBoms);
            }

        } // Добавляет пустышки в слоты изделия  
        #endregion

        #region Update_Product
        public Product UpdateProduct(Product product, int newProductStatusId, bool needABindingRouteMap, bool needBindingEmployees) => _db.ExecuteWithTransactionAndReturnValue<Product>((db) =>
        {
            if (product is null)
                return null;
           
            if (needABindingRouteMap) //true - привязка МК
            {
                AddRouteMapProductOperationNoTransaction(db, product);
                product.ProductStatusId = newProductStatusId;
                product.Progress = 0;
            }

            if (needBindingEmployees && !needABindingRouteMap) //Проверка необходимости привязать "сотрудников по умолчанию" к маршрутной карте
            {
                List<RouteMapProductOperation>  productOperations = SQLServer.Instance.RouteMapProductOperation.GetRouteMapProductOperations(product.Id);

                if (productOperations?.Any() ?? false)
                {
                    foreach (RouteMapProductOperation productOperation in productOperations)
                    {
                        productOperation.OperationExecuterId = Shaper.GetDefaultOperationExcecuterId(product, productOperation.Operation);
                        db.Entry(productOperation).State = EntityState.Modified;
                    }
                }
            }
            return product;
        });
        public Product EditProductStatus(Product product, ProductStatus newProductStatus) => _db.ExecuteWithTransactionAndReturnValue((db) =>
        {
            product.ProductStatusId = newProductStatus.Id;
            db.Entry(product).State = EntityState.Modified;

            return product;
        });

        //Метод с вытягиванием изделиея из БД на случай, если входной параметр взят из другого контекста
        public Product EditProductStatus(int productId, ProductStatus newProductStatus) => _db.ExecuteWithTransactionAndReturnValue((db) =>
        {
            Product product = db.Products.Single(prod => prod.Id == productId);
            product.ProductStatusId = newProductStatus.Id;
            db.Entry(product).State = EntityState.Modified;

            return product;
        });
        #endregion

        #region Reject_Product
        public bool RejectModule(Product module, int newProductStatusId, User user) => _db.ExecuteWithTransactionAndSaveAndReturnBool((db) =>
        {           
            if (module.ProductBOMs?.Any() ?? false)
            {
                List<Product> childrens = GetProductsChildren(module.Id);
                string detailedInformation = string.Join(", ", childrens.Select(product => product.Revision.Model.Name + " №" + product.SerialNumber));
                MessageBoxResult result = Dialog.ShowOKCancel(TextHelper.DoYouWantRejectModuleWithComponents(module.Revision.Model.Name, module.SerialNumber, detailedInformation));

                if (result == MessageBoxResult.Cancel)
                    return false;
                if (result == MessageBoxResult.OK)
                {
                    RejectModuleWithChildrens(module, newProductStatusId, childrens, user);
                    return true;
                }
            }
            else
            {
                RejectProductOnly(module, newProductStatusId, user);
                return true;
            }
            Dialog.ShowError(TextHelper.UnknownEror());
            return false;
        }); // Дает диалог на забракование модуля со всеми входящими узлами или забраковывает корпус без диалога
        public bool RejectBody(Product complexProduct, int newProductStatusId, User user) => _db.ExecuteWithTransactionAndSaveAndReturnBool((db) =>
        {
            if (complexProduct.ProductBOMs.Count != 0 && GetProductsChildren(complexProduct.Id).Count != 0)  
            {
                List<Product> childrens = GetProductsChildren(complexProduct.Id);
                string detailedInformation = string.Join(", ", childrens.Select(product => product.Revision.Model.Name + " №" + product.SerialNumber));
                Dialog.ShowError(TextHelper.CantRejectWithComponents(complexProduct.Revision.Model.Name, complexProduct.SerialNumber, detailedInformation));
                return false;
            }
            else
            {
                RejectProductOnly(complexProduct, newProductStatusId, user);
                return true;
            }
        }); // Забракование оснаски(с именным шилдиком) комплексированного изделия.
        private void RejectModuleWithChildrens(Product module, int newProductStatusId, List<Product> childrens, User user)
        {
            module.ProductStatusId = newProductStatusId;
            _db.ProductLogs.Add(Shaper.FormNewLog(TextHelper.Rejected(), module.Id, null, user));

            foreach (Product product in childrens)
            {
                product.ProductStatusId = newProductStatusId;
                _db.Entry(product).State = EntityState.Modified;
                _db.ProductLogs.Add(Shaper.FormNewLog(TextHelper.Rejected(), product.Id, null, user));
            }
            _db.Entry(module).State = EntityState.Modified;
        } //Внутренний метод без открытия транзакции
        private void RejectProductOnly(Product module, int newProductStatusId, User user)
        {
            module.ProductStatusId = newProductStatusId;
            _db.ProductLogs.Add(Shaper.FormNewLog(TextHelper.Rejected(), module.Id, null, user));
            _db.Entry(module).State = EntityState.Modified;
        } //Внутренний метод для забракования "корпуса" (="модуль или КИ", который не содержит установленных узлов или модулей соотв.)

        public bool UnrejectProduct(Product product, ProductStatus status, User user) => _db.ExecuteWithTransactionAndSaveAndReturnBool((db) =>
        {
             product.ProductStatusId = status.Id;
             db.Entry(product).State = EntityState.Modified;
             db.ProductLogs.Add(Shaper.FormNewLog(TextHelper.Unrejected, product.Id, null, user));

             return true;
         });
        #endregion

        #region Dispatch_Product
        public Task<bool> PseudoDispatchProductsAsync(List<Product> productTrees, ProductStatus newProductStatus, List<ProductLog> logs) => _db.ExecuteWithTransactionAndSaveAndReturnBoolAsync((db) =>
        {
            foreach (Product product in productTrees)
            {
                product.ProductStatusId = newProductStatus.Id;
                db.Entry(product).State = EntityState.Modified;
            }

            db.ProductLogs.AddRange(logs);

            return true;
        });

        public Task<bool> DispatchProductsAsync(List<Product> productTrees, List<OrderElement> orderElementsTree, ProductStatus newProductStatus, List<ProductLog> logs) => _db.ExecuteWithTransactionAndSaveAndReturnBoolAsync((db) =>
        {           
            foreach (Product product in productTrees)
            {
                OrderElement currentOrderElement = orderElementsTree.Single(orderElement => orderElement.OrderTree.RevisionId == product.RevisionId);
                product.OrderElementId = currentOrderElement.Id;
                product.ProductStatusId = newProductStatus.Id;
                currentOrderElement.CompletedQuantity++; // Инкрементим в месячный план
                currentOrderElement.OrderTree.DispatchedQuantity++; // Инкрементим в годовой план

                if (currentOrderElement.OrderTree.Order.Quantity == currentOrderElement.OrderTree.DispatchedQuantity)
                {
                    currentOrderElement.OrderTree.Order.OrderStatusId = CodeDictionary.Instance.AuxiliaryOrderStatus.Completed.Id;
                    db.Entry(currentOrderElement.OrderTree.Order).State = EntityState.Modified;
                }

                db.Entry(currentOrderElement).State = EntityState.Modified;
                db.Entry(product).State = EntityState.Modified;
            }

            db.ProductLogs.AddRange(logs);

            return true;   
        });

        public bool DispatchProducts(List<Product> productTrees, List<OrderElement> orderElementsTree, ProductStatus newProductStatus, List<ProductLog> logs) => _db.ExecuteWithTransactionAndSaveAndReturnBool((db) =>
        {
            foreach (Product product in productTrees)
            {
                OrderElement currentOrderElement = orderElementsTree.Single(orderElement => orderElement.OrderTree.RevisionId == product.RevisionId);
                product.OrderElementId = currentOrderElement.Id;
                product.ProductStatusId = newProductStatus.Id;
                currentOrderElement.CompletedQuantity++; // Инкрементим в месячный план
                currentOrderElement.OrderTree.DispatchedQuantity++; // Инкрементим в годовой план

                db.Entry(currentOrderElement).State = EntityState.Modified;
                db.Entry(product).State = EntityState.Modified;
            }

            db.ProductLogs.AddRange(logs);

            return true;
        });

        public Task<bool> PseudoDispatchProductsAsync(List<Product> productTrees, int newProductStatusId, List<ProductLog> logs) => _db.ExecuteWithTransactionAndSaveAndReturnBoolAsync((db) =>
        {
            foreach (Product product in productTrees)
            {
                product.ProductStatusId = newProductStatusId;
                db.Entry(product).State = EntityState.Modified;
            }

            db.ProductLogs.AddRange(logs);

            return true;
        });
        #endregion

        public bool ReclamationProductsWithoutOrderElement(Product parent, List<Product> productsTree, int newChildStatusId, int newParentStatusId, ProductLog log) => _db.ExecuteWithTransactionAndSaveAndReturnBool((db) =>
        {
            foreach (Product product in productsTree)
            {
                product.ProductStatusId = newChildStatusId;
                db.Entry(product).State = EntityState.Modified;
            }

            parent.ProductStatusId = newParentStatusId;
            db.Entry(parent).State = EntityState.Modified;

            db.ProductLogs.Add(log);

            return true;
        });

        public bool ReclamationProduct(Product selectedProduct, int newParenProductStatusId, int newChildrenProductStatusId, List<Product> productsTree, List<OrderElement> orderElementsTree, ProductLog log = null) => _db.ExecuteWithTransactionAndSaveAndReturnBool((db) =>
        {
            if (productsTree.Count == orderElementsTree.Count)
            {
                foreach (Product product in productsTree)
                {
                    product.ProductStatusId = newChildrenProductStatusId; //Все изделия из дерева - "установлены в рекламационное"         
                    product.OrderElement.CompletedQuantity--; // Дикремент месячного плана
                    product.OrderElement.OrderTree.DispatchedQuantity--; // Дикремент годового плана
                    product.OrderElementId = null; // Отвязывание изделия от месяца отгрузки
                    product.OrderElement.OrderTree.Order.OrderStatusId = CodeDictionary.Instance.AuxiliaryOrderStatus.ReclamationWork.Id; //Заказ в рекламационную-работу                   

                    db.Entry(product).State = EntityState.Modified;
                }
                selectedProduct.ProductStatusId = newParenProductStatusId; // Изделие высшего уровня - "рекламация" / среднего-нижнего - "установлены в рекламационное"

                if (log != null)
                    db.ProductLogs.Add(log);

                return true;
            }
            string modelNamesTree = TextHelper.ListToString(productsTree.Select(p => p.Revision.Model.Name), ",");
            Dialog.ShowError(TextHelper.ProductAndOrderElementTreeEqualityCountError(modelNamesTree, productsTree.Count, orderElementsTree.Count));
            return false;
        });
        public Product EndInstallationWithoutTransaction(Product product, ProductStatus newProductStatus, float newProgress, ProductLog log = null)
        {
            product.ProductStatusId = newProductStatus.Id;
            product.CurrentOperationId = CodeDictionary.Instance.AuxiliaryOperation.Equipment.Id;
            product.Progress = newProgress;

            _db.Entry(product).State = EntityState.Modified;

            if (log != null && log.ProductOperation != null)
                _db.ProductLogs.Add(log);

            return product;
        }
        public Product AddProductToFavorite(Product product, bool isFavorite) => _db.ExecuteWithoutTransactionAndReturnValue((db) =>
        {
            product.IsFavorite = isFavorite;
            db.Entry(product).State = EntityState.Modified;
            return product;
        });


        #region Remove_Products
        public Product RemoveProduct(Product product) => _db.ExecuteWithTransactionAndReturnValue<Product>((db) =>
        {
            db.RouteMapProductOperations.RemoveRange(SQLServer.Instance.RouteMapProductOperation.GetRouteMapProductOperations(product.Id));
            db.ProductBOMs.RemoveRange(SQLServer.Instance.ProductBom.GetProductBOMs(product.Id));

            product = db.Products.Remove(product);
            return product;
        }); //Метод каскадного удаления для новых изделий (без истории, без заполненых слотов, без изделий-родителей)
        #endregion

        #region Push_Product
        public bool PushProductUntilReady(Product product, int newProductStatusId ,List<RouteMapProductOperation> productOperations, bool installactionIsCompleted) => _db.ExecuteWithTransactionAndSaveAndReturnBool(db =>
        {
            foreach (RouteMapProductOperation item in productOperations)
            {
                db.RouteMapProductOperations.Remove(db.RouteMapProductOperations.First(productOperation => productOperation.Id == item.Id));
            } //Удаляем маршрут

            product.ProductStatusId = newProductStatusId;
            product.RouteMapId = null;
            product.RouteMap = null;
            product.CurrentOperationId = productOperations.Last().OperationId;

            db.Entry(product).State = EntityState.Modified;

            return true;
        });

        public bool PushProductOperationsUntilReady(Product product, int newProductStatusId, List<RouteMapProductOperation> productOperations) => _db.ExecuteWithTransactionAndSaveAndReturnBool(db =>
        {
            RouteMapProductOperation equipment = productOperations.FirstOrDefault(productOperation => productOperation.OperationId == CodeDictionary.Instance.AuxiliaryOperation.Equipment.Id);
            if (equipment != null && equipment.OperationStatusId != CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id)
            {
                Dialog.ShowWarning(TextHelper.EquipmentNotDone());
                return false;
            } // Если "комплектация" завершена, то продолжаем. Если нет - выходим.

            foreach (RouteMapProductOperation item in productOperations)
            {
                if (item.OperationStatusId == CodeDictionary.Instance.AuxiliaryOperationStatus.InProgress.Id)
                {
                    SQLServer.Instance.RouteMapProductOperation.UpdateStartProductOperation(item, CodeDictionary.Instance.AuxiliaryOperationStatus.Completed, DateTime.Now);

                    item.OperationStatusId = CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id;
                    item.EndDate = DateTime.Now;
                }
                else if (item.OperationStatusId == CodeDictionary.Instance.AuxiliaryOperationStatus.ReadyToStart.Id || item.OperationStatusId == CodeDictionary.Instance.AuxiliaryOperationStatus.NotDone.Id)
                {
                    SQLServer.Instance.RouteMapProductOperation.UpdateProductOperation(item, CodeDictionary.Instance.AuxiliaryOperationStatus.Completed, DateTime.Now, DateTime.Now);

                    item.OperationStatusId = CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id;
                    item.StartDate = DateTime.Now;
                    item.EndDate = DateTime.Now;
                }
                else if (item.OperationStatusId != CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id)
                {
                    //Этот блок кода должен быть недостижимым с текущим набором статусов операций
                    Dialog.ShowWarning(TextHelper.OperationCantBeProcessing(item.Operation.Name, item.OperationStatus.Name));
                    return false;
                }
            }

            RouteMapProductOperation notCompletedProductOperation = productOperations.FirstOrDefault(productOperation => productOperation.OperationStatusId != CodeDictionary.Instance.AuxiliaryOperationStatus.Completed.Id);
            if (notCompletedProductOperation != null)
            {
                Dialog.ShowWarning(TextHelper.OperationStatusNotUpdated(notCompletedProductOperation.Operation.Name, notCompletedProductOperation.OperationStatus.Code));
                return false;
            }

            product.ProductStatusId = newProductStatusId;
            product.CurrentOperationId = productOperations.Last().OperationId;

            db.Entry(product).State = EntityState.Modified;
            return true;
        });          
        #endregion
    }
}
