﻿using OMVP.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.DataViews
{
    public class ProductProgressAndLetter : BasicNotifyModel
    {
        public float Progress { get; set; }
        public string Letter { get; set; }
    }
}
