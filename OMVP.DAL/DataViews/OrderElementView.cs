﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.DataViews
{
    //Представление OrderElement для OrderElementGroupView.xaml
    public class OrderElementView
    {
        public byte Month { get; set; }
        public int Quantity { get; set; }
        public int CompletedQuantity { get; set; }
        public int ProjectedCompletedQuantity { get; set; } //Прогнозируемое количество с учетом текущих изделий в работе
        public double CurrentProgress { get; set; }
        public double Progress { get; set; } // Рассчитывается через усредненный прогресс изделий (в количестве = Quantity - CompletedQuantity) в работе, учитывая проргесс уже учтенных (CompletedQuantity)
        public string OrderLetter { get; set; }
        public string Description { get; set; }
    }
}
