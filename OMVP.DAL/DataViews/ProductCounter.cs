﻿using OMVP.Core;
using OMVP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.DataViews
{
    public class ProductCounter : BasicNotifyModel
    {
        public int InWorkAtTheExitCount { get; set; } //.Status.InWork AND Progress >= 90%
        public int InWorkCount { get; set; } //.Status.InWork AND Progress < 90%
        public int ReadyCount { get; set; } //.Status.ReadyToInstall +.Status.Installed +.Status.ReadyToDispatch
        public int ReclamationWorkCount { get; set; } //.Status.Reclamation
        public int InstalledInReclamationCount { get; set; } //.Status.InstalledInReclamation
        public int ReclamationReadyCount { get; set; } //.Status.ReadyToReclamationDispatch +.Status.ReadyToReclamationInstall
    }
}
