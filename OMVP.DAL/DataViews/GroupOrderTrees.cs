﻿using OMVP.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.DAL.DataViews
{
    public class GroupOrderTrees : BasicNotifyModel
    {
        public string RevisionName { get; set; }
        public int DispatchedQuantity { get; set; }
        public int GroupQuantity { get; set; }
        public DateTime Year { get; set; }
        public string DetailedInformation { get; set; }
    }
}
