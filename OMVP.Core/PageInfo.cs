﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace OMVP.Core
{
    public class PageInfo
    {
        public UserControl View { get; set; }
        public string PageTitle { get; set; }
        public BaseWindowHelper HelperWindow { get; set; }
    }
}
