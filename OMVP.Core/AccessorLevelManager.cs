﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.Core
{
    public class AccessorLevelManager
    {
        #region Init_maganer
        private static readonly Dictionary<string, int> _accessorLevels;
        static AccessorLevelManager()
        {
            _accessorLevels = new Dictionary<string, int>();
            RegisterAccessorLevels();
        }

        private static void RegisterAccessorLevels()
        {
            //3
            _accessorLevels.Add("CanArchiveRouteMap", 3); //Уровень доступа для архивации маршрутной карты

            //4
            _accessorLevels.Add("CanClaimSlots", 4); //Уровень доступа для фиксации посадочных мест ревизии
            _accessorLevels.Add("CanClaimOrders", 4); //Уровень доступа для фиксации годового заказа
            _accessorLevels.Add("CanDeleteRouteMap", 4); //Уровень доступа для удалиния маршрутной карты          
        }
        #endregion

        #region Interface_methods
        public static bool AccessorLevelCheck(string key, int currentLevel) => GetAccessorLevel(key) <= currentLevel;
        
        public static int GetAccessorLevel(string key)
        {
            if (!_accessorLevels.ContainsKey(key))
                throw new ArgumentException(TextHelper.NullPageCodeThrow(key));

            return _accessorLevels[key];
        }
        #endregion 
    }
}
