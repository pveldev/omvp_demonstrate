﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OMVP.Core
{
    public abstract class BaseWindowHelper : Window
    {
        public abstract void ShowHelper();
    }
}
