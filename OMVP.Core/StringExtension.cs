﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.Core
{
	public static class StringExtension
	{
		public static bool ContainsAsLower(this string input, string value) => input.ToLower().Contains(value);
	}
}
