﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.Core
{
    public class TextHelper
    {
        #region ProductLogActions
        //15 символов в БД на поле OperaionAction
        public static string StartOperation() => "Начата"; //execute
        public static string EndOperation() => "Завершена";
        public static string CancelOperation() => "Отменена";
        public static string ReturnOperation() => "Начата повторно"; //re-execute
        public static string RepeatEquipment = "Повторно"; //"Проведена повторно"
        public static string Rejected() => "Забракован";
        public const string Unrejected = "Разбракован";
        public const string Dispatch = "Отгрузка";
        public const string PseudoDispatch = "Псевдо отгрузка";
        public const string Reclamation = "Рекламация";
        #endregion

        #region AnnotationMessages
        public static string ProductMovedToAnotherTab(string serialNumber, string newTabName) => $"Изделие с номером {serialNumber} перемещено во вкладку \"{newTabName}\"";
        public static string ProductStatusSuccessfullyChanged(string serialNumber, string productStatus) => $"Статус изделия с номером {serialNumber} успешно изменен на {productStatus}";
        public const string OrderElementsChangesWasSaved = "Изменения успешно сохранены!";
        public static string InitialCompletedMessage() => "Инициализация БД произведена успешно";
        public static string DbCleaningCompletedMessage() => "Очистка БД произведена успешно";
        public static string ProductsDispatchSuccessfulMessage(string serialNumbers) => $"Группа изделий отгружена!\nНомера отгруженных изделий: {serialNumbers}";
        public static string ProductsPseudoDispatchSuccessfulMessage(string serialNumbers, string status) => $"Группа изделий перемещена во вкладку отгруженых со статусом [{status}]!\nНомера отгруженных изделий: {serialNumbers}";
        public const string ProductBomsIsFine = "Целостность данных не нарушена";
        #endregion

        #region RequestResponceMessages
        //Requests
        public static string DoYouWantToDoIt(string it) => $"Вы дейстивтельно хотите {it} выбранный элемент?"; //generic
        public static string DoYouWantInitialDbMessage() => "Вы дейстивтельно хотите добавить данные в системные таблицы?";
        public static string DoYouWantDropSystemDbDataMessage() => "Вы дейстивтельно хотите удалить данные системных таблиц?";
        public static string DoYouWantDeleteRowMessage() => "Вы действительно хотите безвозвратно удалить выбранную запись?";
        public static string DoYouWantDeleteProductMessage(string modelName, string productSerialNumber) => $"Вы действительно хотите безвозвратно удалить изделие {modelName} [{productSerialNumber}]?";
        public static string DoYouWantPushWithoutRouteMessage(string modelName, string productSerialNumber) => $"Выбранное изделие: {modelName} №[{productSerialNumber}] будет доведено до статуса \"готов\"\nУдалить все записи по маршрутной карте из базы данных?";
        public static string DoYouWantAddProductWithoutRouteMapMessage() => "Вы уверены, что хотите добавить изделие(я) без маршрутной карты?";
        public static string DoYouWantRejectModuleWithComponents(string moduleName, string moduleSerialNumber, string detailedInformation) => $"Вы действительно хотите забраковать весь модуль\n[{moduleName} №{moduleSerialNumber}]\nсо всеми установленными узлами и компонентами:\n[{detailedInformation}] ?";
        public static string DoYouWantToExcludeEmployeeFromGroup(string firstName, string lastName, string groupName) => $"Вы действительно хотите исключить [{firstName} {lastName}] из [{groupName}]?";
        public static string DoYouWantToClaimSelectedRevision(string revisionName, string slots) => $"Вы действительно хотите зафиксировать посадочные места:\n[{slots}] для ревизии [{revisionName}]?\nПосле фиксации редактирование списка посадочных мест будет невозможно.";
        public static string DoYouWantToEditProductStatusToCompleted(string modelName, string serialNumber) => $"Изделие [{modelName}:{serialNumber}] имеет завершненный маршрут. Перенести изделие в список готовых?\n(статус изделия изменится на \"Готов к установке\"(для модулей) или \"Готов к отгрузке\"(для КИ))\nНажмите \"Да\" для изменения статуса изделия или нажмите \"Нет\" для продолжения работы (повторное выполнение операций или добавление внеплановых операций) с данным маршуртом";
        public static string DoYouWantToAddParamToModel(string modelName, string paramName) => $"Модель [{modelName}] уже имеет параметр с типом [{paramName}].\nВы уверены, что хотите добавить еще один параметр с таким же типом?";
        public const string DoYouWantToDeleteParameter = "Вы действительно хотите удалить выбарнный параметр?";
        public const string DoYouWantToPseudoDispatch = "Псевдо отгрузка используется для изделий среднего и нижнего уровня, у которых не заполняются графики в данный программе\n.Вы действительно хотите отгрузить выбранные изделия?";

        //Responces
        public static string NetConnectionError(string message) => $"Отсутствует соединение с сервером!\n {message}";
        public static string OmvpVersionError(string message) => $"Скорее всего вы пользуетесь устаревшей версий программного обеспечения OMVP!\n {message}\n";
        public const string SelectedProductHaventLog = "Не найдено ни одной строки истории";
        #endregion

        #region ErrorsAndWarnings
        public const string InvalidCastErrorMessage = "Произошла неизвестная ошибка преобразования типа";
        public const string MissProductStatusInitialization = "Не удалось найти все необходимые данные (статусы изделий) для корректной работы приложения. Необходима инициализация статусов.";
        public static string BaseFailConnectionMessage() => "База не выбрана!";
        public static string CantWorkWithOperationMessage(string operationName) => $"Невозможно начать/завершить/отменить операцию [{operationName}] для группы изделий. Вернитесь на предыдущую страницу и выберите другую операцию.";
        public static string EqualityProductsAndOperationsMessage(int productCount, int operationCount) => $"Количество выбранных изделий не соответствует[{productCount}] вычисленному количеству операций[{operationCount}]";
        public static string OrderElementQuantityDestributeMessage(int orderQiantity, int orderElementSumQuantity) => $"Необходимо распределить {orderQiantity} изделий по месяцам. Попытка распределить {orderElementSumQuantity} безуспешна. Изменения не сохранены!";
        public static string WrongLoginOrPasswordMessage() => "Ошибка в сочетании логин/пароль";
        public static string ConnectionCheckMessage() => "Проверка подключения. Подождите!";
        public static string CantStartOperationMessage() => "Невозможно начать текущую операцию!\nВозможно не все \"посадочные места\" заполенны!";
        public const string CantDispatchProductsMessage = "Несоответствие количества изделий, необходимых для отгрузки!\nПроверьте заполнение \"посадочных мест\"!";
        public static string CantAddUnsheduledOperationMessage() => "Нельзя добавить новую операцию после выбранной";
        public static string SaveErrorDescriptionMessage() => "Произошла ошибка базы данных! Комментарий не сохранен!";
        public static string InformationSaveErrorMessage() => "Информация не сохранена!";
        public static string WrongParameters() => "Действие не выполнено, ибо внутренние проверки обнаружели ошибку при проверке входных параметров";
        public static string UnknownEror() => "Произошла неизвестная ошибка!"; //Использовать для недостежимого кода
        public static string CantRejectWithComponents(string complexProductName, string complexProductNumber, string detailedInformation) => $"Нельзя забраковать оснаску КИ:\n[{complexProductName} №{complexProductNumber}]\nс установленными модулями:\n[{detailedInformation}]!\n\nТребуется снять модули перед забракованием!";
        public static string ProductAndOrderElementTreeEqualityCountError(string modelNamesTree, int productsCount, int orderElementsTreeCount) => $"Ошибка при сопоставлении дерева изделий с деревом отгрузки (по количеству)! Убедитесь, что все посадочные места заполнены!\n Количество изделий: [{productsCount}], необходимо в заказе [{orderElementsTreeCount}]\nПеречень дерева моделей для отгрузки:[{modelNamesTree}]";
        public static string EquipmentNotDone() => "Изделие не было \"проброшено\" до готовности, потому что операция \"комплектация\" не завершена!";
        public static string OperationCantBeProcessing(string operationName, string statusName) => $"Работа прервана! Операция {operationName} со статусом {statusName} не может быть обработана!";
        public static string OperationStatusNotUpdated(string operationName, string operationStatus) => $"Работа прервана! Причина: Операция [{operationName}] не обновила свой статус (текущий статус: {operationStatus})";
        public static string ActionNotFound = "Выбранное действие не найдено в списке перечисления!";
        public static string ModelXWorkingGroupIsDuplicate = "Невозможно связать выбранную модель с выбранной рабочей группой, т.к. такая связь уже существует!";
        public static string FailAddingRouteMap = "Произошла ошибка! Новый маршрут не был сохранен в базе данных!";
        public static string FailDeletingRouteMap = "Произошла ошибка при удалении выбранного маршрута! Маршрут не был удален!";
        public static string FailRemovingEmployeeFromGroup = "Не удалось исключить выбранного сотрудника из выбранной рабочей группы!";
        public static string FailRemovingModelXWorkingGroupRelation = "Не удалось удалить выбранную связь модели и рабочей группы!";
        public static string CantDeleteSelectedRouteMap = "Нельзя удалять маршрут, который использовался (привязывался к изделиям) ранее. Используйте функцию \"архивировать\" для того, что бы исключить данный маршрут из списка выбора";
        public static string ArchiveSelectedRouteMapError = "Произошла ошибка при попытке архивировать выбранную маршрутную карту!";
        public static string ArchiveSelectedOperationError = "Произошла ошибка при попытке архивировать выбранную операцию!";
        public static string ArchiveSelectedRevisionError = "Произошла ошибка при попытке архивировать выбранную ревизию!";
        public static string DeleteSelectedRevisionError = "Произошла ошибка при попытке удалить выбранную ревизию!";
        public static string SelectedOperationIsNull = "Выберите операцию, которую необходимо архивировать!";
        public static string CollectionIsNullError = "Ошибка при обработке коллекции!";
        public static string CantAddEmployeeToSelectedGroup(string firstName, string lastName) => $"Сотрудник [{firstName} {lastName}] уже состоит в выбранной группе!";
        public static string CantAddSlotToSelectedRevision(string slotName) => $"Посадочное место с именем [{slotName}] уже имеется в списке! Пожалуйста введите другое имя!";
        public static string EmployeeArchivingFailCozInvalidCast = "Не удалось архивировать выбранного сотрудника из-за ошибки преобразования типа данных!";
        public static string WorkingGroupArchivingFailCozInvalidCast = "Не удалось архивировать выбранную рабочую группу из-за ошибки преобразования типа данных!";
        public static string NumberIsADuplicate(string serialNumber) => $"Изделие с номером [{serialNumber}] не было добавлено, поскольку является дубликатом!";
        public static string EmployeeIsADuplicate(string firstName, string lastName) => $"Сотрудник [{firstName} {lastName}] не был(-а) добавлен(-а), поскольку является дубликатом!";
        public static string NumbersIsADuplicate(string serialNumbers) => $"Изделия с номерами [{serialNumbers}] не были добавлены, поскольку являются дубликатами!";
        public static string OperationIsADuplicate(string operationName) => $"Операция [{operationName}] уже присутствует в маршрутной карте!\nИспользуйте \"Повторное выполнение\" операции в данной ситуации!";
        public static string SlotsIsNotEmpty => "*Изделие не может быть удалено, т.к.его его посадочные места не пустые!";
        public static string ProductBindedToOrderAndCantbeDeleted(string orderCode) => $"*Изделие не может быть удалено, т.к. оно привязано к заказу {orderCode}";
        public static string ProductBindedToSlotAndCantbeDeleted(string revisionName, string serialNumber) => $"*Изделие не может быть удално, т.к. оно установлено в {revisionName} [№{serialNumber}]";
        public static string ProductHaveStoryAndCantbeDeleted(int storyCounter) => $"*Нельзя удалить изделие с \"историей\"! Найдено [{storyCounter}] записей";
        public static string ProductHaveBomStoryAndCantbeDeleted(int parentStoryCounter, int childStoryCounter) => $"*Нельзя удалить изделие с \"историей уставновки в посадочное место\"! " +
                                                                                                                    $"Найдено [{parentStoryCounter}] упоминан(-ий/-ия) изделия в качестве \"родителя\" (с посадочными местами)" +
                                                                                                                    $"и [{childStoryCounter}] упоминан(-ий/-ия) изделия в качестве \"наследника\" (установлен в посадочные места)";
        public static string SQLServerLoginFailed(string exMessage) => $"\nПроблемы с авторизацией на сервере. Необходимо оповестить службу ИТ \"19-75\" о необхдимости перезагрузки сервера \"Сервер3.НПК-2\" \n\nИсходный текст ошибки:({exMessage})";
        public const string IntegersMustBeAboveZero = "Вводимые значения должны быть больше ноля!";
        public const string OrderElementQuantityValueError = "Вводимые значения не могут быть меньше чем количество ГОТОВЫХ на данный момент!";
        public const string ClearSelectedOperationExecuterError = "При очистке выбранного исполнителя произошла ошибка!";
        public const string ClaimRevisionDbTransactionExCatched = "Ошибка при подтверждении выбранной посадочных мест выбранной ревизии!";
        public static string CantAddProductWhileRevisionIstClaimed(string revisionName, int accessorLevel) => $"Необходимо зафиксировать посадочные места ревизии [{revisionName}] перед добавлением изделий.\nЭто необходимо для гарантии того, что количество посадочных мест не изменится после добавления изделия.\nФиксацию может осуществить любой сотрудник с уровнем доступа [{accessorLevel}]";
        public static string CantAddOrderWhileRevisionIstClaimed(string revisionName) => $"Зафиксируйте ревизию [{revisionName}] перед формированием заказа. Это необходимо для гарантии того, что посадочные места не изменятся после добавления заказа.";
        public static string CantAddSlotWhileRevisionIsClaimed(string revisionName, string modelName) => $"Невозможно добавить посадочные места к зафиксированной резвизии [{revisionName}].\nЕсли вам необходима модель [{modelName}] с другим набором посадочных мест, то необходимо добавить новую ревизию в рамках этой модели.";
        public const string CantAddRevisionAsSlot = "Невозможно добавить ревизию в качестве слота к самой себе!";
        public const string CantOpenBomsPageWarningMessage = "Невозможно открыть страницу для комплектования, пока изделие не дойдет до этой операции по маршрутной карте!";
        public static string CreateNewRouteMapWarningMessage(string revisionName, string bomNames) => $"Т.к. ревизия [{revisionName}] имеет составляющие:\n{bomNames}\nнеобходимо добавить в маршрут операцию \"комплектация\".";
        public static string OrderElementQuantityValidationErrorMessage(int quantity) => $"Количество для отгрузки [{quantity}] не может иметь отрицаительное значение! ";
        public static string SerialNumberParseException(string serialNumber) => $"Произошла ошибка при попытке преобразовать введенное значение [{serialNumber}] в серийный номер. Убедитесь, что в поле для ввода введены только числовые значения.";
        public const string RouteMapIstCompleteWarningMessage = "Необходимо завершить все операции в маршрутной карте перед переносом изедлия в список готовых!";
        public static string DispatchCountErrorMessage(string neededCount, string selectedCount) => $"Необходимо отгрузить [{neededCount}] издел-ий/ия).\nПопытка отгрузить [{selectedCount}] изделий была отклонена!";
        public static string DispatchLetterErrorMessage(string selectedLetter) => $"Все выбранные изделия должны соответствовать выбранному элементу заказа по литеру.\nВыбранный элемент заказа имеет литер: [{selectedLetter}]";
        public static string BuggedProductsBomsWarningMessage(string oneLineProducts) => $"Перень изделий, которые оказались установлены в несколько посадочных мест одновременно:\n[{oneLineProducts}]";
        public const string RefreshDataErrorMessage = "Произошла ошибка при обновлении данных на этой старнице";
        public const string ParamIsDuplicateWarningMessage = "Добавление невозмонжо!\nВыбранный параметр является дубликатом!";
        public static string CannotClaimOrderWarningMessage(string userNames) => $"Список пользователей, имеющих уровень доступа, необходимый для фиксации заказа:\n{userNames}";
        public static string OrderIsNotClaimedWarningMessage(string userNames) => $"Выбранный заказ не зафиксирован!\nНеобходимо перейти на страницу \"[Годовой заказ]\", выбрать элемент заказа и нажать кнопку \"[Зафиксирвать]\"\nСписок пользователей, имеющих необходимый уровень доступа:\n{userNames}";
        #endregion

        #region ViewTitles
        public static string RevisionNavigationTitle() => "Список тем";
        public static string CurrentModelPageTitle(string modelTypeName, string revisionName) => $"Составляющие выбранной модели: [{modelTypeName} {revisionName}]";
        public static string ProductBomTitle(string revisionName, string productSerialNumber) => $"Посадочные места изделия: {revisionName} №[{productSerialNumber}]";
        public static string InstallationProductTitle(string modelName, string productSerialNumber) => $"Комплектация изделия: {modelName} №[{productSerialNumber}]";
        public static string ProductBomsViewTitle(string revisionName, string productSerialNumber) => $"Посадочные места изделия: {revisionName} [{productSerialNumber}]";
        public static string ProductsViewTitle(string revisionName) => $"График изделий {revisionName}";
        public static string CurrentProductOperationsViewTitle(string revisionName, string productSerialNumber) => $"Операционный паспорт изделия: {revisionName} [{productSerialNumber}]";
        public const string GroupProductOperationsViewTitle = "Операционный паспорт для выбранной группы изделий";
        public static string SelectedProductLogViewTitle(string revisionName, string serialNumber) => $"История операций для изделия [{revisionName}] №[{serialNumber}]";
        public static string SearchedProductViewTitle(bool isAdvancedSearch, string serialNumber) =>
             isAdvancedSearch ?
                $"Список изделий, серийный номер которых СОДЕРЖИТ [{serialNumber}]"
                :
                $"Список изделий, серийный номер которых РАВЕН [{serialNumber}]";

        public const string SearchedProductBasicViewTitle = "Поиск изделий по номеру";
        public static string ProductParametersViewTitle(string revisionName, string productNumber) => $"Список параметров для изделия: [{revisionName}] №[{productNumber}]";


        //Multi-operations
        public static string GroupStartOperationTitle(string operationName) => $"Выбор изделий для группового отправления на операцию [{operationName}]";
        public static string GroupEndOperationTitle(string operationName) => $"Выбор изделий для группового завершения операции [{operationName}]";
        public static string GroupCancelOperationTitle(string operationName) => $"Выбор изделий для групповой отмены операции [{operationName}]";
        public static string GroupReturnToOperationTitle(string operationName) => $"Выбор изделий для группового повторного выполнения операции [{operationName}]";
        #endregion

        #region InnerErrorMessages
        public static string NullPageCodeThrow(string code) => $"Несуществующее представление с кодом {code}";
        public static string NullPageCodeThrow(int id) => $"Несуществующее представление с кодом {id.ToString()}";
        #endregion

        #region Button_ToolTips_and_titles
        //Title
        public const string ComplexProductBomsButtonTitle = "Состав КИ";
        public const string ComplexProductModulesButtonTitle = "Модули";
        public const string ModuleComponentsButtonTitle = "Компоненты";
        public const string RejectBodyButtonTitle = "Забраковать корпус";
        public const string RejectButtonTitle = "Забраковать";
        public const string ReclamationButtonTitle = "Рекламация";
        public const string ShowComponentsButtonTitle = "Показать компоненты";
        public const string ComplexProductPageButtonTitle = "Страница КИ";
        public const string ModulePageButtonTitle = "Страница модуля";
        public const string ProductsButtonTitle = "Изделия";
        public const string OperationsButtonTitle = "Операции";
        public const string ProductParametersButtonTitle = "Параметры";
        public const string GroupOperationsButtonTitle = "Групповые-операции";
        public const string ExportButtonTitle = "Экспорт";
        public const string ToFavoriteButtonTitle = "В избранное";
        public const string EditProductButtonTitle = "Редактировать";
        public const string DeleteProductButtonTitle = "Удалить";
        public const string RecycleProductButtonTitle = "Утилизировать";
        public const string ShowGroupOrderElementstButtonTitle = "Показать готовность";
        public const string ShowCurrentProductLogButtonTitle = "Показать историю";

        //ToolTip
        public const string ComplexProductBomsButtonTooltip = "Переход к списку изделий, из которых собрано выбранное КИ";
        public const string ComplexProductModulesButtonTooltip = "Переход к списку модулей, из которых состоит выбранное КИ";
        public const string ModuleComponentsButtonTooltip = "Переход к списку узлов и компонентов, из которых состоит выбранный модуль";
        public const string RejectBodyButtonTooltip = "Забраковать выбранный корпус. Это действие изменит статус выбранного изделия и перенесет его в соответствующую вкладку!";
        public const string RejectButtonTooltip = "Отправить выбранное изделие в изолятор брака!";
        public const string ReclamationButtonTooltip = "Вернуть выбранное изделие в работу как рекламационное. Это действие изменит статус и перенесет выбранное издлие в соответствующую вкладку!";
        public const string ShowComponentsButtonTooltip = "Переход к списку изделий, из которых собран выбранный модуль!";
        public const string ModulePageButtonTooltip = "Показать модуль, в который установлен выбранный узел!";
        public const string ProductsButtonTooltip = "Переход к списку изделий выбранной ревизии!";
        public const string OperationsButtonTooltip = "Переход к списку операций, согласно привязанной маршрутной карте!";
        public const string GroupOperationsButtonTooltip = "Переход к маршрутной карте выбранного изделия для выбора операции";
        public const string ExportButtonTooltip = "Переход к станице для настройки экспорта данных в эксель таблицу";
        public const string ToFavoriteButtonTooltip = "Добавить или удалить изделие из избранного. Избранные изделия отображаются вверху списка";
        public const string EditProductButtonTooltip = "Открыть окно редактирования выбранного изделия";
        public const string DeleteProductButtonTooltip = "Безвозвратно удалить выбранное изделие";
        public const string RecycleProductButtonTooltip = "Отправить выбранное изделие на утилизацию";
        public const string ShowGroupOrderElementstButtonTooltip = "Посмотреть готовность согласно плану для выбранной ревизии";
        public const string ShowCurrentProductLogButtonTooltip = "Переход на страницу с историей пройденых операций для выбранного изделия";
        public const string ShowCurrentProductParametersButtonTooltip = "Переход к списку параметров выбранного изделия";
        #endregion

        #region ViewDictionary_keys      
        public const string RevisoinNavigationViewKey = "ShowRevisoinNavigationView";
        public const string InstallationProductViewKey = "ShowInstallationProductView";
        public const string RevisionBomsViewKey = "ShowRevisionBOMs";
        public const string ProductParentInstallationViewKey = "ShowProductParentInstallationView";
        public const string ProductsViewKey = "ShowProducts";
        public const string ProductOperationsViewKey = "ShowProductRoute";
        public const string MultiProductOperationViewKey = "ShowMultiProductOperationView";
        public const string ProductLogViewKey = "ShowProductLogView";
        public const string SelectedProductLogViewKey = "ShowCurrentProductLogView";
        public const string SearchedProductViewKey = "ShowSearchedProductView";
        public const string ProductParametersViewKey = "ShowProductParametersView";
        #endregion

        #region ProductView_TabControl_TabNames
        public const string ReadyToDispatchTabName = "Готовы к отгрузке";
        public const string DispatchedTabName = "Отгруженные";
        public const string NotForWorkTabName = "Брак/Утиль";
        public const string InWorkTabName = "В работе";
        #endregion

        #region Converters
        public static string ListToString(IEnumerable<string> strings, string separator = " ") => string.Join(separator, strings);
        #endregion
    }
}
