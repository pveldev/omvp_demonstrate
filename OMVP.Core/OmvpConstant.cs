﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.Core
{
    public class OmvpConstant
    {
        #region CalculateProgress_constants
        public static float OperationStartWeight = 1; //Вес начала операции
        public static float OperationEndWeight = 1; //Вес окончания операции

        public static float StepWeight = OperationStartWeight + OperationEndWeight; // Вес одной операции (начало+окончание)
        #endregion

        public const string HomePage = TextHelper.RevisoinNavigationViewKey;
        public const int MinimumAccessorLevel = 1;

        #region Parameters
        //ProductLogParametersWindow, ProductBomLogParametersWindow
        public static DateTime DefaultStartDateParameter = DateTime.Today.Date;
        public static DateTime DefaultEndDateParameter = DateTime.Now.AddDays(1);
        #endregion

        public static DateTime IstokBDay = new DateTime(1943, 7, 4);

        #region ProgressBar_steps
        // Прогресс разбит на "шаги". 
        // Сумма всех шагов не должна привышать 1 (100%)
        //  Шаг 1   + Шаг 2   + Шаг 3   + Шаг 4
        //  0.01 + 0.16    + 0.22    + 0.51    + 0.9   = 0.99
        public const float Step1 = 0.16f;
        public const float Step2 = 0.22f;
        public const float Step3 = 0.51f;
        public const float Step4 = 0.09f;
        public const float DefaultProgressValue = 0.01f;
        public const float FullCompleteProgressValue = 1f;
        #endregion

        public const int MaximumRowsByOneQuery = 1000; //Значение для linq.Take(value) метода

        public const int FirstDispatchYear = 2020;
        public static DateTime OmvpBDay = new DateTime(2019, 09, 25, 15, 59, 46); //db creation date
    }
}
