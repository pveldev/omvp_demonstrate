﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMVP.Core
{
    public class CurrentActionInfo : BasicNotifyModel
    {
        public string CurrentActionDescription { get; set; } = "";
        public bool ActionInProcess { get; set; } = false; //Триггер для отображения экрана со спинером/прогресс-баром

        private float _progress;
        public float Progress
        {
            get { return _progress; }
            set
            {
                if (value >= 0 && value <= 1)
                {
                    _progress = value;
                }
                else
                {
                    if (value < 0.0f) _progress = 0;
                    if (value > 1.0f) _progress = 1;
                }              
            }
        }

    }
}
