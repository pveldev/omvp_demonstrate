﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OMVP.Modal
{
	public class Dialog
	{
		readonly static string _questionCaption = "Выбор исхода";
		readonly static string _errorCaption = "Ошибка";
        readonly static string _resultCaption = "Результат";

		public static MessageBoxResult ShowOKCancel(string text) =>	
			MessageBox.Show(text, _questionCaption, MessageBoxButton.OKCancel, MessageBoxImage.Question);

		public static MessageBoxResult ShowOk(string text) =>
			MessageBox.Show(text, _resultCaption, MessageBoxButton.OK, MessageBoxImage.Information);

        public static MessageBoxResult ShowWarning(string text) =>
            MessageBox.Show(text, _resultCaption, MessageBoxButton.OK, MessageBoxImage.Warning);

        public static MessageBoxResult ShowYesNoCancel(string text) =>
           MessageBox.Show(text, _resultCaption, MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

        public static MessageBoxResult ShowYesNo(string text) =>
           MessageBox.Show(text, _resultCaption, MessageBoxButton.YesNo, MessageBoxImage.Question);

        public static MessageBoxResult ShowError(string text) =>
            MessageBox.Show(text, _errorCaption, MessageBoxButton.OK, MessageBoxImage.Error);
    }
}
